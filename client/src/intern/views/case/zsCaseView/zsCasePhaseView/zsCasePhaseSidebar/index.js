import angular from 'angular';
import template from './template.html';
import composedReducerModule from './../../../../../../shared/api/resource/composedReducer';
import rwdServiceModule from './../../../../../../shared/util/rwdService';
import zsCasePhaseSidebarTabListModule from './zsCasePhaseSidebarTabList';
import zsCaseCheckListModule from './zsCaseCheckList';
import zsCaseActionListModule from './zsCaseActionList';
import observableStateParamsModule from './../../../../../../shared/util/route/observableStateParams';
import shortid from 'shortid';
import get from 'lodash/get';
import './styles.scss';

export default
	angular.module('zsCasePhaseSidebar', [
		composedReducerModule,
		rwdServiceModule,
		zsCasePhaseSidebarTabListModule,
		zsCaseCheckListModule,
		zsCaseActionListModule,
		observableStateParamsModule
	])
		.directive('zsCasePhaseSidebar', [ '$state', 'rwdService', 'composedReducer', ( $state, rwdService, composedReducer ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					actions: '&',
					checklist: '&',
					tab: '&',
					isCollapsed: '&',
					onActionAutomaticToggle: '&',
					onActionUntaint: '&',
					onActionTrigger: '&',
					onChecklistAdd: '&',
					onChecklistRemove: '&',
					onChecklistToggle: '&',
					requestor: '&',
					templates: '&',
					caseDocuments: '&',
					phases: '&',
					canEdit: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( $scope ) {

					let ctrl = this,
						tabReducer;

					ctrl.handleCollapseClick = ( ) => {
						
						$state.go('case.phase', { tab: null }, { inherit: true });
					};

					ctrl.isCollapsed( {
						$getter: ( ) => {
							return ctrl.isSidebarCollapsed();
						}
					});

					ctrl.handleActionAutomaticToggle = ( actionId, automatic ) => {
						ctrl.onActionAutomaticToggle({ $actionId: actionId, $automatic: automatic });
					};

					ctrl.handleActionUntaint = ( actionId ) => {
						ctrl.onActionUntaint({ $actionId: actionId });
					};

					ctrl.handleActionTrigger = ( action, trigger, data ) => {
						return ctrl.onActionTrigger({ $action: action, $trigger: trigger, $data: data });
					};

					ctrl.handleChecklistAdd = ( content ) => {
						ctrl.onChecklistAdd({ $content: content });
					};

					ctrl.handleChecklistRemove = ( item ) => {
						ctrl.onChecklistRemove({ $item: item });
					};

					ctrl.handleChecklistToggle = ( item, checked ) => {
						ctrl.onChecklistToggle({ $item: item, $checked: checked });
					};

					ctrl.getSelectedTab = ( ) => {

						let tab = ctrl.tab()
							|| (rwdService.isActive('large-and-up') ? 'acties' : null);

						return tab;
					};

					ctrl.canEditActionList = ( ) => {
						return ctrl.canEdit();
					};

					ctrl.canEditCheckList = ( ) => {
						return ctrl.canEdit();
					};

					ctrl.isSidebarCollapsed = ( ) => {
						return !ctrl.getSelectedTab() && !rwdService.isActive('large-and-up');
					};

					tabReducer = composedReducer({ scope: $scope }, ctrl.actions, ctrl.checklist, ctrl.getSelectedTab)
						.reduce(( actions, checklist, tab ) => {

							let filteredActions =
									actions.filter(action => action.type !== 'object_mutation' && !get(action, 'data.interface_id'));

							return [
								{
									id: shortid(),
									name: 'acties',
									label: 'Acties',
									count: (filteredActions || []).filter(action => action.automatic).length,
									selected: tab === 'acties',
									icon: 'play-circle-outline',
									link: $state.href($state.current, { tab: 'acties' }, { inherit: true }),
									children: filteredActions
								},
								{
									id: shortid(),
									name: 'checklist',
									label: 'Checklist',
									count: (checklist || []).filter(item => !item.checked).length,
									selected: tab === 'checklist',
									icon: 'checkbox-multiple-marked-outline',
									link: $state.href($state.current, { tab: 'checklist' }, { inherit: true }),
									children: checklist
								}
							];
						});

					ctrl.getTabs = tabReducer.data;

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
