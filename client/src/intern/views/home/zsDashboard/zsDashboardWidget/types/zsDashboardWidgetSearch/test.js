import zsDashboardWidgetSearch from './index.js';
import angular from 'angular';
import 'angular-mocks';

xdescribe('zsDashboardWidgetSearch', ( ) => {

	let $rootScope,
		$compile,
		scope,
		dashboardWidgetSearchEl,
		ctrl;

	beforeEach(angular.mock.module(zsDashboardWidgetSearch));

	beforeEach(angular.mock.inject([ '$rootScope', '$compile', ( ...rest ) => {

		[ $rootScope, $compile ] = rest;

		dashboardWidgetSearchEl = angular.element('<zs-dashboard-widget-search/>');
		
		scope = $rootScope.$new();

		$compile(dashboardWidgetSearchEl)(scope);

		ctrl = dashboardWidgetSearchEl.controller('zsDashboardWidgetSearch');

		ctrl.state = 'foo'; // otherwise a child directive will run and create errors.

	}]));
	
	it('should have a controller', () => {

		expect(ctrl).toBeDefined();

	});
	
	it('controller should have a handleSelect function', () => {

		expect(ctrl.handleSelect).toBeDefined();

	});
});
