import zsDashboardWidget from './index.js';
import angular from 'angular';
import 'angular-mocks';
import snackbarServiceModule from './../../../../../shared/ui/zsSnackbar/snackbarService';
import SomaEvents from 'soma-events';

describe('zsDashboardWidget', ( ) => {

	let $rootScope,
		$compile,
		$httpBackend,
		$document,
		apiCacher,
		snackbarService,
		scope,
		el,
		ctrl;

	let initWithType = ( type, widgets = [] ) => {

		el = angular.element('<zs-dashboard-widget data-widget="widget" data-widgets="widgets" on-widget-create="handleCreate($widgetType)" on-widget-remove="handleRemove($widget)"/>');

		$document.find('body').append(el);
		
		scope = $rootScope.$new();

		scope.widget = {
			widget: type
		};

		scope.widgets = widgets;

		$compile(el)(scope);

		ctrl = el.controller('zsDashboardWidget');
	};

	beforeEach(angular.mock.module([ '$provide', ( $provide ) => {

		$provide.constant('HOME', '/');

	}]));

	beforeEach(angular.mock.module(zsDashboardWidget, snackbarServiceModule));

	beforeEach(angular.mock.inject([ '$rootScope', '$compile', '$httpBackend', '$document', 'apiCacher', 'snackbarService', ( ...rest ) => {

		[ $rootScope, $compile, $httpBackend, $document, apiCacher, snackbarService ] = rest;

	}]));

	describe('with any type', ( ) => {

		beforeEach(( ) => {
			initWithType('test');
		});
		
		it('should have a controller', () => {

			expect(ctrl).toBeDefined();

		});

		it('should have created a element', ( ) => {

			expect(el.find('zs-dashboard-widget-test').length).toBe(1);

		});

		it('should trigger handleRemove', ( ) => {

			scope.handleRemove = jasmine.createSpy('remove');

			ctrl.handleRemoveClick();

			expect(scope.handleRemove).toHaveBeenCalledWith(scope.widget);

		});

		it('should not propagate a mousedown event when button is clicked', ( ) => {

			let event = new SomaEvents.Event('mousedown', null, true),
				spy = jasmine.createSpy('mousedown');

			el.bind('mousedown', spy);

			el.find('button')[0].dispatchEvent(event);

			expect(spy).not.toHaveBeenCalled();

		});

	});

	describe('with type create', ( ) => {

		beforeEach(( ) => {

			initWithType('create');

		});

		it('should add a `on-widget-create` attribute', ( ) => {

			let child = el.find('zs-dashboard-widget-create');

			expect(child.attr('on-widget-create')).not.toBeUndefined();

		});

		it('should trigger handleCreate', ( ) => {

			scope.handleCreate = jasmine.createSpy('create');

			ctrl.handleWidgetCreate('foo');

			expect(scope.handleCreate).toHaveBeenCalled();
			expect(scope.handleCreate).toHaveBeenCalledWith('foo');

		});
	});

	describe('with type casetype', ( ) => {

		beforeEach( ( ) => {

			$httpBackend.expectGET(/.*/)
				.respond([]);

			initWithType('casetype', [
				{
					widget: 'casetype'
				}
			]);

		});

		it('should only allow one instance', ( ) => {

			spyOn(snackbarService, 'error');

			ctrl.handleWidgetCreate('casetype');

			expect(snackbarService.error).toHaveBeenCalled();

		});

		afterEach( ( ) => {

			$httpBackend.flush();

		});


	});

	afterEach( ( ) => {

		scope.$destroy();

		apiCacher.clear();

	});

});
