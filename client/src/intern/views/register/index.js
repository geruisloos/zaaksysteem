import angular from 'angular';
import createCaseRegistrationModule from './../../../shared/case/createCaseRegistration';

export default angular.module('Zaaksysteem.intern.register', [
		createCaseRegistrationModule
	])
	.name;
