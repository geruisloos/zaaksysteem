export default base64EncodedString => {

	Word.run( (context) => { //eslint-disable-line

	let thisDocument = context.document;

	thisDocument.body.clear();

	let mySelection = thisDocument.getSelection();

	mySelection.insertFileFromBase64(base64EncodedString, 'replace');

	return context.sync()
		.then( ( ) => {
		});
	});
};
