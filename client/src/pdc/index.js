import angular from 'angular';
import 'angular-i18n/angular-locale_nl';
import ngAnimate from 'angular-animate';
import routing from './routing';
import zsResourceConfiguration from './../shared/api/resource/resourceReducer/zsResourceConfiguration';
import serviceworkerModule from './../shared/util/serviceworker';
import sessionServiceModule from '../shared/user/sessionService';

export default
	angular.module('Zaaksysteem.pdc', [
		routing,
		ngAnimate,
		serviceworkerModule,
		sessionServiceModule
	])
	.config(['$provide', ( $provide ) => {
		
		// make sure sourcemaps work
		// from https://github.com/angular/angular.js/issues/5217

		$provide.decorator('$exceptionHandler', [ '$delegate', ( $delegate ) => {

			return ( exception, cause ) => {
				$delegate(exception, cause);
				setTimeout(( ) => {
					console.error(exception.stack);
				});
			};

		}]);

	}])
	.config([ 'resourceProvider', ( resourceProvider ) => {

		zsResourceConfiguration(resourceProvider.configure);

	}])
	.run([ '$window', 'serviceWorker', '$animate', ( $window, serviceWorker, $animate ) => {

		let enable = PROD, //eslint-disable-line
			worker = serviceWorker();

		$animate.enabled(true);

		if (worker.isSupported()) {
			worker.isEnabled()
				.then( ( enabled ) => {
					if (enabled !== enable) {
						return (enable ?
							worker.enable()
							: worker.disable()
								.then(( ) => {
									if (enabled) {
										$window.location.reload();
									}
								})
						);
					}
				});
		}

	}])
		.name;
