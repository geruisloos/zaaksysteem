import angular from 'angular';
import composedReducerModule from '../../../shared/api/resource/composedReducer';
import objectListItemListModule from './objectListItemList';
import template from './template.html';
import seamlessImmutable from 'seamless-immutable';
import './styles.scss';

export default
		angular.module('Zaaksysteem.pdc.objectListView', [
			composedReducerModule,
			objectListItemListModule
		])
		.directive('objectListView', [ 'composedReducer', '$state', '$stateParams', '$timeout', ( composedReducer, $state, $stateParams, $timeout ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					objectItems: '&',
					onLoadMore: '&',
					isLoading: '&'
				},
				bindToController: true,
				controller: [ '$scope', '$element', function ( scope, $element ) {

					let ctrl = this,
						objectReducer,
						labelReducer,
						input = $element.find('input');

					$timeout( ( ) => {
						input[0].focus();
					}, 0, false);

					ctrl.query = $stateParams.searchQuery;

					objectReducer = composedReducer( { scope }, ctrl.objectItems )
						.reduce( objects => {
							return objects.objects || seamlessImmutable([]);
						});

					ctrl.searchObjects = ( ) => {

						let searchQuery = ctrl.query;

						$state.go($state.current.name, { searchQuery } );

					};

					labelReducer = composedReducer( { scope }, ctrl.objectItems )
						.reduce( object => {
							return object.label;
						});

					ctrl.getItems = objectReducer.data;

					ctrl.getSearchplaceholder = labelReducer.data;

					ctrl.isSearchResult = ( ) => !!($stateParams.searchQuery !== '');

				}],
				controllerAs: 'vm'
			};

		}
		])
		.name;
