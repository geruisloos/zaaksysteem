import ruleEngine from '.';
import difference from 'lodash/difference';
import keys from 'lodash/keys';

describe('ruleEngine', ( ) => {

	it('should return an object with the keys hidden, disabled, values', ( ) => {

		let result = ruleEngine([], {});

		expect(
			difference(
				keys(result),
				[ 'hidden', 'disabled', 'values' ]
			).length
		).toBe(0);

	});

	it('should execute the given rules', ( ) => {

		let result =
			ruleEngine([
				{
					conditions: {
						conditions: []
					},
					then: [
						{
							type: 'set_value',
							data: {
								value: 'foo',
								attribute_name: 'foo'
							}
						}
					]
				},
				{
					conditions: {
						conditions: [
							{
								validates_true: false
							}
						],
						type: 'and'
					},
					else: [
						{
							type: 'set_value',
							data: {
								value: 'bar',
								attribute_name: 'bar'
							}
						}
					]
				}
			], {});

		expect(result.values.foo).toBe('foo');
		expect(result.values.bar).toBe('bar');

	});

	it('should overwrite the results of earlier actions with the last one', ( ) => {

		let result =
			ruleEngine([
				{
					conditions: {
						conditions: []
					},
					then: [
						{
							type: 'set_value',
							data: {
								value: 'foo',
								attribute_name: 'foo'
							}
						}
					]
				},
				{
					conditions: {
						conditions: [

						]
					},
					then: [
						{
							type: 'set_value',
							data: {
								value: 'bar',
								attribute_name: 'foo'
							}
						}
					]
				}
			], {});

		expect(result.values.foo).toBe('bar');


	});

	it('should clear the values of hidden attributes', ( ) => {

		let result =
			ruleEngine([
				{
					conditions: {
						conditions: []
					},
					then: [
						{
							type: 'hide_attribute',
							data: {
								attribute_name: 'foo'
							}
						}
					]
				}
			], { foo: 'bar' });

			expect(result.values.foo).toBe(null);
	});

});
