export default ( input ) => {
	let output = input;

	if (typeof input === 'string') {
		output = `"${input.replace(/\\/g, '\\\\').replace(/\"/g, '\\"')}"`;
	}
	return output;
};
