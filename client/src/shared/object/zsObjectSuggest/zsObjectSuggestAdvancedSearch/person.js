import pickBy from 'lodash/pickBy';
import identity from 'lodash/identity';
import assign from 'lodash/assign';
import mapKeys from 'lodash/mapKeys';
import omit from 'lodash/omit';
import includes from 'lodash/includes';
import first from 'lodash/first';
import get from 'lodash/get';

export default ( locals = { modules: [ ], user: [] } ) => {

	let modules = locals.modules,
		user = locals.user,
		remoteField;

	if (modules.length === 1 && get(first(modules), 'instance.interface_config.gbav_search')) {

		remoteField = {
			name: 'remote',
			template: 'checkbox',
			label: '',
			data: {
				checkboxLabel: 'Zoeken in BRP'
			},
			when: get(first(modules), 'instance.interface_config.gbav_search_role_restriction') ?
				includes(user.instance.logged_in_user.capabilities, 'contact_search_extern')
				: true
		};

	} else if (modules.length > 1) {

		remoteField = {
			name: 'remote',
			template: 'select',
			label: 'Bron',
			data: {
				notSelectedLabel: 'Intern',
				options: modules.asMutable().map(
					module => {

						let option = {
								value: module.instance.id,
								label: module.instance.interface_config.search_form_title || module.instance.name
							},
							gbavSearchEnabled = !!get(module, 'instance.interface_config.gbav_search'),
							gbavSearchRoleRestriction = get(module, 'instance.interface_config.gbav_search_role_restriction'),
							contactSearchExternPermission = includes(user.instance.logged_in_user.capabilities, 'contact_search_extern');

						if (
							gbavSearchEnabled
							&& (
								(
									gbavSearchRoleRestriction
									&& contactSearchExternPermission
								)
								|| !gbavSearchRoleRestriction
							)
						) {
							return option;
						}

						return null;

					}
				).filter(identity)
			}
		};
	}

	return {
		format: ( values ) => {

			if (modules.length === 1) {
				return values.merge({
					remote: values.remote ?
						modules[0].instance.id
						: null
				});
			}

			return values;

		},
		parse: ( values ) => {

			if (modules.length === 1) {
				return values.merge({ remote: !!values.remote });
			}

			return values;

		},
		request: ( values ) => {

			return {
				method: 'POST',
				url: !values.remote ?
					'/api/v1/subject'
					: `/api/v1/subject/remote_search/${values.remote}`,
				data:
					{
						query: {
							match: assign(
								{
									subject_type: 'person'
								},
								mapKeys(
									omit(
										pickBy(values, identity),
										'remote'
									),
									( value, key ) => `subject.${key}`
								)
							)
						}
					}
			};
		},
		fields:
			[
				{
					name: 'personal_number',
					label: 'BSN',
					template: 'text',
					required: [ '$values', ( vals ) => {

						return vals.remote && !vals.date_of_birth && !get(vals, 'address_residence.zipcode') && !get(vals, 'address_residence.street_number');

					}]
				},
				{
					name: 'prefix',
					label: 'Voorvoegsel',
					template: 'text'
				},
				{
					name: 'family_name',
					label: 'Achternaam',
					template: 'text'
				},
				{
					name: 'gender',
					label: 'Geslacht',
					template: 'radio',
					data: {
						options: [
							{
								value: 'M',
								label: 'Man'
							},
							{
								value: 'V',
								label: 'Vrouw'
							}
						]
					}
				},
				{
					name: 'date_of_birth',
					label: 'Geboortedatum',
					template: 'date',
					required: [ '$values', ( vals ) => {

						return vals.remote && !vals.personal_number && !get(vals, 'address_residence.zipcode') && !get(vals, 'address_residence.street_number');

					}]
				},
				{
					name: 'address_residence.street',
					label: 'Straat',
					template: 'text'
				},
				{
					name: 'address_residence.zipcode',
					label: 'Postcode',
					template: 'text',
					required: [ '$values', ( vals ) => {

						return vals.remote && !vals.personal_number && !vals.date_of_birth;

					}]
				},
				{
					name: 'address_residence.street_number',
					label: 'Huisnummer',
					template: 'text',
					required: [ '$values', ( vals ) => {

						return vals.remote && !vals.personal_number && !vals.date_of_birth;

					}]
				},
				{
					name: 'address_residence.street_number_letter',
					label: 'Huisletter',
					template: 'text'
				},
				{
					name: 'address_residence.street_number_suffix',
					label: 'Huisnummer toevoeging',
					template: 'text'
				},
				{
					name: 'address_residence.city',
					label: 'Woonplaats',
					template: 'text'
				}
			].concat( remoteField ? remoteField : []),
		columns:
			[
				{
					id: 'gender',
					template:
						`<zs-icon
							icon-type="alert-circle"
							ng-show="item.messages"
							zs-tooltip="{{::item.messages || ''}}"
						></zs-icon>
						<zs-icon icon-type="account"></zs-icon>`,
					label: ''
				},
				{
					id: 'personal_number',
					label: 'BSN'
				},
				{
					id: 'first_names',
					label: 'Voornamen'
				},
				{
					id: 'family_name',
					label: 'Achternaam'
				},
				{
					id: 'address',
					label: 'Adres',
					template:
						'<span>{{::item.address}}</span>'
				}
			]
	};
};
