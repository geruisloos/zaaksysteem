import angular from 'angular';
import vormTemplateServiceModule from './../../vorm/vormTemplateService';
import each from 'lodash/each';

export default
	angular.module('caseAttrTemplateCompiler', [
		vormTemplateServiceModule
	])
		.factory('caseAttrTemplateCompiler', [ 'vormTemplateService', ( vormTemplateService ) => {

			let compiler = vormTemplateService.clone(),
				templates,
				imgEl =
					angular.element(
						'<img class="delegate-value-display" ng-src="{{delegate.value}}" ng-show="delegate.value&&!delegate.valid"/>'
					),
				btwDisplay =
					`<zs-btw-display
						value="delegate.value"
						btw-type="vm.invokeData('btwType')">
					</zs-btw-display>`;
			
			templates = {
				image_from_url: {
					inherits: 'url',
					control: ( el ) => {

						el.append(imgEl.clone());

						return el;
					},
					display: ( el ) => {

						el.append(imgEl.clone());

						return el;

					}
				},
				case_file:
					{
						inherits: 'file',
						display: ( ) => {
							return angular.element(
								`<div class="file-list-item">
									<button type="button" ng-click="vm.templateData().click(delegate.value)" class="delegate-value-display file-list-item-button" ng-disabled="!delegate.value">
										<div class="file-list-item-button-inner">
											<zs-icon icon-type="file"></zs-icon>
											{{delegate.value.filename}}
										</div>
									</button>
									<a ng-href="{{vm.templateData().getDownloadUrl(delegate.value)}}" target="_blank">
										<zs-icon icon-type="download"></zs-icon>
									</a>
								</div>`
							);
						}
					},
				valuta: {
					inherits: 'number',
					control: ( el ) => {

						el.attr('step', '0.01');

						return angular.element(
							`<div class="vorm-input-wrapper">
								${el[0].outerHTML}
								${btwDisplay}
							</div>`
						);

					},
					display: ( ) => {

						return angular.element(
							`<div class="delegate-value-display">
								{{delegate.value | currency}}
								${btwDisplay}
							</div>`
						);

					}
				}
			};

			each(templates, ( value, key ) => {

				compiler.registerType(key, value);

			});

			return compiler;

		}])
		.name;
