import angular from 'angular';
import mapValues from 'lodash/mapValues';
import keyBy from 'lodash/keyBy';
import reduce from 'lodash/reduce';
import assign from 'lodash/assign';
import propCheck from './../../../util/propCheck';
import pull from 'lodash/pull';
import first from 'lodash/head';
import tail from 'lodash/tail';
import listenerFn from './../../../util/listenerFn';

export default
	angular.module('shared.api.resource.resourceReducer', [

	])
		.provider('resourceReducer', [ ( ) => {

			let keys = 'next prev cursor limit totalRows totalPages data error merge'.split(' '),
				transformers =
					mapValues(
						keyBy(keys),
						key => {

							let reducers = [];

							if (key === 'merge') {
								reducers.push(( requestOptions, data, newOpts ) => newOpts);
							} else if (key !== 'data' && key !== 'error') {
								reducers.push( ( ) => {
									throw new Error(`Resource method ${key} not configured`);
								});
							}
							return reducers;
						}
					),
				configure =
					mapValues(
							transformers,
							( value, key ) => {

								let reset = true;

								return ( fn ) => {

									if (reset) {
										transformers[key].length = 0;
										reset = false;
									}

									transformers[key].push(fn);
								};
							}
						);

			return {
				$get: [ ( ) => {

						let getKeyFn = ( ...rest ) => {

							let key,
								fn;

							if (typeof rest[0] === 'function') {
								key = 'data';
								fn = rest[0];
							} else {
								key = rest[0];
								fn = rest[1];
							}

							return { key, fn };
						};

						let factory = ( options = { stateful: false, args: [ ], override: false }) => {

							let reducer = {},
								instanceReducers = {},
								cached = {},
								args = options.args,
								thisObj = options.thisObj || reducer,
								override = !!options.override,
								frozen,
								subscribers = listenerFn({ immediate: false }),
								reducerFns;

							let resetCache = ( ) => {
								if (!frozen) {
									cached = {};
								}
							};

							let setArgs = ( ...rest ) => {
								args = rest;
								resetCache();
							};

							reducerFns =
								mapValues(
									transformers,
									( reducers, key ) => {

										return ( ...rest ) => {
											let initial,
												result,
												reducersToRun = !override ? reducers : null;

											initial = reducer.$source;

											if (instanceReducers[key]) {
												reducersToRun = !override ? reducers.concat(instanceReducers[key]) : instanceReducers[key];
											}

											result = reducersToRun && reducersToRun.length ?
												reduce(
													reducersToRun,
													( data, fn ) => fn(...(args || []), data, ...rest),
													initial
												)
												: initial;

											return result;
										};

									}
								);


							assign(reducer,
								{
									reduce: ( ...rest ) => {

										let { key, fn } = getKeyFn(...rest),
											reducers;

										propCheck.throw(
											propCheck.shape({
												key: propCheck.oneOf(keys),
												fn: propCheck.func
											}),
											{
												key,
												fn
											}
										);

										reducers = instanceReducers[key];

										if (!reducers) {
											reducers = instanceReducers[key] = [];
										}

										reducers.push(fn);

										resetCache();

										return thisObj;
									},
									unreduce: ( ...rest ) => {

										let { key, fn } = getKeyFn(...rest);

										pull(instanceReducers[key], fn);
										resetCache();
										return thisObj;
									},
									frozen: ( ...rest ) => {
										
										if (rest.length) {
											frozen = rest[0];
											if (!frozen) {
												resetCache();
											}
										}

										return frozen;
									},
									run: ( ...rest ) => {
										let key,
											reducerArgs;

										if (!rest.length) {
											key = 'data';
											reducerArgs = [];
										} else {
											key = first(rest);
											reducerArgs = tail(rest);
										}

										return reducerFns[key](...reducerArgs);
									},
									subscribe: subscribers.register,
									subscribers: subscribers.listeners,
									resetCache,
									setArgs
								},
								mapValues(reducerFns,
								( reducerFn, key ) => {

									return ( ...rest ) => {

										let result,
											updated = false;

										try {

											if (!rest.length) {

												if (!frozen && !(key in cached)) {
													result = reducerFn(...rest);
													updated = true;
												} else {
													result = cached[key];
												}

												if (!options.stateful) {
													cached[key] = result;
												}

											} else {
												result = reducerFn(...rest);
												updated = true;
											}

											if (updated && key === 'data') {
												subscribers.invoke(result);
											}

										} catch ( error ) {
											delete cached[key];
											console.error(error);
											result = null;
										}

										return result;
									};
								})
							);

							return reducer;

						};

						return factory;
					}
				],
				configure
					

			};

		}])
		.name;
