import angular from 'angular';
import 'angular-mocks';
import apiModule from '.';

describe('api', ( ) => {

	let $rootScope,
		$httpBackend,
		api;

	beforeEach(angular.mock.module(apiModule));

	beforeEach(angular.mock.inject([ '$rootScope', '$httpBackend', 'api', ( ...rest ) => {
		
		[ $rootScope, $httpBackend, api ] = rest;

	}]));

	it('should support shortcut methods', ( ) => {

		let spy = jasmine.createSpy('resolved');

		$httpBackend.whenGET('/foo')
			.respond(200, []);

		api.get('/foo')
			.then(spy);

		$httpBackend.flush();

		expect(spy).toHaveBeenCalledWith(jasmine.objectContaining({
			data: [],
			status: 200
		}));

	});

	describe('when aborted', ( ) => {

		let promise,
			resolved,
			rejected,
			finallyP;

		beforeEach( ( ) => {
				
			resolved = jasmine.createSpy('resolved');
			rejected = jasmine.createSpy('rejected');
			finallyP = jasmine.createSpy('finally');

			$httpBackend.whenGET('foo')
				.respond([]);

			promise = api({
				method: 'GET',
				url: 'foo'
			})
				.then(resolved)
				.catch(rejected)
				.finally(finallyP);

		});

		it('should return a promise with abort methods', ( ) => {

			expect(promise.abortRequest).toBeDefined();
			expect(promise.abortRequestAndIgnore).toBeDefined();

			$httpBackend.flush();

		});

		it('should abort the request and call the error handler', ( ) => {

			promise.abortRequest();

			$rootScope.$digest();

			expect(resolved).not.toHaveBeenCalled();
			expect(rejected).toHaveBeenCalled();
			expect(finallyP).toHaveBeenCalled();

		});

		it('should abort the request and ignore all handlers', ( ) => {

			promise.abortRequestAndIgnore();

			$rootScope.$digest();

			expect(resolved).not.toHaveBeenCalled();
			expect(rejected).not.toHaveBeenCalled();
			expect(finallyP).not.toHaveBeenCalled();

		});

	});

});
