import angular from 'angular';
import 'angular-mocks';
import cacher from '.';
import clone from 'lodash/clone';
import mapValues from 'lodash/mapValues';
import keyBy from 'lodash/keyBy';
import range from 'lodash/range';
import assign from 'lodash/assign';
import zsStorageModule from './../../util/zsStorage';

describe('apiCacher', ( ) => {

	const LOCAL_STORAGE_KEY = 'apiResponses',
		LIMIT = 1 * 1024 * 1024;

	let apiCacher,
		zsStorage,
		comparator;

	let getStoredResponses = ( ) => {
		return zsStorage.get(LOCAL_STORAGE_KEY) || [];
	};

	beforeEach(angular.mock.module(zsStorageModule, cacher, [ 'apiCacherProvider', ( apiCacherProvider ) => {

		comparator = jasmine.createSpy('compare');

		comparator.and.callFake(( ...rest ) => angular.equals(...rest));

		apiCacherProvider.setLimit(LIMIT);

		apiCacherProvider.setIsEqual(comparator);

	}]));

	beforeEach(angular.mock.inject([ 'apiCacher', 'zsStorage', ( ...rest ) => {

		[ apiCacher, zsStorage ] = rest;

	}]));


	it('should store the supplied response data', ( ) => {

		let data = { foo: 'bar' },
			options = { url: '/foo' };

		expect(getStoredResponses().length).toBe(0);

		apiCacher.store(options, data);

		expect(getStoredResponses().length).toBe(1);

	});


	it('should return the stored response data', ( ) => {

		let data = { foo: 'bar' },
			options = { url: '/foo' };

		apiCacher.store(options, data);

		expect(apiCacher.get(options)).toBeDefined();

		expect(apiCacher.get(options).$data).toEqual(data);

	});

	it('should convert a string to a request options object', ( ) => {

		let data = { foo: 'bar' },
			url = 'foo';

		apiCacher.store(url, data);

		expect(apiCacher.get(url)).toBeDefined();

		expect(apiCacher.get({ url })).toBeDefined();

	});

	it('should clear the local storage', ( ) => {

		let data = { foo: 'bar' },
			options = { url: '/foo' };

		apiCacher.store(options, data);

		expect(getStoredResponses().length).toBe(1);

		apiCacher.clear();

		expect(getStoredResponses().length).toBe(0);

	});

	it('should overwrite similar responses with the new data', ( ) => {

		let options = {
				url: '/foo',
				params: {
					one: 'two',
					two: 'one'
				}
			},
			updateTime = Date.now();

		apiCacher.store(options, { foo: 'bar' });

		expect(apiCacher.get(options)).toBeDefined();

		expect(getStoredResponses().length).toBe(1);

		apiCacher.get(options).$lastUpdated = updateTime - 1;

		spyOn(Date, 'now').and.returnValue(updateTime);

		apiCacher.store(clone(options), { foo: 'foo' });

		expect(getStoredResponses().length).toBe(1);

		expect(apiCacher.get(options).$lastUpdated).toBe(updateTime);

	});

	it('should call the update handlers with the ids of the changed caches', ( ) => {

		let spy = jasmine.createSpy('onUpdate'),
			caches,
			ids;

		apiCacher.store({ url: '/foo' }, { foo: 'bar' });

		apiCacher.store({ url: '/bar' }, { bar: 'foo' });

		caches = zsStorage.get(LOCAL_STORAGE_KEY);

		ids = mapValues(
			keyBy(caches, cache => cache.$pathname),
			cache => cache.$id
		);

		apiCacher.onUpdate.push(spy);

		caches = caches.map( ( cache ) => {
			return assign({ }, cache, { $lastUpdated: Date.now() + 1 });
		});

		zsStorage.set(LOCAL_STORAGE_KEY, caches);

		expect(spy).toHaveBeenCalled();

		expect(spy.calls.argsFor(0)[0]).toEqual([ ids['/bar'], ids['/foo'] ]);

	});

	it(`should not store more than ${LIMIT}b of responses`, ( ) => {

		let div = 4,
			str = new Array(LIMIT / div).join('.');

		range(0, div).forEach( ( val, index ) => {
			apiCacher.store({ url: '/foo/', params: { index } }, { foo: str });
		});

		expect(getStoredResponses().length).toBe(div - 1);

	});

	it('should return the available caches as array', ( ) => {

		expect(apiCacher.caches().length).toBe(0);

		apiCacher.store('/foo', 'foo');

		expect(apiCacher.caches().length).toBe(1);

	});

	it('should return the cache by id', ( ) => {

		apiCacher.store('/foo', { foo: 'bar' });

		let cache = apiCacher.get('/foo');

		expect(apiCacher.getCacheById(cache.$id)).toEqual(cache);

	});

	it('should not update the cache when the request changes', ( ) => {

		let onUpdate = jasmine.createSpy('update');

		apiCacher.onUpdate.push(onUpdate);

		apiCacher.store('/foo', { foo: 'bar' });

		expect(onUpdate).toHaveBeenCalled();

		onUpdate.calls.reset();

		apiCacher.store('/foo', { foo: 'bar' });

		expect(onUpdate).not.toHaveBeenCalled();

	});

	afterEach(( ) => {

		apiCacher.clear();

	});

});
