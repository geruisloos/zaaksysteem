import angular from 'angular';
import seamlessImmutable from 'seamless-immutable';
import invoke from 'lodash/invokeMap';
import find from 'lodash/find';
import identity from 'lodash/identity';

export default
	angular.module('windowUnload', [
	])
		.factory('windowUnload', [ '$rootScope', '$window', ( $rootScope, $window ) => {

			let handlers = seamlessImmutable([]),
				msg;

			$window.addEventListener('beforeunload', ( event ) => {

				msg = find(
					invoke(handlers, 'call', null),
					identity
				);

				if (msg === true) {
					msg = 'U heeft onopgeslagen wijzigingen. Deze gaan verloren als u wegnavigeert van deze pagina.';
				}

				if (msg) {
					event.returnValue = msg;
					$rootScope.$apply();
				}

				return msg;

			});

			return {
				register: ( scope, handler ) => {

					handlers = handlers.concat(handler);

					scope.$on('$destroy', ( ) => {
						handlers = handlers.filter(h => h !== handler);
					});

				}
			};

		}])
		.name;
