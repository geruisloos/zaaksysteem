import angular from 'angular';
import 'angular-mocks';
import cachedCollectionModule from '.';
import zsStorageModule from './../zsStorage';

describe('cachedCollection', ( ) => {

	let zsStorage,
		cachedCollection;

	beforeEach(angular.mock.module(cachedCollectionModule, zsStorageModule));

	beforeEach(angular.mock.inject([ 'zsStorage', 'cachedCollection', ( ...rest ) => {

		[ zsStorage, cachedCollection ] = rest;

	}]));

	it('should return a cachedCollection', ( ) => {

		expect(cachedCollection.array('foo')).not.toBeUndefined();

	});

	it('should throw when name is left empty', ( ) => {

		expect(cachedCollection.array).toThrow();

	});

	it('should throw when collection with name already exists', ( ) => {

		cachedCollection.array('foo');

		expect(cachedCollection.array.bind(null, 'foo')).toThrow();

	});

	it('should replace the value with the one supplied', ( ) => {

		let initial = [ 1, 2, 3],
			update = initial.concat(4, 5, 6),
			array = cachedCollection.array('foo', initial);

		expect(array.value()).toEqual(initial);

		array.replace(update);

		expect(array.value()).not.toEqual(initial);

		expect(array.value()).toEqual(update);

	});

	it('should update its value when updated from elsewhere', ( ) => {

		let initial = [ 1, 2, 3 ],
			update = initial.concat(4, 5, 6),
			key = 'foo',
			array = cachedCollection.array(key, initial);

		expect(array.value()).toEqual(initial);

		zsStorage.set(key, update);

		expect(array.value()).toEqual(update);

	});

	it('should call the update listeners when updated', ( ) => {

		let current = [ 1, 2, 3 ],
			next = [ 2, 3, 4 ],
			array = cachedCollection.array('foo', current),
			spy = jasmine.createSpy('update');

		array.onUpdate.push(spy);

		array.replace(next);

		expect(spy).toHaveBeenCalled();

		expect(spy.calls.argsFor(0)[0]).toEqual(next);

		expect(spy.calls.argsFor(0)[1]).toEqual(current);

	});

	it('should get the value from cache', ( ) => {

		let name = 'foo',
			initial = [ 1, 2, 3 ],
			array;

		zsStorage.set(name, initial);

		array = cachedCollection.array(name);

		expect(array.value()).toEqual(initial);

	});

	afterEach(( ) => {

		zsStorage.clear();
		
	});

});
