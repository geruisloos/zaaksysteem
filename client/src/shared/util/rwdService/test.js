import angular from 'angular';
import rwdServiceModule from '.';
import parseSassVariables from './../parseSassVariables';
import breakpoints from '!raw!!./../../styles/_breakpoints.scss';
import contains from 'lodash/contains';
import somaEvents from 'soma-events';

describe('rwdService', ( ) => {

	let $window,
		$rootScope,
		rwdService,
		vars = parseSassVariables(breakpoints);

	beforeEach(angular.mock.module(rwdServiceModule));

	beforeEach(angular.mock.inject([ '$rootScope', '$window', 'rwdService', ( ...rest ) => {

		[ $rootScope, $window, rwdService ] = rest;

	}]));

	it('should not update before a digest', ( ) => {

		expect(contains(rwdService.getActiveViews(), 'small-and-down')).not.toEqual($window.innerWidth <= parseInt(vars['small-screen'], 10));

	});

	it('should return the active view types', ( ) => {

		$rootScope.$apply();

		expect(contains(rwdService.getActiveViews(), 'small-and-down')).toEqual($window.innerWidth <= parseInt(vars['small-screen'], 10));

		expect(contains(rwdService.getActiveViews(), 'large-and-up')).toEqual($window.innerWidth > parseInt(vars['large-screen'], 10));

	});

	it('should re-interpret the view types when the window resizes', ( ) => {

		// Difficult to change the viewport size from karma, so we just
		// identity check the array

		let viewTypes;

		$rootScope.$apply();

		viewTypes = rwdService.getActiveViews();

		expect(viewTypes).toBe(rwdService.getActiveViews());

		rwdService.parse('');

		$window.dispatchEvent(new somaEvents.Event('resize'));

		$rootScope.$apply();

		expect(viewTypes).not.toEqual(rwdService.getActiveViews());

	});

});
