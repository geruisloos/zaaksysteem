import angular from 'angular';
import 'angular-mocks';
import vormTemplateServiceModule from '.';
import inputModule from './../types/input';

xdescribe('vormTemplateService', ( ) => {
	
	let service,
		$rootScope;
	
	describe('with defaults', ( ) => {
		
		beforeEach(angular.mock.module(vormTemplateServiceModule, inputModule));
		
		beforeEach(angular.mock.inject([ '$rootScope', 'vormTemplateService', ( ...rest ) => {
			
			[ $rootScope, service ] = rest;
			
		}]));
		
		it('should return the default compiler for the specified type', ( ) => {
			
			const compiler = service.getModelCompiler('text');
			
			expect(compiler).toBeDefined();
			
			const el = compiler($rootScope.$new());
			
			expect(el[0].tagName.toLowerCase()).toBe('input');
			
		});
		
		it('should return a custom compiler if specified', ( ) => {
			
			const tpl = `
				<custom-element></custom-element>
			`;
			
			const compiler = service.getModelCompiler('text', tpl);
			
			expect(compiler).toBeDefined();
			expect(compiler).not.toBe(service.getModelCompiler('text'));
			
			const el = compiler($rootScope.$new());
			
			expect(el[0].tagName.toLowerCase()).toBe('custom-element');
			
		});
		
		it('should throw an error if no compiler is available for that type', ( ) => {
			
			expect(( ) => {
				service.getModelCompiler('foo');
			}).toThrow();
			
		});
	});
	
	describe('with modifications', ( ) => {
		
		beforeEach(angular.mock.module(inputModule, vormTemplateServiceModule, ( vormTemplateServiceProvider ) => {
			
			vormTemplateServiceProvider.modifyModelTemplates(( el, type ) => {
				
				if (type === 'text') {
					return angular.element('<custom-input><custom-input/>');
				}
				
				return el;
			});
			
			vormTemplateServiceProvider.modifyDisplayTemplates(( ) => {
				return angular.element('<custom-display/>');
			});
			
			vormTemplateServiceProvider.modifyTemplate(( ) => {
				return angular.element('<custom-template></custom-template>');
			});
		}));
		
		beforeEach(angular.mock.inject([ '$rootScope', 'vormTemplateService', ( ...rest ) => {
			
			[ $rootScope, service ] = rest;

		}]));
		
		
		it('should return the modified default model template', ( ) => {
			
			const el = service.getModelCompiler('text')($rootScope.$new());
			
			expect(el[0].tagName.toLowerCase()).toBe('custom-input');
			
		});
		
		it('should return the modified default display template', ( ) => {
			
			const el = service.getDisplayCompiler('text')($rootScope.$new());
			
			expect(el[0].tagName.toLowerCase()).toBe('custom-display');
			
		});
		
		it('should return the modified default template', ( ) => {
			
			const el = angular.element(service.getDefaultTemplate());
			
			expect(el[0].tagName.toLowerCase()).toBe('custom-template');
			
		});
		
	});
	
	
});
