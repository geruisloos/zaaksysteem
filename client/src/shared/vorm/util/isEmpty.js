export default ( value ) => {
	return value === '' || value === null || value === undefined;
};
