import angular from 'angular';
import htmlsave from 'htmlsave';

export default
	angular.module('zsTruncateHtml', [
	])
		.directive('zsTruncateHtml', [ '$sce', ( $sce ) => {

			return {
				restrict: 'E',
				template:
					`<div
						class="html-truncate-wrapper"
						ng-class="{ collapsed: vm.isCollapsed() }"
					>

						<div ng-bind-html="vm.getHtml()"></div>

						<button
							type="button"
							class="btn btn-flat vorm-fieldset-description-show-more"
							ng-show="vm.isTruncated()"
							ng-click="vm.handleToggle()"
						>
							{{vm.isCollapsed() ? 'Meer': 'Minder' }}
						</button>
					</div>`,
				scope: {
					length: '&',
					value: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( scope ) {

					let ctrl = this,
						html,
						truncated,
						collapsed = true;

					let setHtml = ( value ) => {

						let val = value ? String(value) : '',
							truncatedHtml = htmlsave.truncate(val || '', ctrl.length()),
							htmlToDisplay = collapsed ? truncatedHtml : val;

						truncated = truncatedHtml !== val;

						html = $sce.trustAsHtml(htmlToDisplay);
					};

					scope.$watch(( ) => ctrl.value(), ( value ) => {

						setHtml(value);

					});

					ctrl.getHtml = ( ) => html;

					ctrl.isTruncated = ( ) => truncated;

					ctrl.isCollapsed = ( ) => collapsed;

					ctrl.handleToggle = ( ) => {

						collapsed = !collapsed;
						
						setHtml(ctrl.value());
					};

				}],
				controllerAs: 'vm'

			};
		}])
		.name;
