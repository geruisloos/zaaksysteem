import angular from 'angular';
import controller from './controller';

export default
	angular.module('zsPositionFixed', [
	])
		.directive('zsPositionFixed', [ '$document', '$animate', ( $document, $animate ) => {

			return {
				restrict: 'A',
				link: ( scope, element, attrs ) => {

					let positioner = controller(scope, element, $document, $animate, scope.$eval(attrs.zsPositionFixed));

					positioner.enable();

					scope.$on('$destroy', positioner.disable);

				}
			};
		}])
		.name;
