import angular from 'angular';
import template from './index.html';
import resourceModule from '../../shared/api/resource';
import composedReducerModule from '../../shared/api/resource/composedReducer';
import meetingListViewModule from './meetingListView';
import meetingListItemModule from './meetingListView/meetingListItem';
import snackbarServiceModule from '../../shared/ui/zsSnackbar/snackbarService';
import seamlessImmutable from 'seamless-immutable';
import flattenAttrs from '../shared/flattenAttrs';
import first from 'lodash/head';
import find from 'lodash/find';

export default {

	moduleName:
		angular.module('Zaaksysteem.meeting.meetingList', [
			resourceModule,
			meetingListItemModule,
			meetingListViewModule,
			snackbarServiceModule,
			composedReducerModule
		])
		.controller('meetingListController', [ '$stateParams', '$scope', 'proposalResource', 'meetingResource', 'currentAppConfig', ( $stateParams, $scope, proposalResource, meetingResource, currentAppConfig ) => {

			$scope.proposalResource = proposalResource;
			$scope.meetingResource = meetingResource;
			$scope.meetingType = $stateParams.meetingType;
			$scope.appConfig = currentAppConfig.data;

			$scope.$on('$destroy', ( ) => {
				
				[ proposalResource, meetingResource ].forEach( resource => {
					resource.destroy();
				});
			});

		}])
		.name,
	config: [
		{
			route: {
				url: '/:group/',
				scope: {
					appConfig: '&'
				},
				params: {
					meetingType: { squash: true, value: 'open' }
				},
				template,
				resolve: {
					currentAppConfig: [ '$rootScope', 'composedReducer', '$stateParams', 'appConfig', ( $rootScope, composedReducer, $stateParams, appConfig ) => {

						let group = $stateParams.group;

						return composedReducer( { scope: $rootScope }, appConfig )
							.reduce( config => {
								return find(config, ( configItem ) => {
									return configItem.instance.interface_config.short_name === group;
								});
							});

					}],
					proposalResource: [ '$rootScope', '$q', 'currentAppConfig', 'resource', 'snackbarService', ( $rootScope, $q, currentAppConfig, resource, snackbarService ) => {

						let proposalResource = resource(( ) => {
								
							let publicationAttribute = currentAppConfig.data().instance.interface_config.proposal_publication_filter_attribute,
								publicationAttributeValue = currentAppConfig.data().instance.interface_config.proposal_publication_filter_attribute_value,
								publicationQueryPartial = '';

							if (publicationAttribute && publicationAttributeValue) {
								publicationQueryPartial = `AND (${publicationAttribute.object.column_name} = "${publicationAttributeValue}")`;
							}

							return {
								url: '/api/v1/case',
								params: {
									zql: `SELECT {} FROM case WHERE (case.casetype.id IN (${currentAppConfig.data().instance.interface_config.casetype_voorstel_item.join()})) ${publicationQueryPartial}`,
									rows_per_page: 50
								}
							};

						}, { scope: $rootScope })

							// grab first value of array for every attribute to reduce
							// boilerplate
							
							.reduce( ( requestOptions, data ) => {
								return (data || seamlessImmutable([])).map(flattenAttrs);
							});


						return proposalResource.asPromise()
							.then(( ) => proposalResource)
							.catch( err => {
								snackbarService.error('Er ging iets fout bij het ophalen van de voorstellen. Neem contact op met uw beheerder voor meer informatie.');
								return $q.reject(err);
							});

					}],
					meetingResource: [ '$rootScope', '$q', '$stateParams', 'resource', 'currentAppConfig', 'snackbarService', ( $rootScope, $q, $stateParams, resource, currentAppConfig, snackbarService ) => {

						let meetingResource = resource( ( ) => {

							let statuses =
								$stateParams.meetingType === 'open' ?
									[ 'open', 'new' ]
									: [ 'resolved' ],
								publicationAttribute = currentAppConfig.data().instance.interface_config.meeting_publication_filter_attribute,
								publicationAttributeValue = currentAppConfig.data().instance.interface_config.meeting_publication_filter_attribute_value,
								publicationQueryPartial = '',
								zql;

							if (publicationAttribute && publicationAttributeValue) {
								publicationQueryPartial = `AND (${publicationAttribute.object.column_name} = "${publicationAttributeValue}")`;
							}
								
							zql = `SELECT {} FROM case WHERE (case.casetype.id = ${first(currentAppConfig.data().instance.interface_config.casetype_meeting)}) ${publicationQueryPartial} AND (case.status IN ("${statuses.join('","')}") )`;

							return {
								url: '/api/v1/case',
								params: {
									zql,
									rows_per_page: 50
								}
							};

						}, { scope: $rootScope })
							.reduce( ( requestOptions, data ) => {
								return (data || seamlessImmutable([])).map(flattenAttrs);
							});

						return meetingResource.asPromise()
							.then( ( ) => meetingResource)
							.catch( err => {
								snackbarService.error('Er ging iets fout bij het ophalen van de vergaderingen. Neem contact op met uw beheerder voor meer informatie.');
								return $q.reject(err);
							});

					}]
				},
				controller: 'meetingListController'
			},
			state: 'meetingList'
		}
	]
};
