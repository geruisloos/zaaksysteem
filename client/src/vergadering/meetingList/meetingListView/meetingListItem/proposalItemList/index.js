import angular from 'angular';
import template from './template.html';
import composedReducerModule from '../../../../../shared/api/resource/composedReducer';
import assign from 'lodash/assign';
import shortid from 'shortid';
import get from 'lodash/get';
import find from 'lodash/find';
import sortByOrder from 'lodash/orderBy';
import rwdServiceModule from '../../../../../shared/util/rwdService';
import includes from 'lodash/includes';
import appServiceModule from '../../../../shared/appService';
import seamlessImmutable from 'seamless-immutable';
import angularUiRouterModule from 'angular-ui-router';
import auxiliaryRouteModule from '../../../../../shared/util/route/auxiliaryRoute';
import isArray from 'lodash/isArray';
import isString from 'lodash/isString';
import getAttributes from '../../../../shared/getAttributes';

import './styles.scss';

export default
		angular.module('Zaaksysteem.meeting.meetingListItem.proposalItemList', [
			composedReducerModule,
			rwdServiceModule,
			appServiceModule,
			angularUiRouterModule,
			auxiliaryRouteModule
		])
		.directive('proposalItemList', [ '$sce', '$location', '$stateParams', 'dateFilter', '$state', 'composedReducer', 'rwdService', 'appService', 'auxiliaryRouteService', ( $sce, $location, $stateParams, dateFilter, $state, composedReducer, rwdService, appService, auxiliaryRouteService ) => {

			return {

				restrict: 'E',
				template,
				scope: {
					proposals: '&',
					appConfig: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( $scope ) {

					let ctrl = this,
						attrReducer,
						columnReducer,
						sortReducer,
						proposalReducer,
						sortedProposalReducer,
						sort = {},
						appConfigReducer;

					let getDetailState = ( ) => {
						return auxiliaryRouteService.append($state.current, 'proposalDetail');
					};

					ctrl.getViewSize = ( ) => {
						if ( includes( rwdService.getActiveViews(), 'small-and-down') ) {
							return 'small';
						}
						return 'wide';
					};

					ctrl.isGrouped = ( ) => appService.state().grouped;

					appConfigReducer = composedReducer( { scope: $scope }, ctrl.appConfig )
						.reduce( config => config );

					attrReducer = composedReducer( { scope: $scope }, appConfigReducer, ( ) => appService.state().grouped)
						.reduce( ( config, grouped ) => {

							let attributes = getAttributes(config).voorstel || [];

							let noteCol = {
											external_name: 'voorstelnotitie',
											internal_name: {
												searchable_object_id: 'proposalNote',
												searchable_object_label: 'Notitie'
											},
											checked: 1
										};

							return grouped ?
								[
									{
										external_name: 'voorstelnummer',
										internal_name: {
											searchable_object_id: '$index',
											searchable_object_label: ''
										},
										checked: 1
									}
								].concat(attributes)
								.concat(noteCol)
								: attributes.concat(noteCol);

						});

					sortReducer = composedReducer({ scope: $scope }, ( ) => sort, attrReducer)
						.reduce( ( userSort, attributes ) => {

							let finalSort,
								sortKey;

							if (userSort.key) {
								finalSort = userSort;
							} else {

								let firstCol = find(attributes, ( attr ) => attr.external_name.indexOf('voorstel') === 0),
									key = firstCol.internal_name.searchable_object_id;

								finalSort = seamlessImmutable({
									key,
									reversed: false
								});

							}

							switch ( finalSort.key ) {
								case 'zaaknummer':
								sortKey = 'instance.number';
								break;
								case 'zaaktype':
								sortKey = 'instance.casetype.instance.name';
								break;
								default:
								sortKey = `instance.attributes.${finalSort.key}`;
							}

							finalSort = finalSort.merge({
								resolve: sortKey
							});

							return finalSort;

						});
						
					columnReducer = composedReducer({ scope: $scope }, attrReducer, sortReducer)
						.reduce( ( attributes, sortOptions ) => {

							return seamlessImmutable(attributes)
								.filter( n => {
									return n.external_name.indexOf('voorstel') === 0 && n.checked === 1;
								})
								.map( attr => {

									let isSortedOn = sortOptions.key === get(attr, 'internal_name.searchable_object_id'),
										isSortReversed = isSortedOn && sortOptions.reversed,
										colName = attr.external_name,
										templateValue = attr.internal_name.searchable_object_label_public || attr.internal_name.searchable_object_label,
										wideCol = false,
										mediumCol = false,
										smallCol = false;

										if (colName === 'voorstelnummer') {
											templateValue = '';
											smallCol = true;
										}

										if (colName === 'voorstelnotitie') {
											templateValue = '';
											smallCol = true;
										}

										if (colName === 'voorstelzaaknummer') {
											templateValue = '#';
											mediumCol = true;
										}

										if (colName === 'voorstelonderwerp') {
											wideCol = true;
										}

										if (colName === 'voorstelaanvrager') {
											templateValue = 'Aanvrager naam';
										}

									return attr.merge({
										icon: isSortReversed ?
											'chevron-up'
											: (isSortedOn ? 'chevron-down' : ''),
										classes: {
											'sorted-on': isSortedOn,
											'sort-reversed': isSortReversed
										},
										colClass: {
											'wide': wideCol,
											'medium': mediumCol,
											'small': smallCol
										},
										templateValue
									});

								});
						});
					
					ctrl.getConfigColumns = columnReducer.data;

					ctrl.sortByColumn = ( column ) => {

						let sortOptions = sortReducer.data(),
							key = sortOptions.key,
							reversed = sortOptions.reversed,
							colId = get(column, 'internal_name.searchable_object_id');

						if (sortOptions.key === colId) {
							reversed = !reversed;
						} else {
							reversed = false;
							key = colId;
						}

						sort = seamlessImmutable({ key, reversed });
					};

					ctrl.handleProposalClick = ( proposal ) => {
						$state.go(getDetailState(), { zaakID: proposal.casenumber });
					};

					sortedProposalReducer = composedReducer( { scope: $scope }, ctrl.proposals, ctrl.getConfigColumns, sortReducer, ( ) => appService.state().grouped, $stateParams)
						.reduce( ( proposals, columns, sortOptions, isGrouped, stateParams ) => {

							let sortedProposals,
								state = stateParams.meetingType === 'open' ? ['open', 'new'] : ['resolved'];

							if (!isGrouped) {
								sortedProposals = proposals.filter( proposal => {

									return includes(state, proposal.instance.status) ? proposal : null;

								});
							} else {
								sortedProposals = proposals;
							}

							if (sortOptions && sortOptions.key) {
								sortedProposals = seamlessImmutable(sortByOrder(sortedProposals,
									( proposal ) => get(proposal, sortOptions.resolve),
									sortOptions.reversed ? 'desc' : 'asc'
								));
							}

							return sortedProposals;

						});

					// For bigger viewports
					proposalReducer = composedReducer({ scope: $scope }, sortedProposalReducer, ctrl.getConfigColumns )
						.reduce( ( sortedProposals, columns ) => {

							let mutableCols = columns.asMutable(),
								formattedProposals;

							formattedProposals =
								sortedProposals.asMutable().map( ( proposal ) => {

									return assign(
										{
											$id: shortid(),
											id: proposal.reference,
											casenumber: proposal.instance.number,
											casetype: proposal.instance.casetype.instance.name,
											classes: {
												closed: !!(proposal.instance.status === 'resolved')
											}
										},
										{
											values: mutableCols.map( ( column ) => {

												let columnName = column.internal_name.searchable_object_id,
													value = proposal.instance.attributes[columnName],
													tpl;

												if ( isArray(value) ) {

													if (value.length > 1) {
														tpl = '<ul>';

														value.map( ( el ) => {
															tpl += `<li>${el}</li>`;
														});

														tpl += '</ul>';
													} else {
														tpl = `${value.join()}`;
													}

												} else if ( isString(value) && value.split(' ').length > 20 ) {

													let resultArray = value.split(' ');
													
													if (resultArray.length > 20) {
														resultArray = resultArray.slice(0, 20);
														tpl = `${resultArray.join(' ')}...`;
													}

												} else if (columnName === '$index' || isString(value) ) {
													tpl = value;
												} else {
													tpl = '-';
												}

												if (column.external_name === 'voorstelnummer') {
													tpl = `<span class="proposal__number">${tpl}</span>`;
												}

												if (columnName === 'zaaknummer') {
													tpl = String(proposal.instance.number || '');
												}

												if (columnName === 'zaaktype') {
													tpl = String(proposal.instance.casetype.instance.name || '-');
												}

												if (columnName === 'vertrouwelijkheid') {
													tpl = String(proposal.instance.confidentiality.mapped);
												}

												if (columnName === 'aanvrager_naam') {
													tpl = String(`${proposal.instance.requestor.instance.subject.instance.first_names} ${proposal.instance.requestor.instance.subject.instance.surname}`);
												}

												if (columnName === 'resultaat') {
													tpl = String(proposal.instance.result || '-');
												}

												if (column.attribute_type === 'file') {
													tpl = `<a href="/download/${value.id}">Download</a>`;
												}

												if (column.external_name === 'voorstelnotitie') {
													if ( proposal.notes ) {
														tpl = '<i class="mdi mdi-note-plus"></i>';
													} else {
														tpl = '';
													}
												}

												if (column.external_name.includes('datum') ) {
													tpl = dateFilter( value, 'dd MMM yyyy');
												}

												return {
													id: shortid(),
													name: columnName,
													template: $sce.trustAsHtml(tpl),
													value
												};

											})
										}
									);
								});

							return formattedProposals;
						});

					ctrl.getProposals = proposalReducer.data;

					// For smaller viewports
					ctrl.getSmallProposals = composedReducer({ scope: $scope }, sortedProposalReducer, ctrl.getConfigColumns)
						.reduce( ( proposals, columns ) => {

							return proposals.map(( proposal, index ) => {

								let descriptionCol = find(columns, ( col ) => col.external_name === 'voorstelonderwerp' ),
									value = proposal.instance.attributes[descriptionCol.internal_name.searchable_object_id] || proposal.instance.casetype.instance.name || '-';

								if (value.split(' ').length > 20) {
									
									let resultArray = value.split(' ');
									
									if (resultArray.length > 20) {
										resultArray = resultArray.slice(0, 20);

										value = `${resultArray.join(' ')}...`;

									}
								}

								return {
									$id: shortid(),
									id: proposal.reference,
									casenumber: proposal.instance.number,
									subject: `${proposal.instance.number}: ${value}`,
									agendapunt_nummer: index + 1,
									notes: proposal.notes || false,
									link: $state.href(getDetailState(), { zaakID:  proposal.instance.number }),
									classes: {
										closed: !!(proposal.instance.status === 'resolved')
									}
								};

							});

						})
						.data;

				}],
				controllerAs: 'proposalItemList'

			};
		}
		])
		.name;
