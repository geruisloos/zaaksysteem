import angular from 'angular';
import 'angular-mocks';
import proposalItemListModule from '.';
import configData from '../../shared/mock/config.json';
import proposalData from '../../shared/mock/proposal.json';
import cacherModule from '../../../shared/api/cacher';
import angularUiRouterModule from 'angular-ui-router';
import appServiceModule from '../../shared/appService';
import configServiceModule from './../../shared/configService';
import first from 'lodash/head';
import mapValues from 'lodash/mapValues';
import merge from 'lodash/merge';

describe('proposalItemList', ( ) => {

	let proposalItemList,
		$httpBackend,
		apiCacher,
		el;

	beforeEach(angular.mock.module(proposalItemListModule, cacherModule, angularUiRouterModule, appServiceModule, configServiceModule, [ 'appServiceProvider', ( appServiceProvider ) => {

		appServiceProvider.setDefaultState({ expanded: true, grouped: true })
			.reduce('toggle_grouped', 'grouped', ( isGrouped ) => !isGrouped)
			.reduce('toggle_expand', 'expanded', ( isExpanded ) => !isExpanded);

	}]));


	beforeEach(angular.mock.inject([ '$httpBackend', 'apiCacher', '$state', ( ...rest ) => {

		[ $httpBackend, apiCacher ] = rest;

		$httpBackend.expectGET(/\/api\/v1\/app\/bbvapp/)
			.respond([ configData ]);

		apiCacher.clear();

	}]));

	beforeEach(angular.mock.inject([ '$rootScope', '$compile', '$document', 'configService', ( ...rest ) => {

		let $rootScope, $compile, $document;

		[ $rootScope, $compile, $document ] = rest;

		$httpBackend.flush();

		el = angular.element(`<proposal-item-list
			data-proposals="proposals"
		/>`);

		$document.find('body').append(el);

		let scope = $rootScope.$new();

		scope.proposals = [ proposalData ]
			.map( ( proposal ) => {
				return merge(proposal, {
					instance: {
						attributes: mapValues(proposal.instance.attributes, first)
					}
				});
			});

		$compile(el)(scope);

		proposalItemList = el.controller('proposalItemList');

	}]));


	it('should have a controller', ( ) => {

		expect(proposalItemList).toBeDefined();

	});

	it('should return a viewport size', ( ) => {

		expect(proposalItemList.getViewSize() ).toEqual('wide');

	});

	it('should be able to return whether items are grouped', ( ) => {

		expect(proposalItemList.isGrouped).toBeDefined();

		expect( typeof proposalItemList.isGrouped() ).toEqual('boolean');

	});


	it('should return the configured columns', ( ) => {

		expect( proposalItemList.getConfigColumns).toBeDefined();

		expect( proposalItemList.getConfigColumns().length ).not.toEqual(0);

	});

	it('should be able to sort items per column', ( ) => {

		expect( proposalItemList.sortByColumn).toBeDefined();

	});


	it('should be able to trigger the opening of a detail view', ( ) => {

		expect( proposalItemList.handleProposalClick).toBeDefined();

	});

	it('should be able return proposals for tablet and desktop viewports', ( ) => {

		expect( proposalItemList.getProposals).toBeDefined();

		expect( proposalItemList.getProposals().length).not.toEqual(0);

	});

	it('should be able return proposals tailored to smaller viewports', ( ) => {

		expect( proposalItemList.getSmallProposals ).toBeDefined();

		expect( proposalItemList.getSmallProposals().length ).not.toEqual(0);

	});

});
