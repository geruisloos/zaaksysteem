import partition from 'lodash/partition';
import filter from 'lodash/filter';
import get from 'lodash/get';
import seamlessImmutable from 'seamless-immutable';

export default ( config ) => {

	let split =
		partition(
			filter(get(config, 'instance.interface_config.attribute_mapping', []), (attribute) => attribute.hasOwnProperty('internal_name') && attribute.internal_name !== null ),
			( attribute ) => attribute.external_name.indexOf('voorstel') === 0
		);

	return {
		voorstel: seamlessImmutable(split[0]),
		vergadering: seamlessImmutable(split[1]),
		all: seamlessImmutable( config.instance.interface_config.attribute_mapping )
	};

};
