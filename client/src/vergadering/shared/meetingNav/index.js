import angular from 'angular';
import template from './index.html';
import uiRouter from 'angular-ui-router';
import proposalListServiceModule from '../../meetingList/proposalListService';
import zsIcon from '../../../shared/ui/zsIcon';
import rwdServiceModule from '../../../shared/util/rwdService';
import composedReducerModule from '../../../shared/api/resource/composedReducer';
import zsUiViewProgressModule from './../../../shared/ui/zsNProgress/zsUiViewProgress';
import appServiceModule from './../appService';
import zsSuggestionListModule from './../../../shared/ui/zsSuggestionList';
import sessionServiceModule from '../../../shared/user/sessionService';
import snackbarServiceModule from '../../../shared/ui/zsSnackbar/snackbarService';
import zsStorageModule from './../../../shared/util/zsStorage';
import nGramFilter from '../nGramFilter';
import includes from 'lodash/includes';
import get from 'lodash/get';
import last from 'lodash/last';
import pick from 'lodash/pick';
import './styles.scss';

export default
	angular.module('Zaaksysteem.meeting.nav', [
		uiRouter,
		proposalListServiceModule,
		zsIcon,
		rwdServiceModule,
		composedReducerModule,
		zsUiViewProgressModule,
		appServiceModule,
		zsSuggestionListModule,
		sessionServiceModule,
		snackbarServiceModule,
		zsStorageModule
	])
		.directive('meetingNav', [ '$window', '$http', '$document', '$stateParams', '$rootScope', '$state', '$timeout', 'proposalListService', 'rwdService', 'composedReducer', 'appService', 'sessionService', 'snackbarService', 'zsStorage', ( $window, $http, $document, $stateParams, $rootScope, $state, $timeout, proposalListService, rwdService, composedReducer, appService, sessionService, snackbarService, zsStorage ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					titleBind: '&',
					onButtonClick: '&',
					views: '&',
					appConfig: '&'
				},
				bindToController: true,
				controller: [ '$scope', '$element', function ( $scope, $element ) {

					let ctrl = this,
						styleReducer,
						titleReducer,
						input = $element.find('input'),
						userResource = sessionService.createResource($scope),
						appConfigReducer;

					appConfigReducer = composedReducer( { scope: $rootScope }, ctrl.appConfig)
							.reduce( config => config);

					styleReducer = composedReducer( { scope: $scope }, appConfigReducer )
						.reduce( config => {

							let style = {
								container: {
									'background-image': `url(${get(config, 'instance.interface_config.header_bgimage')})`
								},
								bg: {
									'background-color': get(config, 'instance.interface_config.header_bgcolor')
								}
							};

							return style;

						});

					titleReducer = composedReducer({ scope: $scope }, rwdService.getActiveViews, appConfigReducer )
						.reduce( ( views, config ) => {

							let title;

							if (includes(views, 'small-and-down')) {
								title = get(config, 'instance.interface_config.title_small');
							} else {
								title = get(config, 'instance.interface_config.title_normal');
							}

							return title;
						});

					ctrl.getState = ( ) => $state.current.name;

					ctrl.getActiveFilters = ( ) => appService.state().filters;

					ctrl.addFilter = ( query ) => {

						if (appService.state().filters.length < 3) {
						
							appService.dispatch('filter_add', query);

							ctrl.searchQuery = '';

							$timeout( ( ) => {
								input[0].focus();
							}, 0, false);

						} else {

							snackbarService.info('U kunt maximaal 3 filters toevoegen. Verwijder een filter en probeer het opnieuw.');

						}

					};

					ctrl.removeFilter = ( filter ) => {
						appService.dispatch('filter_remove', filter);
					};

					ctrl.handleSuggestionSelect = ( suggestion ) => {
						if (appService.state().filters.length < 3) {
							ctrl.searchQuery = '';
							appService.dispatch('filter_add', suggestion.id);
						}
					};

					ctrl.handleSearchToggle = ( ) => {
						ctrl.searchActive = !ctrl.searchActive;

						if (!ctrl.searchActive) {
							appService.dispatch('filter_clear');
						}
						
					};

					ctrl.isSearchVisible = ( ) => {
						return ctrl.searchActive && $state.current.name !== 'proposalDetail';
					};

					ctrl.getSuggestions = composedReducer({ scope: $scope }, ( ) => ctrl.searchQuery )
						.reduce( ( query ) => {

							return query ?
								nGramFilter.getFromIndex(query)
									.map(( suggestion ) => {

										let rex = /(<([^>]+)>)|(&lt;([^>]+)&gt;)/ig;

										return {
											id: suggestion,
											label: suggestion.replace(rex, ' ')
										};

								}) : null;

						})
						.data;

					ctrl.handleSearchKeyUp = ( event ) => {

						switch (event.keyCode) {
							case 27:
							ctrl.handleSearchToggle();
							break;

							case 8:
							if (!ctrl.searchQuery && ctrl.getActiveFilters().length) {
								ctrl.removeFilter(last(ctrl.getActiveFilters()));
							}
							break;

							case 13:
							if (ctrl.searchQuery.length ) {
								ctrl.addFilter(ctrl.searchQuery);
							}
							break;
						}

					};

					ctrl.getKeyInputDelegate = ( ) => ({ input });

					// Get header nav styling
					ctrl.getContainerStyle = ( ) => get(styleReducer.data(), 'container');
					ctrl.getBgContainerStyle = ( ) => get(styleReducer.data(), 'bg');

					// Expanding / contracting items
					ctrl.toggleExpand = ( ) => {
						appService.dispatch('toggle_expand');
					};


					ctrl.handleLoginAction = ( ) => {

						if ( ctrl.isLoggedIn() ) {

							$http({
								url: '/auth/logout',
								method: 'POST'
							})
							.then( ( ) => {

								zsStorage.clear();
								$window.location.reload();

							});

						} else {
							zsStorage.clear();
							$window.location = `/auth/login?referer=${$window.location.pathname}`;
						}
					};

					ctrl.isLoggedIn = ( ) => {

						if ( userResource.state() === 'resolved') {
							return true;
						}

						return false;
					};

					ctrl.isWriteModeEnabled = ( ) => appConfigReducer.data().instance.interface_config.access === 'rw';

					ctrl.isMeetingListActive = ( ) => !appConfigReducer.data().instance.interface_config.groupList;

					// Get title
					ctrl.getViewportTitle = titleReducer.data;

					// Grouping / ungrouping proposals by meeting
					ctrl.groupItems = ( ) => {
						$window.scrollTo(0, 0);
						appService.dispatch('toggle_grouped');
					};

					ctrl.isGrouped = ( ) => appService.state().grouped;

					// Back button
					ctrl.goBack = () => {
						$state.go('meetingList', null, { inherit: true });
					};


					$rootScope.$on('meeting.view.scroll', ( event, options ) => {
						
						let container = $element[0].querySelector('.meeting-nav__tabs-slidecontainer'),
							slider = container.querySelector('.meeting-nav__tabs-slide'),
							percentage = options.percentage,
							scaleX = 1 / ctrl.views().length,
							translateX = percentage * scaleX - scaleX / 2;

						slider.style.transform = slider.style.webkitTransform = `translateX(${Math.round(translateX * 100)}%) scaleX(${scaleX})`;

						if (!angular.equals(
							pick(slider.style, 'transitionTimingFunction', 'transitionDuration'),
							pick(options, 'transitionTimingFunction', 'transitionDuration')
						)) {

							slider.style.transitionTimingFunction = options.transitionTimingFunction;
							slider.style.transitionDuration = options.transitionDuration;
						}

					});

				}],
				controllerAs: 'meetingNav'
			};
		}])
		.directive('focusOnCondition', ['$timeout', ( $timeout ) => {
			return {
				restrict: 'A',
				link: ( scope, element, attrs ) => {
					$timeout( ( ) => {
						scope.$watch(attrs.focusOnCondition, ( value ) => {
							if (value) {
								element[0].focus();
							}
						});
					});
				}
			};
		}])
		.directive('zsScroll', ['$window', ($window) => {

			return {
				restrict: 'A',
				bindToController: true,
				controller: [ '$scope', '$element', function ( $scope, $element ) {

					let el = $element[0],
						tabsEl = el.querySelector('.meeting-nav__tabs'),
						containerEl = el.querySelector('.meeting-nav__container'),
						buttonEl = el.querySelector('.meeting-nav__button-bar'),
						containerHeight = containerEl.clientHeight,
						tabsHeight = tabsEl.clientHeight,
						maxOpacity = 0.25,
						lastSet = NaN;

					let getMultiplier = ( ) => Math.max(0, Math.min(1, $window.pageYOffset / (containerHeight - tabsHeight)));

					let setDimensions = ( ) => {

						let multiplier = getMultiplier(),
							maxMargin = buttonEl.clientWidth + parseFloat(buttonEl.style.paddingLeft || 0) + parseFloat(buttonEl.style.paddingRight || 0);

						tabsEl.style.marginRight = `${multiplier * maxMargin}px`;
					};

					let setStyle = ( ) => {

						let multiplier = getMultiplier(),
							opacity = (1 - multiplier) * maxOpacity;

						if (lastSet === multiplier) {
							return;
						}

						lastSet = multiplier;

						if (multiplier >= 1) {
							$element.addClass('small');
						} else {
							$element.removeClass('small');
						}

						el.querySelector('.meeting-nav__bg').style.opacity = opacity;

						setDimensions();

					};

					angular.element($window).bind('scroll', setStyle);
					angular.element($window).bind('resize', setDimensions);

					$scope.$$postDigest(setStyle);
					$scope.$$postDigest(setDimensions);

				}]
			};
		}])
		.name;
