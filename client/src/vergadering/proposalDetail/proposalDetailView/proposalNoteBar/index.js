import angular from 'angular';
import template from './template.html';
import angularUiRouter from 'angular-ui-router';
import first from 'lodash/head';
import get from 'lodash/get';
import zsModalModule from '../../../../shared/ui/zsModal';
import templateNoteEdit from './template-noteedit.html';
import seamlessImmutable from 'seamless-immutable';
import findIndex from 'lodash/findIndex';
import has from 'lodash/has';
import './styles.scss';

export default
		angular.module('Zaaksysteem.meeting.proposalNoteBar', [
			angularUiRouter,
			zsModalModule
		])
		.directive('proposalNoteBar', [ '$window', '$timeout', '$compile', '$state', 'zsModal', ( $window, $timeout, $compile, $state, zsModal ) => {

			return {

				restrict: 'E',
				template,
				scope: {
					proposalId: '&',
					proposalTitle: '&',
					proposalNote: '&',
					onSaveNote: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( $scope) {

					let ctrl = this,
						noteStorage = seamlessImmutable( JSON.parse( $window.localStorage.getItem('meetingAppNotes') ) ),
						noteEditModal;

					ctrl.getTitle = ctrl.proposalTitle;

					if (!noteStorage) {
						$window.localStorage.setItem('meetingAppNotes', JSON.stringify([]));
						noteStorage = seamlessImmutable( JSON.parse( $window.localStorage.getItem('meetingAppNotes') ) );
					}

					if (noteStorage && noteStorage.length ) {
						ctrl.noteContent = get( first( noteStorage.filter( ( note ) => get(note, 'id') === ctrl.proposalId() ) ), 'content') || '';
					}

					ctrl.getNote = ( ) => {

						if (noteStorage && noteStorage.length ) {
						
							return get( first( noteStorage.filter( ( note ) => get(note, 'id') === ctrl.proposalId() ) ), 'content') || 'Voeg een persoonlijke notitie toe';
						}

						return 'Voeg een persoonlijke notitie toe';
					};

					ctrl.handleNoteClick = ( ) => {

						let noteModalTemplate = angular.element(templateNoteEdit);

						noteEditModal = zsModal({
							el: $compile(noteModalTemplate)($scope),
							title: ctrl.proposalTitle(),
							classes: 'note-modal'
						});

						noteEditModal.open();

						$timeout(( ) => {
							noteModalTemplate.find('textarea')[0].focus();
						}, 0, false);

					};

					ctrl.saveNote = ( ) => {

						let index = findIndex(noteStorage, ( note ) => get(note, 'id') === ctrl.proposalId() ),
							noteObj = { id: ctrl.proposalId(), content: ctrl.noteContent },
							newNoteStorage = noteStorage;

						if (index === -1) {
							newNoteStorage = newNoteStorage.concat(noteObj);
						} else {
							newNoteStorage = newNoteStorage.map( ( note ) => {
								if ( get(note, 'id') === ctrl.proposalId() && noteObj.content !== '') {
									return noteObj;
								}
								if ( get(note, 'id') === ctrl.proposalId() && noteObj.content === '') {
									return { };
								}
								return note;
							});
						}

						$window.localStorage.setItem('meetingAppNotes', JSON.stringify( newNoteStorage.filter( ( note ) => has(note, 'id') ) ));

						noteStorage = newNoteStorage;

						noteEditModal.close();

						ctrl.onSaveNote({
							$proposalId: ctrl.proposalId()
						});
					};

					ctrl.goBack = ( ) => {
						noteEditModal.close();
					};

					$scope.$on('$destroy', ( ) => {
						if (noteEditModal) {
							noteEditModal.close();
						}
					});


				}],
				controllerAs: 'proposalNoteBar'

			};
		}
		])
		.name;
