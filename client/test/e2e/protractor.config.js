require('babel-core/register')({
	presets: [ 'es2015' ],
	plugins: [ 'add-module-exports' ]
});

var path = require('path'), // eslint-disable-line
	login = require('./functions/common/auth/login'),
	baseUrl = 'https://testbase-local.zaaksysteem.nl';

exports.config = {
	allScriptsTimeout: 11000,
	specs: [
		'tests/**/*.js'
	],
	capabilities: {
		'browserName': 'chrome',
		'chromeOptions': {
			args: [
				'--disable-extensions',
				'--disable-web-security',
				`--user-data-dir=${path.join(__dirname, './../../../inc/chrome-profile')}`
			]
		}
	},
	chromeOnly: true,
	framework: 'jasmine2',
	directConnect: true,
	baseUrl: baseUrl, //eslint-disable-line
	seleniumAddress: 'http://127.0.0.1:4444/wd/hub',
	jasmineNodeOpts: {
		defaultTimeoutInterval: 30000
	},
	onPrepare: function ( ) { //eslint-disable-line

		browser.driver.get(baseUrl + '/auth/login'); //eslint-disable-line

		login();

	}
};
