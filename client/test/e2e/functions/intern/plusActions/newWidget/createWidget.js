import openPlusAction from './../openPlusAction';

export default ( type, name ) => {

	openPlusAction('create_widget');

	$(`zs-dashboard-widget-create [data-name="${type}"]`).click();

	browser.waitForAngular();

	if ( type === 'Zoekopdracht') {

		let searches = element.all(by.css('zs-dashboard-widget-search-select-list-group li'));

		searches.filter((elm) => {
			return elm.getText().then((text) => {
				return text === `${name}`;
			});
		}).first().click();

	}

};
