export default ( name ) => {

	browser.actions()
		.mouseMove(element(by.css('.top-bar-create-case')), { x: 0, y: 0 })
		.perform();
	browser.actions()
		.mouseMove(element(by.css('.top-bar-create-case')), { x: 1, y: 1 })
		.perform();

	return element(
		by.css(`zs-contextual-action-menu-button [data-name=${name}] button`)
	).click();

};
