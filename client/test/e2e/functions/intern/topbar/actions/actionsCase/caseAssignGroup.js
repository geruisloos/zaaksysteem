import openAction from './../openAction';

export default ( department, role ) => {

	let form = $('zs-case-admin-view form');

	openAction('Toewijzing wijzigen');

	$('[data-name="department-picker"] select').sendKeys(department);
	$('[data-name="department-picker"] select').sendKeys(role);

	form.submit();

};
