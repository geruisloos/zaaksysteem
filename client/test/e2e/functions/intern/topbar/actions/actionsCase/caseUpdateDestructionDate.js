import openAction from './../openAction';
import inputDate from './../../../../common/input/attribute/inputDate';

export default ( day, month, year ) => {

	let form = $('zs-case-admin-view form'),
		dateField = $('[data-name="vernietigingsdatum"]');

	openAction('Vernietigingsdatum wijzigen');

	$('[data-name="vernietigingsdatum"] input[type="radio"]').click();

	inputDate(dateField, day, month, year);

	form.submit();

};
