import openAction from './../openAction';

export default ( type, termAmounType, termAmount ) => {

	let form = $('zs-case-admin-view form');

	openAction('Opschorten');

	$('[data-name="reason"] input').sendKeys('Reason');

	if (type === 'indeterminate') {

		$('[data-name="term"] [value="indeterminate"]').click();

	} else {

		$('[data-name="term_amount_type"] select').sendKeys(termAmounType);
		$('[data-name="term_amount"] input').clear().sendKeys(termAmount);

	}

	form.submit();

};
