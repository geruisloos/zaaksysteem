export default ( attribute, numberToInput ) => {

	attribute.getAttribute('class').then((attributeClass) => {

		let inputNumber,
			i;

		if (attributeClass.includes('multiple')) {

			inputNumber = numberToInput ? numberToInput : ['1', '2', '3', '4'];

		} else {

			inputNumber = numberToInput ? numberToInput : ['1234'];

		}

		for (i = 0; i < inputNumber.length; i++) {

			attribute.$(`li:nth-child(${i + 1}) input`).sendKeys(inputNumber[i]);

			if ( inputNumber.length - 1 > i ) {

				attribute.$('.vorm-field-add-button').click();

			}

		}
		
	});

};
