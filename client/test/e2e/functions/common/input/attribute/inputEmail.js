export default ( attribute, emailToInput ) => {

	let inputEmail = emailToInput ? emailToInput : 'test@test.nl';

	attribute.$('input').sendKeys(inputEmail);

};
