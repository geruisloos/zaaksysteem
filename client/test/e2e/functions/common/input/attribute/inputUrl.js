export default ( attribute, urlToInput ) => {

	let inputUrl = urlToInput ? urlToInput : 'https://www.google.com';

	attribute.$('input').sendKeys(inputUrl);

};
