export default ( attribute, dayToInput, monthToInput, yearToInput ) => {

	let inputDay = dayToInput ? dayToInput : '13',
		inputMonth = monthToInput ? monthToInput : 'Jan',
		inputYear = yearToInput ? yearToInput : '2010';

	attribute.$('input').sendKeys(inputDay);
	attribute.$('input').sendKeys(inputMonth);
	attribute.$('input').sendKeys(protractor.Key.TAB);
	attribute.$('input').sendKeys(inputYear);

};
