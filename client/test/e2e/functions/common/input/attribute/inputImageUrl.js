export default ( attribute, imageUrlToInput ) => {

	let inputImageUrl = imageUrlToInput ? imageUrlToInput : 'https://wiki.zaaksysteem.nl/skins/common/images/bussum.png';

	attribute.$('input').sendKeys(inputImageUrl);

};
