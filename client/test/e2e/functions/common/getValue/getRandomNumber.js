import getRandomNumber from 'lodash/random';

export default ( numberLength ) => {

	let number = Math.floor( getRandomNumber(0.1, 1) * Math.pow(10, numberLength) );

	return number;

};
