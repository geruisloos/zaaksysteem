import attemptLogin from './../../../functions/common/auth/attemptLogin';
import login from './../../../functions/common/auth/login';
import logout from './../../../functions/common/auth/logout';
import getPassword from './../../../functions/common/auth/getPassword';

describe('when logging in', ( ) => {

	beforeAll(( ) => {

		logout();

	});

	it('should have a form', ( ) => {

		expect(
			element(by.css('#loginwrap form')).isPresent()
		).toBe(true);

	});

	it('should have a username input field', ( ) => {

		expect(
			element(by.css('input[type=text]#id_username')).isPresent()
		).toBe(true);

	});

	it('should have a password input field', ( ) => {

		expect(
			element(by.css('input[type=password]#id_password')).isPresent()
		).toBe(true);

	});

	it('should have a submit button', ( ) => {

		expect(
			element(by.css('#loginwrap input[type=submit]')).isPresent()
		).toBe(true);

	});

	describe('when logging in with valid credentials', ( ) => {

		it('should redirect to the application', ( ) => {

			attemptLogin('admin', getPassword('admin'));

			expect(browser.getCurrentUrl()).toMatch(/intern/);

			logout();

		});

	});

	describe('when logging in with invalid credentials', ( ) => {

		it('should redirect to the application', ( ) => {

			attemptLogin('admin', 'wrong password');

			expect(browser.getCurrentUrl()).not.toMatch(/intern/);

		});

	});

	afterAll(( ) => {

		login();

	});

});
