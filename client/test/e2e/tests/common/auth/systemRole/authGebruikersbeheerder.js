import login from './../../../../functions/common/auth/login';
import loginAs from './../../../../functions/common/auth/loginAs';
import logout from './../../../../functions/common/auth/logout';
import openNotifications from './../../../../functions/intern/topbar/mainMenu/openNotifications';

describe('when logging in as behandelaar', ( ) => {

	beforeAll(( ) => {

		logout();
		loginAs('behandelaar');

	});

	describe('and open the notifications', ( ) => {

		beforeAll(( ) => {

			openNotifications();

		});

		it('should not have a new account notification present', ( ) => {

			expect($('[icon-type="account-star-variant"]').isPresent()).toBe(false);

		});

	});

	afterAll(( ) => {

		logout();
		login();

	});

});

describe('when logging in as gebruikersbeheerder', ( ) => {

	beforeAll(( ) => {

		logout();
		loginAs('gebruikersbeheerder');

	});

	describe('and open the notifications', ( ) => {

		beforeAll(( ) => {

			openNotifications();

		});

		it('should have a new account notification present', ( ) => {
			
			expect($('[icon-type="account-star-variant"]').isPresent()).toBe(true);
			
		});

	});

	afterAll(( ) => {

		logout();
		login();

	});

});
