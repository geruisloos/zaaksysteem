import createContact from './../../../../functions/intern/plusActions/newContact/createContact';

describe('when opening the form to register a contact', ( ) => {

	afterEach(( ) => {

		browser.ignoreSynchronization = false;
		
		browser.get('');

	});

	describe('and registering a citizen with a residence address', ( ) => {

		let newContact = {
			type: 'natuurlijk_persoon'
		};
	
		beforeAll(( ) => {
	
			createContact(newContact);

			browser.ignoreSynchronization = true;

			browser.sleep(1000);

		});
	
		it('there should be a snack with a link', ( ) => {

			expect($('.snack-action a').isPresent()).toBe(true);
		
		});

	});

	describe('and registering a citizen with a correspondence address', ( ) => {

		let newContact = {
			type: 'natuurlijk_persoon',
			correspondenceAddress: 'yes'
		};
	
		beforeAll(( ) => {
	
			createContact(newContact);

			browser.ignoreSynchronization = true;

			browser.sleep(1000);
	
		});
	
		it('there should be a snack with a link', ( ) => {
	
			expect($('.snack-action a').isPresent()).toBe(true);
	
		});
	
	});

	describe('and registering a citizen with a foreign address', ( ) => {

		let newContact = {
			type: 'natuurlijk_persoon',
			country: 'Marokko'
		};
	
		beforeAll(( ) => {
	
			createContact(newContact);

			browser.ignoreSynchronization = true;

			browser.sleep(1000);
	
		});
	
		it('there should be a snack with a link', ( ) => {
	
			expect($('.snack-action a').isPresent()).toBe(true);
	
		});
	
	});

	describe('and registering an organisation with an establishment address', ( ) => {

		let newContact = {
			type: 'bedrijf'
		};
	
		beforeAll(( ) => {
	
			createContact(newContact);

			browser.ignoreSynchronization = true;

			browser.sleep(1000);
	
		});
	
		it('there should be a snack with a link', ( ) => {
	
			expect($('.snack-action a').isPresent()).toBe(true);
	
		});
	
	});

	describe('and registering an organisation with a foreign address', ( ) => {

		let newContact = {
			type: 'bedrijf',
			country: 'Marokko'
		};
	
		beforeAll(( ) => {
	
			createContact(newContact);

			browser.ignoreSynchronization = true;

			browser.sleep(1000);
	
		});
	
		it('there should be a snack with a link', ( ) => {
	
			expect($('.snack-action a').isPresent()).toBe(true);
	
		});
	
	});

});
