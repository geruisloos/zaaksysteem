import toggleActionMenu from './../../../../../functions/intern/topbar/actions/toggleActionMenu';
import getCaseUrl from './../../../../../functions/common/getValue/getCaseUrl';
import openCaseView from './../../../../../functions/intern/caseView/openCaseView';
import closeDialog from './../../../../../functions/common/closeDialog';
import openAdditionalInformation from './../../../../../functions/intern/caseView/additionalInformation/openAdditionalInformation';
import caseRelate from './../../../../../functions/intern/topbar/actions/actionsCase/caseRelate';
import caseStall from './../../../../../functions/intern/topbar/actions/actionsCase/caseStall';
import caseResume from './../../../../../functions/intern/topbar/actions/actionsCase/caseResume';
import caseAssignGroup from './../../../../../functions/intern/topbar/actions/actionsCase/caseAssignGroup';
import caseDuplicate from './../../../../../functions/intern/topbar/actions/actionsCase/caseDuplicate';
import caseSetTargetDate from './../../../../../functions/intern/topbar/actions/actionsCase/caseSetTargetDate';
import caseCloseEarly from './../../../../../functions/intern/topbar/actions/actionsCase/caseCloseEarly';
import caseUpdateStatus from './../../../../../functions/intern/topbar/actions/actionsCase/caseUpdateStatus';
import getAboutValue from './../../../../../functions/intern/caseView/additionalInformation/getAboutValue';

describe('when opening case 26', ( ) => {

	beforeAll(( ) => {

		browser.get(getCaseUrl('26'));

	});

	describe('when relating case 27 to the case', ( ) => {

		let	caseToRelateTo = '27';

		beforeAll(( ) => {

			caseRelate(caseToRelateTo);

			openCaseView('relations');

		});

		it('the case should have 27 as related case', ( ) => {

			expect($(`.related-cases [href="/intern/zaak/${caseToRelateTo}"]`).isPresent()).toBe(true);

		});

		afterAll(( ) => {

			openCaseView('phase');

		});

	});

	describe('when stalling the case', ( ) => {

		beforeAll(( ) => {

			caseStall('determinate', 'Kalenderdagen', '2');

		});

		it('the case should have a stalled notifcation', ( ) => {

			expect($('[data-name="stalled"]').isPresent()).toBe(true);

		});

		afterAll(( ) => {

			toggleActionMenu();

			caseResume();

			toggleActionMenu();

		});

	});

	describe('when resuming the case', ( ) => {

		beforeAll(( ) => {

			caseStall('determinate', 'Kalenderdagen', '2');

			toggleActionMenu();

			caseResume();

		});

		it('the case should not have a stalled notifcation', ( ) => {

			expect($('[data-name="stalled"]').isPresent()).toBe(false);

		});

		afterAll(( ) => {

			toggleActionMenu();

		});

	});

	describe('when assigning the case to a group', ( ) => {

		beforeAll(( ) => {

			caseAssignGroup('-Zaakacties', 'Behandelaar');

		});

		it('the user should be redirected to the dashboard', ( ) => {

			expect($('.gridster-desktop').isPresent()).toBe(true);

		});

		describe('and when opening the case again', ( ) => {

			beforeAll(( ) => {

				browser.get(getCaseUrl('26'));

				openAdditionalInformation();

			});

			it('the department of the case should have changed to that of the group', ( ) => {

				expect(getAboutValue('Afdeling')).toEqual('Zaakacties');

			});

			afterAll(( ) => {

				closeDialog();

			});

		});

	});

	describe('when closing the case early', ( ) => {

		beforeAll(( ) => {

			caseCloseEarly('aangehouden');

		});

		it('the case should have a resolved notification', ( ) => {

			expect($('.case-message-list [data-name="resolved"]').isPresent()).toBe(true);

		});

		afterAll(( ) => {

			caseUpdateStatus('In behandeling');

			toggleActionMenu();

		});

	});

	describe('when duplicating the case', ( ) => {

		beforeAll(( ) => {

			caseDuplicate();

			browser.ignoreSynchronization = true;

			openCaseView('timeline');

			browser.sleep(5000);

		});

		it('the case should have a duplicate message in the timeline', ( ) => {

			expect($('[data-event-type="case/duplicate"]').isPresent()).toBe(true);

		});

		afterAll(( ) => {

			browser.ignoreSynchronization = false;

			browser.get(getCaseUrl('26'));

		});

	});

	describe('when setting a new target date for the case', ( ) => {

		beforeAll(( ) => {

			caseSetTargetDate('13', 'Jan', '2010');

			openAdditionalInformation();

		});

		it('the target date of the case should have changed to the new target date', ( ) => {

			expect(getAboutValue('Streefafhandeldatum')).toEqual('13-01-2010');

		});

		afterAll(( ) => {

			closeDialog();

		});

	});

	afterAll(( ) => {

		browser.waitForAngular();

		browser.get('');

	});

});
