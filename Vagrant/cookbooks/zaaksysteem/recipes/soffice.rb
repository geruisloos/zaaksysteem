#
# Cookbook Name:: zaaksysteem
# Recipe:: soffice
#
# Copyright 2013, Example Com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

apt_package "openoffice.org" do
    action :install
end

cookbook_file "/etc/init/openoffice-headless.conf" do
    source "openoffice-headless.conf"
    mode 00600
    owner "root"
    group "root"
end

service "openoffice-headless" do
    provider Chef::Provider::Service::Upstart
    supports :status => true, :restart => true
    action [ :enable, :start ]
end

apt_package "tomcat7" do
    action :install
end

apt_package "jodconverter" do
    action :install
end

execute "copy_converter" do
    command "cp /vagrant/contrib/converter.war /var/lib/tomcat7/webapps"
end
