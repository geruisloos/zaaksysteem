#
# Cookbook Name:: zaaksysteem
# Recipe:: database
#
# Copyright 2013, Example Com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

include_recipe "zaaksysteem::ubuntu"
include_recipe "zaaksysteem::clamav"

apt_package "curl" do
    action :install
end

apt_package "git" do
    action :install
end

execute "add_my_apt_key" do
        command "curl http://packages.public.zaaksysteem.nl/mintlab-pubkey.txt | /usr/bin/apt-key add -"
        user "root"
        notifies :run, resources(:execute => "apt-get-update"), :immediately
end

apt_repository "mintlab" do
    uri "http://packages.public.zaaksysteem.nl/ubuntu-precise/"
    arch "amd64"
    components ["precise","main"]
end

File.open("/vagrant/MANIFEST.Debian", "r") do |file_handle|
    file_handle.each_line do |server|
        if server !~ /^\s*$/ && server !~ /^\s*#/
            apt_package server.chomp do
                not_if { node['zaaksysteem']['package'] }
                action :upgrade
            end
        end
    end
end

bash "install_zs_test_dependencies" do
    user "root"
    code <<-EOS
        cpanm Test::Harness
        cpanm Mock::Quick Test::Class::Moose Import::Into Test::Fatal
        cpanm Authen::SASL Email::Sender IO::Socket::SSL
        cpanm Term::ReadPassword::Win32
    EOS
end

bash "install_zs_cpanm_dependencies" do
    user "root"
    code <<-EOS
        cpanm Number::Phone
    EOS
end

include_recipe "zaaksysteem::zs-queue"

apt_package "libzaaksysteem-perl" do
    only_if { node['zaaksysteem']['package'] }
    action :install
end

cookbook_file "/etc/init/zaaksysteem.conf" do
    source "zaaksysteem.conf"
    mode 00700
    owner "root"
    group "root"
end

# remote_file requires a a higher version of chef
# or a higher version of something that is used by chef
# coz it breaks horrible with file:// and not with http://
%w{
    log4perl.conf
    log4testsuite.conf
    zaaksysteem-test.conf
    zaaksysteem.conf
}.each do |file|
    path        = "/vagrant/etc/" + file
    dist_path   = path + ".dist"
    #remote_path = "file://" + dist_path

    bash path + "_file_copy" do
        user "vagrant"
        cwd "/vagrant"
        code "cp #{dist_path} #{path}"
        not_if do ::File.exists?(path) end;
    end

    #remote_file file do
    #    path path
    #    source remote_path
    #    mode 0644
    #    action :create_if_missing
    #end
end

path        = "/vagrant/etc/customer.d/vagrant.conf"
dist_path   = "/vagrant/etc/vagrant_default.conf.dist"
#remote_path = "file://" + dist_path

bash path + "_file_copy" do
    user "vagrant"
    cwd "/vagrant"
    code "cp #{dist_path} #{path}"
    not_if do ::File.exists?(path) end;
end

#remote_file 'vagrant.conf' do
#    path '/vagrant/etc/customer.d/vagrant.conf'
#    source 'file:///vagrant/etc/vagrant_default.conf.dist'
#    mode 0644
#    action :create_if_missing
#end

directory "/usr/share/ca-certificates/extra" do
    mode 00755
    owner "root"
end

execute "extra-root-certificates" do
    command <<-EOF
    cp /vagrant/etc/caroots/*.crt /usr/share/ca-certificates/extra;
    ls /usr/share/ca-certificates/extra | xargs -I {} -n1 echo extra/{} >> /etc/ca-certificates.conf;
    update-ca-certificates
    EOF
    user "root"
end

basedir = "/var/tmp/zs"

directory basedir do
    mode 00750
    owner "vagrant"
    recursive true
end

%w{storage tmp}.each do |d|
    d = File.join(basedir, d)
    directory d do
        mode 00750
        owner "vagrant"
        recursive true
    end
end


include_recipe "zaaksysteem::webserver";
include_recipe "zaaksysteem::database";
include_recipe "zaaksysteem::soffice";
include_recipe "zaaksysteem::development";
include_recipe "zaaksysteem::postfix";
include_recipe "zaaksysteem::clientutils";

