name                "dev"
description         "Role applied to all development"
default_attributes(
    "domain"    => "zaaksysteem.nl",
    "ntp" => {
        "servers" => [
            '0.nl.pool.ntp.org',
            '1.nl.pool.ntp.org',
            '2.nl.pool.ntp.org',
            '3.nl.pool.ntp.org'
        ]
    },
    "zaaksysteem" => {
        "database"  => {
            "config"  => {
                "port"  => 5432
            },
            "username"  => {
                "postgres"    => "postgres"
            },
            "password"  => {
                "postgres"    => "zaaksysteem123"
            }
        },
        "queue" => {
            "repository" => "https://bitbucket.org/mintlab/zaaksysteem-queue.git"
        }
    },
    "postgresql"    => {
        "password"  => {
            "postgres"  => "md56ebaded8c26d261f86ffc8a1dcbb252e"
        }
    },
    "openldap"      => {
        "rootpw"        => "{SSHA}136xvdSzL6IOPNXVACRl3RFf1vKj7Wcy",
        "rootpwplain"   => "zaaksysteem123",
        "basedn"        => "dc=zaaksysteem,dc=nl",
        "slapd_type"    => "master",
    }
)
