#!/bin/bash

TID=`echo "select uuid, processor_params from transaction order by id desc limit 1" |psql -t zaaksysteem_sprint_dev | grep _create_file_from_template | perl -pe 's/^\s+([\d\w-]+)\s+.*/$1/'`;
CID=`echo "select uuid, processor_params from transaction order by id desc limit 1" |psql -t zaaksysteem_sprint_dev | grep _create_file_from_template | perl -pe 's/.*"case":(\d+).*/$1/'`;

# echo $INPUT;
CASEUUID=`echo "select uuid from zaak where id = $CID" | psql -t zaaksysteem_sprint_dev|sed -e 's/\s//'`
curl -k --form "upload=@test.docx" "https://10.44.0.11/api/v1/sysin/interface/5a756f2d-65f8-446f-8426-3e18288e90b1/trigger/api_post_file?transaction_uuid=$TID&case_uuid=$CASEUUID"
