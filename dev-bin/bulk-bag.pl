#!/usr/bin/perl
use warnings;
use strict;

use FindBin qw/$Bin/;
use lib "$Bin/../lib";

our $VERSION = 1_0;

use autodie;
use Data::Dumper;
use File::Basename;
use File::Spec::Functions qw(catfile);

use Zaaksysteem::CLI;
use Zaaksysteem::Tools;

my $cli = Zaaksysteem::CLI->init;

my $options = $cli->options;

my $interface = $cli->schema->resultset('Interface')
    ->search_active({ module => 'bagcsv' })->first;

die "No BAG CSV interface found" if !$interface;

$cli->do_transaction(
    sub {
        print "Processing $options->{file}\n";
        my $file = basename($options->{file});
        my $fs   = $cli->schema->resultset('Filestore')->filestore_create(
            {
                file_path     => $options->{file},
                original_name => $file,
            }
        );

        my $t = $interface->process(
            {
                input_filestore_uuid    => $fs->uuid,
                external_transaction_id => "ZS::CLI: " . DateTime->now->iso8601,
            }
        );

        printf("'%s' inserted %d BAG records\n",
            $options->{file}, $t->transaction_records->count);

        if (!$t->success_count) {
            warn "Unsuccesful run\n";
            my $rs = $t->transaction_records->search_rs({});
            while (my $r = $rs->next) {
                local $Data::Dumper::Maxdepth = 2;
                warn $r->output;
            }
        }
    }
);

1;

__END__

=head1 NAME

bulk-bag.pl - A bulk bag importer

=head1 SYNOPSIS

bulk-ztb.pl OPTIONS

=head1 OPTIONS

=over

=item * directory

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
