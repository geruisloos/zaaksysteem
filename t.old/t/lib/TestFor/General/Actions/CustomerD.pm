package Zaaksysteem::General::Action::CustomerD;
use base qw(ZSTest);

use TestSetup;
use Zaaksysteem::General::Actions;

sub load_config : Tests {

    $zs->txn_ok(
        sub {

            no warnings qw(redefine once);
            local *MockModel::_check_digest = sub { return "foo" };
            use warnings;

            Zaaksysteem::General::Actions::load_customer_d_configs($zs->c);

            my $customers = $zs->c->customer;
            is(keys $customers, 3, "Got three instances");

            # If we die, we want to keep our previous instances
            no warnings qw(redefine once);
            local *Zaaksysteem::Config::customers
                = sub { die "A horrible death" };
            use warnings;

            Zaaksysteem::General::Actions::load_customer_d_configs($zs->c);
            $customers = $zs->c->customer;
            is(keys $customers, 3, "Got three instances");
        }
    );

}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
