package TestFor::General::Case::Delete;
use base qw(Test::Class);
use TestSetup;

sub zs_case_delete : Tests {

    $zs->zs_transaction_ok(
        sub {
            my $username = $zs->set_current_user->username;
            my $case = $zs->create_case_ok();
            $case->set_deleted(no_checks => 1);

            my $log = $zs->schema->resultset('Logging')->search({ zaak_id => $case->id})->first;

            is_deeply($log->data,
                { case_id => $case->id, acceptee_name => $username });

            $case = $zs->create_case_ok();
            $case->set_deleted(process => "Foo", no_checks => 1);

            $log = $zs->schema->resultset('Logging')->search({ zaak_id => $case->id})->first;
            is_deeply($log->data,
                { case_id => $case->id, acceptee_name => 'Foo' });

        },
        "Case->set_deleted"
    );

}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
