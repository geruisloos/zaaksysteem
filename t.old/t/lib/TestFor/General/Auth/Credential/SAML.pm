package TestFor::General::Auth::Credential::SAML;

# ./zs_prove -v t/lib/TestFor/General/Auth/Credential/SAML.pm
use base 'ZSTest';

use TestSetup;

use Zaaksysteem::Auth::Credential::SAML;

sub test_auth_credential_saml : Tests {

SKIP: {
    skip 'AUTUMN2015BREAK: auth/saml/interface_config: Configuration does not allow employee logins using this IdP',3;
    ### There is a check, please look at it closely: it looks like it does absolutely not what you want..
    ### throw('[user not allowed]') unless $self->interface->jpath('$.login_type_user') which is the opposide...
    ### I have no idea how this COULD work...

    my $self = shift;
    my $mock_interface = Test::MockObject->new();
    $mock_interface->set_isa('Zaaksysteem::Model::DB::Interface');
    $mock_interface->mock(id => sub { return 'identifier' });

    {
        $self->{find_user} = [];
        $self->{update_properties_from_saml} = [];
        $self->{log_warn} = [];

        # Realm with user
        my $mock_realm     = $self->_mock_realm_with_user(active => 1);

        my $auth = Zaaksysteem::Auth::Credential::SAML->new();
        $auth->interface($mock_interface);

        my $authinfo = { uid => 'some_uid' };
        my $result = $auth->authenticate(undef, $mock_realm, $authinfo);

        isa_ok($result, 'MockUser', "Authentication result is a user object");
        is_deeply(
            $self->{find_user},
            [
                [
                    {
                        username => 'some_uid',      # See $authinfo
                        source   => 'identifier',    # See $mock_interface
                    },
                    undef,
                ],
            ],
            'find_user was called, with the correct arguments',
        );

        is_deeply(
            $self->{update_properties_from_saml},
            [
                [
                    $authinfo,
                    { interface_id => 'identifier' },
                ],
            ],
            'update_properties_from_saml was called correctly',
        );
    }

    {
        $self->{find_user} = [];
        $self->{update_properties_from_saml} = [];
        $self->{log_warn} = [];

        # Realm with inactive user
        my $mock_realm = $self->_mock_realm_with_user(active => 0);
        my $mock_context = $self->_mock_context();

        my $auth = Zaaksysteem::Auth::Credential::SAML->new();
        $auth->interface($mock_interface);

        my $authinfo = { uid => 'some_uid' };
        my $result = $auth->authenticate($mock_context, $mock_realm, $authinfo);
        isnt($result, "Non-active users can't authenticate");

        is_deeply(
            $self->{find_user},
            [
                [
                    {
                        username => 'some_uid',      # See $authinfo
                        source   => 'identifier',    # See $mock_interface
                    },
                    $mock_context,
                ],
            ],
            'find_user was called, with the correct arguments',
        );
        is_deeply(
            $self->{update_properties_from_saml},
            [
                [
                    $authinfo,
                    { interface_id => 'identifier' },
                ],
            ],
            'update_properties_from_saml was called correctly',
        );
        is_deeply(
            $self->{log_warn},
            [
                [ "User marked as deleted" ],
            ],
            "A warning was logged"
        );
    }

    {
        $self->{find_user} = [];
        # Realm with user-not-found
        my $mock_realm = $self->_mock_realm_without_user();

        my $mock_model = Test::MockObject->new();
        $mock_model->mock(
            'create_from_saml' => sub {
                my $s = shift;

                push @{$self->{create_from_saml}}, [@_];

                return "foobar";
            }
        );
        my $mock_context = Test::MockObject->new();
        $mock_context->mock(
            'model' => sub {
                my $s = shift;

                push @{$self->{model}}, [@_];

                return $mock_model;
            }
        );

        my $auth = Zaaksysteem::Auth::Credential::SAML->new();
        $auth->interface($mock_interface);

        my $authinfo = { uid => 'some_uid' };
        my $result = $auth->authenticate($mock_context, $mock_realm, $authinfo);

        is($result, "foobar", "When creating a new user, the create_from_saml result is returned");
        is_deeply(
            $self->{find_user},
            [
                [
                    {
                        username => 'some_uid',      # See $authinfo
                        source   => 'identifier',    # See $mock_interface
                    },
                    $mock_context,
                ],
            ],
            'find_user was called, with the correct arguments',
        );
        is_deeply(
            $self->{model},
            [ ["DB::Subject"] ],
            "Correct model retrieved (DB::Subject)",
        );
        is_deeply(
            $self->{create_from_saml},
            [
                [
                    {
                        interface_id => 'identifier',
                        saml_data    => $authinfo
                    },
                ],
            ],
            "DB::Subject->create_from_saml called with correct arguments"
        );
    }

}; # END SKIP

}

sub _mock_realm_without_user {
    my $self = shift;
    my %args = @_;

    my $mock_realm = Test::MockObject->new();
    $mock_realm->mock(
        'find_user' => sub {
            my $s = shift;

            push @{ $self->{find_user} }, [@_];

            return;
        }
    );

    return $mock_realm;
}

sub _mock_realm_with_user {
    my $self = shift;
    my %args = @_;

    my $mock_user = Test::MockObject->new();
    $mock_user->set_isa('MockUser');
    $mock_user->mock(
        update_properties_from_saml => sub {
            my $s = shift;
            push @{ $self->{update_properties_from_saml} }, [@_];

            return;
        }
    );
    $mock_user->mock( is_active => sub { return $args{active} } );

    my $mock_realm = Test::MockObject->new();
    $mock_realm->mock(
        'find_user' => sub {
            my $s = shift;

            push @{ $self->{find_user} }, [@_];

            return $mock_user;
        }
    );

    return $mock_realm;
}

sub _mock_context {
    my $self = shift;

    my $log = Test::MockObject->new();
    $log->mock(
        'warn' => sub {
            my $s = shift;

            push @{ $self->{log_warn} }, [@_];

            return;
        }
    );

    my $c = Test::MockObject->new();
    $c->mock( 'log' => sub { return $log; } );

    return $c;
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
