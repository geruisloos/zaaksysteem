package TestFor::General::Constants;
use base 'Test::Class';

use TestSetup;
use Zaaksysteem::Constants;

sub test_numeric_filter : Tests {
    # The filter expects (but never uses) a database schema as a first
    # argument. So use this instead for cleaner-looking test code.
    my $dummy = bless({}, 'Zaaksysteem::Constants');

    is(
        $dummy->_numeric_fix_filter("123"),
        123,
        "Integers stay the same",
    );
    is(
        $dummy->_numeric_fix_filter("123.45"),
        "123.45",
        "Numbers with decimals stay the same",
    );
    is(
        $dummy->_numeric_fix_filter("123,45"),
        "123.45",
        "Numbers with decimals using ',' get fixed to use '.' instead",
    );
    is(
        $dummy->_numeric_fix_filter("123.456"),
        "123456",
        "Numbers with 3 digits after '.' are assumed to use '.' as a thousands separator",
    );
    is(
        $dummy->_numeric_fix_filter("100.000"),
        "100000",
        "Numbers with 3 digits after '.' are assumed to use '.' as a thousands separator",
    );
    is(
        $dummy->_numeric_fix_filter("123,456"),
        "123456",
        "Numbers with 3 digits after ',' are assumed to use ',' as a thousands separator",
    );
    is(
        $dummy->_numeric_fix_filter("123,456,789"),
        "123456789",
        "Numbers with 3 digits after ',' are assumed to use ',' as a thousands separator",
    );
    is(
        $dummy->_numeric_fix_filter("123,456,789.0"),
        "123456789.0",
        "Numbers with both thousands separators and decimal point",
    );
    is(
        $dummy->_numeric_fix_filter("EUR 123,456,789.0"),
        "123456789.0",
        "Disallowed characters are stripped.",
    );
    is(
        $dummy->_numeric_fix_filter("12.5,30.2"),
        "12530.2",
        "Insane number-like things are handled in the least insane way possible.",
    );
    is(
        $dummy->_numeric_fix_filter("0"),
        "0",
        "Zero isn't flattened into nothingness/undefinedness",
    );

    is(
        $dummy->_numeric_fix_filter("."),
        undef,
        "A non-numeric string with only a '.' results in undef"
    );

    is(
        $dummy->_numeric_fix_filter("garbage"),
        undef,
        "A garbage non-numeric string *does* result in undef"
    );
}

sub test_magic_string_suggestion : Tests {
    is(
        BETROKKENE_RELATEREN_MAGIC_STRING_SUGGESTION->([], "foo", "bar"),
        "foo",
        "Basic magic string with no 'in use' names"
    );
    is(
        BETROKKENE_RELATEREN_MAGIC_STRING_SUGGESTION->([], "foo bar baz", "bar"),
        "foobarbaz",
        "Basic magic string with no 'in use' names, disallowed character space"
    );
    is(
        BETROKKENE_RELATEREN_MAGIC_STRING_SUGGESTION->([], "foo%", "bar"),
        "foo",
        "Basic magic string with no 'in use' names, disallowed character"
    );
    is(
        BETROKKENE_RELATEREN_MAGIC_STRING_SUGGESTION->(["foo"], "foo", "bar"),
        "foo1",
        "Basic magic string with 'in use' name, no disallowed character"
    );
    is(
        BETROKKENE_RELATEREN_MAGIC_STRING_SUGGESTION->(["foo"], "foo%", "bar"),
        "foo1",
        "Basic magic string with 'in use' name, disallowed character"
    );
    is(
        BETROKKENE_RELATEREN_MAGIC_STRING_SUGGESTION->(["bar"], "foo", "bar"),
        "foo",
        "Basic magic string with different 'in use' name, disallowed character"
    );
    is(
        BETROKKENE_RELATEREN_MAGIC_STRING_SUGGESTION->([], "", "bar"),
        "bar",
        "Basic magic string with no suggestion, role, no disallowed character"
    );
    is(
        BETROKKENE_RELATEREN_MAGIC_STRING_SUGGESTION->(["bar"], "", "bar"),
        "bar1",
        "Basic magic string with 'in use' name, no suggestion, role, no disallowed character"
    );
    is(
        BETROKKENE_RELATEREN_MAGIC_STRING_SUGGESTION->(["foo"], "", "bar"),
        "bar",
        "Basic magic string with different 'in use' name, no suggestion, role, no disallowed character"
    );
    is(
        BETROKKENE_RELATEREN_MAGIC_STRING_SUGGESTION->([], "", "bar%"),
        "bar",
        "Basic magic string with no suggestion, role, disallowed character"
    );
    is(
        BETROKKENE_RELATEREN_MAGIC_STRING_SUGGESTION->([], "ontvanger", ""),
        "ontvanger",
        "Basic magic string with just a role"
    );
    is(
        BETROKKENE_RELATEREN_MAGIC_STRING_SUGGESTION->([], "", "ontvanger"),
        "ontvanger",
        "Basic magic string with suggestion"
    );
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
