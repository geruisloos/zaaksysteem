package TestFor::General::Tools::Term;
use base qw(Test::Class);

use TestSetup;
use Zaaksysteem::Backend::Tools::Term qw/calculate_term/;
use Zaaksysteem::Backend::Tools::WorkingDays qw/add_working_days diff_working_days/;

sub calculate_term_test : Tests {
    throws_ok(sub {
        calculate_term();
    }, qr/unable to assert profile/, "Fails when called with arguments");

    my $thursday = DateTime->new(year => 2014, month => 4, day => 17);
    my $friday   = DateTime->new(year => 2014, month => 4, day => 18);
    my $monday   = DateTime->new(year => 2014, month => 4, day => 21);
    my $tuesday  = DateTime->new(year => 2014, month => 4, day => 22);

    # werkdagen
    my $result = calculate_term({ start => $thursday, type => 'werkdagen', amount => 1 });
    is $result, $friday, "1 workday after a thursday yields the friday after it";

    $result = calculate_term({ start => $thursday, type => 'werkdagen', amount => 2 });
    is $result, $tuesday, "2 workdays after a thursday yields the tuesday after it - when monday happens to be easter";


    # kalenderdagen
    $result = calculate_term({ start => $thursday, type => 'kalenderdagen', amount => 1 });
    is $result, $friday, "1 day after a thursday yields friday";

    $result = calculate_term({ start => $thursday, type => 'kalenderdagen', amount => 4 });
    is $result, $monday, "4 days after a thursday yields monday";

    $result = calculate_term({ start => $thursday, type => 'kalenderdagen', amount => 5 });
    is $result, $tuesday, "5 days after a thursday yields monday";

    # weken
    $result = calculate_term({ start => $thursday, type => 'weken', amount => 1 });
    is $result, $thursday->add(weeks => 1), "1 week after a thursday yields thursday next week";

    $result = calculate_term({ start => $thursday, type => 'weken', amount => 52 });
    is $result, $thursday->add(weeks => 52), "52 weeks after a thursday works";

    # einddatum
    $thursday = DateTime->new(year => 2014, month => 4, day => 17);
    $result = calculate_term({ start => $thursday, type => 'einddatum', amount => '17-04-2014' });
    is $result, $thursday, "Fixed enddate works";
}

sub working_days_test : Tests {
    my $datetime = DateTime->new({year => 2013, month => 12, day => 10});

    my $result = add_working_days({ datetime => $datetime, working_days => 0 });
    is $result->strftime('%F'), '2013-12-10', 'Add zero days, receive the same answer';

    $result = add_working_days({ datetime => $datetime, working_days => 1 });
    is $result->strftime('%F'), '2013-12-11', 'Add one day, advance one day';

    $result = add_working_days({ datetime => $datetime, working_days => -20 });
    is $result->strftime('%F'), '2013-11-12', 'Subtract 20 days, receive consistent answer';

    # 876 days should cover it
    $result = add_working_days({ datetime => $datetime, working_days => 876 });
    is $result->strftime('%F'), '2017-05-24', 'Add 876 days, receive consistent answer';
}

sub diff_working_days_tests : Tests {
    my $datetime1 = DateTime->new({year => 2015, month => 6, day => 1});
    my $datetime2 = DateTime->new({year => 2015, month => 6, day => 3});

    my $result = diff_working_days({ date1 => $datetime1, date2 => $datetime2 });
    is $result, 2, "Two days from june 1 -> june 3 (2015)";

    # "Old" end is during the weekend
    $datetime2 = DateTime->new({year => 2015, month => 6, day => 6});
    $result = diff_working_days({ date1 => $datetime1, date2 => $datetime2 });
    is $result, 4, "4 working days from june 1 -> june 6 (2015; end-in-weekend)";

    $datetime2 = DateTime->new({year => 2015, month => 6, day => 8});
    $result = diff_working_days({ date1 => $datetime1, date2 => $datetime2 });
    is $result, 5, "5 work days from june 1 -> june 8 (2015)";

    $datetime2 = DateTime->new({year => 2015, month => 6, day => 10});
    $result = diff_working_days({ date1 => $datetime1, date2 => $datetime2 });
    is $result, 7, "7 work days from june 1 -> june 10 (2015)";

    $result = diff_working_days({ date1 => $datetime2, date2 => $datetime1 });
    is $result, -7, "-7 work days from june 10 -> june 1 (2015)";

    # Easter
    $datetime1 = DateTime->new({year => 2015, month => 4, day => 3});
    $datetime2 = DateTime->new({year => 2015, month => 4, day => 8});
    $result = diff_working_days({ date1 => $datetime1, date2 => $datetime2 });
    is $result, 2, "2 work days from april 4 -> april 8 (2015; easter)";
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

