package TestFor::General::Backend::Rules::Rule::Condition::SetValue;
use base qw(ZSTest);

use TestSetup;
use File::Spec::Functions qw(catfile);

use Zaaksysteem::Backend::Rules;
use JSON qw/decode_json encode_json/;


=head1 NAME

TestFor::General::Backend::Rules::Rule::Condition::SetValue - Rule condition: SetValue

=head1 SYNOPSIS

    ./zs_prove -v t/lib/TestFor/General/Backend/Rules/Rule/Condition/SetValue.pm

=head1 CODE TESTS

Code tests, testing the implementation itself

=head2 rules_rule_condition_setvalue_result

=cut

sub rules_rule_condition_setvalue_result : Tests {
    my $self            = shift;

    $zs->zs_transaction_ok(sub {
        my ($casetype, $zaaktype_status) = $self->_generate_rules_for_setvalue;

        my $case     = $zs->create_case_ok(zaaktype => $casetype);

        my $engine   = Zaaksysteem::Backend::Rules->new_from_case(
            $case,
        );

        is (@{ $engine->rules }, 2, 'Only two rules, because third has zero actions');

        ### Second condition, with undef values, should be validated true
        is($engine->rules->[0]->then->[0]->attribute_name, 'case.result', 'Engine translates case_result into case.result');
        is($engine->rules->[0]->then->[0]->value, 'verwerkt', 'case.result is stringified entry');
        is($engine->rules->[1]->then->[0]->value, 'aangekocht', 'case.result is stringified entry');
    }, 'condition::area: checked set_value for case.result');
}

sub _generate_rules_for_setvalue {
    my $self            = shift;
    my (%opts)          = @_;

    my $zaaktype_node   = $zs->create_zaaktype_node_ok;
    my $casetype        = $zs->create_zaaktype_ok(node => $zaaktype_node);
    my $zaaktype_status = $zs->create_zaaktype_status_ok(
        status => 1,
        fase   => 'registratiefase',
        node   => $zaaktype_node
    );
    my $afhandelstatus  = $zs->create_zaaktype_status_ok(
        status => 3,
        fase   => 'afhandelfase',
        node   => $zaaktype_node
    );

    $zs->create_zaaktype_resultaat_ok(
        status      => $afhandelstatus,
        resultaat   => 'aangekocht',
    );

    $zs->create_zaaktype_resultaat_ok(
        status      => $afhandelstatus,
        resultaat   => 'verwerkt',
    );


    ### Generate zaaktype_kenmerken, checkboxes
    my $zt_kenmerk = $zs->create_zaaktype_kenmerk_ok(
        status              => $zaaktype_status,
        bibliotheek_kenmerk => $zs->create_bibliotheek_kenmerk_ok(
            naam            => 'checkbox_wijken',
            magic_string    => 'checkbox_wijken',
            value_type      => 'checkbox',
            values          => [
                {
                    value   => 'Noord',
                    active  => 1,
                },
                {
                    value   => 'Oost',
                    active  => 1,
                },
                {
                    value   => 'Zuid',
                    active  => 1,
                },
                {
                    value   => 'West',
                    active  => 1,
                },
                {
                    value   => 'Geen',
                    active  => 1,
                }
            ]
        )
    );

    ### Generate zaaktype_kenmerken, checkboxes
    my $bag_adres = $zs->create_zaaktype_kenmerk_ok(
        status              => $zaaktype_status,
        bibliotheek_kenmerk => $zs->create_bibliotheek_kenmerk_ok(
            naam            => 'bag_adres',
            magic_string    => 'bag_adres',
            value_type      => 'bag_adres',
        )
    );


    $zs->create_zaaktype_regel_ok(
        status  => $zaaktype_status,
        # node => $zaaktype_node,
        naam    => 'Rule check: vul_waarde_in',
        settings => {
            'voorwaarde_1_kenmerk'          => $zt_kenmerk->bibliotheek_kenmerken_id->id,
            'voorwaarde_1_value'            => 'Zuid',
            'voorwaarde_1_value_checkbox'   => '1',
            'voorwaarden'                   => '1',


            'actie_1'                       => 'vul_waarde_in',
            'actie_1_kenmerk'               => 'case_result',
            'actie_1_value'                 => '2',
            'acties'                        => '1',

            'naam'                          => 'Vul waarde in',
        }
    );

    $zs->create_zaaktype_regel_ok(
        status  => $zaaktype_status,
        # node => $zaaktype_node,
        naam    => 'Rule check: vul_waarde_in',
        settings => {
            'voorwaarde_1_kenmerk'          => $zt_kenmerk->bibliotheek_kenmerken_id->id,
            'voorwaarde_1_value'            => 'West',
            'voorwaarde_1_value_checkbox'   => '1',
            'voorwaarden'                   => '1',


            'actie_1'                       => 'vul_waarde_in',
            'actie_1_kenmerk'               => 'case_result',
            'actie_1_value'                 => '1',
            'acties'                        => '1',

            'naam'                          => 'Vul waarde in',
        }
    );


    $zs->create_zaaktype_regel_ok(
        status  => $zaaktype_status,
        # node => $zaaktype_node,
        naam    => 'Rule check: vul_waarde_in',
        settings => {
            'voorwaarde_1_kenmerk'          => $zt_kenmerk->bibliotheek_kenmerken_id->id,
            'voorwaarde_1_value'            => 'Oost',
            'voorwaarde_1_value_checkbox'   => '1',
            'voorwaarden'                   => '1',


            'actie_1'                       => 'vul_waarde_in',
            'actie_1_kenmerk'               => 'case_result',
            'actie_1_value'                 => '3',
            'acties'                        => '1',

            'naam'                          => 'Vul waarde in',
        }
    );

    return ($casetype, $zaaktype_status);
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
