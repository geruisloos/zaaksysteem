package Test::DummyProfile;

use Moose;

use Zaaksysteem::Tools;

define_profile basic_wrapped_args => (
    wrap => 1,
    required => {
        foo => 'Str'
    }
);

sub basic_wrapped_args { @_ }

define_profile custom_wrapped_args => (
    wrap => sub { $_[1]->{ data }, @_ },
    required => {
        bar => 'Int'
    }
);

sub custom_wrapped_args { @_ }

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
