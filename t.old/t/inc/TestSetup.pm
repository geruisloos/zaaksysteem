package TestSetup;

use warnings;
use strict;
use feature 'state';

# This variable and BEGIN block are here to make overriding of perl built-ins
# work in *every* module loaded afterwards.
my $time_offset;

BEGIN {
    *CORE::GLOBAL::time = sub {
        return $time_offset // CORE::time;
    };
    *CORE::GLOBAL::gmtime = sub (;$) {
        my $stamp = shift // $time_offset // CORE::time;
        CORE::gmtime($stamp);
    };
    *CORE::GLOBAL::localtime = sub (;$) {
        my $stamp = shift // $time_offset // CORE::time;
        CORE::localtime($stamp);
    };
}

use autodie qw(:all);
use Config::Auto;
use DBI;
use File::Temp qw(tempfile);
use File::Spec::Functions;
use File::Slurp;
use JSON;
use Log::Log4perl qw(:easy);
use LWP::UserAgent;
use POSIX qw(strftime);
use Params::Check qw(check);
use Test::Builder::Module;
use Test::Deep;
use Test::Exception;
use Test::More;
use Test::NoWarnings ();
use Test::MockObject;
use Zaaksysteem::Model::DB;
use Zaaksysteem::TestUtils;
use Zaaksysteem::Database;

use base 'Test::Builder::Module';
my $tb = __PACKAGE__->builder;

Log::Log4perl::init('etc/log4testsuite.conf');

# 8378 = "TEST" on a phone keyboard
my $TEST_HOST = "127.0.0.1";
my $TEST_PORT = 8378;
my $TEST_URL = "http://$TEST_HOST:$TEST_PORT/";

our @EXPORT = (
    qw(
        connect_test_db_ok
        remove_test_db_ok
        zs_done_testing

        start_test_server_ok
        ensure_test_server_ok
        stop_test_server_ok

        read_test_config
        parse_zapi_response_ok

        STUF_TEST_XML_PATH

        zs_configfile_ok
        initialize_test_globals_ok
        with_stopped_clock

        $zs
        $schema
    ),
    @Test::More::EXPORT,
    @Test::Deep::EXPORT,
    @Test::Exception::EXPORT,
    @Test::NoWarnings::EXPORT,
);
use constant STUF_TEST_XML_PATH => 't.old/t/inc/API/StUF';

# Connect to the test database, create one if no mytest is found.
sub connect_test_db_ok {
    my %credentials;
    if (-e '.mytest') {
        %credentials = _read_mytest();
    }
    else {
        %credentials = _create_mytest();
        zs_configfile_ok();
    }

    use Zaaksysteem::Config;
    use File::Spec::Functions;

    my $ZS = Zaaksysteem::Config->new(
        zs_customer_d => catdir('etc', 'customer.d'),
        zs_conf       => catfile('etc','zaaksysteem.conf'),
    );

    my $schema = $ZS->get_customer_schema('testsuite', 1);
    $schema->log(Log::Log4perl->get_logger(ref $schema));

    # Create a mandatory interface
    $schema->resultset('Interface')->update_or_create(
        {
            name             => 'LDAP Authenticatie',
            module           => 'authldap',
            active           => 1,
            max_retries      => 10,
            multiple         => 1,
            interface_config => '{}',
        }
    );

    my %required_config = (
        document_intake_user    => 'intake',
        file_username_seperator => '-',
        jodconverter_url        => 'http://localhost:8080/converter/service',
    );
    foreach my $t (keys %required_config) {
        my $x = $schema->resultset('Config')->update_or_create({
            parameter => $t,
            value     => $required_config{$t},
        });
        $tb->ok($x, "Added $t to config");
    }


    return $schema;
}

sub _read_mytest {
    my $config = Config::Auto::parse('./.mytest');

    my $args = check(
        {
            dsn         => {required => 1},
            db_user     => {required => 0},
            db_password => {required => 0},
        },
        $config,
    );


    return %$args;
}

sub _create_mytest {
    my $args = read_test_config() or die('Could not read test_config');

    my $unix_username = getpwuid($<);
    my $dbname = strftime("zaaksysteem_test_${unix_username}_%Y%m%d_%H%M%S", localtime);

    my @mytest;

    my $dsn = sprintf(
        'dbi:Pg:dbname=%s%s%s',
        $dbname,
        $args->{db_host} ? ";host=" . $args->{db_host} : "",
        $args->{port} ? ";port=" . $args->{port} : ""
    );
    push(@mytest, "dsn = $dsn");

    foreach (qw(db_user db_password)) {
        push(@mytest, "$_ = $args->{$_}") if exists $args->{$_};
    }

    my $db = Zaaksysteem::Database->new(
        username => $args->{db_user},
        password => $args->{db_password},
        host     => $args->{db_host},
        port     => $args->{port},
    );

    {
        local $SIG{__WARN__} = sub {
            my $warning = shift;
            if ($warning !~ /WARNING:  => is deprecated as an operator name/) {
                fail("TestSetup found $warning");
            }
        };
        $db->create(
            name     => $dbname,
            sql      => scalar read_file(catfile(qw(db template.sql))),
        );
    }

    open my $mytest, '>', '.mytest';
    print $mytest join("\n", @mytest, "\n");
    close $mytest;

    return _read_mytest();
}

# Set the test-plan automatically and include test-lib(s)
sub import {
    warnings->import();
    strict->import();
    Test::NoWarnings->import();
    goto &Test::Builder::Module::import;
}

# Clean up the test database. Should really only be called by 999-cleanup.t
sub remove_test_db_ok {

    my $mytest = '.mytest';

    if (!-f $mytest) {
        $tb->ok(1, "Test database already removed");
        return;
    }

    my $args = eval {
        read_test_config();
    };
    if ($@) {die("Could not read test_config: $@") }

    my %mytest = _read_mytest();
    my $db = Zaaksysteem::Database->new(
        username => $args->{db_user},
        password => $args->{db_password},
        host     => $args->{db_host},
        port     => $args->{port},
    );

    my ($dbname) = $mytest{dsn} =~ /dbname=([^;]+)/;
    $db->delete(name => $dbname);
    unlink($mytest);

    $tb->ok(1, "Removed database $mytest{dsn} and .mytest");
    return;
}

=head2 start_test_server_ok

Start a "test" zaaksysteem server for controller tests.

=cut

sub start_test_server_ok {
    my $test_msg = "";
    if (-e '.mycatalyst') {
        $tb->BAIL_OUT("Running test server found. Aborted a previous test run? Run t/998-kill-test_server.t");
    }
    else {
        my $pid = _start_test_server($TEST_HOST, $TEST_PORT);
        $test_msg = "New test server started, pid = $pid";
    }
    $ENV{CATALYST_SERVER} = $TEST_URL;

    $tb->ok(1, $test_msg);
    return;
}

=head2 zs_configfile_ok

Generates a proper configfile for the testsuite and drops it in the etc/customer.d directory
This is used for Catalyst testing.

=head3 SYNOPSIS

    zs_configfile_ok();

=head3 ARGUMENTS

None

=head3 RETURNS

None

=cut

sub zs_configfile_ok {
    my $filename = 'etc/customer.d/testsuite.conf';
    open my $zs_config, ">", $filename;

    my %credentials = _read_mytest();

    open (my $template, "<", "etc/testsuite.customer.d.conf");
    while (my $line = <$template>) {
        # Template substitution
        $line =~ s/\[%\s+dsn\s+%\]/$credentials{dsn}/g;
        if (defined $credentials{db_user}) {
            $line =~ s/\[%\s+dbuser\s+%\]/$credentials{db_user}/g;
        }
        else {
            $line =~ s/.*\[%\s+dbuser\s+%\].*\n//g;
        }
        if (defined $credentials{db_password}) {
            $line =~ s/\[%\s+dbpass\s+%\]/$credentials{db_password}/g;
        }
        else {
            $line =~ s/.*\[%\s+dbpass\s+%\].*\n//g;
        }
        print $zs_config $line;
    }
}

sub _start_test_server {
    my ($host, $port) = @_;

    pipe my $reader, my $writer;
    my $pid = fork();
    if ($pid) {
        close $writer;

        my $got_accepting = 0;
        while (my $line = readline($reader)) {
            if ($line =~ /Accepting connections/) {
                close $reader;
                $got_accepting = 1;
                last;
            }
        }

        if (!$got_accepting && !$ENV{ZS_CATALYST_TRACE}) {
            $tb->BAIL_OUT(
                "Zaaksysteem test server died before starting to accept connections (hint: netstat -ltp)"
            );
        }
    }
    else {
        close STDIN;
        close STDOUT;
        close $reader;

        unless ($ENV{ZS_CATALYST_TRACE}) {
            open(STDERR, ">&=" . fileno($writer)) or die $!;
        }

        zs_configfile_ok();
        my @execlist = (
            "./script/zaaksysteem_server.pl",
            "--host"    => $host,
            "--port"    => $port,
            "--pidfile" => ".mycatalyst",
        );
        exec(@execlist) or die $!;
    }

    return $pid;
}

=head2 ensure_test_server_ok

Ensure the test server is still running, and set the environment variable
required to make L<Catalyst::Test> work.

=cut

sub ensure_test_server_ok {
    return;
    if (!-e '.mycatalyst') {
        $tb->BAIL_OUT("Test server not found.");
    }

    my $ua = LWP::UserAgent->new(timeout => 15);
    my $res = $ua->get($TEST_URL);
    if (!$res->is_success) {
        $tb->BAIL_OUT("Test server running, but GET / doesn't work.");
    }

    $ENV{CATALYST_SERVER} = $TEST_URL;
    $tb->ok(1, "Test server found");
    return;
}

=head2 stop_test_server_ok

Stop the "test" catalyst server, started by ensure_test_server_ok.

Should probably only be run once, at the end of the test run (starting a new
test server takes a long time).

=cut

sub stop_test_server_ok {
    open(my $fh, "<", ".mycatalyst");

    my $pid = readline($fh);
    chomp $pid;

    kill(TERM => $pid);

    close($fh);
    unlink(".mycatalyst");

    $tb->ok(1, "Test server stopped (pid = $pid)");
    return;
}

=head2 parse_zapi_response_ok

Check if the response looks like a ZAPI response, return the JSON data structure.

=cut

sub parse_zapi_response_ok {
    my ($response, $failure) = @_;

    my $ct = 'application/json';
    my $msg = 'Looks like a ZAPI reponse';

    if (!$failure && !$response->is_success) {
        $tb->diag("Not a success");
        $tb->ok(0, $msg);
        return {};
    }

    my $ok = 1;
    $ok = 0 if ($response->content_type ne 'application/json');

    my $json = decode_json($response->content);

    my @expected_keys = qw(at comment next num_rows prev result rows status_code);
    $ok = 0 if keys %$json != @expected_keys;

    for my $expected_key (@expected_keys) {
        $ok = 0 unless exists $json->{ $expected_key };
    }

    $tb->ok($ok, "Looks like a ZAPI response");
    return $json;
}

=head2 zs_done_testing

Expand the default done_testing a bit with a static no warnings check instead
of relying on do_end_test (which only runs in some cases).

=cut

sub zs_done_testing {
    Test::NoWarnings::had_no_warnings();
    $Test::NoWarnings::do_end_test = 0;
    $tb->done_testing(@_);
}

sub read_test_config {

    my $obj = Config::Auto->new(source => catfile('/vagrant', 'etc', 'zaaksysteem-test.conf'));
    my $config = $obj->parse;

    return check(
        {
            db_host              => {required => 0},
            db_user              => {required => 0},
            db_password          => {required => 0},
            create_db_user       => {required => 0},
            create_db_password   => {required => 0},
            filestore_location   => {required => 1},
            filestore_test_file_path => {required => 1},
            filestore_test_file_name => {required => 1},
        },
        $config,
    );
}

sub _connect_template {
    my $args = shift;

    # Assumptions :)
    my $dsn = "dbi:Pg:dbname=template1";
    $dsn .= ";host=$args->{db_host}" if defined $args->{db_host};

    return DBI->connect(
        $dsn,
        $args->{create_db_user},
        $args->{create_db_password},
        {
            RaiseError => 1,
            PrintError => 0,
            AutoCommit => 1,
        },
    );
}

our $zs;
our $schema;

sub initialize_test_globals_ok {
    if ($zs) {
        $tb->ok(1, "Test framework already initialized");
        return;
    }

    $zs = Zaaksysteem::TestUtils->new();
    $zs->config;

    if (!$zs->schema->resultset('Config')->get_customer_config) {
        $tb->ok(1, "Creating correct config table");
        my %config = (
            naam             => "Zaaksysteem by Mintlab",
            naam_lang        => "Mintlab B.V.",
            naam_kort        => "Mintlab",
            woonplaats       => "Amsterdam",
            adres            => "Donker Curtiusstraat 7 - 521",
            straatnaam       => "Donker Curtiusstraat",
            huisnummer       => "7-521",
            postcode         => "1051 JL",
            postbus          => "",
            postbus_postcode => "",
            website          => "http://www.mintlab.nl/",
            email            => "servicedesk\@mintlab.nl",
            telefoonnummer   => "020 - 737 000 5",
            faxnummer        => "",
            zaak_email       => "servicedesk\@zaaksysteem.nl",
            latitude         => "52.378979",
            longitude        => "4.871620",
            gemeente_id_url  => "http://www.mintlab.nl/",
            gemeente_portal  => "http://www.mintlab.nl/",
        );
        foreach (keys %config) {
            $zs->schema->resultset('Config')->create(
                {
                    parameter => "customer_info_$_",
                    value     => $config{$_},
                    advanced  => 0,
                }
            );
        }
    }

    $schema = $zs->schema;

    $tb->ok(1, "Test framework initialized");
    return;
}

=head2 with_stopped_clock

Run a block with a stopped clock.

The clock will be stopped by overriding L<CORE::GLOBAL::time>,
L<CORE::GLOBAL::gmtime> and L<CORE::GLOBAL::localtime>. This will make
time stand still for the running code.

This can be useful if you're testing logging, which might include timestamps.

Example:

    with_stopped_clock {
        my $x = localtime;
        sleep 10;
        my $y = localtime;

        is($x, $y, "Time stood still");
    };

=cut

sub with_stopped_clock(&) {
    my ($code) = @_;

    $time_offset = time;
    $code->();
    undef $time_offset;

    return;
}

=head2 DISABLE_WARNING_STACKTRACE

Allows the use of warn for debugging without stacktrace.

=cut

if ($ENV{DISABLE_WARNING_STACKTRACE}) {
    no warnings 'redefine';
    *Test::NoWarnings::make_warning = sub {
        my $message = shift;
        print STDERR $message;
        my $warning = Test::MockObject->new;
        $warning->set_always('toString', '');
        return $warning;
    }
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

