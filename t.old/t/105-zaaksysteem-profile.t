#! perl

### Test header start
use warnings;
use strict;

use Test::More;
use Test::Exception;
### Test header end

use Zaaksysteem::Constants qw(SJABLONEN_TARGET_FORMATS);
use Zaaksysteem::Profile;

my $typed = {
    regexp  => 'Str',
    code    => 'Str',

    object_ok   => 'Zaaksysteem::Schema::Zaak',
    object_fail => 'Zaaksysteem::Schema::Zaak',

    # Moose types
    #any        => 'Any',
    #item       => 'Item',
    #undef      => 'Undef',
    #defined    => 'Defined',
    #value      => 'Value',
    str        => 'Str',
    #num        => 'Num',
    int        => 'Int',
    #class_name => 'ClassName',
    #role_name  => 'RoleName',
    #ref        => 'Ref',
    array_ref  => 'ArrayRef',
    hash_ref   => 'HashRef',
    scalar_ref => 'ScalarRef',
    code_ref   => 'CodeRef',
    regexp_ref => 'RegexpRef',
    #glob_ref   => 'GlobRef',
    #filehandle => 'FileHandle',
    #object     => 'Object',
};

define_profile test_zaaksysteem_profile => (
    optional           => [keys %$typed],
    constraint_methods => {
        regexp      => qr/^(?:pdf|odt)$/,
        code        => SJABLONEN_TARGET_FORMATS,
        object_fail => sub {return 0},
        object_ok   => sub {return 1},
    },
    typed => $typed,
);

sub test_zaaksysteem_profile {
    my $opts = {@_};
    $opts = assert_profile($opts)->valid;
    return $opts;
}

my %input = (
    regexp    => 'pdf',
    code      => 'pdf',
    str       => 'Str',
    int       => 1,
    object_ok => bless({}, 'Zaaksysteem::Schema::Zaak'),
    hash_ref  => {key => 'value'},
    code_ref  => sub {return 1},
);

my $result = test_zaaksysteem_profile(%input);
is_deeply($result, \%input, "Parsing is correct");

throws_ok(
    sub {
        test_zaaksysteem_profile(regexp => 'jpg');
    },
    qr/invalid: regexp/m,
    "invalid regexp"
);

throws_ok(
    sub {
        test_zaaksysteem_profile(code => 'jpg');
    },
    qr/invalid: code/m,
    "invalid code"
);


throws_ok(
    sub {
        test_zaaksysteem_profile(
            object_fail => bless({}, 'Zaaksysteem::Schema::Zaak'),);
    },
    qr/invalid: object_fail/m,
    "Valid object, invalid code"
);

### Everything below this line is: https://mintlab.atlassian.net/browse/ZS-1709
#TODO: {
#    local $TODO = "Must be supported and work out of the box";
#    %input = (
#        array_ref  => [qw(array ref)],
#        regexp_ref => qr/regexp/,
#    );
#    $result = test_zaaksysteem_profile(%input);
#    is_deeply($result, \%input, "Parsing is correct");
#}

TODO: {
    local $TODO = "This should break Zaaksysteem::Profile";

    throws_ok(
        sub {
            test_zaaksysteem_profile(string => [qw(an array)]);
        },
        qr/invalid: string/m,
        $TODO
    );

    throws_ok(
        sub {
            test_zaaksysteem_profile(integer => [2, 1, 0]);
        },
        qr/invalid: integer/m,
        $TODO
    );
}

done_testing();

