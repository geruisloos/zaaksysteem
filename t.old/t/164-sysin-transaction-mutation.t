#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
### Test header end

BEGIN {
    use_ok('Zaaksysteem::Backend::Sysin::Transaction::Mutation');
}

$zs->zs_transaction_ok(sub {
    my $mutation_record = Zaaksysteem::Backend::Sysin::Transaction::Mutation->new(
        create      => 1,
        table_id    => 55,
        table       => 'Zaak',
    );


    is(
        $mutation_record->create, 1, 'Correct update type set'
    );
    ok(
        !$mutation_record->update, 'Other update type not set'
    );
    ok(
        !$mutation_record->delete, 'Other update type not set'
    );


    is(
        $mutation_record->table_id, 55, 'Correct table id set'
    );

    is(
        $mutation_record->table, 'Zaak', 'Correct table set'
    );


    $mutation_record->add_mutation(
            {
                column      => 'registratiedatum',
                new_value   => '2013-05-22',
            }
    );
    is(
        $mutation_record->mutations->[0]->column,
        'registratiedatum',
        'Correct column set on mutation'
    );
    is(
        $mutation_record->mutations->[0]->new_value,
        '2013-05-22',
        'Correct value set on mutation'
    );

    $mutation_record->add_mutation(
            {
                column      => 'afhandeldatum',
                new_value   => '2013-05-28',
            }
    );

    $mutation_record->add_mutation(
            {
                column      => 'vernietigingsdatum',
                new_value   => '2013-05-30',
            }
    );

    is(
        @{ $mutation_record->mutations }, 3, 'Correct number of mutations'
    );

}, 'Mutation created');

$zs->zs_transaction_ok(sub {
    my $mutation_record = Zaaksysteem::Backend::Sysin::Transaction::Mutation->new(
        update      => 1,
        table_id    => 55,
        table       => 'Zaak',
    );

    is(
        $mutation_record->update, 1, 'Correct update type set'
    );
    ok(
        !$mutation_record->create, 'Other update type not set'
    );
    ok(
        !$mutation_record->delete, 'Other update type not set'
    );

    $mutation_record->add_mutation(
            {
                column      => 'registratiedatum',
                old_value   => '2013-05-21',
                new_value   => '2013-05-22',
            }
    );

    $mutation_record->add_mutation(
            {
                column      => 'afhandeldatum',
                old_value   => '2013-05-27',
                new_value   => '2013-05-28',
            }
    );

    is(
        $mutation_record->mutations->[1]->column,
        'afhandeldatum',
        'Correct column set on mutation'
    );
    is(
        $mutation_record->mutations->[1]->new_value,
        '2013-05-28',
        'Correct value set on mutation'
    );
    is(
        $mutation_record->mutations->[1]->old_value,
        '2013-05-27',
        'Correct value set on mutation'
    );

    $mutation_record->add_mutation(
            {
                column      => 'vernietigingsdatum',
                old_value   => '2013-05-39',
                new_value   => '2013-05-30',
            }
    );

    note(explain($mutation_record->TO_JSON));

}, 'Mutation updated');

$zs->zs_transaction_ok(sub {
    my $mutation_record = Zaaksysteem::Backend::Sysin::Transaction::Mutation->new(
        delete      => 1,
        table_id    => 55,
        table       => 'Zaak',
    );

    is(
        $mutation_record->delete, 1, 'Correct update type set'
    );
    ok(
        !$mutation_record->create, 'Other update type not set'
    );
    ok(
        !$mutation_record->update, 'Other update type not set'
    );

}, 'Mutation deleted');


zs_done_testing();
