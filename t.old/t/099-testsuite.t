#! perl

# This prevents classes inheriting ZSTest to run twice...better idea welcome.
BEGIN {
    $ENV{'Test::Class::Load'} = 1;
};

use TestSetup;

initialize_test_globals_ok;

use Test::Class::Load 't/lib/TestFor/Testsuite';

Test::Class->runtests();
