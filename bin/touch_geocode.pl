#!/usr/bin/perl -w
use strict;
use warnings;

use Cwd 'realpath';
use FindBin;
use lib "$FindBin::Bin/../lib";

use Zaaksysteem::CLI;

my $cli = Zaaksysteem::CLI->init;
my %opts = %{ $cli->options };

if ($opts{nummeraanduiding}) {
    my $rs = $cli->schema->resultset('BagNummeraanduiding')->search({ gps_lat_lon => undef }, {order_by => { -desc => 'me.id'}});
    load_all_from_rs($rs);
}

if ($opts{openbareruimte}) {
    my $rs = $cli->schema->resultset('BagOpenbareruimte')->search({ gps_lat_lon => undef }, {order_by => { -desc => 'me.id'}});
    load_all_from_rs($rs);
}


sub load_all_from_rs {
    my ($rs, $params) = @_;
    $params //= {};
    $params->{skip_update} //= $cli->dry // 0;

    if ($rs->count == 0) {
        print "Nothing found\n";
        return;
    }

    while(my $bag = $rs->next) {
        $cli->do_transaction(
            sub {
                my ($self, $schema) = @_;
                $bag->load_gps_coordinates($params);
            }
        );
    }
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
