#!/usr/bin/perl
use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../lib";

use Zaaksysteem::CLI::NatuurlijkPersoon::Searchable;

my $cli = Zaaksysteem::CLI::NatuurlijkPersoon::Searchable->init;

$cli->run;

1;

__END__

=head1 NAME

deduplicate_contacts.pl - Move all cases to "authenticated" versions of the requestor.

=head1 SYNOPSIS

deduplicate_contacts.pl OPTIONS [ -o errors=errorlog_filename.csv ] ] [ -o output=output_filename.csv ]

=head1 OPTIONS

=over

=item * config

The Zaaksysteem configuration defaults to /etc/zaaksysteem/zaaksysteem.conf

=item * customer_d

The customer_d dir, defaults to the relative directory where zaaksysteem.conf is found.

=item * hostname

The hostname you want to touch cases for

=item * o

You can use this specify an option.

=back

=over 8

=item * errors

Write errors to this (CSV) log. Defaults to C<errors-yyyy-mm-dd.csv>.

This will list all contacts that don't have authenticated equivalents.

=item * output

Write output to this (CSV) log. Defaults to C<output-yyyy-mm-dd.csv>.

=back

=over

=item * no-dry

Run it and commit the changes. The default is NOT to commit any changes.

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
