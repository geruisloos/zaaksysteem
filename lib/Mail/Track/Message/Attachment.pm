package Mail::Track::Message::Attachment;
use Moose;

use File::Basename;

=head1 NAME

Mail::Track::Attachment - Mail message object

=head1 SYNOPSIS

    my $attachment = $mail_message->attachments->[0];

    my $attachments = $mailer->attachments;
    my $string_body = $mailer->body_as_string;


=head1 DESCRIPTION

Returned attachment object for L<Mail::Track::Message>,
please DO NOT USE THIS OBJECT DIRECTLY. Use L<Mail::Track>

=head1 ATTRIBUTES

=head2 filename

isa: Str

Returns the filename for this attachment. This is the public filenamen, not
the filename on our local disk.

=cut

has filename => (
    is      => 'rw',
    isa     => 'Str',
    lazy    => 1,
    default => sub { return '' },
);

=head2 filename_without_extension

isa: Str

Returns the basename, e.g. the filename without the extension, of this attachment

=cut

has filename_without_extension => (
    is      => 'ro',
    isa     => 'Str',
    lazy    => 1,
    default => sub {
        my ($filename_without_extension, $extension)
            = shift->filename =~ m|(.*?)\.(\w+)$|;
        return $filename_without_extension;
    },
);

=head2 extension

isa: Str

Returns the extension from the filename, e.g.: C<txt>

=cut

has extension => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        my ($filename_without_extension, $extension)
            = shift->filename =~ m|(.*?)\.(\w+)$|;
        return $extension;
    },
    isa => 'Str',
);

=head2 path

isa: Str

Returns the path on the filesystem for this attachment

=cut

has path => (
    is      => 'ro',
    isa     => 'Str',
    lazy    => 1,
    default => sub { return '' },
);

has mimetype => (
    is       => 'ro',
    required => 0,
    lazy     => 1,
    isa      => 'Str',
    default  => sub { return '' },
);

=head2 entity

isa: MIME::Message

Result object of the attachment part

=cut

has 'entity' => (
    'is'  => 'ro',
    'isa' => 'MIME::Entity',
);

around 'BUILDARGS' => sub {
    my $method = shift;
    my $class  = shift;
    my (%opts) = @_;

    my $opts_from_entity
        = $class->_get_object_attributes_from_entity($opts{entity});

    $class->$method(%{$opts_from_entity});
};

sub _get_object_attributes_from_entity {
    my $class  = shift;
    my $entity = shift;
    my %returned_opts;

    unless (UNIVERSAL::isa($entity, 'MIME::Entity')) {
        die('Cannot create attachment object without entity object');
    }

    my $path = $entity->bodyhandle->path;

    $returned_opts{path}   = $path;
    $returned_opts{entity} = $entity;
    if ($entity->head->recommended_filename) {
        $returned_opts{filename} = $entity->head->recommended_filename;
    }
    else {
        $returned_opts{filename} = basename($path);
    }
    $returned_opts{mimetype} = $entity->mime_type;

    return \%returned_opts;
}

1;

__END__

=head1 EXAMPLES

See L<#SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
