package Zaaksysteem::Schema::ZaaktypeNode;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'Zaaksysteem::Result';


=head1 NAME

Zaaksysteem::Schema::ZaaktypeNode

=cut

__PACKAGE__->table("zaaktype_node");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'zaaktype_node_id_seq'

=head2 zaaktype_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 zaaktype_rt_queue

  data_type: 'text'
  is_nullable: 1

=head2 code

  data_type: 'text'
  is_nullable: 1

=head2 trigger

  data_type: 'text'
  is_nullable: 1

=head2 titel

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=head2 version

  data_type: 'integer'
  is_nullable: 1

=head2 active

  data_type: 'integer'
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 1

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1

=head2 deleted

  data_type: 'timestamp'
  is_nullable: 1

=head2 webform_toegang

  data_type: 'integer'
  is_nullable: 1

=head2 webform_authenticatie

  data_type: 'text'
  is_nullable: 1

=head2 adres_relatie

  data_type: 'text'
  is_nullable: 1

=head2 aanvrager_hergebruik

  data_type: 'integer'
  is_nullable: 1

=head2 automatisch_aanvragen

  data_type: 'integer'
  is_nullable: 1

=head2 automatisch_behandelen

  data_type: 'integer'
  is_nullable: 1

=head2 toewijzing_zaakintake

  data_type: 'integer'
  is_nullable: 1

=head2 toelichting

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=head2 online_betaling

  data_type: 'integer'
  is_nullable: 1

=head2 zaaktype_definitie_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 adres_andere_locatie

  data_type: 'integer'
  is_nullable: 1

=head2 adres_aanvrager

  data_type: 'integer'
  is_nullable: 1

=head2 bedrijfid_wijzigen

  data_type: 'integer'
  is_nullable: 1

=head2 zaaktype_vertrouwelijk

  data_type: 'integer'
  is_nullable: 1

=head2 zaaktype_trefwoorden

  data_type: 'text'
  is_nullable: 1

=head2 zaaktype_omschrijving

  data_type: 'text'
  is_nullable: 1

=head2 extra_relaties_in_aanvraag

  data_type: 'boolean'
  is_nullable: 1

=head2 properties

  data_type: 'text'
  default_value: '{}'
  is_nullable: 0

=head2 contact_info_intake

  data_type: 'boolean'
  default_value: true
  is_nullable: 1

=head2 is_public

  data_type: 'boolean'
  default_value: false
  is_nullable: 1

=head2 prevent_pip

  data_type: 'boolean'
  default_value: false
  is_nullable: 1

=head2 contact_info_email_required

  data_type: 'boolean'
  default_value: false
  is_nullable: 1

=head2 contact_info_phone_required

  data_type: 'boolean'
  default_value: false
  is_nullable: 1

=head2 contact_info_mobile_phone_required

  data_type: 'boolean'
  default_value: false
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "zaaktype_node_id_seq",
  },
  "zaaktype_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "zaaktype_rt_queue",
  { data_type => "text", is_nullable => 1 },
  "code",
  { data_type => "text", is_nullable => 1 },
  "trigger",
  { data_type => "text", is_nullable => 1 },
  "titel",
  { data_type => "varchar", is_nullable => 1, size => 128 },
  "version",
  { data_type => "integer", is_nullable => 1 },
  "active",
  { data_type => "integer", is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 1 },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1 },
  "deleted",
  { data_type => "timestamp", is_nullable => 1 },
  "webform_toegang",
  { data_type => "integer", is_nullable => 1 },
  "webform_authenticatie",
  { data_type => "text", is_nullable => 1 },
  "adres_relatie",
  { data_type => "text", is_nullable => 1 },
  "aanvrager_hergebruik",
  { data_type => "integer", is_nullable => 1 },
  "automatisch_aanvragen",
  { data_type => "integer", is_nullable => 1 },
  "automatisch_behandelen",
  { data_type => "integer", is_nullable => 1 },
  "toewijzing_zaakintake",
  { data_type => "integer", is_nullable => 1 },
  "toelichting",
  { data_type => "varchar", is_nullable => 1, size => 128 },
  "online_betaling",
  { data_type => "integer", is_nullable => 1 },
  "zaaktype_definitie_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "adres_andere_locatie",
  { data_type => "integer", is_nullable => 1 },
  "adres_aanvrager",
  { data_type => "integer", is_nullable => 1 },
  "bedrijfid_wijzigen",
  { data_type => "integer", is_nullable => 1 },
  "zaaktype_vertrouwelijk",
  { data_type => "integer", is_nullable => 1 },
  "zaaktype_trefwoorden",
  { data_type => "text", is_nullable => 1 },
  "zaaktype_omschrijving",
  { data_type => "text", is_nullable => 1 },
  "extra_relaties_in_aanvraag",
  { data_type => "boolean", is_nullable => 1 },
  "properties",
  { data_type => "text", default_value => "{}", is_nullable => 0 },
  "contact_info_intake",
  { data_type => "boolean", default_value => \"true", is_nullable => 1 },
  "is_public",
  { data_type => "boolean", default_value => \"false", is_nullable => 1 },
  "prevent_pip",
  { data_type => "boolean", default_value => \"false", is_nullable => 1 },
  "contact_info_email_required",
  { data_type => "boolean", default_value => \"false", is_nullable => 1 },
  "contact_info_phone_required",
  { data_type => "boolean", default_value => \"false", is_nullable => 1 },
  "contact_info_mobile_phone_required",
  { data_type => "boolean", default_value => \"false", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 zaaks

Type: has_many

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->has_many(
  "zaaks",
  "Zaaksysteem::Schema::Zaak",
  { "foreign.zaaktype_node_id" => "self.id" },
  {},
);

=head2 zaaktypes

Type: has_many

Related object: L<Zaaksysteem::Schema::Zaaktype>

=cut

__PACKAGE__->has_many(
  "zaaktypes",
  "Zaaksysteem::Schema::Zaaktype",
  { "foreign.zaaktype_node_id" => "self.id" },
  {},
);

=head2 zaaktype_authorisations

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaaktypeAuthorisation>

=cut

__PACKAGE__->has_many(
  "zaaktype_authorisations",
  "Zaaksysteem::Schema::ZaaktypeAuthorisation",
  { "foreign.zaaktype_node_id" => "self.id" },
  {},
);

=head2 zaaktype_betrokkenens

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaaktypeBetrokkenen>

=cut

__PACKAGE__->has_many(
  "zaaktype_betrokkenens",
  "Zaaksysteem::Schema::ZaaktypeBetrokkenen",
  { "foreign.zaaktype_node_id" => "self.id" },
  {},
);

=head2 zaaktype_kenmerkens

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaaktypeKenmerken>

=cut

__PACKAGE__->has_many(
  "zaaktype_kenmerkens",
  "Zaaksysteem::Schema::ZaaktypeKenmerken",
  { "foreign.zaaktype_node_id" => "self.id" },
  {},
);

=head2 zaaktype_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaaktype>

=cut

__PACKAGE__->belongs_to(
  "zaaktype_id",
  "Zaaksysteem::Schema::Zaaktype",
  { id => "zaaktype_id" },
);

=head2 zaaktype_definitie_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaaktypeDefinitie>

=cut

__PACKAGE__->belongs_to(
  "zaaktype_definitie_id",
  "Zaaksysteem::Schema::ZaaktypeDefinitie",
  { id => "zaaktype_definitie_id" },
);

=head2 zaaktype_notificaties

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaaktypeNotificatie>

=cut

__PACKAGE__->has_many(
  "zaaktype_notificaties",
  "Zaaksysteem::Schema::ZaaktypeNotificatie",
  { "foreign.zaaktype_node_id" => "self.id" },
  {},
);

=head2 zaaktype_regels

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaaktypeRegel>

=cut

__PACKAGE__->has_many(
  "zaaktype_regels",
  "Zaaksysteem::Schema::ZaaktypeRegel",
  { "foreign.zaaktype_node_id" => "self.id" },
  {},
);

=head2 zaaktype_relaties

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaaktypeRelatie>

=cut

__PACKAGE__->has_many(
  "zaaktype_relaties",
  "Zaaksysteem::Schema::ZaaktypeRelatie",
  { "foreign.zaaktype_node_id" => "self.id" },
  {},
);

=head2 zaaktype_resultatens

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaaktypeResultaten>

=cut

__PACKAGE__->has_many(
  "zaaktype_resultatens",
  "Zaaksysteem::Schema::ZaaktypeResultaten",
  { "foreign.zaaktype_node_id" => "self.id" },
  {},
);

=head2 zaaktype_sjablonens

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaaktypeSjablonen>

=cut

__PACKAGE__->has_many(
  "zaaktype_sjablonens",
  "Zaaksysteem::Schema::ZaaktypeSjablonen",
  { "foreign.zaaktype_node_id" => "self.id" },
  {},
);

=head2 zaaktype_standaard_betrokkenens

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaaktypeStandaardBetrokkenen>

=cut

__PACKAGE__->has_many(
  "zaaktype_standaard_betrokkenens",
  "Zaaksysteem::Schema::ZaaktypeStandaardBetrokkenen",
  { "foreign.zaaktype_node_id" => "self.id" },
  {},
);

=head2 zaaktype_statuses

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaaktypeStatus>

=cut

__PACKAGE__->has_many(
  "zaaktype_statuses",
  "Zaaksysteem::Schema::ZaaktypeStatus",
  { "foreign.zaaktype_node_id" => "self.id" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2016-10-06 09:39:08
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:kn3QazMA4l20LBjLo31lHQ

__PACKAGE__->resultset_class('Zaaksysteem::DB::ResultSet::ZaaktypeNode');

__PACKAGE__->load_components(
    "+Zaaksysteem::DB::Component::ZaaktypeNode",
    '+Zaaksysteem::Helper::ToJSON',
    __PACKAGE__->load_components()
);

__PACKAGE__->add_columns('last_modified',
    { %{ __PACKAGE__->column_info('last_modified') },
    set_on_update => 1,
    set_on_create => 1,
});

__PACKAGE__->add_columns('created',
    { %{ __PACKAGE__->column_info('created') },
    set_on_create => 1,
});


### Some relaties can be better
### --> Michiel, we are not interested in your personal life here.

__PACKAGE__->has_many(
  "zaaktype_statussen",
  "Zaaksysteem::Schema::ZaaktypeStatus",
  { "foreign.zaaktype_node_id" => "self.id" },
);
__PACKAGE__->has_many(
  "zaaktype_attributen",
  "Zaaksysteem::Schema::ZaaktypeAttributen",
  { "foreign.zaaktype_node_id" => "self.id" },
);
__PACKAGE__->has_many(
  "zaaktype_betrokkenen",
  "Zaaksysteem::Schema::ZaaktypeBetrokkenen",
  { "foreign.zaaktype_node_id" => "self.id" },
);
__PACKAGE__->has_many(
  "zaaktype_authorisaties",
  "Zaaksysteem::Schema::ZaaktypeAuthorisation",
  { "foreign.zaaktype_node_id" => "self.id" },
);
__PACKAGE__->has_many(
  "zaaktype_resultaten",
  "Zaaksysteem::Schema::ZaaktypeResultaten",
  { "foreign.zaaktype_node_id" => "self.id" },
);
__PACKAGE__->has_many(
  "zaaktype_sjablonen",
  "Zaaksysteem::Schema::ZaaktypeSjablonen",
  { "foreign.zaaktype_node_id" => "self.id" },
);
__PACKAGE__->has_many(
  "zaaktype_relaties",
  "Zaaksysteem::Schema::ZaaktypeRelatie",
  { "foreign.zaaktype_node_id" => "self.id" },
);
__PACKAGE__->has_many(
  "zaaktype_kenmerken",
  "Zaaksysteem::Schema::ZaaktypeKenmerken",
  { "foreign.zaaktype_node_id" => "self.id" },
);

use JSON;
use Data::Dumper;

__PACKAGE__->inflate_column('properties', {
    inflate => sub { from_json(shift) },
    deflate => sub { to_json(shift) },
});


# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

