package Zaaksysteem::Schema::ZaakBetrokkenen;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'Zaaksysteem::Result';


=head1 NAME

Zaaksysteem::Schema::ZaakBetrokkenen

=cut

__PACKAGE__->table("zaak_betrokkenen");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'zaak_betrokkenen_id_seq'

=head2 zaak_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 betrokkene_type

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=head2 betrokkene_id

  data_type: 'integer'
  is_nullable: 1

=head2 gegevens_magazijn_id

  data_type: 'integer'
  is_nullable: 1

=head2 verificatie

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=head2 naam

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 rol

  data_type: 'text'
  is_nullable: 1

=head2 magic_string_prefix

  data_type: 'text'
  is_nullable: 1

=head2 deleted

  data_type: 'timestamp'
  is_nullable: 1

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 1
  size: 16

=head2 pip_authorized

  data_type: 'boolean'
  default_value: false
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "zaak_betrokkenen_id_seq",
  },
  "zaak_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "betrokkene_type",
  { data_type => "varchar", is_nullable => 1, size => 128 },
  "betrokkene_id",
  { data_type => "integer", is_nullable => 1 },
  "gegevens_magazijn_id",
  { data_type => "integer", is_nullable => 1 },
  "verificatie",
  { data_type => "varchar", is_nullable => 1, size => 128 },
  "naam",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "rol",
  { data_type => "text", is_nullable => 1 },
  "magic_string_prefix",
  { data_type => "text", is_nullable => 1 },
  "deleted",
  { data_type => "timestamp", is_nullable => 1 },
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 1,
    size => 16,
  },
  "pip_authorized",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 zaak_behandelaars

Type: has_many

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->has_many(
  "zaak_behandelaars",
  "Zaaksysteem::Schema::Zaak",
  { "foreign.behandelaar" => "self.id" },
  {},
);

=head2 zaak_coordinators

Type: has_many

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->has_many(
  "zaak_coordinators",
  "Zaaksysteem::Schema::Zaak",
  { "foreign.coordinator" => "self.id" },
  {},
);

=head2 zaak_aanvragers

Type: has_many

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->has_many(
  "zaak_aanvragers",
  "Zaaksysteem::Schema::Zaak",
  { "foreign.aanvrager" => "self.id" },
  {},
);

=head2 zaak_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->belongs_to("zaak_id", "Zaaksysteem::Schema::Zaak", { id => "zaak_id" });


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2014-12-03 14:43:31
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:rTnHIKgawio+HqjU08OlxA

__PACKAGE__->load_components(
    "+Zaaksysteem::Zaken::ComponentZaakBetrokkenen",
    __PACKAGE__->load_components()
);

__PACKAGE__->resultset_class('Zaaksysteem::Zaken::ResultSetBetrokkenen');

__PACKAGE__->belongs_to(
    "natuurlijk_persoon", "Zaaksysteem::Schema::GmNatuurlijkPersoon", { id => "betrokkene_id" }, { is_foreign_key_constraint => 0},
);

__PACKAGE__->belongs_to(
    "bedrijf", "Zaaksysteem::Schema::GmBedrijf", { id => "betrokkene_id" }, { is_foreign_key_constraint => 0},
);




# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

