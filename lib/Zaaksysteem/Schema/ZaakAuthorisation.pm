package Zaaksysteem::Schema::ZaakAuthorisation;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'Zaaksysteem::Result';


=head1 NAME

Zaaksysteem::Schema::ZaakAuthorisation

=cut

__PACKAGE__->table("zaak_authorisation");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'zaak_authorisation_id_seq'

=head2 zaak_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 capability

  data_type: 'text'
  is_nullable: 0

=head2 entity_id

  data_type: 'text'
  is_nullable: 0

=head2 entity_type

  data_type: 'text'
  is_nullable: 0

=head2 scope

  data_type: 'text'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "zaak_authorisation_id_seq",
  },
  "zaak_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "capability",
  { data_type => "text", is_nullable => 0 },
  "entity_id",
  { data_type => "text", is_nullable => 0 },
  "entity_type",
  { data_type => "text", is_nullable => 0 },
  "scope",
  { data_type => "text", is_nullable => 0 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 zaak_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->belongs_to("zaak_id", "Zaaksysteem::Schema::Zaak", { id => "zaak_id" });


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2016-10-18 11:40:31
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:hbxqprtGe3ZQX9656TG9iw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
