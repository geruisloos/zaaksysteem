package Zaaksysteem::Schema::Message;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'Zaaksysteem::Result';


=head1 NAME

Zaaksysteem::Schema::Message

=cut

__PACKAGE__->table("message");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'message_id_seq'

=head2 message

  data_type: 'text'
  is_nullable: 0

=head2 subject_id

  data_type: 'text'
  is_nullable: 1
  original: {data_type => "varchar"}

=head2 logging_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 is_read

  data_type: 'boolean'
  default_value: false
  is_nullable: 1

=head2 is_archived

  data_type: 'boolean'
  default_value: false
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "message_id_seq",
  },
  "message",
  { data_type => "text", is_nullable => 0 },
  "subject_id",
  {
    data_type   => "text",
    is_nullable => 1,
    original    => { data_type => "varchar" },
  },
  "logging_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "is_read",
  { data_type => "boolean", default_value => \"false", is_nullable => 1 },
  "is_archived",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 logging_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Logging>

=cut

__PACKAGE__->belongs_to(
  "logging_id",
  "Zaaksysteem::Schema::Logging",
  { id => "logging_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2016-11-22 13:29:12
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:RF7FG+vqcBNoxvOa269HcQ

__PACKAGE__->belongs_to(
  "logging",
  "Zaaksysteem::Schema::Logging",
  { id => "logging_id" },
);

__PACKAGE__->resultset_class('Zaaksysteem::Backend::Message::ResultSet');

__PACKAGE__->load_components(qw/
    +Zaaksysteem::Backend::Message::Component
    +Zaaksysteem::Helper::ToJSON
/);

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

