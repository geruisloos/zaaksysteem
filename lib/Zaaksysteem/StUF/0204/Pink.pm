package Zaaksysteem::StUF::0204::Pink;

use Moose::Role;
use Zaaksysteem::Profile;
use JSON::Path;

=head1 NAME

Zaaksysteem::StUF::0301::Pink - PinkRoccade specifics

=head1 SYNOPSIS


=head1 DESCRIPTION

StUF 0204 Pink related calls for handling of Pink related calls: VOA's

Tested in: TestFor::General::StUF::0204::NatuurlijkPersoon

=head1 ATTRIBUTES

=head1 METHODS

=head2 set_pink_voa

Arguments: \%PARAMS, \%OPTIONS

Return value: $STRING_XML

    my $xml             = $self->xmlcompile->stuf0204->set_pink_voa(
        {
            a_nummer                => '1234567890',
            burgerservicenummer     => '987654321',
            voornamen               => 'Tinus',
            voorletters             => 'TV',
            voorvoegsel             => 'vander',
            geslachtsnaam           => 'Testpersoon',
            geboortedatum           => '19830609',
            datum_overlijden        => '19830610',
            indicatie_geheim        => undef,
            burgerlijkestaat        => undef,
            aanduiding_naamgebruik  => 'P',
        },
        {
            sender                  => 'ZSNL',
            receiver                => 'CMODIS',
            reference               => 'ZS0000229154',
            datetime                => '2014030209011458',
            mutation_type           => 'create',
            follow_subscription     => 1,
        },
    );

Generates the XML for a "voa message to pink".

=cut



define_profile 'set_pink_voa' => (
    required => [],
    require_some => {
        'bsn_or_anummer' => [1, qw/burgerservicenummer a_nummer/],
    },
    ### Below constraints are from official StUF XSD
    constraint_methods  => {
        burgerservicenummer => qr/^\d{8,9}$/,           ### Not from XSD: our own restriction
        a_nummer            => qr/^[1-9][0-9]{9}$/,
    },
);

sub set_pink_voa {
    my $self                = shift;
    my $params              = assert_profile(shift || {})->valid;
    my $options             = shift || {};

    my $perldata            = {};

    if ($params->{a_nummer}) {
        $perldata->{Anummer}    = $params->{a_nummer};
    } elsif ($params->{burgerservicenummer}) {
        $perldata->{BSN}        = $params->{burgerservicenummer};
    }

    return $self->handle_message(
        'pink_voa_kennisgeving',
        $perldata,
        {
            %{ $options },
            translate   => 'WRITER',
        }
    );
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Install>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
