package Zaaksysteem::StUF::Parser::0301;

use Moose;
use File::Spec::Functions qw(catfile);
use XML::LibXML;
use XML::Compile::WSDL11;      # use WSDL version 1.1
use XML::Compile::SOAP11;      # use SOAP version 1.1
use XML::Compile::Transport::SOAPHTTP;
use XML::Compile::Util;
use XML::Compile::Translate::Reader;
use XML::Tidy;
use Zaaksysteem::Exception;
use Zaaksysteem::XML::Compile;
use Zaaksysteem::Constants qw(STUF_XSD_PATH);

use Data::Dumper;

extends 'Zaaksysteem::StUF::Parser';

has 'stuf_version' => (
    is      => 'ro',
    default => '0301',
    isa     => 'Str',
);

has 'stuf_bg_version' => (
    is      => 'ro',
    default => '0310',
    isa     => 'Str',
);

has 'parameters'      => (
    is      => 'ro',
    isa     => 'HashRef',
);

has 'schema'    => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        return Zaaksysteem::XML::Compile->xml_compile->add_class('Zaaksysteem::StUF::0301::Instance')->stuf0301->schema;
    }
);

sub _parse_node_atribute {
    my $egem = shift;
    my $args = {@_};
    if ($args->{node}->hasAttributeNS($egem, $args->{atribute})) {
        $args->{value}{$args->{atribute}} = $args->{node}->getAttributeNS($egem, $args->{atribute});
    }
}

sub process {
    my $self = shift;

    if (!$self->xml) {
        throw('stuf/parser/parse_xml/no_xml', 'No XML given');
    }

    my $stufxml = $self->_get_body_as_element();

    my $elem    = pack_type $stufxml->namespaceURI, $stufxml->localName;

    my $reader = $self->schema->reader($elem);
    $self->namespace($stufxml->namespaceURI);
    $self->element($stufxml->localName);
    $self->data($reader->($stufxml));
}

sub to_xml {
    my $self        = shift;

    my $perldata    = {
        stuurgegevens   => $self->stuurgegevens,
    };

    $perldata->{body}   = $self->body if $self->body;

    my $elem            = pack_type $self->namespace, $self->element;

    my $writer          = $self->schema->writer($elem);
    my $doc             = XML::LibXML::Document->new('1.0', 'UTF-8');

    my $xmlelem         = $writer->($doc, $perldata);

    $doc->setDocumentElement($xmlelem);
    return $doc->toString(1);

    # return '<?xml version="1.0" encoding="utf-8"?>' . "\n"
    #     . $xmlelem->toString(1);
}

sub _reader_config {
    my $self = shift;
    #return {};

    my $egem = $self->egem;

    my $res  = {
        sloppy_integers                => 1,
        interpret_nillable_as_optional => 1,
        check_values                   => 0,
        'hooks'                        => [{
                after => sub {
                    my ($node, $value, $path) = @_;

                    my @find = qw(
                      sleutelGegevensbeheer
                      verwerkingssoort
                      sleutelVerzendend
                      sleutelOntvangend
                    );

                    foreach (@find) {
                        _parse_node_atribute(
                            $egem,
                            node     => $node,
                            atribute => $_,
                            value    => $value
                        );
                    }

                    if ($node->getAttributeNS($egem, 'noValue')) {
                        $value->{'noValue'} =
                          $node->getAttributeNS($egem, 'noValue');
                    }

                    return $value;
                  }
            }]
      };
      return $res;
}

__PACKAGE__->meta->make_immutable();



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 process

TODO: Fix the POD

=cut

=head2 to_xml

TODO: Fix the POD

=cut

