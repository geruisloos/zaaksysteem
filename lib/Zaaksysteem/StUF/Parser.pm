package Zaaksysteem::StUF::Parser;

use File::Spec::Functions qw(catfile);
use FindBin qw/$Bin/;
use Moose;
use XML::Compile::SOAP11;      # use SOAP version 1.1
use XML::Compile::Translate::Reader;
use XML::Compile::Transport::SOAPHTTP;
use XML::Compile::Util;
use XML::Compile::WSDL11;      # use WSDL version 1.1
use XML::LibXML;
use XML::Tidy;
use Zaaksysteem::Constants qw(STUF_XML_URL);
use Zaaksysteem::Exception;

has 'stuf_version' => (
    'is'    => 'ro',
    lazy    => 1,
    default => '0204',
);

has 'stuf_bg_version' => (
    'is'    => 'ro',
    lazy    => 1,
    default => '0204',
);

sub egem {
    my $self            = shift;

    return join('/', STUF_XML_URL, "StUF". ($self->stuf_version || '0204'));
}

sub _apply_vicrea_patches {
    my ($egem, $node, $value, $path)   = @_;

    if ($node->localName =~ /^PRSPRS.+$/) {
        my @children = $node->childNodes();

        for my $child (@children) {
            next unless (
                $child->localName &&
                $child->localName eq 'PRS'
            );

            my @prs_elements = $child->childNodes();

            for my $prs_element (@prs_elements) {
                if (
                    $prs_element->localName &&
                    !grep({ lc($prs_element->localName) eq lc($_) }
                        qw/
                            a-nummer
                            bsn-nummer
                            voornamen
                            voorletters
                            voorvoegselGeslachtsnaam
                            geslachtsnaam
                            geboortedatum
                            geboorteplaats
                            codeGeboorteland
                            geslachtsaanduiding
                            adellijkeTitelPredikaat
                        /
                    )
                ) {
                    $child->removeChild($prs_element);
                }
            }
        }
    }

    ### Vicrea Patch, add nillable when we get a noValue
    ### attribute
    if (
        $node->hasAttributeNS($egem, 'noValue')
        && !$node->hasAttributeNS(
            'http://www.w3.org/2001/XMLSchema-instance', 'nil'
        )
      ) {
        $node->setAttributeNS(
            'http://www.w3.org/2001/XMLSchema-instance',
            'nil',
            'true'
        );
    }
}

sub _reader_config {
    my $self = shift;

    my $egem = $self->egem;

    my $res  = {
        sloppy_integers                => 1,
        interpret_nillable_as_optional => 1,
        check_values                   => 0,
        'hooks'                        => [{
                before => sub {
                    my ($node, $value, $path) = @_;

                    ### StUF Patch, somehow we do not know how to parse PRSADRINS messages. Also,
                    ### we ignore the PRSADRINS information, so this information is not needed
                    ### for zaaksysteem. Therefore, we remove the PRSADRINS blocks to keep the sysin
                    ### from halting
                    if ($node->localName =~ /^PRS$/) {
                        my @children = $node->childNodes();
                        my $found    = {};
                        for my $child (@children) {
                            if (   $child->localName
                                && $child->localName =~ /^PRSADRINS$/) {
                                if ($found->{$child->nodeName}) {
                                    $node->removeChild($child);
                                }

                                $found->{$child->nodeName} = 1;
                            }
                        }
                    }

                    _apply_vicrea_patches($egem, @_);

                    return $node;
                },
                after => sub {
                    my ($node, $value, $path) = @_;

                    my @find = qw(
                      sleutelGegevensbeheer
                      verwerkingssoort
                      sleutelVerzendend
                      sleutelOntvangend
                    );

                    foreach (@find) {
                        _parse_node_atribute(
                            $egem,
                            node     => $node,
                            atribute => $_,
                            value    => $value
                        );
                    }

                    if ($node->getAttributeNS($egem, 'noValue')) {
                        $value->{'noValue'} =
                          $node->getAttributeNS($egem, 'noValue');
                    }

                    return $value;
                  }
            }]
      };

      return $res;
}

# TODO: Zaaksysteem::Profile
sub _parse_node_atribute {
    my $egem = shift;
    my $args = {@_};
    if ($args->{node}->hasAttributeNS($egem, $args->{atribute})) {
        $args->{value}{$args->{atribute}} = $args->{node}->getAttributeNS($egem, $args->{atribute});
    }
}

use constant WRITER_CONFIG  => {
    ignore_unused_tags => 1,
    'hooks' => [
        {
            before => sub {
                my ($doc, $value, $path) = @_;

                if (UNIVERSAL::isa($value, 'HASH')) {
                    for my $key (keys %{ $value }) {
                        # When no_value is true, the objects should not be discarded, but instead passed
                        # as nill value entries.
                        if (UNIVERSAL::isa($value->{$key}, 'Zaaksysteem::StUF::Body::Field')) {
                            if (!$value->{$key}->value && $value->{$key}->no_value) {
                                $value->{$key} = _get_nil_value_for_name($key);
                            }
                            else {
                                $value->{$key} = $value->{$key}->value;
                            }
                        }
                        # Same goes for relationships.
                        elsif (UNIVERSAL::isa($value->{$key}, 'Zaaksysteem::StUF::Body::Relationship') &&
                            $value->{$key}->no_value
                        ) {
                            my $rel = $value->{$key};
                            my @obj_params = $rel->{value}[0]->_object_params;

                            my $xml_nilled;
                            for my $o (@{ $obj_params[0] }) {
                                $xml_nilled->{$o->{name}} = _get_nil_value_for_name($o->{name});
                            }

                            # Clear out the non-XML-objects
                            delete $value->{$key};

                            ### FIX THIS -- How do we make {ADR} generic so it works for other relationships?
                            $value->{$key}{ADR} = $xml_nilled;
                        }
                    }
                }
                return $value;
            }
        },
    ],
};

has 'xml'       => (
    is      => 'rw',
);

has 'namespace'      => (
    is      => 'rw',
);

has 'element'       => (
    is      => 'rw',
);

has 'data'      => (
    is      => 'rw',
);

has 'home'      => (
    is      => 'rw',
    lazy    => 1,
    default => sub {
        return $Bin . '/..';
    }
);

has 'body'                  => (
    is      => 'rw',
);

has 'stuurgegevens'         => (
    is      => 'rw',
);

has 'berichtsoort'          => (
    is      => 'rw',
);

has 'cache'                 => (
    is      => 'rw',
    default => sub {{}; },
);

has 'reader_writer_config'    => (
    is      => 'ro',
    default => sub {
        my $self = shift;
        return {
            'READER'    => $self->_reader_config,
            'WRITER'    => WRITER_CONFIG,
        };
    }
);

has 'xml_definitions'   => (
    'is'    => 'ro',
    lazy    => 1,
    default => sub { return [] },
);

has 'schema'    => (
    is      => 'rw',
    lazy    => 1,
    default => sub {
        my $self    = shift;

        return $self->cache->{schema} if exists $self->cache->{schema};

        my $schema  = XML::Compile::Cache->new($self->xml_definitions);
        $schema->compileAll;
        return $self->cache->{schema} = $schema;
    }
);


sub _get_nil_value_for_name {
    my ($name) = @_;

    my $nil_value = XML::LibXML::Element->new($name);
    $nil_value->setAttribute('xsi:nil',      'true');
    $nil_value->setAttribute('StUF:noValue', 'geenWaarde');

    return $nil_value;
}

sub _get_body_as_element {
    my $self            = shift;
    my $dom             = XML::LibXML->load_xml(string => $self->xml);
    my ($soap_body)     = $dom->findnodes("//*[local-name()='Body']");

    my $elem;
    if ($soap_body) {
        ($elem)          = $soap_body->getChildrenByTagName('*');
    } else {
        ### Could be without Soap body....argh:
        for my $element ($self->schema->elements) {
            next if $elem;
            my ($ns, $localname) = unpack_type $element;

            next unless $ns eq 'http://www.egem.nl/StUF/sector/bg/' . $self->stuf_bg_version;

            ($elem)          = $dom->findnodes("//*[local-name()='" . $localname . "']");
        }
    }
    return $elem;
}

sub process {
    my $self = shift;

    if (!$self->xml) {
        throw('stuf/parser/parse_xml/no_xml', 'No XML given');
    }

    my $stufxml = $self->_get_body_as_element();

    my $elem = pack_type $stufxml->namespaceURI, $stufxml->localName;

    {
        $self->schema->{_saved_readers} ||= {};

        if (!$self->schema->{_saved_readers}->{$elem}) {
            $self->schema->declare('READER' => $elem, %{$self->_reader_config()});

            $self->schema->{_saved_readers}->{$elem} = 1;
        }
    }

    my $reader = $self->schema->reader($elem);
    $self->namespace($stufxml->namespaceURI);
    $self->element($stufxml->localName);
    $self->data($reader->($stufxml));
}

sub to_xml {
    my $self        = shift;

    my $perldata    = {
        stuurgegevens   => $self->stuurgegevens,
    };

    $perldata->{body}   = $self->body if $self->body;

    my $elem            = pack_type $self->namespace, $self->element;

    {
        local $SIG{__WARN__} = sub {}; # locally ignore any warnings
        $self->schema->declare('WRITER' => $elem, %{ WRITER_CONFIG() });
    }

    my $writer = $self->schema->writer($elem);
    my $doc = XML::LibXML::Document->new('1.0', 'UTF-8');

    my $xmlelem = $writer->($doc, $perldata);

    $doc->setDocumentElement($xmlelem);
    return $doc->toString(1);

    # return '<?xml version="1.0" encoding="utf-8"?>' . "\n"
    #     . $xmlelem->toString(1);
}


__PACKAGE__->meta->make_immutable();



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 STUF_XML_URL

TODO: Fix the POD

=cut

=head2 WRITER_CONFIG

TODO: Fix the POD

=cut

=head2 egem

TODO: Fix the POD

=cut

=head2 process

TODO: Fix the POD

=cut

=head2 to_xml

TODO: Fix the POD

=cut

