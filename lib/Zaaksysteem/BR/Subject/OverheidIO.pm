package Zaaksysteem::BR::Subject::OverheidIO;

use Moose::Role;

use Zaaksysteem::Tools;
use List::Util qw/first/;

=head1 NAME

Zaaksysteem::BR::Subject::OverheidIO - Bridge helpers for module Overheid.IO

=head1 DESCRIPTION

Applies logic for the L<Zaaksysteem::BR::Subject> bridge for inflating results from Overheid.IO-KVK-questions.

=head1 ATTRIBUTES

=head1 METHODS

=head2 search

See L<Zaaksysteem::BR::Subject#search> for usage information

=cut

around 'search' => sub {
    my $method      = shift;
    my $self        = shift;
    my ($params)    = @_;

    return $self->$method(@_) if !$self->_is_remote_openkvk($params);

    my $interface   = $self->_assert_openkvk_interface;

    my %cleanparams = map { my $ckey = $_; $ckey =~ s/^subject\.//; $ckey => $params->{$_} } keys %$params;
    delete($cleanparams{subject_type});

    my $transaction         = $interface->process_trigger('search_companies', \%cleanparams);
    my $result              = $transaction->get_processor_params->{result};
    $result = [] unless $result;

    ### We really want a new Iterator class which supportys arrays. Where we can call "next,first,search" etc
    ### on. For now, only allow list context
    if (!wantarray()) {
        throw('br/subject/search_remote', 'Error: remote searching requires list context');
    }

    my @objects;
    for my $company (@$result) {
        ### Skip without residence address
        next unless $company->{subject}->{address_residence}->{street};
        push(@objects, $self->object_from_params($company));
    }

    return @objects;
};


=head1 PRIVATE METHODS

=head2 _is_remote_openkvk

    $self->_is_remote_openkvk({ subject_type => 'company'});

    ### Returns 1 if $self->remote_search eq 'openkvk';

Returns true when searching is remotely and given subject_type equals person.

=cut

sub _is_remote_openkvk {
    my $self        = shift;
    my $params      = shift;

    return 1 if (
        $params->{subject_type} &&
        $params->{subject_type} eq 'company' &&
        $self->remote_search &&
        lc($self->remote_search) eq 'openkvk'
    );

    return;
}

=head2 _assert_openkvk_interface

    my $interface = $self->_assert_openkvk_interface;

Returns the active Overheid.IO interface from L<Zaaksysteem::Backend::Sysin::Modules>

=cut

sub _assert_openkvk_interface {
    my $self                = shift;

    ### StUF Configuration
    my $config_ifaces       = $self->schema->resultset('Interface')->search_active(
        {
            module  => 'overheidio'
        },
    );

    my $interface           = $config_ifaces->next;

    throw(
        'br/subject/search_remote/multiple_cfg_ifaces',
        'Multiple config interfaces found, cannot continue'
    ) if $config_ifaces->next;

    throw(
        'br/subject/search_remote/no_stuf_cfg_iface',
        'Remote search requested, but no active StUF configuration found'
    ) if !$interface;


    throw(
        'br/subject/search_remote/overheidio_has_no_kvk_enabled',
        'Overheid.IO koppeling found, but module KVK not enabled'
    ) unless $interface->jpath('$.overheid_io_module') eq 'kvk';

    return $interface;
}

=head2 remote_import

See L<Zaaksysteem::BR::Subject#remote_import> for usage information

=cut

around 'remote_import' => sub {
    my $method      = shift;
    my $self        = shift;
    my ($params)    = @_;

    my $type        = (blessed($params) ? $params->subject_type : $params->{subject_type});

    return $self->$method(@_) if !$self->_is_remote_openkvk({ subject_type => $type });

    my $interface = $self->_assert_openkvk_interface;

    ### Create this entry into our system.
    my $object = (blessed($params) ? $params : $self->object_from_params($params));

    ### Before trying to import an existing subject, let's see if we already have an authenticated
    ### person with this KVK in our system
    my $existing_objects = $self->search_rs({
        'subject_type'                  => 'company',
        'subject.coc_number'            => $object->subject->coc_number,
        ($object->subject->coc_location_number ? ('subject.coc_location_number'   => $object->subject->coc_location_number) : ()),
    });

    $self->schema->resultset('Logging')->trigger(
        'subject/company/view', {
        component    => 'betrokkene',
        component_id => undef,
        data         => {
            coc_number          => $object->subject->coc_number,
            coc_location_number => ($object->subject->coc_location_number || ''),
            company             => $object->subject->company,
            module              => 'openkvk'
        }
    });

    ### Existing object found, let's make sure the object is up2date.
    if ($existing_objects->count == 1) {
        return $self->_update_company_from_overheid_io($existing_objects->first->as_object, $object);
    }

    ### Now import it into ZS
    try {
        $self->schema->txn_do(
            sub {
                my $subscription_id = $object->external_subscription->external_identifier;

                $object         = $self->save($object);
                $object->set_authenticated($self->schema,$interface,$subscription_id) if $subscription_id;
            }
        );
    } catch {
        throw(
            'br/subject/remote_import/remote_error',
            "$_"
        );
    };

    ### Return fresh object
    return $object->discard_changes(schema => $self->schema);
};

=head2 _update_company

    $self->_update_company($existing_object, $new_object);

Updates the existing company.

=cut

sub _update_company_from_overheid_io {
    my ($self, $existing_object, $object) = @_;

    ### Easyest way is by copying the data from the old object, into the new one:
    $object->id($existing_object->id);
    $object->subject->id($existing_object->subject->id);

    ### Save it, and return
    $object = $self->save($object);

    return $object->discard_changes(schema => $self->schema);
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

