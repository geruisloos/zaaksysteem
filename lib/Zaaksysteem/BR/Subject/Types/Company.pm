package Zaaksysteem::BR::Subject::Types::Company;

use Moose::Role;
use Zaaksysteem::Tools;

use Zaaksysteem::BR::Subject::Constants qw/SUBJECT_CONFIGURATION SUBJECT_MAPPING/;

use constant SUBJECT_TYPE => 'company';
use constant SCHEMA_TABLE => 'Bedrijf';

=head1 NAME

Zaaksysteem::BR::Subject::Types::Company - Bridge specific role for this type

=head1 DESCRIPTION

Applies logic for the L<Zaaksysteem::BR::Subject> bridge for inflating various types.

=head1 METHODS

=head2 _table_id

Private method: contains the reference to the original table ('bedrijf.id')

=cut

has '_table_id' => (
    is      => 'rw',
    isa     => 'Int',
);

=head2 new_from_row

Please do not use this function directly, instead, use the "bridge": L<Zaaksysteem::Bridge::Subject>

=cut

sub new_from_row {
    my ($class, $row) = @_;

    my $table_name  = $row->result_source->name;
    my $subjecttype = SUBJECT_MAPPING->{ $table_name };
    my $mapping     = SUBJECT_CONFIGURATION->{ $subjecttype }->{mapping};

    my %values      = map(
        { $mapping->{$_} => $row->$_ }
        grep({ defined $row->get_column($_) } keys %$mapping)
    );

    ### Fix legal_entity
    $values{company_type}    = Zaaksysteem::Object::Types::LegalEntityType->new_from_code($values{company_type}) if $values{company_type};

    my %contact     = $class->_load_contact_data_from_row($row);
    my %addresses   = $class->_load_addresses_from_row($row);

    my $subject     = $class->new(%values, %contact, %addresses);

    $subject->id($row->uuid);
    $subject->_table_id($row->id);

    return $subject;
}

=head2 new_from_params

Please do not use this function directly, instead, use the "bridge": L<Zaaksysteem::Bridge::Subject>

=cut

sub new_from_params {
    my $class       = shift;
    my $rawparams   = { %{ (shift || {}) } };
    my %params      = map({ $_ => $rawparams->{ $_ } } grep( { defined($rawparams->{ $_ }) } keys %$rawparams));

    ### Only load adresses when either street or foreign_address_line1 is filled. Could be an empty HASHREF
    if (
        $params{address_correspondence} &&
        ($params{address_correspondence}->{street} || $params{address_correspondence}->{foreign_address_line1})
    ) {
        $params{address_correspondence}   = $class->_get_address_object($params{address_correspondence});
    }

    if (
        $params{address_residence} &&
        ($params{address_residence}->{street} || $params{address_residence}->{foreign_address_line1})
    ) {
        $params{address_residence}        = $class->_get_address_object($params{address_residence});
    }

    $params{company_type}             = Zaaksysteem::Object::Types::LegalEntityType->new_from_code($params{company_type}->{code}) if $params{company_type};

    delete($params{$_}) for grep({ !defined($params{$_}) } keys %params);

    return $class->new(%params);
}

=head2 save_to_tables

Please do not use this function directly, instead, use the "bridge": L<Zaaksysteem::Bridge::Subject>

=cut

define_profile 'save_to_tables' => (
    required    => {
        schema  => 'Zaaksysteem::Schema'
    }
);

sub save_to_tables {
    my $self        = shift;
    my $options     = assert_profile({ @_ })->valid;
    my $schema      = $options->{schema};

    $self->check_object(schema => $schema);

    my $mapping     = SUBJECT_CONFIGURATION->{ SUBJECT_TYPE() }->{mapping};

    ### Collect values
    my %values      = map(
        { my $key = $mapping->{ $_ }; $_ => $self->$key }
        keys %$mapping
    );

    ### Correct address and legal type
    %values = (
        %values,
        (map ({ $self->_get_address_values(type => $_) } qw/address_correspondence address_residence/))
    );
    $values{rechtsvorm} = $self->company_type->code if $self->company_type;

    ### TODO: Some validation logic in here: Does every address has the necessary fields filled, like
    ### there is not a landcode of NL and no street set, or it is an international without any foreign_address1

    ### Work the magic
    $schema->txn_do(
        sub {
            my $row;
            if ($self->id) {
                $row = $schema->resultset(SCHEMA_TABLE)->search(
                    {
                        uuid    => $self->id
                    }
                )->first;

                $row->update(
                    \%values
                ) or throw(
                    'object/types/company',
                    'Failed updating company by uuid: ' . $row->uuid . ' / id: ' . $row->id
                );
            } else {
                $row = $schema->resultset(SCHEMA_TABLE)->create(\%values);
            }

            ### Discard changes to retrieve uuid from database
            $row->discard_changes();
            $self->_table_id($row->id);
            $self->id($row->uuid);
        }
    );

    return $self;
}

=head2 check_object

    $entity->check_object(schema => $schema);

Will return the result of a C<assert_profile> when the object is missing some params. The entity
objects are rather free form. Mostly because of our legacy, but sometimes also when we just want
to see part of a subject.

This function makes sure we create or save objects into our database in a complete form, to prevent
further "legacy"

=cut

define_profile check_object => (
    %{ SUBJECT_CONFIGURATION->{ SUBJECT_TYPE() }->{profiles}->{save} }
);

sub check_object {
    my $self        = shift;
    my (%opts)      = @_;

    my %values      = map(
        { my $key = $_->name; $key => $self->$key }
        grep({ $_->does('Zaaksysteem::Metarole::ObjectAttribute') || $_->does('Zaaksysteem::Metarole::ObjectRelation') } $self->meta->get_all_attributes)
    );

    assert_profile({ %values, _schema => $opts{schema}, '_update_existing' => ($self->id || 0) })->valid;

    ### Extra, check addresses
    $self->$_->check_object for grep({ $self->$_ } qw/address_correspondence address_residence/);

    return 1;
}

=head1 PRIVATE METHODS

=head2 _get_address_values

=cut

define_profile _get_address_values => (
    required    => {
        type        => 'Str',
    }
);

sub _get_address_values {
    my $self    = shift;
    my $options = assert_profile({ @_ })->valid;
    my $attrkey = $options->{type};

    my %premap  = (address_correspondence => 'correspondentie', address_residence => 'vestiging');

    my $address = $self->$attrkey;
    return () unless $address;     # No address set

    my $mapping = SUBJECT_CONFIGURATION->{SUBJECT_TYPE()}->{address_mapping};

    my %values;
    for my $key (keys %$mapping) {
        my $attr  = $mapping->{$key};
        my $dbkey = $premap{$attrkey} . "_" . $key;
        my $value = $address->$attr;

        if ($attr eq 'country' && $value) {
            $value = $value->dutch_code;
        }

        if ($attr eq 'municipality' && $value) {
            $value = $value->code;
        }

        $values{$dbkey} = $value;
    }

    return %values;
}

=head2 _load_contact_data_from_row

Loads the contact data into the subject (phone numbers, email address)

=cut

sub _load_contact_data_from_row {
    my ($class, $row) = @_;
    
    my %rv;

    my $contactdata = $row->result_source->schema->resultset('ContactData')->search({
        gegevens_magazijn_id    => $row->id,
        betrokkene_type         => 2, # "Company"
    })->first;

    return unless $contactdata;

    $rv{mobile_phone_number} = $contactdata->mobiel         if $contactdata->mobiel;
    $rv{phone_number}        = $contactdata->telefoonnummer if $contactdata->telefoonnummer;
    $rv{email_address}       = $contactdata->email          if $contactdata->email;

    return %rv;
}

=head2 _load_addresses_from_row

Loads the addresses into this subject

=cut

sub _load_addresses_from_row {
    my ($class, $row) = @_;
    my %rv;

    my $correspondence  = $class->_get_address_object({ $row->get_columns }, 'correspondentie');
    my $residence       = $class->_get_address_object({ $row->get_columns }, 'vestiging');

    $rv{address_correspondence} = $correspondence if $correspondence;
    $rv{address_residence}      = $residence if $residence;

    return %rv;
}

=head2 _get_address_object

Returns a filled object of type L<Zaaksysteem::Object::Types::Address>

=cut

sub _get_address_object {
    my ($class, $params, $prefix) = @_;
    my %values;

    unless ($params) {
        throw('object/types/company/no_row', 'No row given, code error');
    }

    if ($prefix) {
        my $mapping     = SUBJECT_CONFIGURATION->{'company' }->{address_mapping};

        for my $key (keys %$mapping) {
            my $dbkey = $prefix . "_" . $key;

            next unless defined $params->{$dbkey};

            $values{ $mapping->{$key} } = $params->{$dbkey};
        }

        ## Coercion would be even better
        $values{country}    = Zaaksysteem::Object::Types::CountryCode->new_from_code($values{country}) if $values{country};
    } else {
        %values         = map({ $_ => $params->{ $_ } } grep( { defined($params->{ $_ }) } keys %$params));

        $values{country}    = Zaaksysteem::Object::Types::CountryCode->new_from_code($values{country}->{dutch_code}) if $values{country};
    }

    return Zaaksysteem::Object::Types::Address->new(%values);
}

=head2 _build_display_name

Returns the contents of $self->company;

=cut

sub _build_display_name {
    my $self            = shift;

    return $self->company;
}


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
