package Zaaksysteem::BR::Controlpanel;
use Moose;

=head1 NAME

Zaaksysteem::BR::Controlpanel - Controlpanel business rules in one module

=head1 DESCRIPTION

All the business logic which is used for/by the controlpanel feature are kept in this shiney module.
If you want to create, edit or delete controlpanel objects and likes use this module instead of
changing the objects yourself and saving them. This module will make sure all the details are taken care of.

Test:

    ZS_DISABLE_STUF_PRELOAD=1 ./zs_prove -v t/lib/TestFor/General/BR/Controlpanel.pm

=head1 SYNOPIS

    use Zaaksysteem::BR:Controlpanel;
    use Zaaksysteem::CLI;

    my $cli = try {
        Zaaksysteem::CLI->init;
    }
    catch {
        die $_;
    };

    my $br = Zaaksysteem::BR::Controlpanel->new(
        schema  => $cli->schema,
        bedrijf => $cli->schema->betrokkene_model->get({}, 'betrokkene-bedrijf-1'),
    );

    $br->controlpanel();
    # or
    $br->controlpanel({
        template      => 'foo',
        customer_type => 'government',
        shortname     => 'bar',
        read_only     => 1,
    });

    $br->create_instance({
        fqdn     => 'foo.zaaksysteem.nl',
        label    => 'foo zaaksysteem'
        otap     => 'development',
        template => 'foo',
    });

    my $host = $br->create_host({
        fqdn       => 'bar.zaaksysteem.nl',
        label      => 'bar zaaksysteem',
        api_domain => 'bar.api.zaaksysteem.nl',
        ip         => '127.0.0.1',
    });
    $br->save;

    my ($uuid) = $br->get_instance_uuids();
    $br->get_instance_by_uuid($uuid);

    my ($uuid) = $br->get_host_uuids();
    $br->get_host_by_uuid($uuid);

=cut

use Digest::SHA qw(sha1_hex);
use Zaaksysteem::Tools;
use Zaaksysteem::Object::Model;
use Zaaksysteem::Object::Types::Naw;
use Zaaksysteem::Object::Types::Controlpanel;
use Zaaksysteem::Object::Types::Instance;
use Zaaksysteem::Object::Types::Host;
use Zaaksysteem::Types qw(Betrokkene FQDN Otap NonEmptyStr IPv4 Host CustomerType Timestamp ZSNetworkACLs UUID);

require Exporter;
our @ISA        = qw/Exporter/;
our @EXPORT_OK  = qw/
    CONTROLPANEL_VALIDATION_PROFILES
/;

use constant CONTROLPANEL_BLACKLISTED_HOSTS => [
    qr/^(?:.*\.|.?)cyso$/,
    qr/^(?:.*\.|.?)munt$/,
    qr/^(?:.*\.|.?)gov-cloud$/,
    qr/^(?:.*\.|.?)public-cloud$/,
    qr/^(?:.*\.|.?)services$/,
    qr/^(?:.*\.|.?)accept$/,
    qr/^(?:.*\.|.?)saas$/,
    qr/^(?:.*\.|.?)app$/,
    qr/^(?:.*\.|.?)public$/,
    qr/^(?:.*\.|.?)win1.test-ad$/,
    qr/^(?:.*\.|.?)api$/,
    qr/^(?:.*\.|.?)aws$/,
    qr/^aws$/,
    qr/^office$/,
    qr/^mijn$/,
    qr/^www$/,
    qr/^ftp$/,
    qr/^mail$/,
    qr/^pop$/,
    qr/^imap$/,
    qr/^spf$/,
    qr/^wiki$/,
    qr/^win1.test-ad$/,
    qr/^packages.public$/,
    qr/^platform$/,
    qr/^app1-wild$/,
    qr/^app1$/,
    qr/^data1$/,
    qr/^services$/,
    qr/^dev$/,
    qr/^ftp$/,
    qr/^imap$/,
    qr/^localhost$/,
    qr/^pm$/,
    qr/^services$/,
    qr/^template$/,
    qr/^vagrant$/,
    qr/^vm1/,
    qr/^mintlab/,
];

use constant CONTROLPANEL_VALIDATION_PROFILES => {
    'instance/create'   => {
        required => {
            label       => 'Str',
            otap        => Otap,
        },
        optional => {
            template => 'Str',
            password => 'Str',
            protected => 'Bool',
            api_domain => FQDN,
            services_domain => FQDN,
            mail => 'Bool',
            network_acl => IPv4,
        },
        require_some => {
            fqdn => [1, qw/fqdn/],
        },
        constraint_methods => {
            fqdn    => sub {
                my ($dfv, $val)     = @_;

                my $fqdn            = Zaaksysteem::BR::Controlpanel::_verify_host_or_fqdn($dfv, $dfv->{__INPUT_DATA}->{_controlpanel}, $dfv->{__INPUT_DATA}->{_objectmodel}, $val) or return;

                $dfv->get_filtered_data->{fqdn}         = $fqdn;

                my $postdomain   = $dfv->{__INPUT_DATA}->{_controlpanel}->get_object_attribute('domain');
                $postdomain      = $postdomain->value if $postdomain;

                my ($host)          = $fqdn     =~ /^(.*?)\./;

                $dfv->get_filtered_data->{api_domain}   = $host . '.api.' . $postdomain unless $dfv->get_filtered_data->{api_domain};
                $dfv->get_filtered_data->{services_domain}   = $host . '.services.' . $postdomain unless $dfv->get_filtered_data->{services_domain};

                return 1;
            },
        },
        field_filters       => {
            fqdn    => ['lc']
        },
        msgs    => sub {
            my $dfv     = shift;
            my $rv      = {};

            return $rv unless $dfv->{_custom_messages};

            for my $key (keys %{ $dfv->{_custom_messages} }) {
                $rv->{$key} = $dfv->{_custom_messages}->{$key};
            }

            return $rv;
        }
    },
    'instance/update'   => {
        optional    => {
            fqdn  => sub {
                my ($dfv, $val)     = @_;

                my $postdomain   = $dfv->{__INPUT_DATA}->{_controlpanel}->get_object_attribute('domain');
                $postdomain      = $postdomain->value if $postdomain;

                if (
                    $dfv->{__INPUT_DATA}->{_current_instance} &&
                    $dfv->{__INPUT_DATA}->{_current_instance}->get_object_attribute('fqdn')
                ) {
                    my $current_fqdn = $dfv->{__INPUT_DATA}->{_current_instance}->get_object_attribute('fqdn')->value;

                    if (
                        $current_fqdn &&
                        (
                            $current_fqdn eq lc($val) ||
                            $current_fqdn eq lc($val . '.' . $postdomain)
                        )
                    ) {
                        return 1;
                    }
                }

                my $fqdn            = Zaaksysteem::BR::Controlpanel::_verify_host_or_fqdn($dfv, $dfv->{__INPUT_DATA}->{_controlpanel}, $dfv->{__INPUT_DATA}->{_objectmodel}, $val) or return;

                $dfv->get_filtered_data->{fqdn}             = $fqdn;

                my ($host)          = $fqdn     =~ /^(.*?)\./;

                $dfv->get_filtered_data->{api_domain}       = $host . '.api.' . $postdomain unless $dfv->get_filtered_data->{api_domain};
                $dfv->get_filtered_data->{services_domain}  = $host . '.services.' . $postdomain unless $dfv->get_filtered_data->{services_domain};

                return 1;
            },
            label => 'Str',
            # otap  => Otap,
            template => 'Str',
            protected => 'Bool',
            api_domain => FQDN,
            services_domain => FQDN,
            mail => 'Bool',
            delete_on => Timestamp,
            provisioned_on => Timestamp,
            network_acl => IPv4,
            database_provisioned => Timestamp,
            filestore_provisioned => Timestamp,
            disabled => 'Bool',
            password => 'Any',
        },
        field_filters  => {
            fqdn    => ['lc'],
            provisioned_on => sub { DateTime::Format::ISO8601->parse_datetime(shift) },
            delete_on => sub { DateTime::Format::ISO8601->parse_datetime(shift) },
            database_provisioned => sub { DateTime::Format::ISO8601->parse_datetime(shift) },
            filestore_provisioned => sub { DateTime::Format::ISO8601->parse_datetime(shift) },
        },
        msgs    => sub {
            my $dfv     = shift;
            my $rv      = {};

            return $rv unless $dfv->{_custom_messages};

            for my $key (keys %{ $dfv->{_custom_messages} }) {
                $rv->{$key} = $dfv->{_custom_messages}->{$key};
            }

            return $rv;
        }
    }
};

=head1 ATTRIBUTES

=head2 schema

A L<Zaaksysteem::Schema> object. Required.

=cut

has schema => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema',
    required => 1,
);

=head2 model

A L<Zaaksysteem::Object::Model> object. lazily loaded.

=cut

has model => (
    is      => 'ro',
    isa     => 'Zaaksysteem::Object::Model',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return Zaaksysteem::Object::Model->new(schema => $self->schema);
    },
);

=head2 interface

A interface object. Lazily loaded.

=cut

has interface => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return $self->schema->resultset('Interface')
            ->search_active({ module => 'controlpanel' })->first
            || throw(
            'zs/controlpanel/interface/missing',
            "No active 'Controlpanel' interfaces found"
            );
    },
);

=head2 bedrijf

A bedrijf/company. Required.

=cut

has bedrijf => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Model::DB::Bedrijf',
    required => 1,
);

=head2 naw

An NAW object. Created for you.

=cut

has naw => (
    is       => 'rw',
    isa      => 'Zaaksysteem::Object::Types::Naw',
    accessor => '_naw',
);

=head2 controlpanel

A control panel object.

If you don't have a controlpanel object, this will create one for you.

=cut

has controlpanel => (
    is       => 'rw',
    isa      => 'Zaaksysteem::Object::Types::Controlpanel',
    accessor => '_controlpanel',
);

=head2 ssl_checksums

Checksums of the Mintlab SSL files.

=cut

has ssl_checksums => (
    is      => 'ro',
    isa     => 'HashRef',
    default => sub {
        return {
            map { $_ => 1 }
                qw(
                4c4dc17549de75b43dd58847887ae09117b8b5a1
                a6a42d7c7a657f768175c67d02f6d7fa8b51d922
                3e43a7a8eec2464db24208b46b3f80ac3eeb295b
                9cc66c75547cd59ace95ab4e4bdf04e7272ff97c
                )
        };
    },
);

=head2 loadbalancer_ips

The IP addresses of our loadbalancers

=cut

has loadbalancer_ips => (
    is      => 'ro',
    isa     => 'HashRef',
    default => sub {
        return { map { $_ => 1 } qw(37.46.137.172 37.46.137.46 193.239.91.5 37.46.137.164) };
    },
);

=head1 PRIVATE ATTRIBUTES

=head2 _objects_for_saving

Objects which need to be saved when save is called.

=cut

has _objects_for_saving => (
    is => 'rw',
    isa =>
        'ArrayRef[Zaaksysteem::Object::Types::Instance|Zaaksysteem::Object::Types::Host]',
    default => sub { return [] },
);

=head2 _objects_for_deletion

Objects which need to be deleted when save is called.

=cut

has _objects_for_deletion => (
    is => 'rw',
    isa =>
        'ArrayRef[Zaaksysteem::Object::Types::Instance|Zaaksysteem::Object::Types::Host]',
    default => sub { return [] },
);

=head1 PUBLIC METHODS

=head2 create_naw

Create an NAW object.

=cut

sub create_naw {
    my $self = shift;

    if ($self->model->count('naw', { owner => $self->bedrijf->bid })) {
        throw('controlpanel/naw/exists',
            "NAW data already exists, please use update!");
    }
    return $self->naw(
        Zaaksysteem::Object::Types::Naw->new_from_betrokkene(
            betrokkene => $self->bedrijf
        )
    );
}

=head2 get_instance_by_uuid

Get an instance based on the UUID.

=cut

sub get_instance_by_uuid {
    my ($self, $uuid) = @_;
    $self->_get_by_uuid('instance', $uuid);
}

=head2 get_host_by_uuid

Get an instance based on the UUID.

=cut

sub get_host_by_uuid {
    my ($self, $uuid) = @_;
    $self->_get_by_uuid('host', $uuid);
}

=head2 assert_loadbalancer_ip

Check if we supplied a loadbalancer IP, dies if it isn't one.

=cut

sub assert_loadbalancer_ip {
    my ($self, @ips) = @_;
    return 1 if $self->is_loadbalancer_ip(@ips);

    throw(
        "controlpanel/host/loadbalancer",
        "This is not a loadbalancer IP address"
    );
}

=head2 is_loadbalancer_ip

Check if we supplied a loadbalancer IP, returns a boolean-ish value.

=cut

sub is_loadbalancer_ip {
    my ($self, @ips) = @_;
    foreach (@ips) {
        return 1 if $self->loadbalancer_ips->{$_};
    }
    return 0;
}

=head2 is_mintlab_ssl

Check if we supplied mintlab SSL file

=cut

sub is_mintlab_ssl {
    my ($self, %files) = @_;
    my $cert_sum = sha1_hex($files{certificate} // '');
    my $key_sum  = sha1_hex($files{private_key} // '');

    foreach ($cert_sum, $key_sum) {
        return 1 if $self->ssl_checksums->{$_};
    }
    return 0;
}

=head2 create_host

Create a host

=cut

define_profile create_host => (
    required => {
        fqdn       => FQDN,
        label      => NonEmptyStr,
    },
    optional => {
        ip       => IPv4,
        ssl_key  => 'Str',
        ssl_cert => 'Str',
        aliases  => 'Any',
        disabled => 'Bool',
        mail     => 'Bool',
        instance => 'Any',
    },
    defaults => {
        ssl_key  => '',
        ssl_cert => '',
        aliases  => [],
        mail     => 0,
        disabled => 0,
    }
);

sub create_host {
    my ($self, $params) = @_;
    $params = assert_profile($params // {})->valid;

    $params->{owner} = $self->bedrijf->bid;

    if (
        $self->is_mintlab_ssl(
            certificate => $params->{ssl_certificate},
            private_key => $params->{ssl_keyfile}
        )
        )
    {
        foreach (qw(keyfile certificate)) {
            delete $params->{"ssl_$_"};
        }
    }

    if ($self->is_loadbalancer_ip($params->{ip})) {
        delete $params->{ip};
    }

    my $host = Zaaksysteem::Object::Types::Host->new(%$params);
    push(@{ $self->_objects_for_saving }, $host);

    $host->set_instance($self->model, $params->{instance}->id) if $params->{instance};

    return $host;
}

=head2 update_host

Update an host

=cut

define_profile 'update_host' => (
    required => {
        fqdn  => FQDN,
        label => NonEmptyStr,
    },
    optional => {
        ip         => IPv4,
        ssl_key    => 'Str',
        ssl_cert   => 'Str',
        disabled   => 'Bool',
        mail       => 'Bool',
    },
    defaults => {
        ssl_key  => '',
        ssl_cert => '',
    }
);

sub update_host {
    my ($self, $uuid, $params) = @_;
    $params = assert_profile($params // {})->valid;

    my $copy = $self->get_host_by_uuid($uuid);
    foreach (keys %$params) {
        my $value = $params->{$_};
        if (defined $value) {
            $copy->$_($value);
        }
    }
    if (!defined $params->{ip}) {
        $copy->_clear_ip();
    }
    push(@{ $self->_objects_for_saving }, $copy);
    return $copy;
}

=head2 delete_host

Delete the host based on the UUID

=cut

sub delete_host {
    my ($self, $uuid) = @_;

    my $d = $self->get_host_by_uuid($uuid);
    push(@{ $self->_objects_for_deletion }, $d->id);
    return $d;
}

=head2 save

Save all the changes made to all the objects.

=cut

sub save {
    my ($self) = @_;

    my ($behandelaar) = grep ({ $_->system_role && $_->name eq 'Behandelaar' } @{ $self->schema->resultset('Roles')->get_all_cached($self->{_cache}) });
    foreach (qw(naw controlpanel)) {
        my $method = "_$_";
        my $object = $self->$_;

        $object->permit($behandelaar, qw/read write/);

        $self->$method($self->model->save(object => $object)) if $self->$_;
    }

    foreach (@{ $self->_objects_for_saving }) {
        $_->permit($behandelaar, qw/read write/);

        $self->model->save(object => $_);
    }
    $self->_objects_for_saving([]);
    foreach (@{ $self->_objects_for_deletion }) {
        $self->model->delete(uuid => $_);
    }
    $self->_objects_for_deletion([]);
    return 1;
}

sub naw {
    my $self = shift;

    return $self->_naw if $self->_naw;
    my $naw = $self->model->search('naw', { owner => $self->bedrijf->bid })->next;

    if (!$naw) {
        $naw = Zaaksysteem::Object::Types::Naw->new_from_betrokkene(
            betrokkene => $self->schema->betrokkene_model->get({}, $self->bedrijf->bid));
    }

    return $self->_naw($naw);
}

sub controlpanel {
    my ($self, $params) = @_;

    $self->_get_cp;

    if (!defined $params) {
        return $self->_controlpanel if $self->_controlpanel;


        return $self->_controlpanel(
            Zaaksysteem::Object::Types::Controlpanel->new(
                owner           => $self->bedrijf->bid,
                shortname       => $self->bedrijf->bid,
                template        => 'mintlab',
                customer_type   => 'overheid',
                domain          => 'zaaksysteem.nl',
            )
        );
    }
    elsif (blessed $params
        && $params->isa('Zaaksysteem::Object::Types::Controlpanel'))
    {
        if ($params->owner eq $self->bedrijf->bid) {
            if (!$self->_controlpanel || !$self->_controlpanel->id) {
                return $self->_controlpanel($params);
            }
            elsif ($params->id && $params->id eq $self->_controlpanel->id) {
                $self->assert_shortname_change($params);
                return $self->_controlpanel($params);
            }
            throw('controlpanel/object/owner/unique',
                'controlpanel already exists for this owner');
        }
        throw(
            'controlpanel/object/owner/invalid',
            'controlpanel does not belong to this owner'
        );
    }
    elsif (ref $params eq 'HASH') {
        if ($self->_controlpanel) {
            throw('controlpanel/hash/owner/unique',
                'controlpanel already exists for this owner');
        }
        return $self->_controlpanel(
            Zaaksysteem::Object::Types::Controlpanel->new(
                owner           => $self->bedrijf->bid,
                template        => $params->{template} // 'mintlab',
                customer_type   => $params->{customer_type} // 'overheid',
                shortname       => $params->{shortname} // $self->bedrijf->bid,
                domain          => 'zaaksysteem.nl',
            )
        );
    }
    throw(
        'api/v1/controlpanel/action/invalid',
        'controlpanel does not know how to deal with your current action',
    );
}

=head2 assert_shortname_change

Checks if we can change the shortname of a controlpanel object. Dies if you can't.

=cut

sub assert_shortname_change {
    my ($self, $change) = @_;
    $self->_get_cp;
    return 1 if !$self->_controlpanel;

    if ($self->_controlpanel->shortname ne $change->shortname
        && (@{ $self->get_instance_uuids } || @{ $self->get_host_uuids }))
    {
        throw('controlpanel/shortname', "Unable to change shortname");
    }
    return 1;

}

=head2 create_instance

Create an instance

=cut

define_profile create_instance => (
    required => {
        fqdn  => FQDN,
        label => 'Str',
        otap  => Otap,
    },
    optional => {
        template  => 'Str',
        filestore => 'Str',
        database  => 'Str',
        provisioned_on => 'Str',
        protected => 'Bool',
        customer_type => CustomerType,
        fallback_url => NonEmptyStr,
        api_domain => FQDN,
        services_domain => FQDN,
    },
);

sub create_instance {
    my ($self, $params) = @_;
    $params = assert_profile($params)->valid;

    my $cp = $self->controlpanel();

    my $instance = Zaaksysteem::Object::Types::Instance->new(
        owner           => $self->bedrijf->bid,
        template        => $cp->template,
        customer_type   => $cp->customer_type,
        %$params,
    );

    my ($behandelaar) = grep ({ $_->system_role && $_->name eq 'Behandelaar' } @{ $self->schema->resultset('Roles')->get_all_cached($self->{_cache}) });
    $instance->permit($behandelaar, qw/read write/);

    $instance = $self->model->save(object => $instance);
    return $instance;
}

=head2 update_instance

Create an instance

=cut

sub update_instance {
    my ($self, $uuid, $params) = @_;
    $params //= {};

    my @allowed_attributes = qw(
        fqdn
        label
        template
        deleted
        disabled
        network_acl
        template
        database_provisioned
        filestore_provisioned
        provisioned_on
        delete_on
        fallback_url
        api_domain
        services_domain
    );

    # Force a live query
    my $copy = $self->get_instance_by_uuid($uuid);

    if (!defined $params->{template}
        || $params->{template} =~ /^\p{XPosixSpace}*$/)
    {
        $params->{template} = $self->controlpanel->template;
    }

    foreach (@allowed_attributes) {
        my $value = $params->{$_};
        if (defined $value) {
            $copy->$_($value);
        }
    }
    push(@{ $self->_objects_for_saving }, $copy);
    return $copy;
}

=head2 delete_instance

Deletes an instance

=cut

sub delete_instance {
    my ($self, $uuid) = @_;

    my $d = $self->get_instance_by_uuid($uuid);
    push(@{ $self->_objects_for_deletion }, $d->id);
    return $d;
}

=head2 get_instance_uuids

Get all the UUID's of the instances for this customer

=cut

sub get_instance_uuids {
    my $self = shift;
    return $self->_get_uuids('instance');
}

=head2 get_host_uuids

Get all the UUID's of the hosts for this customer

=cut

sub get_host_uuids {
    my $self = shift;
    return $self->_get_uuids('host');
}

=head1 PRIVATE METHODS

=head2 _get_uuids

Convenience method to get the UUID's of a type.

=cut

sub _get_uuids {
    my ($self, $type) = @_;

    my $rs = $self->model->search($type, { owner => $self->bedrijf->bid });
    my @results;
    while (my $r = $rs->next) {
        push(@results, $r->id);
    }
    return \@results;
}

=head2 _get_by_uuid

Convenience method to get an object by type by UUID.

=cut

sub _get_by_uuid {
    my ($self, $type, $uuid) = @_;

    my $rs = $self->model->search_rs($type, { owner => $self->bedrijf->bid });
    $rs = $rs->find($uuid);
    if ($rs) {
        return $self->model->inflate_from_row($rs);
    }
    throw("controlpanel/$type/uuid/404",
        "Unable to find $type with uuid '$uuid'");
}

=head2 _get_cp

Get a controlpanel object if it is stored in the database.

=cut

sub _get_cp {
    my $self = shift;
    return $self->_controlpanel if $self->_controlpanel;

    my ($cp) = $self->model->search('controlpanel', {
        owner => $self->bedrijf->bid
    })->next;

    $self->_controlpanel($cp) if $cp;
}

=head2 _get_bedrijf

Get a bedrijf based on the handelsnaam.

=cut

sub _get_bedrijf {
    my ($self, $bedrijf) = @_;

    die "No company given!" unless defined $bedrijf;
    my $rs = $self->schema->resultset('Bedrijf')->search_rs(
        {
            handelsnaam => $bedrijf,
            deleted_on  => undef
        }
    );

    return $rs->first if $rs->count == 1;

    die sprintf "Found to many instances of company '%s': %d\n", $bedrijf,
        $rs->count;
}

=head2 new_from_betrokkene

Arguments: \%OPTIONS

Return value: L<Zaaksysteem::BR::Controlpanel> object

B<options>

=over 4

=item betrokkene_id [required]

The betrokkene id for this controlpanel

=back

=cut

sub new_from_betrokkene {
    my ($self, %opts) = @_;

    my $betrokkene = $opts{schema}->betrokkene_model->get({}, $opts{betrokkene_id});
    if (!$betrokkene) {
        throw("zs/br/betrokkene/404", "No betrokkene found with ID $opts{betrokkene_id}");
    }
    my $bedrijf = $opts{schema}->resultset('Bedrijf')->search_rs({id => $betrokkene->gmid })->first;
    if (!$bedrijf) {
        throw("zs/br/bedrijf/404", "No bedrijf found with ID " . $betrokkene->gmid);
    }
    return $self->new(
        schema => $opts{schema},
        bedrijf => $bedrijf,
    );
}

=head2 _verify_host_or_fqdn

Validates whether the given domain exists or is on the reserved list

=cut

sub _verify_host_or_fqdn {
    my $dfv             = shift;
    my $controlpanel    = shift;
    my $objectmodel     = shift;
    my $host_or_fqdn    = shift;
    my $postdomain      = shift;

    unless ($postdomain) {
        $postdomain      = $controlpanel->get_object_attribute('domain');
        $postdomain      = $postdomain->value if $postdomain;
    }

    my $fulldomain;

    if ($host_or_fqdn !~ /\./) {
        unless (Host->check($host_or_fqdn)) {
            $dfv->{_custom_messages}->{fqdn} = 'Invalid hostname, make sure it contains only characters and/or digits, like example';
            return;
        }

        $fulldomain     = $host_or_fqdn . '.' . $postdomain;
    } else {
        unless (FQDN->check($host_or_fqdn)) {
            $dfv->{_custom_messages}->{fqdn} = 'Domain is invalid, make sure you enter a correct domain, like test.example.com';
            return;
        }

        $fulldomain = $host_or_fqdn;
    }

    my $given_prefix;
    unless (($given_prefix) = $fulldomain =~ /^(.*)\.$postdomain$/) {
        $dfv->{_custom_messages}->{fqdn} = 'Invalid hostname';
        return;
    }

    ### Make sure prefixed hostname does not have a dot in the name
    if ($given_prefix =~ /\./) {
        $dfv->{_custom_messages}->{fqdn} = 'Invalid hostname, make sure the hostname you enter does not contain a dot';
        return;
    }

    if (grep( { $given_prefix =~ $_ } @{ CONTROLPANEL_BLACKLISTED_HOSTS() })) {
        $dfv->{_custom_messages}->{fqdn} = 'Hostname given is reserved';
        return;
    }

    if ($objectmodel->count('instance', { fqdn => lc($fulldomain) })) {
        $dfv->{_custom_messages}->{fqdn} = 'Given domain is already in use, please choose another one';
        return;
    }

    return $fulldomain;
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
