package Zaaksysteem::POD;

use strict;
use warnings;
use File::Spec::Functions qw(catfile);

use base 'Pod::Simple::HTML';

__PACKAGE__->_accessorize(qw/
    requested_object
    index_list
    homedir
/);

=head1 NAME

Zaaksysteem::POD - Support links to /man for Zaaksysteem packages in POD

=head1 METHODS

=head2 do_pod_link

See L<Pod::Simple::HTML/SUBCLASSING>.

=cut

sub DEBUG { 0; };

sub _get_package_name {
    my $self        = shift;
    my $filename    = shift;

    $filename       =~ s|^(?:lib/\|t/lib/\|t/inc/)||;
    $filename       =~ s|/|::|g;
    $filename       =~ s/\.(?:pod|pm)$//;

    return $filename;
}

sub _load_index {
    my $self        = shift;

    my $manifest    = catfile($self->homedir, 'MANIFEST.pod');

    my %index;
    open(my $fh, '<' . $manifest) or die('Cannot load manifest file: ' . $manifest);
    while (<$fh>) {
        chomp;

        $index{ $self->_get_package_name($_) } = $_;
    }
    close($fh);

    $self->index_list(\%index);
}

sub new {
    my $self = shift->next::method(@_);
    my $opts = $_[0] || {};

    for my $key (keys %$opts) {
      $self->$key($opts->{$key});
    }

    $self->_load_index;

    $self->html_css('/css/documentation.css');

    $self->accept_targets('html','perl','javascript','html');

    $self->{Tagmap}->{'for'} = "\n<pre><code>";
    $self->{Tagmap}->{'/for'} = "</pre></code>\n";

    return $self;
}

sub get_title       {
  my $self  = shift;

  my $title;
  for my $name (qw/NAME Name name/) {
    next if $title;
    $title = $self->_get_titled_section(
      $name, max_token => 50, desperate => 1, @_
    );
  }

  return $title;
}

my %ToIndex = map {; $_ => 1 } qw(head1 head2 head3 head4 );
sub _do_middle_main_loop {
  my $self = $_[0];
  my $fh = $self->{'output_fh'};
  my $tagmap = $self->{'Tagmap'};

  $self->__adjust_html_h_levels;

  my($token, $type, $tagname, $linkto, $linktype);
  my @stack;
  my $dont_wrap = 0;

  while($token = $self->get_token) {

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    if( ($type = $token->type) eq 'start' ) {
      if(($tagname = $token->tagname) eq 'L') {
        $linktype = $token->attr('type') || 'insane';

        $linkto = $self->do_link($token);

        if(defined $linkto and length $linkto) {
          Pod::Simple::HTML::esc($linkto);
            #   (Yes, SGML-escaping applies on top of %-escaping!
            #   But it's rarely noticeable in practice.)
          print $fh qq{<a href="$linkto" class="podlink$linktype"\n>};
        } else {
          print $fh "<a>"; # Yes, an 'a' element with no attributes!
        }

      } elsif ($tagname eq 'item-text' or $tagname =~ m/^head\d$/s) {
        print $fh $tagmap->{$tagname} || next;

        my @to_unget;
        while(1) {
          push @to_unget, $self->get_token;
          last if $to_unget[-1]->is_end
              and $to_unget[-1]->tagname eq $tagname;

          # TODO: support for X<...>'s found in here?  (maybe hack into linearize_tokens)
        }

        my $name = $self->linearize_tokens(@to_unget);
        $name = $self->do_section($name, $token) if defined $name;

        print $fh "<a ";
        if ($tagname =~ m/^head\d$/s) {
            print $fh "class='u'", $self->index
                ? " href='#___top' title='click to go to top of document'\n"
                : "\n";
        }

        if(defined $name) {
          my $esc = Pod::Simple::HTML::esc(  $self->section_name_tidy( $name ) );
          print $fh qq[name="$esc"];
          DEBUG and print "Linearized ", scalar(@to_unget),
           " tokens as \"$name\".\n";
          push @{ $self->{'PSHTML_index_points'} }, [$tagname, $name]
           if $ToIndex{ $tagname };
            # Obviously, this discards all formatting codes (saving
            #  just their content), but ahwell.

        } else {  # ludicrously long, so nevermind
          DEBUG and print "Linearized ", scalar(@to_unget),
           " tokens, but it was too long, so nevermind.\n";
        }
        print $fh "\n>";
        $self->unget_token(@to_unget);

      } elsif ($tagname eq 'Data') {
        my $next = $self->get_token;
        next unless defined $next;
        unless( $next->type eq 'text' ) {
          $self->unget_token($next);
          next;
        }
        DEBUG and print "    raw text ", $next->text, "\n";
        print $fh $next->text;
        next;

      } else {
        if( $tagname =~ m/^over-/s ) {
          push @stack, '';
        } elsif( $tagname =~ m/^item-/s and @stack and $stack[-1] ) {
          print $fh $stack[-1];
          $stack[-1] = '';
        }

        if ($tagname =~ /^for/) {
            if ($token->attr('target') =~ /perl|html|javascript/) {
                print $fh '<pre class="codeblock"><code class="language-' . $token->attr('target') . '">' || next;
            }
        } else {
            print $fh $tagmap->{$tagname} || next;
        }
        ++$dont_wrap if $tagname eq 'Verbatim' or $tagname eq "VerbatimFormatted"
          or $tagname eq 'X';
      }

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    } elsif( $type eq 'end' ) {
      if( ($tagname = $token->tagname) =~ m/^over-/s ) {
        if( my $end = pop @stack ) {
          print $fh $end;
        }
      } elsif( $tagname =~ m/^item-/s and @stack) {
        $stack[-1] = $tagmap->{"/$tagname"};
        if( $tagname eq 'item-text' and defined(my $next = $self->get_token) ) {
          $self->unget_token($next);
          if( $next->type eq 'start' ) {
            print $fh $tagmap->{"/item-text"},$tagmap->{"item-body"};
            $stack[-1] = $tagmap->{"/item-body"};
          }
        }
        next;
      }
      print $fh $tagmap->{"/$tagname"} || next;
      --$dont_wrap if $tagname eq 'Verbatim' or $tagname eq 'X';

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    } elsif( $type eq 'text' ) {
      Pod::Simple::HTML::esc($type = $token->text);  # reuse $type, why not
      $type =~ s/([\?\!\"\'\.\,]) /$1\n/g unless $dont_wrap;
      print $fh $type;
    }

  }
  return 1;
}

sub do_pod_link {
    my ($self, $link) = @_;

    if ($link->tagname eq 'L' && $link->attr('type') eq 'pod') {
        my $to = $link->attr('to') || '';

        if($to =~ m[^Zaaksysteem]) {
            my $path = sprintf('/man/%s', $to);

            if($link->attr('section')) {
                $path = sprintf('%s#%s', $path, $link->attr('section'));
            }

            return $path;
        }
    }

    return $self->SUPER::do_pod_link($link);
}

=head2 load_html_header_after_title

Create the HTML header (after the <title> tag).

=cut

sub load_html_header_after_title {
    my $self            = shift;

    my $head = q|
  </title>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <link rel="stylesheet" href="/css/jquery.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="/tpl/zaak_v1/nl_NL/js/prism.css" type="text/css" media="screen" />
  <link rel="stylesheet" type="text/css" title="pod_stylesheet" href="/css/documentation.css">
  <script type="text/javascript" src="/tpl/zaak_v1/nl_NL/js/jquery-1.7.1.min.js"></script>
  <script type="text/javascript" src="/tpl/zaak_v1/nl_NL/js/prism.js"></script>
  <script type="text/javascript" src="/tpl/zaak_v1/nl_NL/js/jquery-ui-1.8.24.custom.min.js"></script>
  <script>
  var availableModules = |;

  $head   .= JSON::encode_json({ map { $_->{package} => $_ } @{ $self->_get_module_list({all => 1}) } });

  $head   .= q|;

  </script>
  <script type="text/javascript" src="/tpl/zaak_v1/nl_NL/js/documentation.js"></script>
</head>
<body class="pod">

<div class="man_header">
    <div class="man_header_inner cf">
        <h1 class="man_header_title"><span class="logo">Zaaksysteem.nl</span> developer documentation</h1>
        <input type="text" name="module" id="tags" size="150" placeholder="Zoek door de documentatie" class="man_autocomplete" />
    </div>
</div>
<div class="man_content">
<div class="man_article">
|;

    $head .= '<h1 class="man_page_title">' . $self->requested_object . '</h1>';

    return $self->html_header_after_title($head);
}

=head2 load_html_footer

Create the HTML footer for POD pages.

=cut

sub _load_toc_rows {
    my $self     = shift;
    my $modules  = shift;
    my $depth    = shift || 0;

    my $rv = '';
    for my $module (@$modules) {
        my $haschildren = (@{ $module->{children} } ? 1 : 0);

        my $active;
        if ($module->{package} eq $self->requested_object) {
            $active = 1;
        }

        if ($haschildren) {
            $rv .= '<li class="man_toc_category' . ($active ? ' active' : '') . '">';
            # $rv .= '<h2>';
            $rv .= '<h2>' if ($depth == 0);
            $rv .= '<a href="/man/' . $module->{package}. '">' . $module->{name} . '</a>';
            $rv .= '</h2>' if ($depth == 0);
            # $rv .= '</h2>';
            $rv .= '<ul>';
            $rv .= $self->_load_toc_rows($module->{children}, ($depth + 1));
            $rv .= '</ul>';
        } else {
            $rv .= '<li class="man_toc_module' . ($active ? ' active' : '') . '">';
            $rv .= '<a href="/man/' . $module->{package} . '">' . $module->{name} . '</a>';
            $rv .= '</li>';
        }
    }

    return $rv;
}

sub load_html_footer {
    my ($self)  = @_;

    my $footer  = qq{
        </div>
        <div class="man_toc_modules">
            <ul>
    };

    my $toc             = $self->_get_toc_list();
    $footer            .= $self->_load_toc_rows($toc);

    $footer     .= qq{
            </ul>
        </div>
        </div>
    };

    return $self->html_footer($footer);
}

sub _get_toc_list {
    my $self        = shift;
    my $modules     = shift;
    my $parent      = shift;

    if (!$modules) {
        my $tocmodules  = $self->_get_module_list();

        ### Sort by number of parts
        $modules        = [ sort { @{ $a->{parents} } <=> @{ $b->{parents} } } @$tocmodules ];
    }

    my (@rv, @skips);
    for my $module (@$modules) {
        my $name      = $module->{name};
        my $package   = $module->{package};

        next if grep { $package =~ /^$_\::/ } @skips;

        if ($parent) {
            next unless $package =~ /^$parent\::[^:]+$/;
        }

        ### Find all children
        my $children  = $self->_get_toc_list($modules, $package);
        $module->{children} = $children;

        push(@skips, $package);
        push(@rv, $module);
    }

    return \@rv;
}

my $cache = {};
sub _get_module_list {
    my $self        = shift;
    my $opts        = shift || {};

    my $cachelocation = $opts->{all} ? 'all' : 'public';

    return $cache->{$cachelocation} if $cache->{$cachelocation};

    my $index       = $self->index_list;

    if (!$opts->{all}) {
        $index      = { map { $_ => $index->{$_} } grep(/Zaaksysteem::Manual::/, keys %$index) };
    }

    my @modules;
    for my $package (keys %$index) {
        my $file    = $index->{$package};

        my @parts   = split(/::/, $package);
        my $depth   = scalar(@parts);

        my $name    = pop(@parts);

        push(
            @modules,
            {
                depth   => $depth,
                parents => \@parts,
                name    => $name,
                package => $package,
            }
        );
    }

    @modules = sort(
        { $a->{package} cmp $b->{package} || $a->{depth} <=> $b->{depth} }
        @modules
    );

    return $cache->{$cachelocation} = \@modules;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

=head1 UNDOCUMENTED FUNCTIONS

=head2 new

TODO: Fix me.

=cut

=head2 get_title

TODO: Fix me.

=cut
