package Zaaksysteem::Backend::Rules::ValidationResults;
use Moose;
use Zaaksysteem::Tools;
use Zaaksysteem::Backend::Rules::Integrity;
use JSON::Path;

with 'MooseX::Log::Log4perl';

use List::Util qw/first/;


=head1 NAME

Zaaksysteem::Backend::Rules::ValidationResults - Validation results for given rules

=head1 SYNOPSIS

=head1 ATTRIBUTES

=head2 actions

List of L<Zaaksysteem::Backend::Rules::Rule::Action> active for the given parameters. Loop of the actions to implement the changes

=cut

has 'actions'       => (
    is      => 'rw',
    isa     => 'Maybe[ArrayRef[Zaaksysteem::Backend::Rules::Rule::Action]]',
    default => sub { []; }
);

=head2 active_attributes

Convenient method to find the active attributes (or visible_fields)

=cut

has 'active_attributes'     => (
    is      => 'rw',
    isa     => 'ArrayRef',
    default => sub { []; }
);

=head2 inactive_attributes

Convenient method to find the inactive attributes (or empty fields)

=cut

has 'hidden_attributes'     => (
    is      => 'rw',
    isa     => 'ArrayRef',
    default => sub { []; }
);

=head2 hidden_groups

=cut

has 'hidden_groups'     => (
    is      => 'rw',
    isa     => 'ArrayRef',
    lazy    => 1,
    default => sub { return [] }
);

=head2 is_paused

=cut

has 'is_paused'     => (
    is      => 'rw',
    isa     => 'Boolean',
);

=head2 changes

=cut

has 'changes'       => (
    is      => 'rw',
    isa     => 'HashRef',
    default => sub { {}; },
);

has '_hidden_attributes_metadata' => (
    is      => 'rw',
    isa     => 'HashRef',
    default => sub { {}; },
);

=head1 INTERNAL METHODS

=cut

sub _remove_active_attributes {
    my $self        = shift;
    my @attributes  = @_;

    my @active_attributes = @{ $self->active_attributes };

    for my $attribute (@attributes) {
        @active_attributes = grep { $_ ne $attribute } @active_attributes;
    }

    return $self->active_attributes(\@active_attributes);
}

=head2 _add_hidden_attributes

    $self->_add_active_attributes('attribute.zt_kenmerk1', 'attribute.zt_kenmerk2');

Add one or more active attribute

=cut

sub _add_active_attributes {
    my $self        = shift;
    my @attributes  = @_;

    my @active_attributes   = (@{ $self->active_attributes }, @attributes);

    my @hidden_attributes   = @{ $self->hidden_attributes };

    ### Remove duplicates
    my %seen;
    @active_attributes   = grep { !$seen{$_}++ } @active_attributes;

    ### Remove active attributes from the hidden attributes
    @hidden_attributes   = grep { !$seen{$_} } @hidden_attributes;

    $self->hidden_attributes(\@hidden_attributes);
    return $self->active_attributes(\@active_attributes);
}

=head2 _add_hidden_attributes

    $self->_add_hidden_attributes('attribute.zt_kenmerk1', 'attribute.zt_kenmerk2');

Add one or more hidden attribute

=cut

sub _add_hidden_attributes {
    my $self        = shift;
    my @attributes  = @_;

    my @hidden_attributes   = (@{ $self->hidden_attributes }, @attributes);

    ### Remove duplicates
    my %seen;
    @hidden_attributes   = grep { !$seen{$_}++ } @hidden_attributes;

    return $self->hidden_attributes(\@hidden_attributes);
}



=head2 return_active_data

=cut

sub return_active_data {
    my $self            = shift;
    my %params          = %{ shift() };
    my $mapping         = shift;

    for my $key (keys %params) {
        my $attr = $mapping->{ $key };

        next if grep { 'attribute.' . $attr eq $_ } @{ $self->active_attributes };

        delete $params{$key};
    }

    return \%params;
}

=head2 process_params

    my $processed_values = $val_obj->process_params({
        engine      => $rules,                                  # The rule engine
        values      => { 2423 => "Heineken", 8912 => "Brand" }, # required, values to process
        keyformat   => 'bibliotheek_kenmerken_id',              # required, only supported: 'bibliotheek_kenmerken_id',
                                                                # tells what kind of value the "key" of the given parameters is

        set_values_except_for_attrs => [2423, 9923],
                                                                # will run the rule "vul_waarde_in" (SetValue) for all attribute keys
                                                                # except the ones given in this array.
    });

Will run the rule engine and validates it against the given values. It will C<undef> the hidden attributes, so
you will get empty values for "verberg kenmerk" rules.

When given the option C<set_values_except_for_attrs>, the processor will process all vul_waaarde_in rules for the
attributes which are NOT given in this option. In the above example, a "vul_waarde_in" rule could only affect kenmerk_id "8912",
because 2423 is "excepted"

=cut

define_profile process_params => (
    required    => {
        values          => 'HashRef',
        keyformat       => 'Str',
        engine          => 'Zaaksysteem::Backend::Rules',
    },
    optional => {
        set_values_except_for_attrs  => 'Any',  # ArrayRef...
    },
    defaults => {
        set_values_except_for_attrs => sub { return [] },
    }
);

sub process_params {
    my $self        = shift;
    my $params      = assert_profile(shift || {})->valid;

    my %values      = %{$params->{values}};
    my $attrmap     = $params->{engine}->_attribute_mapping;

    ### Wipe all hidden attributes
    if ($self->log->is_trace) {
        $self->log->trace(
            "Hidden attributes are: " . dump_terse($self->hidden_attributes));
    }
    my %hidden = map { $_ => 1 } @{ $self->hidden_attributes };
    for my $key (keys %values) {
        my $attrkey = $key;
        if ($params->{keyformat} eq 'bibliotheek_kenmerken_id') {
            if (exists $attrmap->{$key}) {
                $attrkey = 'attribute.' . $attrmap->{$key};
            }
            else {
                $self->log->trace("Attribute mapping does not have key $key");
                next;
            }
        }

        if ($hidden{$attrkey}) {
            $self->log->trace("Hiding $attrkey / $key from values");
            $values{$key} = undef;
        }
    }

    ### Fill changes in attributes
    my $attrmap_reversed = { map({ 'attribute.' . $attrmap->{$_} => $_ } keys %{ $attrmap }) };

    for my $attrkey (keys %{ $self->changes }) {
        my $key = $attrkey;
        if ($params->{keyformat} eq 'bibliotheek_kenmerken_id') {
            if ($attrkey =~ /^attribute\./) { # attrkey could be case.result for instance
                $key = $attrmap_reversed->{$attrkey};
            }
        }
        if (first { $key eq $_ } @{ $params->{set_values_except_for_attrs} }) {
            $self->log->trace("Skipping key $key , cannot set it according to set_values_except_for_attrs");
            next;
        }

        # Force hidden attributes
        if ($hidden{$attrkey}) {
            $self->log->trace("Hiding $attrkey / $key from values");
            $values{$key} = undef;
        }
        else {
            $self->log->trace("Setting $attrkey / $key from changes");
            $values{$key} = $self->changes->{$attrkey};
        }

    }

    return \%values;
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Backend::Rules> L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
