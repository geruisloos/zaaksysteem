package Zaaksysteem::Backend::Rules::Rule::Condition::Zipcode;

use Moose::Role;
use Zaaksysteem::Tools;

=head1 NAME

Zaaksysteem::Backend::Rules::Rule::Condition::Properties - Handles customer properties of casetype

=head1 SYNOPSIS

=head1 DESCRIPTION

=cut

has 'value_zipcode_from'   => (
    is  => 'rw',
    isa => 'Str',
);

has 'value_zipcode_to'   => (
    is  => 'rw',
    isa => 'Str',
);

after 'BUILD' => sub {
    my $self        = shift;

    if ($self->attribute eq 'aanvrager_postcode') {
        $self->values([$self->rules_params->{'case.requestor.zipcode'}]);

        $self->validation_type('fixed');
        $self->validates_true(0);

        my $zipcode = $self->_extract_zipcode;

        if(
            $zipcode &&
            $self->value_zipcode_from &&
            $self->value_zipcode_to
        ) {
            if($self->_zipcode_in_range({
                value   => $zipcode,
                from    => $self->value_zipcode_from,
                to      => $self->value_zipcode_to,
            })) {
                $self->validates_true(1);
            }
        }
    }
};

=head2 conditions

=cut

sub _extract_zipcode {
    my ($self, $opts) = @_;

    my $values = $self->values;

    return unless @$values;

    my ($potential_bag_value) = @$values;

    my $dispatch = {
        '^nummeraanduiding-(\d+)$' => sub {
            my $nummeraanduiding = $self->_schema->resultset('BagNummeraanduiding')->find({
                    identificatie => $1
            });

            return $nummeraanduiding->postcode if $nummeraanduiding;
            return;
        },
        # plain Dutch zipcode
        '^([1-9][0-9]{3}[a-zA-Z]{2})$'     => sub {
            $1
        }
    };

    # execute regex keys, if we have a hit, execute its sub to retrieve the matching zip
    map {
        return $dispatch->{$_}->()
            if $potential_bag_value =~ m|$_|s;
    } keys %$dispatch;

    return;
}

sub _zipcode_in_range {
    my ($self, $opts) = @_;

    my $value   = $opts->{value}    or die "need value";
    my $to      = $opts->{to}       or die "need to";
    my $from    = $opts->{from}     or die "need from";

    my $value_numeric   = $self->_zipcode_to_number({ zipcode => $value });
    my $to_numeric      = $self->_zipcode_to_number({ zipcode => $to    });
    my $from_numeric    = $self->_zipcode_to_number({ zipcode => $from  });

    return
        $value_numeric >= $from_numeric &&
        $value_numeric <= $to_numeric;
}

sub _zipcode_to_number {
    my ($self, $opts) = @_;

    my $zipcode = $opts->{zipcode} or die "need zipcode";

    $zipcode = uc($zipcode);

    die "incorrect zipcode"
        unless $zipcode =~ m|^[1-9][0-9]{3}[A-Z]{2}$|;

    # ord = numeric value of a char.
    my $base = ord('A');

    $zipcode =~ s|
        ([A-Z])
        |
        sprintf("%02d", ord($1)-$base)
    |gsex;

    return int $zipcode;
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Backend::Rules> L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
