package Zaaksysteem::Backend::Groups::ResultSet;

use Moose;

use Zaaksysteem::Tools;
use Clone qw(clone);

extends 'Zaaksysteem::Backend::ResultSet';

=head1 NAME

Zaaksysteem::Backend::Groups::ResultSet - Convenient functions for Groups

=head1 SYNOPSIS

    # See USAGE tests:
    # TEST_METHOD="groups_usage.*" ./zs_prove -v t/testclass/100-general.t

=head1 DESCRIPTION

Groups within zaaksysteem functions

=head1 REQUIRED ATTRIBUTES

=head1 METHODS

=head2 create_group

Arguments: \%options

Return value: $COMPONENT_GROUP (Component of Group)

    my $group = $self->schema->resultset('Groups')->create_group(
        {
            name            => "Mintlab",
            description     => "Mintlab Test Organization",
        }
    );

    my $child = $self->schema->resultset('Groups')->create_group(
        {
            name            => "Backoffice",
            description     => "Mintlab Backoffice",
            parent_group_id => $group->id,
        }
    );

Create a group in our database. First group needs to be the organization.

B<Options>

=over 4

=item name [required]

isa: Str

Short name of the group, e.g. C<Backoffice>

=item description

isa: Str

Description of the group, e.g. C<Where all the magic is created>

=item parent_group_id

isa: NUMBER

Id of the parent_group to place this group in.

=back

=cut

define_profile create_group     => (
    required            => [qw/name/],
    optional            => [qw/description parent_group_id override_id/],
    constraint_methods  => {
        parent_group_id     => qr/^\d+$/,
        override_id         => qr/^\d+$/,
    }
);

sub create_group {
    my $self            = shift;
    my $params          = assert_profile(shift || {})->valid;
    my $schema          = $self->result_source->schema;

    my @cols            = qw/description name parent_group_id/;

    ### TODO
    my @parent_ids;
    if ($params->{parent_group_id}) {
        my $parent = $self->find($params->{parent_group_id});

        throw(
            'groups/create_group/invalid_parent_group_id',
            'Cannot find parent row by given id: ' . $params->{parent_group_id}
        ) unless $parent;

        @parent_ids = @{ $parent->path };
    }

    my $path_string = 'ARRAY[' . join(',', @parent_ids, "currval('groups_id_seq')") . ']';
    my $id_string   = ($params->{override_id} ? "setval('groups_id_seq', " . $params->{override_id} . ')' : "nextval('groups_id_seq')");

    my $group           = $self->create(
        {
            id          => \$id_string,
            name        => $params->{name},
            description => $params->{description},
            path        => \$path_string,
        }
    );

    if ($group) {
        if ($params->{override_id}) {
            ### Make sure any counters get reset to correct value
            $self->result_source->schema->storage->dbh_do(
                sub {
                    my ($storage, $dbh) = @_;
                    $dbh->do("SELECT setval('groups_id_seq', (SELECT MAX(id) FROM groups));");
                }
            );
        }
        return $group->discard_changes();
    } else {
        return;
    }
}

=head2 set_primary_groups_on_subject

Arguments: $COMPONENT_SUBJECT, \@ARRAYREF_OF_GROUP_IDS

Return value: $SUBJECT_COMPONENT

    $groups->set_primary_groups_on_subject($subject, [$schema->resultset('Groups')->first->id]);

    $groups->set_primary_groups_on_subject($subject, [21]);

Sets the primary groups for this users. Currently, only one value is allowed.

=cut

sub set_primary_groups_on_subject {
    my $self            = shift;
    my $subject         = shift;
    my @groups          = @{ shift() };
    my $schema          = $self->result_source->schema;


    throw(
        'groups/set_primary_group_on_subject/not_one_group',
        'Need an arrayref of exactly one group id'
    ) unless scalar @groups == 1;

    throw(
        'groups/set_primary_group_on_subject/not_a_number',
        'Only group IDs allowed'
    ) for grep { $_ !~ /^\d+$/ } @groups;

    my $count           = $schema->resultset('Groups')->search(
        {
            id  => \@groups
        }
    )->count;

    if ($count != scalar @groups) {
        throw(
            'groups/set_primary_group_on_subject/groups_not_found',
            'One or more given group ids not found in database'
        );
    }

    $subject->group_ids(\@groups);
    $subject->update;
    return $subject->discard_changes;
}

=head2 groups_for_subject

Arguments: $COMPONENT_SUBJECT

Return value: \%RETURN VALUE

    my $groups = $groups->groups_for_subject($subject);

    # Prints
    {
        primary_groups      => [$group_component],
        inherited_groups    => [$parent_group1, $parent_group2],
    }

Returns a hash containing the primary group and groups we inherit rights from.

=cut

sub groups_for_subject {
    my $self            = shift;
    my $subject         = shift;

    my $rv              = {
        'primary_groups'        => [],
        'inherited_groups'      => [],
    };

    return $rv unless $subject->group_ids && @{ $subject->group_ids };

    throw(
        'groups/set_primary_group_on_subject/not_one_group',
        'Only a single group supported in zaaksysteem for now'
    ) unless scalar @{ $subject->group_ids } == 1;

    ### First, retrieve the primary group
    my $primary_group_id    = $subject->group_ids->[0];

    my $primary_group       = $self->search({ id => $primary_group_id})->first;

    return $rv unless $primary_group;

    my $group_path_string   = 'ARRAY[' . join(',', @{ $primary_group->path }) . ']';

    my @groups              = $self->search(\['ARRAY[id] && ' . $group_path_string])->all;

    for my $group (@groups) {
        if ($group->id eq $primary_group_id) {
            push(
                @{ $rv->{primary_groups} },
                $group
            );
            next;
        }

        push(
            @{ $rv->{inherited_groups} },
            $group
        );
    }

    return $rv;
}


=head2 get_all_cached

Arguments: [ $STASH ]

    my $stash   = $c->stash

    my $groups  = $schema->resultset('Groups')->get_all_cached($stash);

Will retrieve _all_ groups, when stash is given, it will make sure it will load
only once, and set it on the stash key C<__cache_groups_all>. When it is already
set, it will return the value without a call to the database

TODO TESTS

=cut

sub get_all_cached {
    my $self        = shift;
    my $hash        = shift || {};

    return clone($hash->{__cache_groups_all}) if $hash->{__cache_groups_all};

    $hash->{__cache_groups_all} = [
        $self->search(
            {},
            {
                order_by    => { '-asc' => [\"array_length(me.path, 1)", 'name'] },
            }
        )->all
    ];

    return $hash->{__cache_groups_all};
}

=head2 get_as_tree

Arguments: [ $STASH, \%OPTIONS ]

Get all groups in tree representation, combined with available roles in tree

=cut

sub get_as_tree {
    my $self        = shift;
    my $cachehref   = shift;
    my $groups      = $self->get_all_cached($cachehref);

    return [] unless $groups && @{ $groups };

    my $root        = $groups->[0];

    my $roles       = $self->result_source->schema->resultset('Roles')->get_all_cached($cachehref);
    
    return $self->_load_tree($groups, $root->id, 0, $roles);
}

sub _load_tree {
    my $self        = shift;
    my $groups      = shift;
    my $index       = shift;
    my $depth       = shift;
    my $roles       = shift;

    my @leafs       = grep { $_->path->[$depth] && $_->path->[$depth] == $index && scalar(@{ $_->path }) == ($depth+1)} @$groups;

    my $tree        = [];
    for my $leaf (@leafs) {
        my $rv = $leaf->TO_JSON;
        $rv->{children}     = [];

        ### Add roles
        $rv->{roles}        = [ map {
            $_->TO_JSON
        } grep { $_->get_column('parent_group_id') && $_->get_column('parent_group_id') == $leaf->id } @$roles ];

        my @children = grep { $_->path->[$depth] && $_->path->[$depth] == $leaf->id && $_->id != $leaf->id && scalar(@{ $_->path }) == ($depth+2) } @$groups;
        for my $child (@children) {
            push (
                @{ $rv->{children} },
                @{ $self->_load_tree($groups, $child->id, ($depth+1), $roles) }
            );
        }

        push(@$tree, $rv);
    }

    return $tree;
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Install>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
