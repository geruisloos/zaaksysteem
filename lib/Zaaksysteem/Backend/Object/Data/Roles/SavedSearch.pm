package Zaaksysteem::Backend::Object::Data::Roles::SavedSearch;
use Moose::Role;

=head1 NAME

Zaaksysteem::Backend::Object::Data::Roles::SavedSearch

=head1 DESCRIPTION

Role for "saved search" objects.

=head1 METHODS

=head2 text_vector_terms

Override, so only the saved search *title* is stored in the text vector.

Putting all of the JSON in there gets crowded (and overloads the full-text
search engine in PostgreSQL) fast.

=cut

override text_vector_terms => sub {
    my $self = shift;

    my ($title_attr) = grep { $_->name eq 'title' } @{ $self->object_attributes };

    return (
        $title_attr->vectorize_value
    );
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

