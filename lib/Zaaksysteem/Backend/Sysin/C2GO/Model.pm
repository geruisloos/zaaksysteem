package Zaaksysteem::Backend::Sysin::C2GO::Model;
use Moose;

extends 'Zaaksysteem::Backend::Sysin';
with 'Zaaksysteem::Backend::Sysin::Roles::UA';
with 'Zaaksysteem::Backend::Sysin::Roles::Zorginstituut';

=head1 NAME

Zaaksysteem::Backend::Sysin::C2GO::Model

=head1 SYNOPSIS

    use Zaaksysteem::Backend::Sysin::C2GO::Model;

    my $interface = $zs->schema->resultset('Interface')
        ->search_active({ module => 'c2go' })->first;

    my $c2go = Zaaksysteem::Backend::Sysin::C2GO::Model->new_from_interface($interface);

    # Send iWMO 2.0 message to C2GO
    $c2go->get_oauth_token;
    $c2go->send_wmo_301_message($case);



=cut

use JSON::XS qw(decode_json encode_json);

use Zaaksysteem::Tools;
use Zaaksysteem::XML::Compile;
use MIME::Base64 qw(decode_base64);

has tenant => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has token => (
    is     => 'ro',
    isa    => 'Str',
    writer => '_set_oauth_token',
);

=head2 get_oauth_token

    $self->get_oauth_token;

B<Return value>: The token as a scalar.

Gets and sets the OAuth token for C2GO. You van later get the token via C<token>

=cut

sub get_oauth_token {
    my $self = shift;
    return $self->token if $self->token;

    my $response = $self->_ua_post(
        $self->endpoints->{oauth},
        {
            grant_type => 'password',
            username   => $self->username . '@' . $self->tenant,
            password   => $self->password,
        }
    );

    my $json = $self->parse_json_response($response);
    $self->_set_oauth_token($json->{access_token});
    return $self->token;
}

=head2 call

Make an API call to C2GO

=cut

define_profile call => (
    required => {
        endpoint => 'Str',
        message  => 'Defined',
    },
);

sub call {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    $self->assert_endpoint($opts->{endpoint});

    if ($self->log->is_trace) {
        $self->log->trace(
            "Calling C2GO $opts->{endpoint} with data " . ref $opts->{message}
            ? dump_terse($opts->{message})
            : $opts->{message}
        );
    }

    $self->get_oauth_token();

    return $self->parse_json_response(
        $self->ua->post(
            $opts->{endpoint},
            Authorization => "Bearer " . $self->token,
            Content_Type => 'application/json',
            Content       => $opts->{message},
        )
    );
}

define_profile _send_301_message => (
    required => {
        endpoint => 'Str',
        xml      => 'Str',
    },
);

sub _send_301_message {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    my $json = $self->call(endpoint => $opts->{endpoint}, message => $opts->{xml});
    return scalar decode_base64($json->{Content});
}

sub _ua_post {
    my ($self, $url, $data) = @_;

    $self->log->trace(
        sprintf("Calling %s with data %s", $url, dump_terse($data)));

    return $self->ua->post(
        $url,
        Content_Type => 'application/x-www-form-urlencoded',
        Content      => $data,
    );
}

=head1 BUILDERS

=head2 _build_endpoints

Build the endpoints for oath and message type based on the interface configuration.

=cut

sub _build_endpoints {
    my $self = shift;
    return {
        oauth   => $self->interface->get_interface_config->{endpoint_oauth},
        message => $self->interface->get_interface_config->{endpoint_message},
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
