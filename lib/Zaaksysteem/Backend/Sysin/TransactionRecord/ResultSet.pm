package Zaaksysteem::Backend::Sysin::TransactionRecord::ResultSet;

use Moose;

use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

extends 'DBIx::Class::ResultSet';

Params::Profile->register_profile(
    method  => 'transaction_record_create',
    profile => {
        required => [qw/
            transaction_id
            input
        /],
        optional => [qw/
            output
            is_error
            date_executed
        /],
        constraint_methods => {
            transaction_id         => qr/^\d+$/,
        },
    }
);

sub transaction_record_create {
    my $self   = shift;
    my $opts   = assert_profile(
        {
            %{ $_[0] },
            schema  => $self->result_source->schema,
        },
    )->valid;

    return $self->create($opts);
}


1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 transaction_record_create

TODO: Fix the POD

=cut

