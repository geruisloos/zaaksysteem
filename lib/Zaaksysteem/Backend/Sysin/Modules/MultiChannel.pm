package Zaaksysteem::Backend::Sysin::Modules::MultiChannel;

use Moose;

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

use Zaaksysteem::Tools;

use LWP::UserAgent;
use HTTP::Request;
use XML::XPath;
use URI;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
    Zaaksysteem::Backend::Sysin::Modules::Roles::Tests
/;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::MultiChannel - Integration module for
DataB's MultiChannel platform

=head1 DESCRIPTION

=head1 SYNOPSIS

=head1 CONSTANTS

=head2 INTERFACE_ID

C<qmatic>

=cut

use constant INTERFACE_ID               => 'multichannel';

=head2 INTERFACE_CONFIG_FIELDS

C<ArrayRef[Zaaksysteem::ZAPI::Form::Field]>

=cut

use constant INTERFACE_CONFIG_FIELDS    => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_endpoint',
        type => 'text',
        label => 'URL MultiChannel Documentserver',
        required => 1,
        description => 'Configureer de endpoint URL voor de documentserver'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_cid',
        type => 'text',
        label => 'DataB CID',
        required => 1,
        description => 'Klantcode voor het gebruik van de DataB API'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_secret',
        type => 'text',
        label => 'DataB sleutel',
        required => 1,
        description => 'De geheime sleutel om veilig met DataB te communiceren'
    )
];

=head2 MODULE_SETTINGS

C<HashRef>

=cut

use constant MODULE_SETTINGS            => {
    name                            => INTERFACE_ID,
    label                           => 'DataB MultiChannel DMS',
    interface_config                => INTERFACE_CONFIG_FIELDS,
    direction                       => 'outgoing',
    manual_type                     => [],
    is_multiple                     => 0,
    is_manual                       => 0,
    retry_on_error                  => 0,
    allow_multiple_configurations   => 0,
    is_casetype_interface           => 0,
    trigger_definition  => {
        get_session_url => { method => 'get_session_url', update => 1 }
    },

    test_interface                  => 1,
    test_definition                 => {
        description => qq{
            Om te controleren of de applicatie goed geconfigureerd is, kunt u
            hieronder een aantal tests uitvoeren. Hiermee controleert u de verbinding
            van uw profiel.
        },

        tests => [
            {
                id => 1,
                label => 'Test verbinding',
                name => 'connection_test',
                method => 'test_connection',
                description => 'Test verbinding naar profiel URL'
            }
        ],
    },
};

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};

=head1 ATTRIBUTES

=head2 user_agent

Holds a reference to a L<LWP::UserAgent> instance to be used for connecting
with the document server. Built via L</build_user_agent>.

=cut

has user_agent => (
    is => 'rw',
    isa => 'LWP::UserAgent',
    lazy => 1,
    builder => 'build_user_agent'
);

=head1 TRIGGERS

=head2 get_session_url

The main trigger for this module, requests a new session URL from the
document server and returns it.

=head3 Usage

    my $interface = ...;    # The 'multichannel' interface component
    my $subject = ...;      # Betrokkene object
    my $client_ip = ...;    # DataB does some sanity checks using the connecting IP

    my $url = $interface->process_trigger('get_session_url', {
        subject => $subject,
        client_ip => $client_ip
    });

=head3 Exceptions

=over 4

=item sysin/multichannel/request_failed

Failure to connect to the MultiChannel Documentserver

=item sysin/multichannel/request_unsuccessful

MultiChannel Documentserver responded with an unsuccessful status

=back

=cut

define_profile get_session_url => (
    required => {
        subject => 'Zaaksysteem::Betrokkene::Object',
        client_ip => 'Str'
    }
);

sub get_session_url {
    my $self = shift;
    my $params = assert_profile(shift)->valid;
    my $interface = shift;

    my $ua = $self->user_agent;

    my $res = $ua->request(
        $self->new_session_request($interface, $params)
    );

    unless ($res->is_success) {
        throw('sysin/multichannel/request_failed', sprintf(
            'MultiChannel session URL request failed: %s',
            $res->status_line
        ));
    }

    my $xpath = XML::XPath->new(xml => $res->decoded_content);

    unless ($xpath->findvalue('/docserver/response/succes') eq 'true') {
        throw('sysin/multichannel/request_unsuccessful', sprintf(
            'MultiChannel session URL request did not succeed: %s',
            $xpath->findvalue('/docserver/response/error/@description')->value
        ))
    }

    return $xpath->findvalue('/docserver/response/url');
}

=head1 METHODS

=head2 build_user_agent

Helper method for constructing a L<LWP::UserAgent> instance.

=cut

sub build_user_agent {
    my $self = shift;

    my $ua = LWP::UserAgent->new;
    
    $ua->agent('Zaaksysteem');

    return $ua;
}

=head2 new_session_request

Helper method for constructing a MultiChannel Authentication request. Returns
an instance of L<HTTP::Request> for that purpose.

=cut

sig new_session_request => 'Zaaksysteem::Backend::Sysin::Interface::Component, HashRef';

sub new_session_request {
    my $self = shift;
    my $interface = shift;
    my $params = shift;

    my $req = HTTP::Request->new('GET');

    my $uri = URI->new($interface->jpath('$.endpoint'));

    $uri->query_form({
        cid => $interface->jpath('$.cid'),
        secret => $interface->jpath('$.secret'),
        request => 'authenticate',
        clientip => $params->{ client_ip },
        uid => sprintf('%09d', $params->{ subject }->burgerservicenummer)
    });

    $req->uri($uri);

    return $req;
}

=head2 test_connection

Hook for the Sysin function testing interface. This method will attempt to
connect to the given endpoint. Checks are do to ensure the endpoint is SSL
encryted.

=cut

sub test_connection {
    my ($self, $interface) = @_;

    return $self->test_host_port_ssl($interface->jpath('$.endpoint'));
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
