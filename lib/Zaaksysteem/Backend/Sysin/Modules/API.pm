package Zaaksysteem::Backend::Sysin::Modules::API;

use Moose;

use Zaaksysteem::Tools;
use Zaaksysteem::Types qw(UUID);

use JSON;
use JSON::Path qw[];

use Moose::Util::TypeConstraints qw[union];

use Zaaksysteem::ZAPI::Form::Field;
use Zaaksysteem::ZAPI::Form;

use Zaaksysteem::API::v1::Serializer;
use Zaaksysteem::API::v1::Message::Ping;

use LWP::UserAgent;
use HTTP::Headers;
use HTTP::Request;

extends 'Zaaksysteem::Backend::Sysin::Modules';

# I'm cargo-culting the processor role here... anyone know why that's
# realy needed?
with qw[
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
    Zaaksysteem::Backend::Sysin::Modules::Roles::Tests
];

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::API

=cut

use constant INTERFACE_ID => 'api';

use constant INTERFACE_CONFIG_FIELDS => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_api_version',
        type        => 'select',
        label       => 'Versie',
        required    => 1,
        description => 'Selecteer de gewenste API versie.',
        data        => {
            options => [
                { value => 'v1', label => 'API v1' }
            ]
        }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_api_key',
        type        => 'text',
        label       => 'API sleutel',
        required    => 1,
        description => 'Interface key voor externe koppeling',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name     => 'interface_medewerker',
        type     => 'spot-enlighter',
        label    => 'Medewerker',
        required => 1,
        description => 'Bepaal hier welke medewerkerrechten de externe partij krijgt.',
        data => {
            restrict    => 'contact/medewerker',
            placeholder => 'Type uw zoekterm',
            label       => 'naam',
        }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_access',
        type => 'select',
        label => 'Toegangsniveau',
        required => 1,
        description => 'Het toegangsniveau bepaalt of de API gebruiker alleen gegevens mag raadplegen, of mogelijk ook mag wijzigen. Deze instelling werkt globaal over de API, dus zelfs als de ingestelde medewerker schrijfrechten heeft op een bepaald object, maar deze instelling op "Raadplegen" staat, zal de API gebruiker niet kunnen schrijven naar het Zaaksysteem.',
        data => {
            options => [
                { value => 'ro', label => 'Raadplegen' },
                { value => 'rw', label => 'Behandelen' }
            ]
        }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_query_constraint',
        type => 'spot-enlighter',
        label => 'Zoekopdracht',
        required => 0,
        description => 'Door een hier een opgeslagen zoekopdracht te specificeren kan bepaald worden welke objecten de externe partij kan zien en/of gebruiken',
        data => {
            restrict => 'objects',
            placeholder => 'Type uw zoekterm',
            label => 'label',
            params => { object_type => 'saved_search' }
        }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_trigger_endpoint',
        type => 'text',
        label => 'Trigger URL',
        description => 'Voer hier een URL in waarnaar het Zaaksysteem zaak regelactie triggers kan sturen'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_trigger_cert',
        type => 'file',
        label => 'Trigger CA Certificaat',
        description => 'Upload hier het CA certificaat van de server waarnaar triggers verstuurd worden.'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_api_uri',
        type => 'display',
        label => 'API URI',
        description => 'De genoteerde URI is de locatie waar de externe partij met het Zaaksysteem kan communiceren',
        required => 0,
        data => {
            template => '<a href="<[field.value]>" target="_blank"><[field.value]></a>'
        }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_powershell_example',
        type => 'display',
        label => 'Example: CSV via Powershell',
        description => 'Onderstaand powershell script kan gebruikt worden voor het downloaden van een CSV via deze API',
        required => 0,
        data => {
            template => '<pre><[field.value]></pre>'
        }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_curl_example',
        type => 'display',
        label => 'Example: CSV via Curl',
        description => 'Onderstaand curl script kan gebruikt worden voor het downloaden van een CSV via deze API',
        required => 0,
        data => {
            template => '<pre><[field.value]></pre>'
        }
    ),

];

use constant MODULE_SETTINGS => {
    name                          => INTERFACE_ID,
    label                         => 'Zaaksysteem API',
    interface_config              => INTERFACE_CONFIG_FIELDS,
    direction                     => 'incoming',
    manual_type                   => ['text'],
    module_type                   => ['apiv1', 'api'],
    is_multiple                   => 1,
    is_manual                     => 1,
    retry_on_error                => 0,
    allow_multiple_configurations => 1,
    is_casetype_interface         => 0,
    has_attributes                => 0,
    test_interface  => 1,

    test_definition => {
        description => 'Hier kunt u de verbinding met de externe interface testen.',

        tests => [
            {
                id => 1,
                label => 'Test verbinding',
                name => 'test_connection',
                method => 'test_connection',
                description => 'Zaaksysteem zal proberen verbinding naar de externe partij op te zetten.'
            },
            {
                id => 2,
                label => 'Test bericht',
                name => 'test_delivery',
                method => 'test_delivery',
                description => 'Zaaksysteem zal proberen een bericht naar de externe partij te sturen.'
            }
        ]
    },

    trigger_definition  => {

        log_mutation => {
            method  => 'log_mutation',
        },

        post_message => {
            method => 'post_message',
            update => 1
        }
    },
};

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig(%{MODULE_SETTINGS()});
};

has 'api_supported_scope' => (
    is      => 'ro',
    default => sub {
        return ['case']
    }
);

=head2 _load_values_into_form_object

=cut

around _load_values_into_form_object => sub {
    my $orig        = shift;
    my $self        = shift;
    my $opts        = $_[1]; # get options

    my $form        = $self->$orig(@_);
    my $interface   = $opts->{entry};

    ### Powershell
    $form->load_values({
        interface_api_uri => $opts->{ base_url } . 'api/v1/',
        interface_powershell_example => $self->_load_value_for_examples(
            {
                output      => 'powershell',
                type        => 'csv',
                base_url    => $opts->{ base_url },
                entry       => $opts->{ entry },
            }
        ),
        interface_curl_example => $self->_load_value_for_examples(
            {
                output      => 'curl',
                type        => 'csv',
                base_url    => $opts->{ base_url },
                entry       => $opts->{ entry },
            }
        )
    });

    return $form;
};

=head2 post_message

This method is an interface trigger that handles posting messages to external
systems. The only expected argument is an object instance that
L<Zaaksysteem::API::v1::Serializer> can serialize.

=cut

define_profile post_message => (
    required => {
        object => 'Object'
    }
);

sub post_message {
    my $self = shift;
    my $params = assert_profile(shift)->valid;
    my $interface = shift;

    my $serializer = Zaaksysteem::API::v1::Serializer->new;

    my $payload = try { $serializer->serialize($params->{ object }) };

    return unless $payload;

    return $interface->process({
        input_data => sprintf(
            "Trigger endpoint: %s\n\n-----BEGIN PAYLOAD-----\n%s\n-----END PAYLOAD-----",
            $interface->jpath('$.trigger_endpoint'),
            $payload
        ),

        direction => 'outgoing',

        processor_params => {
            processor => '_process_post_message',
            payload => $payload
        }
    });
}

=head2 _process_post_message

This private method implements the actual logic for L</post_message>.

=cut

sub _process_post_message {
    my $self = shift;
    my $record = shift;

    my $txn = $self->process_stash->{ transaction };

    my $params = $txn->get_processor_params;
    my $interface = $txn->interface;

    my $agent = $self->get_trigger_agent($interface);

    my $request = HTTP::Request->new('POST', $interface->jpath('$.trigger_endpoint'));

    $request->content($params->{ payload });

    return $agent->request($request);
}

=head2 test_connection

This test case tests a configured trigger endpoint for connectability and
SSL certificate checks.

=cut

sub test_connection {
    my $self = shift;
    my $interface = shift;

    my $url = $interface->jpath('$.trigger_endpoint');

    unless ($url) {
        throw('api/v1/trigger/test_connection', 'Geen trigger URL geconfigureerd.');
    }

    my $cert = try { $self->get_trigger_cert($interface)->get_path };

    return $self->test_host_port_ssl($url, $cert);
}

=head2 test_delivery

This method executes a message delivery test to the remotely configured
trigger endpoint.

=cut

sub test_delivery {
    my $self = shift;
    my $interface = shift;

    my $url = $interface->jpath('$.trigger_endpoint');

    unless ($url) {
        throw('api/v1/trigger/test_delivery', 'Geen trigger URL geconfigureerd.');
    }

    my $agent = $self->get_trigger_agent($interface);

    my $request = HTTP::Request->new('POST', $url);
    my $serializer = Zaaksysteem::API::v1::Serializer->new;
    my $ping = Zaaksysteem::API::v1::Message::Ping->new;

    my $instance_data = $serializer->read($ping);

    my $data = {
        api_version => $interface->jpath('$.api_version'),
        message => $instance_data
    };

    $request->content($serializer->encode($data));

    my $response = $agent->request($request);

    unless ($response->is_success) {
        throw('api/v1/trigger/test_delivery', sprintf(
            'Kon bericht niet sturen: %s.', $response->status_line
        ));
    }

    my $response_data = JSON->new->utf8->decode($response->decoded_content);
    my $jpath = JSON::Path->new('$.result.instance.payload');

    unless ($instance_data->{ instance }{ payload } eq $jpath->value($response_data)) {
        throw('api/v1/trigger/payload_mismatch', sprintf(
            'Bericht resulteerde in incorrecte reactie.'
        ));
    }
}

=head2 get_trigger_agent

Shortcut method to build an L<LWP::UserAgent> instance configured for
calling external systems.

=cut

sub get_trigger_agent {
    my $self = shift;
    my $interface = shift;

    my $cert = $self->get_trigger_cert($interface);

    my $agent_string = sprintf(
        'Zaaksysteem/%s ZSAPI/%s',
        $Zaaksysteem::VERSION,
        $interface->jpath('$.api_version')
    );

    my $headers = HTTP::Headers->new(
        Content_Type => 'application/json'
    );

    my $agent = LWP::UserAgent->new(
        agent => $agent_string,
        default_headers => $headers,
        timeout => 10
    );

    if ($cert) {
        $agent->ssl_opts(
            verify_hostname => 1,
            SSL_ca_file => $cert->get_path
        );
    }

    return $agent;
}

=head2 get_trigger_cert

Shortcut method to retrieve the
L<file|Zaaksysteem::Backend::Filestore::Component> instance for the possibly
configured 'trigger_cert' attribute.

=cut

sub get_trigger_cert {
    my $self = shift;
    my $interface = shift;

    my $filestore_id = $interface->jpath('$.trigger_cert[0].id');

    return unless $filestore_id;

    my $schema = $interface->result_source->schema;

    return $schema->resultset('Filestore')->find($filestore_id);
}

sub log_mutation {
    my ($self, $params, $interface) = @_;

    my $transaction = $interface->process({
            external_transaction_id => $params->{request_id},
            input_data              => $self->create_input_data($params),
            processor_params        => {
                processor       => '_process_log_mutation',
                request_body    => $params->{request_body},
                request_method  => $params->{request_method},
                request_call    => $params->{request_call},
                client_ip       => $params->{client_ip}
            },
        },
    );

    return $transaction;
}

sub create_input_data {
    my $self        = shift;
    my $params      = shift;

    return 'Client IP: ' . $params->{client_ip} . "\n"
        .'Request method: ' . $params->{request_method} . "\n"
        .'Request API call: ' . $params->{request_call} . "\n"
        ."Success: UNKNOWN\n"
        ."Request body:\n" . $params->{request_body};
}

sub _process_log_mutation {
    my ($self, $record) = @_;

    my $transaction = $self->process_stash->{transaction};
    my $params      = $transaction->get_processor_params();


    my $interface   = $transaction->interface;
    my $config      = $interface->get_interface_config;

    $record->input($self->create_input_data($params));
}

=head2 _load_value_for_examples

    my $string = $self->_load_value_for_examples(
        {
            entry   => $interface,      # required L<Zaaksysteem::Backend::Sysin::Interface::Component>
            type    => 'csv',           # ENUM: csv
            output  => 'powershell'     # ENUM: powershell / curl
        }
    );

    # $string contains:
    # "powershell blabla"

B<Return value>: String

Returns the powershell command for calling C<$type> on this API. Only supported type is "csv" for now.


=cut

define_profile _load_value_for_examples => (
    required    => {
        type        => 'Str',
        entry       => 'Zaaksysteem::Backend::Sysin::Interface::Component',
        base_url    => 'Str',
        output      => 'Str',
    }
);

sub _load_value_for_examples {
    my $self        = shift;
    my $opts        = assert_profile(shift || {})->valid;
    my $rv          = '';

    my $config      = $opts->{entry}->get_interface_config;

    if ($opts->{type} eq 'csv') {
        if ($opts->{output} eq 'powershell') {
            $rv .= 'powershell -ExecutionPolicy unrestricted -Command "';
            $rv .= '$cmd = (New-Object Net.WebClient); ';
            $rv .= '$cmd.Headers.Add(\"API-Interface-Id\",\"' . $opts->{entry}->id . '\"); ';
            $rv .= '$cmd.Headers.Add(\"API-Key\",\"' . $config->{api_key} . '\"); ';
            $rv .= '$cmd.DownloadString(\"' . $opts->{base_url} . 'app/api2csv\");'
        }

        if ($opts->{output} eq 'curl') {
            $rv .= 'curl ';
            $rv .= '--header "API-Interface-Id: ' . $opts->{entry}->id . '" ';
            $rv .= '--header "API-Key: ' . $config->{api_key} . '" ';
            $rv .= '"' . $opts->{base_url} . 'app/api2csv"';
        }
    }

    return $rv;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016 Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 log_mutation

TODO: Fix the POD

=head2 create_input_data

TODO: Fix the POD

=cut
