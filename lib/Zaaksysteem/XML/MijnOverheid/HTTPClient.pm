package Zaaksysteem::XML::MijnOverheid::HTTPClient;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::XML::HTTPClient';

use DateTime;
use Data::UUID;
use Zaaksysteem::Tools;

=head1 NAME

Zaaksysteem::XML::MijnOverheid::HTTPClient - MijnOverheid XML-over-HTTP client

=head1 SYNOPSIS

    my $client = Zaaksysteem::XML::MijnOverheid::HTTPClient->new(
        sender => 'OIN',
        call_urls => {
            has_berichtenbox => 'https://example.com/has_berichtenbox',
            berichtenbox_message => 'https://example.com/berichtenbox_message',
            lopende_zaak => 'https://example.com/lopende_zaak',
        },
    );

    $client->has_berichtenbox(bsn => '012345678');

=head1 ATTRIBUTES

=head2 sender

The "sender" to use in the message. This is used for identification purposes by
OpenTunnel.

=cut

has sender => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 call_urls

Configures the URLs used for the different calls to OpenTunnel.

This is a hash reference with three keys, one per supported call:

    {
        has_berichtenbox => 'https://example.com/has_berichtenbox',
        berichtenbox_message => 'https://example.com/berichtenbox_message',
        lopende_zaak => 'https://example.com/lopende_zaak',
    },

=cut

has call_urls => (
    is       => 'ro',
    isa      => 'HashRef',
    required => 1,
);

=head2 _build_call_mapping

Overrides the builder of the L<Zaaksysteem::XML::HTTPClient> C<call_mapping> attribute.

This version build the call_mapping from the C<call_urls> attribute.

=cut

override _build_call_mapping => sub {
    my $self = shift;

    my %mapping;
    for my $call (qw(has_berichtenbox berichtenbox_message lopende_zaak)) {
        $mapping{$call} = {
            request  => $call,
            response => "${call}_response",
            url      => $self->call_urls->{$call},
        };
    }

    return \%mapping;
};

=head2 _header

Creates the header data structure that's common to all OpenTunnel messages.

=cut

sub _header {
    my $self = shift;

    my $uuid = Data::UUID->new->create_str();

    return (
        header => {
            timestamp  => DateTime->now()->iso8601 . 'Z',
            identifier => $uuid,
            sender     => $self->sender,
        },
    );
}

=head2 has_berichtenbox

Perform a "has_berichtenbox" call to OpenTunnel and return the result and the
raw return XML (for logging purposes).

=head3 Arguments

=over

=item * bsn

The BSN to check BerichtenBox access for.

=back

=cut

define_profile has_berichtenbox => (
    required => {
        'bsn' => 'Str',
    },
);

sub has_berichtenbox {
    my $self = shift;
    my $params = assert_profile({@_})->valid;

    return $self->call(
        'has_berichtenbox',
        {
            $self->_header(),
            recipient => {
                bsn => $params->{bsn},
            },
        }
    );
}

=head2 lopende_zaak

Perform a "lopende zaak" update call to OpenTunnel and return the result and
raw XML (for logging).

=head3 Arguments

=over

=item * bsn

=item * subject

=item * content

=item * reference

=back

All provided "date" arguments should be C<DateTime> instances.

=cut

define_profile lopende_zaak => (
    required => {
        bsn                  => 'Str',
        case_id              => 'Int',
        date_of_registration => 'DateTime',
        date_target          => 'DateTime',
        casetype_name        => 'Str',
        status               => 'Str',
        status_name          => 'Str',
        url                  => 'Str',
    },
    optional => {
        date_of_completion => 'DateTime',
        result             => 'Str',
        explanation        => 'Str',
    },
);

sub lopende_zaak {
    my $self = shift;
    my $params = assert_profile({@_})->valid;

    my %arguments = (
        $self->_header(),
        case => {
            requestor => { bsn => $params->{bsn} },
            number    => $params->{case_id},

            date_of_registration => $params->{date_of_registration}->ymd,
            date_target          => $params->{date_target}->ymd,
            casetype             => { name => $params->{casetype_name} },
            explanation          => $params->{explanation} || '',
            status               => $params->{status},
            status_name          => $params->{status_name},
            url                  => $params->{url},
        },
    );

    for my $optional (qw(date_of_completion result)) {
        if (exists $params->{ $optional }) {
            my $val = $params->{ $optional };
            if (blessed($val) && $val->isa('DateTime')) {
                $val = $val->ymd;
            }
            $arguments{case}{ $optional } = $val;
        }
    }

    return $self->call(
        'lopende_zaak',
        \%arguments,
    );
}

=head2 berichtenbox_message

Perform a "berichtenbox" update call to OpenTunnel and return the result and
raw XML (for logging).

=head3 Arguments

=over

=item * bsn

=item * case_id

=item * date_of_registration

=item * date_of_completion (optional)

=item * date_target

=item * casetype_name

=item * explanation

=item * status

=item * status_name

=item * result (optional)

=item * url

=back

All provided "date" arguments should be C<DateTime> instances.

=cut

define_profile berichtenbox_message => (
    required => {
        bsn       => 'Str',
        subject   => 'Str',
        content   => 'Str',
        reference => 'Str',
    },
);

sub berichtenbox_message {
    my $self = shift;
    my $params = assert_profile({@_})->valid;

    my %arguments = (
        $self->_header(),
        recipient => { bsn => $params->{bsn} },
        message => {
            subject   => $params->{subject},
            content   => $params->{content},
            reference => $params->{reference},
        },
    );

    return $self->call(
        'berichtenbox_message',
        \%arguments,
    );
}

__PACKAGE__->meta->make_immutable();

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
