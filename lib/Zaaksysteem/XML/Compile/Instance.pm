package Zaaksysteem::XML::Compile::Instance;

use Moose::Role;

use XML::Compile;
use XML::Compile::Cache;

use Zaaksysteem::Tools;

with 'MooseX::Log::Log4perl';

#requires 'bla';

=head1 NAME

Zaaksysteem::XML::Compile::Instance - Instance object, where the schema lives for this L<XML::Compile> instance/

=head1 SYNOPSIS

    ### Within a different module
    my $instance    = $backend->stuf0204;

    ### Convenience method, write XML from perl hash
    $instance->kennisgevingsbericht('writer', { stuurgegevens => {...}, body => {...});

    ### Convenience method, transform xml to perl hash
    $instance->kennisgevingsbericht('reader', 'xml><stuurgegevens></stuurgegevens>[...]');

    ### Access to schema
    $instance->schema

    ### Access to readers and writers
    $instance->schema->reader($elem, $xml);

    $instance->schema->writer($elem, $perlhash);


=head1 DESCRIPTION

This object holds the XSD schema instance. It contains a description what needs to be loaded
at application start time. You probably need to load a bunch of XSD schema's, and would like
to define some elements to precompile.

=head1 REQUIRED ATTRIBUTES

These attributes must be implemented in classes which consumes this Role.

=head2 name

isa: Str

    has 'name' => (
        'is'        => 'ro',
        'default'   => 'stuf0204'
    );

Defines a name for this XML Schema, a method is created below the backend object with
this name, e.g. C<< $backend->stuf0204 >>

=cut

has 'name' => (
    'is'        => 'ro',
    'required'  => 1,
);

=head2 schemas

isa: ArrayRef

    has 'schemas' => (
        'is'        => 'ro',
        'default'   => sub {
            return [
                catfile($self->home, STUF_XSD_PATH, '0204/stuf0204.xsd'),
                catfile($self->home, STUF_XSD_PATH, 'bg0204/bgstuf0204.xsd'),
                catfile($self->home, STUF_XSD_PATH, 'bg0204/bg0204.xsd'),
            ];
        }
    );

Defines the schema's to be loaded, use C<< $self->home >> to point to the application root

=cut

has 'schemas' => (
    'is'        => 'ro',
    'isa'       => 'ArrayRef',
    'required'  => 1,
);

=head2 elements

isa: ArrayRef

    has 'name' => (
        'is'        => 'ro',
        'default'   => sub {
            return [
                {
                    element  => '{http://www.egem.nl/StUF/sector/bg/0204}kennisgevingsBericht',
                    compile  => 'RW',
                    method   => 'kennisgevingsbericht',
                },
                {
                    element  => '{http://www.egem.nl/StUF/sector/bg/0204}bevestigingsBericht',
                    compile  => 'RW',
                    method   => 'bevestigingsbericht',
                },
            ]
        }
    );

Defines the XML elements which should be preloaded in Zaaksysteem. This way calls to the
readers and writers won't have a large performance hit.

B<Format>

The format is an ArrayRef containing elements with the following keys

=over 4

=item element

The element name to compile

=item compile

Defines whether you want a C<READER>, a C<WRITER> or C<RW> (both).

=item method

Defines a convenience method below the instance. In the above example two methods are defined:

    $instance->kennisgevingsbericht('READER', '<xml>[...]');
    $instance->bevestigingsbericht('WRITER', { code => 444, bevestiging => {...} });

=back

=cut

has 'elements' => (
    'is'        => 'ro',
    'isa'       => 'ArrayRef',
    'required'  => 1,
);

=head2 _reader_config

isa: ArrayRef

    has '_reader_config' => (
        'is'        => 'ro',
        'default'   => sub {
            return {
                sloppy_integers                => 1,
                interpret_nillable_as_optional => 1,
                check_values                   => 0,
                'hooks'                        => [{
                }];
            };
        }
    );

L<XML::Compile> config for readers

=cut

has '_reader_config' => (
    'is'        => 'ro',
    'required'  => 1,
);

=head2 _writer_config

isa: ArrayRef

    has '_writer_config' => (
        'is'        => 'ro',
        'default'   => sub {
            return {
                sloppy_integers                => 1,
                interpret_nillable_as_optional => 1,
                check_values                   => 0,
                'hooks'                        => [{
                }];
            };
        }
    );

L<XML::Compile> config for writers

=cut

has '_writer_config' => (
    'is'        => 'ro',
    'default'   => sub { {} },
);

=head1 ATTRIBUTES

=head2 home

Root directory of this application, e.g. './'

=cut

has 'home' => (
    'is' => 'rw',
);

=head2 schema

isa: XML::Compile::Schema

L<XML::Compile::Schema> instance. Well, to be exactly: it will be the L<XML::Compile::Cache> object
but it inherits from Schema.

=cut

has 'schema' => (
    'is'    => 'rw',
    'isa'   => 'XML::Compile::Schema'
);

=head2 wsdl

isa: ArrayRef

Optionally, when it contains a WSDL, the schema will be inherited from L<XML::Compile::WSDL11>,
and soapcalls defined in elements will be compiled.

=cut

has 'wsdl' => (
    is      => 'rw',
    isa     => 'ArrayRef',
    default => sub { [] },
);

sub BUILD {
    my $self        = shift;

    $self->_load_instance;
}

# sub by_element {
#     my $self        = shift;
#     my $elem        = shift;

#     my ($data) = grep { $_->{element} eq $elem } @{ $self->elements };

#     my $method = $data->{method};

#     return $self->$method;
# }

=head1 METHODS

=head2 call

Arguments: $METHOD_NAME, \%PERLHASH [, \%OPTIONS ]

Return value: (\%answer, $trace) OR \%answer

    my $options = {};
    my ($reply, $trace) = $instance->call(
        'vraagbericht',
        {
            stuurgegevens   => { berichtSoort => 'Lk01' },
            body            => { [...] }
        },
        $options
    );

Initiates a SOAP call by calling the soapcall supplied in C<< $self->elements >>.

B<Options>

Inherits from options of L<XML::Compile::WSDL11>->compileClient.

Below a few usefull examples:

=over 4

=item transport_hook [optional]

Sets a custom transport_hook, e.g. for testing:

    $options->{transport_hook} = sub {
        return HTTP::Response->new(200, 'answer manually created',
            [ 'Content-Type' => 'text/xml' ], '<xml><my /><custom /></xml>'
        );
    };

=item endpoint [optional]

Overrule the destination address(es).

    $options->{endpoint} = 'https://localhost/api/stuf/bg0204'

=item server [optional]

Overrule only the server part in the endpoint, not the whole endpoint. Only used when
no explicit endpoint is provided.

    $options->{server} = 'username:password@myhost:4711'

=back

=cut

sub call {
    my ($self, $method, $perldata, $options) = @_;

    $perldata //= {};
    $options  //= {};

    my ($element)           = grep { $_->{method} eq $method } @{ $self->elements };

    if (!$element) {
        $self->log->error("Unable to call method $method, it is not mapped to anything");
        throw("instance/call/invalid/method", "Unable to call $method, not mapped");
    }

    if (!$self->schema->isa("XML::Compile::WSDL11")) {
        throw("instance/schema/invalid/object", "Unable to call $method, not a WSDL11 object");
    }

    if ($self->log->is_trace) {
        $self->log->trace("Calling method $method, mapped to " . dump_terse($element));
    }

    my @compile_options     = (
        service     => $element->{soapcall}->{service} // undef,
        port        => $element->{soapcall}->{port} // undef,
        %{ $options },
    );

    ## HACK HACK, need to recompile
    {

        my $op = $self->schema->operation($element->{soapcall}->{call}, @compile_options);

        if ($self->schema->{XCW_ccode}{$op->name}) {
            delete $self->schema->{XCW_ccode}{$op->name};
        }

    }

    $self->schema->compileCall(
        $element->{soapcall}->{call},
        @compile_options
    );

    if ($self->log->is_trace) {
        $self->log->trace(
            sprintf(
                'Calling %s with data %s',
                $element->{soapcall}{call},
                dump_terse($perldata, 5)
            )
        );
    }

    return $self->schema->call($element->{soapcall}->{call}, $perldata);
}


=head1 INTERNAL METHODS

=head2 _load_instance

Arguments: none

Return value: 1

Loads this instance by creating a new L<XML::Compile::Cache> object in C<< $self->schema >>,
and declaring all XML Elements in C<< $self->elements >> for preloading.

Als calls C<_create_method> for creating convenience methods to the readers and writers
of XML::Compile

=cut

sub _load_instance {
    my $self        = shift;

    my $xmlc = $self->get_schema;

    for my $element (@{ $self->elements }) {
        $xmlc->declare(
            $element->{compile} => $element->{element}
        );

        $xmlc->reader($element->{element});

        if ($element->{method}) {
            $self->_create_method($element);
        }

        if (
            UNIVERSAL::isa($xmlc, 'XML::Compile::WSDL') &&
            $element->{soapcall}
        ) {
            $xmlc->compileClient(
                $element->{soapcall}->{call},
                service     => $element->{soapcall}->{service},
                port        => $element->{soapcall}->{port}
            );
        }
    }

    $self->meta->make_immutable();

    $self->schema($xmlc);

    return 1;
}

=head2 get_schema

Overridable subroutine that returns the XML::Compile object. Particular
reason for making this overridable is to allow XML::Compile::WSDL11 to be
used.

Returns an XML::Compile::Schema object.

Note: This could be better rewritten as the builder routine for
$self->schema, however at the time of the writing this code is still
being worked on by other parties so I'm making this changeset as small
as possible.

=cut

sub get_schema {
    my $self = shift;

    if (scalar @{ $self->wsdl }) {
        $self->log->trace("Loading as XML::Compile::WSDL11");
        my @wsdls   = @{ $self->wsdl };

        if ($self->log->is_trace) {
            $self->log->trace(dump_terse($self->wsdl));
        }

        my $wsdl = XML::Compile::WSDL11->new(
            shift @wsdls,
            opts_readers => $self->_reader_config,
            opts_writers => $self->_writer_config,
        );

        $wsdl->addWSDL($_) for @wsdls;

        if ($self->log->is_trace) {
            $self->log->trace(dump_terse($self->schemas));
        }

        $wsdl->importDefinitions($self->schemas);

        return $wsdl;

    } else {
        $self->log->trace("Loading as XML::Compile::Cache");
        return XML::Compile::Cache->new(
            $self->schemas,
            opts_readers => $self->_reader_config,
            opts_writers => $self->_writer_config,
        );
    }
}


=head2 _create_method

Arguments: \%element

Return Value: (0 | 1)

Creates an method on this object

Creates the in C<< $self->elements >> requested methods below this object, e.g.
C<< $instance->bevestigingsBericht >> when C<< $element->{method} >> says C<bevestigingsBericht>

=cut

sub _create_method {
    my $self        = shift;
    my $element     = shift;

    my $method      = $element->{method};
    my $elem        = $element->{element};
    my $compile     = $element->{compile};

    if ($self->can($method)) {
        throw('xml/instance/method/exists', sprintf(
                'Method %s already exists on class %s', $method, $self->name)
        );
    }

    no strict 'refs';

    $self->meta->add_method($method, sub {
        my $self = shift;
        my $type = shift;

        if (lc($type) eq 'reader') {
            return $self->schema->reader($elem)->(@_);
        } else {
            my $doc     = XML::LibXML::Document->new('1.0', 'UTF-8');

            my $elem    = $self->schema->writer($elem)->($doc, @_);

            $doc->setDocumentElement($elem);

            return $doc->toString(1);
        }
    });

    return 1;

}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Install>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 BUILD

TODO: Fix the POD

=cut

