package Zaaksysteem::Zaken::ResultSetZaakKenmerk;
use Moose;

use Data::Dumper;
use Params::Profile;

use Zaaksysteem::Constants;
use Zaaksysteem::Tools;

use Clone qw/clone/;
use List::Util;
use Time::HiRes qw/tv_interval gettimeofday/;

use DateTime::Format::Strptime;
use DateTime::Format::DateParse;

use DateTime::Format::Strptime;
use DateTime::Format::ISO8601;

use Array::Compare;

extends 'DBIx::Class::ResultSet';

with qw(MooseX::Log::Log4perl);

has numeric_types => (
    is => 'ro',
    isa => 'HashRef',
    default => sub {
        return { map { $_ => 1 } qw(numeric valuta valutain valutaex valutain6 valutaex6 valutain21 valutaex21) };
    },
    lazy => 1,
);

=head2 _format_date

Format a date value from ISO8601 to dd-mm-yyy

=cut

sub _format_date {
    my $self = shift;
    my $str = shift;

    unless ($str) {
        $self->log->trace("No value in date kenmerk");
        return "";
    }

    my $f = DateTime::Format::Strptime->new(pattern => '%d-%m-%Y');

    my $dt = try {
        $f->parse_datetime($str);
    }
    catch {
        $self->log->trace("Unable to parse $str with Strptime: $_");
        return;
    };

    if (!$dt) {
        my $i = DateTime::Format::ISO8601->new();
        $dt = try {
            $i->parse_datetime($str);
        }
        catch {
            $self->log->warn("Unable to parse $str with ISO8601: $_");
            return;
        };
    }

    if ($dt) {
        $dt->set_formatter($f);
        return $dt . "";
    }
    return "";
}



{
    Params::Profile->register_profile(
        method  => 'create_kenmerk',
        profile => {
            required => [ qw/zaak_id bibliotheek_kenmerken_id values/],
        }
    );

    sub create_kenmerk {
        my ($self, $params) = @_;

        my $schema = $self->result_source->schema;
        my $bibliotheek_kenmerk = $schema->resultset('BibliotheekKenmerken')->find($params->{bibliotheek_kenmerken_id});
        my $value_type = $bibliotheek_kenmerk->value_type;

        my $values;

        if ($self->numeric_types->{$value_type}) {
            $values = $bibliotheek_kenmerk->filter( $params->{values} );
        }
        elsif ($value_type eq 'date') {
            my $ref = ref $params->{values};
            if ($ref eq ''|| $ref eq 'DateTime') {
                $params->{values} = [ $params->{values} ];
            }
            my @values;
            foreach (@{$params->{values}}) {
                push(@values, $self->_format_date($_));
            }
            $values = \@values;
        }
        else {
            $values = $params->{values};
        }

        if (ref $values ne 'ARRAY') {
            $values = [ $values ];
        }

        foreach my $value (@{ $values }) {
            next unless (length($value) && !ref($value));

            my $row = $self->create({
                zaak_id                     => $params->{zaak_id},
                bibliotheek_kenmerken_id    => $params->{bibliotheek_kenmerken_id},
                value                       => $value
            });
            $row->set_value($value);
        }
    }
}

sub log_field_update {
    my ($self, $params) = @_;

    my $values = $params->{values} || [];
    my $value_string = join(", ", grep { defined($_) } @$values);

    return unless defined($value_string);

    my $raw_data;

    # Convert nummeraanduiding to human readable.
    if ($value_string =~ /^(nummeraanduiding|openbareruimte|woonplaats)-\d+.*$/) {
        $raw_data = $value_string;
        $value_string = $self->get_bag_hr(@$values);
    }

    my $match_date = qr[\d{4}\-\d{2}\-\d{2}T\d{2}:\d{2}:\d{2}(?:Z|\+\d{4})];

    if ($value_string =~ m[^($match_date);($match_date);\d+$]) {
        my $date1 = $1;
        my $date2 = $2;

        my $bibliotheek_kenmerken = $self->result_source->schema->resultset('BibliotheekKenmerken');
        my $bibliotheek_kenmerk = $bibliotheek_kenmerken->find($params->{bibliotheek_kenmerken_id});

        if ($bibliotheek_kenmerk->value_type eq 'calendar') {
            $raw_data = $value_string;

            my $start_date = DateTime::Format::DateParse->parse_datetime($date1);
            my $start_time = DateTime::Format::DateParse->parse_datetime($date2);

            # Ensure local time
            $start_date->set_time_zone('Europe/Amsterdam');
            $start_time->set_time_zone('Europe/Amsterdam');

            my $fmt_date = DateTime::Format::Strptime->new(pattern => '%d-%m-%Y');
            my $fmt_time = DateTime::Format::Strptime->new(pattern => '%R');

            $value_string = sprintf(
                'Afspraak op %s om %s',
                $fmt_date->format_datetime($start_date),
                $fmt_time->format_datetime($start_time),
            );
        }
        elsif ($bibliotheek_kenmerk->value_type eq 'calendar_supersaas') {
            $raw_data = $value_string;

            my $start = DateTime::Format::DateParse->parse_datetime($date1);
            my $end   = DateTime::Format::DateParse->parse_datetime($date2);

            # Ensure local time
            $start->set_time_zone('Europe/Amsterdam');
            $end->set_time_zone('Europe/Amsterdam');

            my $fmt = DateTime::Format::Strptime->new(
                pattern => '%d-%m-%Y %R'
            );

            $value_string = sprintf(
                '%s - %s',
                $fmt->format_datetime($start),
                $fmt->format_datetime($end)
            );
        }
        # Not qmatic, AND not SuperSaaS? Then it must be a manual entry. Don't bother manipulating the value.
    }

    my $data = {
        case_id         => $params->{zaak}->id,
        attribute_id    => $params->{bibliotheek_kenmerken_id},
        attribute_value => $value_string,
        $raw_data ? (attribute_value_raw => $raw_data) : (),
    };

    my $event = do {
        local $data->{attribute_value} = '%';

        my $logging = $self->result_source->schema->resultset('Logging');

        $logging->find_recent({
            data => $data,
            event_type => 'case/attribute/update'
        });
    };

    if ($event) {
        $event->data($data);
        $event->restricted(1) if ($params->{zaak}->confidentiality ne 'public');
        $event->update;
    }
    else {
        $event = $params->{zaak}->trigger_logging(
            'case/attribute/update',
            {
                component => 'kenmerk',
                data      => $data,
            }
        );
    }
    return $event;
}




{
    Params::Profile->register_profile(
        method  => 'replace_kenmerk',
        profile => 'Zaaksysteem::Zaken::ResultSetZaakKenmerk::create_kenmerk',
    );

    sub replace_kenmerk {
        my ($self, $params) = @_;

        eval {
            $self->result_source->schema->txn_do(sub {

                # Remove existing values for this bibliotheek_kenmerken_id
                $self->result_source->resultset->search({
                    zaak_id                     => $params->{zaak_id},
                    bibliotheek_kenmerken_id    => $params->{bibliotheek_kenmerken_id},
                })->delete;

                # Re-create this kenmerk with new values
                $self->create_kenmerk({
                    zaak_id                     => $params->{zaak_id},
                    bibliotheek_kenmerken_id    => $params->{bibliotheek_kenmerken_id},
                    values                      => $params->{values}
                });
            });
        };

        if ($@) {
            warn('Arguments: ' . Dumper($params));
            die('Replace kenmerk failed: ' . $@)
        }

        return 1;
    }
}




{
    Params::Profile->register_profile(
        method  => 'get',
        profile => {
            required => [ qw/bibliotheek_kenmerken_id/ ]
        }
    );


    sub get {
        my ($self, $params) = @_;

        my $dv = Params::Profile->check(params  => $params);
        die "invalid options for get" unless $dv->success;

        my $bibliotheek_kenmerken_id = $params->{bibliotheek_kenmerken_id};

        my $kenmerken   = $self->search(
            {
                bibliotheek_kenmerken_id => $bibliotheek_kenmerken_id
            },
            {
                prefetch        => [
                    'bibliotheek_kenmerken_id'
                ],
            }
        );

        return $kenmerken->first();
    }
}



Params::Profile->register_profile(
    method  => 'update_field',
    profile => {
        required => [ qw/bibliotheek_kenmerken_id zaak/ ],
        optional => [ qw/new_values/ ]
    }
);

sub update_field {
    my ($self, $opts) = @_;

    my $dv = Params::Profile->check(params  => $opts);
    die "invalid options for update_field" unless $dv->success;

    my $bibliotheek_kenmerken_id    = $dv->valid('bibliotheek_kenmerken_id');
    my $new_values                  = $dv->valid('new_values');
    my $zaak_id                     = $dv->valid('zaak')->id;

    my ($removed,$race, $raceprotector) = (0,1,0);
    my ($errormsg);

    ### Race condition security, prevent multiple updates;
    while ($race && $raceprotector < 5) {
        $raceprotector++;

        my $params = {
            bibliotheek_kenmerken_id    => $bibliotheek_kenmerken_id,
            zaak_id                     => $zaak_id,
        };

        eval {
            $self->result_source->schema->txn_do(sub {
                my $attributes = $self->result_source->resultset->search($params);
                $removed = $attributes->count;

                if($removed) {
                    $attributes->delete;
                };

                $params->{values} = $new_values;

                if(defined $new_values)  {
                    # Re-create this kenmerk with new values
                    $self->create_kenmerk($params);
                }

                my $log_params = {
                    %$params,
                };
                delete $log_params->{zaak_id};
                $log_params->{zaak} = $dv->valid('zaak');

                $self->log_field_update($log_params);
            });
        };

        ### RACE PROTECTION CODE
        ### The code below checks the amount of kenmerken which would be added
        ### as a result of the above code. Another request could in theory and,
        ### unfortunatly, already call another update_field which results
        ### in duplicated entries.
        ###
        ### See: https://github.com/mintlab/Zaaksysteem/issues/1833
        ### Only testable on more or less slow connections, like test-e
        ###
        ### Improvement hint: do not use count from $params->{values}, but from
        ### a counter of the real rows above.
        ###
        ### - michiel
        if ($@) {
            $errormsg = $@;
            if ($errormsg =~ /ERROR:  deadlock detected/) {
                ### Try again
                warn('Deadlock, try again');
                next;
            }
        } else {
            $errormsg = undef;
        }

        {
            my %check_params = %{ $params };
            delete($check_params{values});

            $params->{values} = [ $params->{values} ]
                unless UNIVERSAL::isa($params->{values}, 'ARRAY');

            my $kenmerk_count = $self->result_source->resultset->search(\%check_params);

            if ($kenmerk_count <= scalar @{ $params->{values}}) {
                $race=0;
            } else {
                warn('RACE CONDITION, TRY AGAIN');
            }
        }
    }

    if ($errormsg) {
        throw('rs/case.attribute/update_field', "Could not update attribute " . $errormsg);
    }


    return $removed;
}

=head2 update_fields_authorized

    my $success = $zaak->zaak_kenmerken->update_fields_authorized(
        {
            new_values  => {
                text_value     => 'New value',
                radio_beer     => 'Heineken'
            },
            zaak        => $schema->resultset('Zaak')->find(44),
        }
    );

Will only update values when user has proper rights to edit this case.

=cut

define_profile update_fields_authorized => (
    required => {
        new_values => 'HashRef',
        zaak => 'Zaaksysteem::Zaken::ComponentZaak',
    },
    optional => {
        user => 'Any',
        ignore_log_update => 'Bool'
    },
);

sub update_fields_authorized {
    my $self        = shift;
    my $params      = assert_profile(shift)->valid;
    my $new_values  = $params->{ new_values };
    my $zaak        = $params->{ zaak };
    my $user        = ($params->{ user } || $self->result_source->schema->current_user);

    my @groups      = @{ $user->primary_groups };
    my @roles       = @{ $user->roles };

    ### 1: Retrieve all kenmerken according to given magic strings
    my @kenmerken   = $zaak->zaaktype_node_id->zaaktype_kenmerken->search(
        {
            'bibliotheek_kenmerken_id.magic_string' => { -in => [ keys %$new_values ] },
        },
        {
            join    => 'bibliotheek_kenmerken_id',
        }
    )->all;

    ### 2: Loop over the database kenmerken, to check for valid permissions. Also, prepare
    ### the new_values hash.
    my %fields;
    for my $kenmerk (@kenmerken) {
        my $permissions     = $kenmerk->format_required_permissions;

        ### First check for propery permission on the attribute when attribute is set
        if (@$permissions) {
            my $ok;
            for my $permission (@$permissions) {
                ### Check for group
                if (!$permission->{group} || !grep { $permission->{group}->id == $_->id } @groups) {
                    next;
                }

                ### Now check for role
                if (!$permission->{role} || !grep { $permission->{role}->id == $_->id } @roles) {
                    next;
                }

                $ok = 1
            }

            if (!$ok) {
                throw(
                    'case/update_fields_authorized/field_forbidden',
                    "You do not have the proper rights to update kenmerk: " . $kenmerk->magic_string
                );
            }
        }

        ### Permissions check out, continue
        ### We do not support 'opplusbare velden' yet, deref first item.
        $fields{ $kenmerk->get_column('bibliotheek_kenmerken_id') } = $new_values->{ $kenmerk->magic_string }->[0];
    }

    return $self->update_fields(
        {
            %$params,
            new_values  => \%fields,
        }
    );
}

=head2 _process_rules_updated_fields

    $self->_process_rules_updated_fields({
        case    => $schema->resultset('Zaak')->find(24),
        values  => {
            '2280' => [
                        'Vul 3 in'
                      ],
            '2281' => [
                        'Onwijzigbaar 3'
                      ]
        },
    })

Will get all current fields in this case via C<field_values> and merges it with the given C<values>. It will
feed it to the "ruler", and make sure the "hidden fields" will be emptied in the C<$return_values>. It will
also fill values with "vul_waarde_in" when the rule engine requests it.

=cut

define_profile _process_rules_updated_fields => (
    required => {
        case   => 'Zaaksysteem::Model::DB::Zaak',
        values => 'HashRef',
    },
    optional => { set_values_except_for_attrs => 'Any', },
    defaults => { set_values_except_for_attrs => sub { return [] }, }
);

sub _process_rules_updated_fields {
    my $self    = shift;
    my $params  = assert_profile(shift || {})->valid;

    my $rules = $params->{case}->rules;

    return $params->{values} unless $rules->has_rules;

    my $case_values = $params->{case}->field_values;

    my $old_values = $self->_process_rules(
        rules  => $rules,
        values => $case_values
    );


    # Exclude user input fields from being updated. It makes no sense to
    # update the field according to a rule when the attribute you want
    # to save it touched by a rule.
    push(@{$params->{set_values_except_for_attrs}}, keys %{$params->{values}});

    my $new_values = $self->_process_rules(
        rules  => $rules,
        values => { %$case_values, %{ $params->{values} } },
        set_values_except_for_attrs => $params->{set_values_except_for_attrs},
    );

    # Validate the results of the rule execution:
    # If the old values do not match the case values
    # and the new values equals the old value, the value may
    # not be changed.
    my ($old, $new, $case);
    foreach (keys %$new_values) {
        $old  = $old_values->{$_} // '';
        $new  = $new_values->{$_} // '';
        $case = $case_values->{$_};

        if ($case && $old eq $new && $old ne $case || $case && $case eq $new) {
            delete $new_values->{$_};
        }
    }

    return $new_values;
}

define_profile _process_rules => (
    required    => {
        rules  => 'Zaaksysteem::Backend::Rules',
        values => 'HashRef',
    },
    optional    => {
        set_values_except_for_attrs => 'Any',
    },
    defaults => {
        set_values_except_for_attrs => sub { return [] },
    }
);

sub _process_rules {
    my $self = shift;
    my $params = assert_profile({@_})->valid;

    my $rules  = $params->{rules};
    my $values = $params->{values};

    my $val_obj = $rules->validate({
        %{$rules->rules_params},
        %{$values}
    });

    return $val_obj->process_params({
        engine    => $rules,
        values    => $values,
        keyformat => 'bibliotheek_kenmerken_id',
        set_values_except_for_attrs =>
            $params->{set_values_except_for_attrs},
    });
}

define_profile update_fields => (
    required => {
        new_values => 'HashRef',
        zaak => 'Zaaksysteem::Zaken::ComponentZaak',
    },
    optional => {
        ignore_log_update => 'Bool',
        set_values_except_for_attrs => 'Any',
    },
);

sub update_fields {
    my $self = shift;

    my $params = assert_profile(shift)->valid;

    my $zaak = $params->{ zaak };
    my $zaak_id = $zaak->id;

    my $error = 0;

    my $new_values = $self->_process_rules_updated_fields(
        {
            case   => $zaak,
            values => $params->{new_values},
            set_values_except_for_attrs =>
                $params->{set_values_except_for_attrs},
        }
    );

    $self->log->info(
        sprintf(
            "Updating attributes %s for case %d changed by rules to: %s",
            dump_terse($params->{new_values}), $zaak_id, dump_terse($new_values)
        )
    );

    my $result       = delete $new_values->{'case.result'};
    my $confidential = delete $new_values->{'case.confidentiality'};
    my $price        = delete $new_values->{'case.price'};

    ### Delete every value which does not have an integer key (like case.price...)
    $new_values = { map({ $_ => $new_values->{$_} } grep(/^\d+$/, keys %$new_values)) };

    my $args = {
        bibliotheek_kenmerken_id    => { in => [ keys %$new_values ]},
        zaak_id                     => $zaak_id,
    };

    my $kenmerken = $self->search_rs($args,
        {
            prefetch => 'bibliotheek_kenmerken_id'
        }
    );

    ### Only create kenmerk when value is not equal 'new value'
    $self->strip_identical_fields($new_values, $kenmerken);
    my @changed_keys = keys %$new_values;

    if (!@changed_keys && !$result && !$confidential && !defined $price) {
        return 1;
    }

    $self->result_source->schema->txn_do(sub {
        try {
            if ($result) {
                $zaak->set_resultaat((ref $result eq 'ARRAY' ? $result->[0] : $result));
                $zaak->update;
            }

            if ($confidential) {
                $zaak->set_confidentiality((ref $confidential eq 'ARRAY' ? $confidential->[0] : $confidential));
                $zaak->update;
            }

            if (defined $price) {
                # case.price is guaranteed to be xx,xx
                $price =~ s/,/./g;
                $zaak->update({ payment_amount => $price });
            }

            for my $bibid (@changed_keys) {
                my $create_kenmerk_params = {
                    bibliotheek_kenmerken_id    => $bibid,
                    zaak_id                     => $zaak_id,
                    values                      => $new_values->{$bibid}
                };

                $self->replace_kenmerk($create_kenmerk_params);

                unless ($params->{ ignore_log_update }) {
                    my $values = $create_kenmerk_params->{values};

                    $create_kenmerk_params->{values} =
                        UNIVERSAL::isa($values, 'ARRAY') ? $values : [$values];

                    delete $create_kenmerk_params->{zaak_id};
                    $create_kenmerk_params->{zaak} = $zaak;

                    $self->log_field_update($create_kenmerk_params);
                }
            }

            $zaak->discard_changes;
            if (!$params->{skip_touch}) {
                $zaak->touch;
            }
        }
        catch {
            $error++;
            $self->log->error($_);
            throw('rs/case.attribute/update_fields', "Could not update attribute " . $_);
        }

    });

    return $error ? 1 : 0;
}

sub strip_identical_fields {
    my ($self, $new_values, $kenmerken) = @_;

    # Force order so eq_deeply plays nice
    $kenmerken = $kenmerken->search_rs(undef, { order_by => { -asc => 'me.id' }});

    my $compare = Array::Compare->new();

    my %db_values;
    while (my $kenmerk = $kenmerken->next) {
        my $bibid = $kenmerk->get_column('bibliotheek_kenmerken_id');
        push(@{$db_values{$bibid}}, $kenmerk->value) if defined $kenmerk->value;
    }

    foreach my $bibid (keys %db_values) {
        my $ref_db = ref($db_values{$bibid});
        my $ref_nv = ref($new_values->{$bibid});

        # Sanitize the data to something that is comparable
        if ($ref_db eq '' && $ref_nv eq 'ARRAY') {
            $db_values{$bibid} = [ $db_values{$bibid} ];
            $ref_db = "ARRAY";
        }
        elsif ($ref_db eq 'ARRAY' && $ref_nv eq '') {
            $new_values->{$bibid} = [ $new_values->{$bibid} ];
            $ref_nv = "ARRAY";
        }

        if (!$ref_db && !$ref_nv &&
           ( $db_values{$bibid} // '') eq ($new_values->{$bibid} //'')) {
            delete($new_values->{$bibid})
        }
        elsif ($ref_db eq 'ARRAY' && $ref_nv eq 'ARRAY' &&
            $compare->compare($db_values{$bibid}, $new_values->{$bibid})) {
            delete($new_values->{$bibid});
        }
    }
}


=head2 get_bag_hr

Convert bag strings to something human readable.

=cut

sub get_bag_hr {
    my ($self) = shift;
    my @values = @_;

    my @hr;
    my $value_string;
    for my $bag_id (@values) {
        push @hr, $self->result_source->schema->resultset('BagNummeraanduiding')
            ->get_record_by_source_identifier($bag_id)->to_string;
    }
    if (@hr > 1) {
        $value_string = join(", ", grep { defined($_) } @hr);
    }
    else {
        $value_string = $hr[0];
    }

    return $value_string;
}


=head2 delete_fields

The use case for this method is fields that are being hidden by rules.
We receive a list of field_ids (bibliotheek_kenmerken) and we look
if any values are stored. If so, these are deleted, and the fields that
are actually affected are gathered and put in a log.

=cut

sub delete_fields {
    my ($self, $arguments) = @_;

    my $bibliotheek_kenmerken_ids = $arguments->{bibliotheek_kenmerken_ids} or die "need bibliotheek_kenmerken_ids";

    throw('delete_fields/missing_argument', 'ZaakKenmerk::delete_fields needs a "zaak" argument')
        unless $arguments->{zaak};

    my $zaak_id = $arguments->{zaak}->id;

    if ($self->log->is_trace) {
        $self->log->trace("Hiding fields in case $zaak_id " . join ", ", @$bibliotheek_kenmerken_ids);
    }

    my $current = $self->search({
        zaak_id => $zaak_id, # ensure only this case is affected
        bibliotheek_kenmerken_id => {
            '-in' => $bibliotheek_kenmerken_ids
        }
    });

    # if there are no values stored that need deletion, we're done. nothing to do here
    my @current = $current->all
        or return;

    # a list with removed attributes. because checkboxes are stored as separate values
    # abuse a hash to ensure uniqueness.
    # using get_column to avoid subqueries - performance tweak
    my $deleted = { map { $_->get_column('bibliotheek_kenmerken_id') => 1 } @current };

    $current->delete;

    my $schema = $self->result_source->schema;

    my $logging = $schema->resultset('Logging');
    my $bibliotheek_kenmerken = $schema->resultset('BibliotheekKenmerken');

    # get the names of the deceased attributes
    my $attributes = join ", ", map { $_->naam } $bibliotheek_kenmerken->search({
        id => {
            '-in' => [keys $deleted]
        }
    })->all;

    # if you delete a bunch of fields, the logging line get too long
    # we need to solve this in the logging module, danger, danger
    $attributes = substr($attributes, 0, 100) . '...' if length $attributes > 100;

    $arguments->{zaak}->trigger_logging('case/attribute/removebulk', {
        component => 'kenmerk',
        data => {
            attributes => $attributes,
            reason => 'verborgen door regels'
        }
    });
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 create_kenmerk

TODO: Fix the POD

=cut

=head2 create_kenmerken

TODO: Fix the POD

=cut

=head2 get

TODO: Fix the POD

=cut

=head2 log_field_create

TODO: Fix the POD

=cut

=head2 log_field_update

TODO: Fix the POD

=cut

=head2 replace_kenmerk

TODO: Fix the POD

=cut

=head2 strip_identical_fields

TODO: Fix the POD

=cut

=head2 update_field

TODO: Fix the POD

=cut

=head2 update_fields

TODO: Fix the POD

=cut

