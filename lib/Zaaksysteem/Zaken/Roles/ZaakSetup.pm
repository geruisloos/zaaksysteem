package Zaaksysteem::Zaken::Roles::ZaakSetup;

use Moose::Role;
use Data::Dumper;

use File::Copy;

use Zaaksysteem::Constants qw/
    ZAKEN_STATUSSEN
    ZAKEN_STATUSSEN_DEFAULT
    LOGGING_COMPONENT_ZAAK
    ZAAK_CREATE_PROFILE
/;
use Zaaksysteem::Tools;
use Zaaksysteem::BR::Subject;
use DateTime::Format::DateParse;

### Roles
with
    'MooseX::Log::Log4perl',
    'Zaaksysteem::Zaken::Roles::BetrokkenenSetup',
    'Zaaksysteem::Zaken::Roles::BagSetup',
    'Zaaksysteem::Zaken::Roles::KenmerkenSetup',
    'Zaaksysteem::Zaken::Roles::RelatieSetup';

has 'z_betrokkene'   => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        shift->result_source->schema->betrokkene_model;
    }
);


{

    sub find_as_session {
        my $self            = shift;
        my $zaak            = shift;

        unless (
            $zaak &&
            (
                ref($zaak) ||
                $zaak =~ /^\d+$/
            )
        ) {
            die('Kan niet dupliceren, geen zaak of zaaknr meegegeven');
        }

        unless (ref($zaak)) {
            $zaak = $self->find($zaak)
                or die('Geen zaak gevonden met nummer: ' .  $zaak);
        }

        my $pp_profile      = Params::Profile->get_profile(
            method  => 'create_zaak'
        );

        ### _get_zaak_as_session
        my %zaak_data   = $zaak->get_columns;
        my %zaak_copy;
        for my $col (keys %zaak_data) {
            next if grep( { $_ eq $col } qw/
                id
                pid
                relates_to
                vervolg_van
                related_because
                vervolg_because
                child_because

                zaaktype_node_id

                last_modified
                created

                days_perc
                days_running
                days_left

                aanvrager
                coordinator
                behandelaar
                locatie_zaak
                locatie_correspondentie
            /);

            $zaak_copy{$col} = $zaak_data{$col};
        }

        ### _get_betrokkenen
        for my $btype (qw/behandelaar coordinator aanvrager/) {
            next unless $zaak->$btype;


            my $btype_p     = $btype . 's';

            my $get_sub     = $btype . '_object';

            next unless($zaak->$get_sub);
            $zaak_copy{$btype_p} = [{
                betrokkene_type => $zaak->$get_sub->btype,
                betrokkene      => $zaak->$get_sub->betrokkene_identifier,
                verificatie     => ($zaak->$btype->verificatie || 'medewerker'),
            }]
        }

        ### _get_locatie
        for my $locatie (qw/locatie_zaak locatie_correspondentie/) {
            next unless $zaak->$locatie;

            $zaak_copy{$locatie}    = {
                bag_type        => $zaak->$locatie->bag_type,
                bag_id          => $zaak->$locatie->bag_id,
            }
        }

        ### _get_kenmerken
        my $kenmerken           = $zaak->field_values();

        if (scalar(keys %{ $kenmerken })) {
            $zaak_copy{kenmerken} = [];

            while (my ($kenmerk_id, $kenmerk_value) = each %{ $kenmerken }) {
                push(
                    @{ $zaak_copy{kenmerken} },
                    { $kenmerk_id   => $kenmerk_value }
                );
            }
        }

        return \%zaak_copy;
    }

    define_profile create_zaak => %{ZAAK_CREATE_PROFILE()};
    sub create_zaak {
        my ($self, $opts) = @_;
        $opts = assert_profile($opts)->valid;

        $self->log->info('Create zaak called with the following options: ' . dump_terse($opts));
        if (!$self->_validate_zaaktype($opts)) {
            throw(
                type => '/Zaaksysteem/Zaak/create_zaak',
                message => 'Invalid case type',
                object => $opts
            );
        }

        my $schema = $self->result_source->schema;

        my $zaak;
        $schema->txn_do(
            sub {
                $zaak = $self->_create_zaak($opts);

                $schema->resultset('Checklist')->create_from_case($zaak);
                $schema->resultset('CaseAction')->create_from_case($zaak);

                $zaak->trigger_logging(
                    'case/create', {
                        component => LOGGING_COMPONENT_ZAAK,
                        data      => {case_id => $zaak->id}});


                ### Zaak loaded, now ask zaak to bootstrap himself with given options
                $zaak->_bootstrap($opts);
            });


        $zaak->discard_changes;

        if ($zaak->aanvrager_object->can('uuid')) {
            my $subject_br = Zaaksysteem::BR::Subject->new(
                schema => $schema,
            );
            my $subject = $subject_br->find($zaak->aanvrager_object->uuid);
            $subject_br->enable_subscription($subject);
        }

        return $zaak;
    }

    sub _validate_zaaktype {
        my ($self, $opts) = @_;
        my ($ztn);

        if ($opts->{zaaktype_node_id}) {
            $ztn = $self->result_source->schema->resultset('ZaaktypeNode')->find(
                $opts->{zaaktype_node_id},
                {
                    prefetch    => 'zaaktype_id'
                }
            );

            unless ($ztn) {
                throw('Zaken/_validate_zaaktype', "Cannot find zaaktype_node_id for $opts->{zaaktype_node_id}");
            }

            $opts->{zaaktype_id} = $ztn->zaaktype_id->id;
        } else {
            my $zt = $self->result_source->schema->resultset('Zaaktype')->find(
                $opts->{zaaktype_id},
                {
                    prefetch    => 'zaaktype_node_id'
                }
            );

            unless ($zt) {
                throw('Zaken/_validate_zaaktype', "Cannot find zaaktype_id for $opts->{zaaktype_id}");
            }

            $opts->{zaaktype_node_id} = $zt->zaaktype_node_id->id;

            $ztn = $self->result_source->schema->resultset('ZaaktypeNode')->find(
                $opts->{zaaktype_node_id}
            );
        }
        return $ztn;
    }

    sub _create_betrokkenen {
        my ($self, $opts) = @_;

        return unless $opts->{aanvragers};

        # DUMMY
        $opts->{aanvrager} = $self->result_source->schema->resultset('ZaakBetrokkenen')->create(
            {
                betrokkene_type         => 'natuurlijk_persoon',
                betrokkene_id           => 1,
                gegevens_magazijn_id    => 24640,
                verificatie             => 'digid',
            },
        );

        push(@{ $opts->{_update_zaak_ids} },
            $opts->{aanvrager}
        );
    }

#    define_profile _create_zaak => (
#        required => [qw()],
#        optional => [qw(confidentiality override_zaak_id created registratiedatum)],
#        typed    => {
#            confidentiality => 'Bool',
#            override_zaak_id => 'Bool',
#        },
#        defaults => {
#            confidentiality => 0,
#        },
#    );

    sub _create_zaak {
        my ($self, $opts) = @_;
        my (%create_params);

        if ($opts->{registratiedatum} && !ref($opts->{registratiedatum})) {
            $opts->{registratiedatum} = DateTime::Format::DateParse->parse_datetime($opts->{registratiedatum});
        }
        $opts->{registratiedatum} ||= DateTime->now();

        $create_params{ $_ } = $opts->{ $_ }
            for $self->result_source->columns;
        # TODO: define_profile..
        # The code above defaults to undef, which results in an error because
        # confidentiality is not allowed to be undef. the database has the proper
        # default value. a more constructive solution is to avoid the above construct
        # altogether.

        ### Delete autoincrement integer, UNLESS IT IS SET and OVERRIDE
        ### zaak_nr is true
        unless ($opts->{override_zaak_id}) {
            delete($create_params{id});
        }

        $create_params{created} = $create_params{registratiedatum};

        my $zaak = $self->create(
            \%create_params
        );

        $self->log->info(
            sprintf('Created zaak with id: %d', $zaak->id)
        );

        return $zaak;
    }
}

{
    Params::Profile->register_profile(
        method  => 'create_relatie',
        profile => {
            required        => [ qw/
                zaaktype_id
                type_zaak
            /],
            'optional'      => [ qw/
                subject
                start_delay
                actie_kopieren_kenmerken
                actie_automatisch_behandelen
                role_id
                ou_id
                behandelaar_id
                behandelaar_type
                current_user
            /],
            'require_some'  => {
                'aanvrager_id_or_aanvrager_type' => [
                    1,
                    'aanvrager_id',
                    'aanvrager_type'
                ],
            },

            # this doesn't seem to have influence
            'constraint_methods'    => {
                type_zaak => qr/^gerelateerd|vervolgzaak|vervolgzaak_datum|deelzaak|wijzig_zaaktype$/,
            },
        }
    );

    sub create_relatie {
        my ($self, $zaak, %opts) = @_;
        my ($nid, $aanvrager_id, $behandelaar_id);

        ### VALIDATION
        my $dv = Params::Profile->check(
            params  => \%opts,
        );

        throw "create_relatie: invalid options:" . Dumper($dv) unless $dv->success;

        ### Aanvrager information
        if ($dv->valid('aanvrager_type')) {
            if ($dv->valid('aanvrager_type') eq 'aanvrager') {
                $aanvrager_id = $zaak->aanvrager_object->betrokkene_identifier;
            } elsif ($dv->valid('aanvrager_type') eq 'behandelaar') {
                unless (defined $zaak->behandelaar_object) {
                    throw('case/create_relation/subject_not_found', sprintf(
                        "Attempted to create relation with originator case's (%d) assignee subject, but it could not be found",
                        $zaak->id
                    ));
                }

                $aanvrager_id = $zaak->behandelaar_object->betrokkene_identifier;
            } elsif ($dv->valid('aanvrager_type') eq 'ontvanger') {
                unless (defined $zaak->ontvanger_object) {
                    throw('case/create_relation/subject_not_found', sprintf(
                        "Attempted to create relation with originator case's (%d) receiver subject, but it could not be found",
                        $zaak->id
                    ));
                }

                $aanvrager_id = $zaak->ontvanger_object->betrokkene_identifier;
            } elsif ($dv->valid('aanvrager_type') eq 'anders') {
                $aanvrager_id = $dv->valid('aanvrager_id');
            }
        } else {
            $aanvrager_id = $dv->valid('aanvrager_id');
        }

        my $behandelaars    = [];
        if ($dv->valid('behandelaar_type')) {
            if ($zaak->behandelaar_object && $dv->valid('behandelaar_type') eq 'behandelaar') {
                $behandelaar_id = $zaak->behandelaar_object->betrokkene_identifier;
            }
        } elsif ($dv->valid('behandelaar_id')) {
            $behandelaar_id = $dv->valid('behandelaar_id');
        }

        if ($behandelaar_id) {
            $behandelaars   = [{
                betrokkene          => $behandelaar_id,
                verificatie         => 'medewerker',
            }];
        }

        my $type_zaak_msg = ucfirst($dv->valid('type_zaak'));

        if ($type_zaak_msg =~ /Vervolgzaak/) {
            $type_zaak_msg = 'Vervolgzaak';
        }

        ### Subject
        my $subject = $dv->valid('subject') ||
            $type_zaak_msg . ' van zaaknummer: ' .  $zaak->id;

        ### First, search for acturele zaaktype_node_id
        my $zaaktype_node_id;
        {
            my $zt = $self->result_source->schema->resultset('Zaaktype')->find(
                $dv->valid('zaaktype_id')
            );

            unless ($zt) {
                $self->log->debug('Zaaktype_id not found or complete', $dv->valid('zaaktype_id'));

                return;
            }

            $zaaktype_node_id = $zt->zaaktype_node_id->id;
        }

        # type_zaak is an alias for relatie_type, to be corrected
        my $registratiedatum = $self->determine_registratiedatum(
            $dv->valid('type_zaak'),
            $dv->valid('start_delay')
        );


        ### Zaak informatie
        my %zaak_opts = (
            zaaktype_node_id          => $zaaktype_node_id,
            behandelaars              => $behandelaars,
            onderwerp                 => $subject,
            registratiedatum          => $registratiedatum,
            relatie                   => $dv->valid('type_zaak'),
            contactkanaal             => $zaak->contactkanaal,
            aanvraag_trigger          => $zaak->aanvraag_trigger,
            route_role                => ($dv->valid('role_id') || undef),
            route_ou                  => ($dv->valid('ou_id') || undef),
            aanvragers                => [{
                betrokkene                  => $aanvrager_id,
                verificatie                 => 'medewerker',
            }],
            actie_kopieren_kenmerken  => (
                $dv->valid('actie_kopieren_kenmerken')
                    ? 1
                    : undef
            )
        );

        # Change to trace, create_zaak contains log->info() for the
        # create args
        $self->log->trace(
            'Creating case with options:',
            dump_terse(\%zaak_opts)
        );

        # Add zaak to zaakopts after debug log, prevent spam
        $zaak_opts{ zaak } = $zaak;

        my $nieuwe_zaak = $self->create_zaak(\%zaak_opts);

        return unless $nieuwe_zaak;

        if ($dv->valid('actie_automatisch_behandelen') && $dv->valid('current_user')) {
            $nieuwe_zaak->open_zaak($dv->valid('current_user'));
        }

        return $nieuwe_zaak;
    }
}

=head2 determine_registratiedatum

Follow-up cases (vervolgzaken) can be created with a delay of either
a set date (relatie_type: vervolgzaak_datum) or an interval in days (relatie_type: vervolgzaak).

=cut

sub determine_registratiedatum {
    my ($self, $relatie_type, $start_delay) = @_;

    $start_delay //= 0;

    if ($relatie_type eq 'vervolgzaak' && $start_delay =~ m|^\d+$|) {
        return DateTime->now->add(days => $start_delay);
    }

    if ($relatie_type eq 'vervolgzaak_datum' && $start_delay =~ m|^\d+-\d+-\d+$|) {

        my ($day, $month, $year) = $start_delay =~ m|^(\d+)-(\d+)-(\d+)$|;

        return DateTime->new(
            year    => $year,
            day     => $day,
            month   => $month,
        );
    }

    return DateTime->now();
}



sub duplicate {
    my ($self, $zaak, $opts) = @_;

    unless ($zaak && (ref($zaak) || $zaak =~ /^\d+$/)) {
        throw("ZaakSetup", 'Kan niet dupliceren, geen zaak of zaaknr meegegeven');
    }

    unless (ref($zaak)) {
        $zaak = $self->find($zaak)
            or throw('ZaakSetup', "Geen zaak gevonden met nummer: $zaak");
    }

    ### Retrieve aanvragers / kenmerken / bag data etc
    my $new_zaak_session    = $self->find_as_session($zaak);

    $opts ||= {};
    delete $new_zaak_session->{ $_ } for qw/uuid duplicate_prevention_token/;

    ### Change behandelaar to current user
    $opts->{behandelaars}   = [
        {
            betrokkene  => $self->current_user->betrokkene_identifier,
            verificatie => 'medewerker',
        }
    ];

    ### Commit new zaak
    my ($new_zaak);

    eval {
        $self->result_source->schema->txn_do(sub {
            if ($opts->{simpel}) {
                $opts->{registratiedatum}   = DateTime->now();
                $opts->{streefafhandeldatum}= undef;
            }

            $new_zaak = $self->create_zaak({
                %{$new_zaak_session},
                %{$opts}
            });

            if ($opts->{simpel}) {
                $new_zaak->milestone(1);
                $new_zaak->set_heropen;
            } else {

                $zaak->duplicate_checklist_items({target => $new_zaak});
                $zaak->duplicate_relations({target => $new_zaak});

                ### Copy files
                my $case_files  = $zaak->files->search(
                    {},
                    {
                        order_by => { '-asc' => 'id' }
                    }
                );
                $case_files->update({case_id => $new_zaak->id});

                # when changing case type, old case is deleted, new case is created.
                # on deletion nothing may be preserved, so don't copy logging either.
                unless($opts->{dont_copy_log}) {
                    ### Copy logboek
                    my $zaak_logging  = $zaak->logging->search(
                        {},
                        {
                            order_by => { '-asc' => 'id' }
                        }
                    );
                    while (my $logging = $zaak_logging->next) {
                        $logging->copy(
                            {
                                zaak_id => $new_zaak->id
                            }
                        );
                    }
                }
            }
        });
    };

    if ($@) {
        my $msg = sprintf("Failed duplicating case %d: %s", $zaak->id, $@);
        $self->log->error($msg);
        throw("Zaaksetup", $msg);
        return;
    }

    $zaak->trigger_logging('case/duplicate', { component => LOGGING_COMPONENT_ZAAK, data => {
        duplicate_id => $new_zaak->id,
        case_id => $zaak->id
    }});

    $new_zaak->trigger_logging('case/duplicated', { component => LOGGING_COMPONENT_ZAAK, data => {
        case_id => $new_zaak->id,
        duplicated_from => $zaak->id
    }});

    return $new_zaak;
}


=head2 duplicate_relations

Copy relations of case to target case

=cut

define_profile duplicate_relations => (
    required => [qw/target/]
);
sub duplicate_relations {
    my $self = shift;
    my $params = assert_profile(shift)->valid;

    my $target = $params->{target};
    my $rs = $self->result_source->schema->resultset('CaseRelation');

    $rs->add_relation($target->id, $_->case_id) for $rs->get_sorted($self->id);
}


=head2 duplicate_checklist_items

A case can have a checklist for each phase (why not link them to the phase?).
When duplicating a case, copy all items to the new case. The checklist is
already assumed to be duplicated.

To test:
Create two cases.
Add a checklist to the first, populate it with items.
Call this routine, after that the second case should have the same.

Much room for improvement there is.
The duplication process should be delegated to the checklist resultset.

=cut

define_profile duplicate_checklist_items => (
    required => [qw/target/]
);
sub duplicate_checklist_items {
    my $self = shift;
    my $params = assert_profile(shift)->valid;

    my $target = $params->{target};

    my $schema = $self->result_source->schema;
    my $checklist_rs = $schema->resultset('Checklist');
    my $checklists = $self->checklists->search;

    my $rs_items = $schema->resultset('ChecklistItem');

    while (my $checklist = $checklists->next) {

        # get the corresponding checklist_id in the new case
        my $new_checklist = $checklist_rs->search({
            case_id => $target->id,
            case_milestone => $checklist->case_milestone
        })->first;

        # the addition of new checklists happens elsewhere, right now
        # i will only account for those that have been added.
        if ($new_checklist) {
            my $new_id = $new_checklist->id;

            my @items = $rs_items->search({ checklist_id => $checklist->id })->all;

            foreach my $item (@items) {
                my $columns = {$item->get_columns};
                delete $columns->{id};
                $columns->{checklist_id} = $new_id;
                $rs_items->create($columns);
            }
        }
    }
}



sub _copy_document {
    my $self        = shift;
    my $document    = shift;
    my $nieuwe_doc  = shift;

    return 1 if $document->documents_mails->count;

    my $files_dir   = $self->config->{files} . '/documents';

    die('File not found: ' . $files_dir . '/' . $document->id)
        unless ( -f $files_dir . '/' . $document->id);

    die('Failed copying: ' . $files_dir . '/' . $document->id
       . ' TO ' . $files_dir . '/' . $nieuwe_doc->id
    ) unless copy(
        $files_dir . '/' . $document->id,
        $files_dir . '/' . $nieuwe_doc->id
    );

    return 1;
}


sub wijzig_zaaktype {
    my ($self, $zaak, $opts) = @_;

    unless($opts && $opts->{zaaktype_id}) {
        throw "wijzig_zaaktype: need zaaktype_id";
    }

    my $schema   = $self->result_source->schema;
    my $zaaktype = $schema->resultset('Zaaktype')->find($opts->{zaaktype_id}) or
        throw("change/case_type/no_valid_id/$opts->{zaaktype_id}", "Zaaktype ID is niet gevonden");

    my $node_id = $zaaktype->zaaktype_node_id->id;

    my $rs = $zaak->case_documents;
    while (my $file = $rs->next) {
        my $cd = $file->case_documents->first;
        my $id = $cd->case_document_id->bibliotheek_kenmerken_id->id;
        my $ztk = $schema->resultset('ZaaktypeKenmerken')->search_rs({
                bibliotheek_kenmerken_id => $id,
                zaaktype_node_id => $node_id
        })->first;
        if ($ztk) {
            $cd->update({case_document_id => $ztk->id});
        }
        else {
            $cd->delete;
        }
    }

    my $old_case_type = $zaak->zaaktype_id;
    $zaak->update({
        zaaktype_node_id => $node_id,
        zaaktype_id      => $opts->{zaaktype_id},
    });

    $zaak->_bootstrap();
    $zaak->case_actions->delete;
    $zaak->set_vernietigingsdatum();

    # Re-set checklists.
    my $items = $schema->resultset('ChecklistItem')->search({
        checklist_id => {
            -in => $zaak->checklists->get_column('id')->as_query
        }
    });

    $items->delete;
    $zaak->checklists->delete;

    # Cleanup out of date case->casetype embedded relation
    $zaak->object_data->object_relation_object_ids->search({
        name => 'casetype',
    })->delete;

    $schema->resultset('Checklist')->create_from_case($zaak);

    $zaak->trigger_logging(
        'case/update/case_type',
        {
            component => LOGGING_COMPONENT_ZAAK,
            data => {
                case_id       => $zaak->id,
                old_case_type => $old_case_type->id,
                casetype_id   => $zaaktype->id,
            }
        }
    );
    return $zaak;
}


sub execute_rules {
    my ($self, $options) = @_;

    ### What are we doing here...
    return if $self->is_afgehandeld;

    my $status = $options->{ status }; # optional
    my $cache;

    if(!defined $options->{ cache } || $options->{ cache }) {
        $cache = $self->result_source->schema->cache;
        if (!$cache) {
            throw("ZS/Z/R/ZS", "No cache found");
        }
        my $retval = $cache->get({
            key => 'execute_rules',
            params => $options
        });

        return $retval if $retval;
    }


    my $field_values_params = {};

    $field_values_params->{ fase } = $status if $status;
    $field_values_params->{ cache } = $options->{ cache } if defined($options->{ cache });

    my $rules_result = $self->zaaktype_node_id->rules($options)->execute(
        {
            kenmerken      => $self->field_values($field_values_params),
            aanvrager      => $self->aanvrager_object,
            contactchannel => $self->contactkanaal,
            payment_status => $self->payment_status,
            casetype       => $self->zaaktype_node_id,
            confidentiality => $self->confidentiality,
            case_result    => $self->resultaat,
        }
    );

    $self->_set_case_result($rules_result);

    return $cache ? $cache->set({
        key         => 'execute_rules',
        value       => $rules_result,
        params      => $options
    }) : $rules_result;
}


sub _set_case_result {
    my ($self, $rules_result) = @_;

    # TODO check for possible problems
    if(my $set_case_result_action = $rules_result->{set_case_result}) {
        eval {
            my $resultaat_index = $set_case_result_action->{value} - 1;
            my @results = $self->zaaktype_node_id->zaaktype_resultaten->search({}, {order_by => {'-asc' => 'id'}})->all;

            my $new_resultaat = $results[$resultaat_index];
            $self->set_resultaat($new_resultaat->resultaat);
            $self->update;
        };
        if($@) {
            warn "something went terribly wrong, but we just continue" . $@;
        }
    }
    return $rules_result;
}


sub result_info {
    my $self = shift;

    return $self->zaaktype_node_id->zaaktype_resultaten->search({
        resultaat => $self->resultaat
    })->first;
}


sub notifications {
    my ($self) = @_;

    return scalar $self->zaaktype_node_id->zaaktype_notificaties->search({}, {
        prefetch => 'bibliotheek_notificaties_id',
        order_by => {'-asc' => 'me.id'},
    });
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 LOGGING_COMPONENT_ZAAK

TODO: Fix the POD

=cut

=head2 ZAAK_CREATE_PROFILE

TODO: Fix the POD

=cut

=head2 create_relatie

TODO: Fix the POD

=cut

=head2 create_zaak

TODO: Fix the POD

=cut

=head2 duplicate

TODO: Fix the POD

=cut

=head2 execute_rules

TODO: Fix the POD

=cut

=head2 find_as_session

TODO: Fix the POD

=cut

=head2 notifications

TODO: Fix the POD

=cut

=head2 result_info

TODO: Fix the POD

=cut

=head2 wijzig_zaaktype

TODO: Fix the POD

=cut

