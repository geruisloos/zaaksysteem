package Zaaksysteem::Zaken::RelationView;

use Moose;
use namespace::autoclean;

use constant RELATION_TYPES => {
    plain => 'Gerelateerd',
    continuation => 'Vervolgzaak',
    initiator => 'Initiatorzaak'
};

has 'relation' => ( is => 'rw', isa => 'Zaaksysteem::Backend::Case::Relation::Component' );

has 'case_id' => ( is => 'rw', isa => 'Int' );

sub order_seq {
    my $self    = shift;

    if($self->relation->get_column('case_id_a') eq $self->case_id) {
        return $self->relation->order_seq_a(@_);
    } else {
        return $self->relation->order_seq_b(@_);
    }
}

*order_seq_gs = \&order_seq;

sub case {
    my $self = shift;

    if($self->relation->get_column('case_id_a') eq $self->case_id) {
        return $self->relation->case_id_a;
    } else {
        return $self->relation->case_id_b;
    }
}

*case_gs = \&case;

sub type {
    my $self = shift;

    if($self->relation->get_column('case_id_a') eq $self->case_id) {
        return $self->relation->type_a(@_);
    } else {
        return $self->relation->type_b(@_);
    }
}

*type_gs = \&type;

around BUILDARGS => sub {
    my $orig = shift;
    my $self = shift;

    die 'need exactly 2 arguments' unless scalar(@_) == 2;

    my $relation = shift;
    my $case_id = shift;

    $self->$orig({
        relation => $relation,
        case_id => $relation->get_column('case_id_a') eq $case_id ?
            $relation->get_column('case_id_b') :
            $relation->get_column('case_id_a')
    });
};

# Some passthru methods
sub update { shift->relation->update(@_); }
sub to_string { shift->relation->to_string(@_); }
sub id { shift->relation->id; }

sub TO_JSON {
    my $self = shift;

    return {
        id => $self->relation->id,
        order => $self->order_seq,
        type => RELATION_TYPES->{ $self->type } // 'Onbekend',
        case => $self->case
    };
}

__PACKAGE__->meta->make_immutable;




__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 RELATION_TYPES

TODO: Fix the POD

=cut

=head2 TO_JSON

TODO: Fix the POD

=cut

=head2 case

TODO: Fix the POD

=cut

=head2 case_gs

TODO: Fix the POD

=cut

=head2 id

TODO: Fix the POD

=cut

=head2 order_seq

TODO: Fix the POD

=cut

=head2 order_seq_gs

TODO: Fix the POD

=cut

=head2 to_string

TODO: Fix the POD

=cut

=head2 type

TODO: Fix the POD

=cut

=head2 type_gs

TODO: Fix the POD

=cut

=head2 update

TODO: Fix the POD

=cut

