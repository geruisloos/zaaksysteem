package Zaaksysteem::ActionRole::ZAPI;

use Moose::Role;
use namespace::autoclean;

use JSON;
use Try::Tiny;
use Zaaksysteem::Tools;

use constant DEFAULT_JSON_VIEW          => 'Zaaksysteem::View::ZAPI';
use constant DEFAULT_TT_VIEW            => 'Zaaksysteem::View::TT';
use constant DEFAULT_TT_TEMPLATE        => 'zapi/help.tt';

use constant DEFAULT_API_ATTR           => 'ZAPI';
use constant DEFAULT_API_ATTR_MAN       => 'ZAPIMan';

# with qw/
#     Zaaksysteem::ActionRole::Profile
# /;

has 'zapi_man'   => (
    is      => 'rw',
    default => sub { {}; }
);

has '_zapi_actions'   => (
    is      => 'rw',
    default => sub { {}; }
);

=head2 execute

=cut

around execute      => sub {
    my $orig    = shift;
    my $self    = shift;

    my ($controller, $c) = @_;

    my $DEFAULT_API_ATTR_MAN    = DEFAULT_API_ATTR_MAN;
    my $DEFAULT_API_ATTR        = DEFAULT_API_ATTR;

    if (
        exists($self->attributes->{$DEFAULT_API_ATTR_MAN}) &&
        $self->attributes->{$DEFAULT_API_ATTR_MAN}
    ) {
        $self->_execute_help(@_);
    }

    my $call;

    my @execute_opts = @_;
    try {
        $call = $self->$orig(@execute_opts);
    } catch {
        die($_) unless UNIVERSAL::isa($_, 'Zaaksysteem::Exception::Base');

        $self->_handle_exception($controller, $c, $_);
    };

    if (
        exists($self->attributes->{$DEFAULT_API_ATTR}) &&
        $self->attributes->{$DEFAULT_API_ATTR}
    ) {
        $c->forward(DEFAULT_JSON_VIEW);
    }

};

use constant EXCEPTION_MAP => {
    type        => 'type',
    message     => 'messages',
    stack_trace => 'stacktrace',
    object      => 'data',
};

sub _handle_exception {
    my $self                            = shift;
    my ($controller, $c, $exception)    = @_;

    my $error                           = Zaaksysteem::ZAPI::Error->new(
        map {
            EXCEPTION_MAP()->{ $_ } => $exception->$_
        } keys %{ EXCEPTION_MAP() }
    );

    $c->stash->{zapi} = $error;

    $c->detach($c->view('ZAPI'));
}

=head2 _help($controller)

Private method for loading the help functiolity of the controller, it will
load the zapi_actions attribute and the zapi_manual attribute

=cut

sub _help {
    my $self                = shift;
    my ($controller, $c)    = @_;

    return $self->zapi_man if scalar(keys %{ $self->zapi_man });

    $self->_load_zapi_actions(@_);
    $self->_load_zapi_man(@_);
}

=head2 _execute_help

Will generate a help page for the different calls in this controller. It will
also display a readable definition of the necessary json input

=cut

sub _execute_help {
    my $self                = shift;
    my ($controller, $c)    = @_;

    $self->_help($controller, $c);

    burp($self->zapi_man);

    if (
        $c->req->is_xhr
    ) {
        $c->stash->{zapi}   = $self->zapi_man;

        $c->forward(
            $controller->config->{json_view} ||
            DEFAULT_JSON_VIEW
        );
    } else {
        $c->stash->{results}    = $self->zapi_man;

        $c->stash->{template}   = (
            $controller->config->{tt_template} ||
            DEFAULT_TT_TEMPLATE
        );

        $c->forward(
            $controller->config->{tt_view} ||
            DEFAULT_TT_VIEW
        );
    }
}

=head2 _load_zapi_actions($controller)

Will load every action into a hash which contain the ZAPI attribute

 e.g.:
    {
        create  => $ACTION_REF,
        update  => $ACTION_REF,
        read    => $ACTION_REF,
        delete  => $ACTION_REF,
    }

=cut

sub _load_zapi_actions {
    my $self                = shift;
    my ($controller)        = @_;

    for my $action ($controller->get_action_methods()) {
        if (grep { uc($_) eq DEFAULT_API_ATTR } @{ $action->attributes }) {
           $self->_zapi_actions->{ $action->name } = $controller->action_for(
               $action->name
           )
        }
    }

    return 1;
}

=head2 _load_zapi_man($controller, $c)

Will generate a hash containing a manual for this controller

 e.g.:
    {
        methods => {
            list    => profile,
            of      => profile,
            methods => profile
        },
        namespace => 'controller_namespace'
    }

=cut

sub _load_zapi_man {
    my $self                = shift;
    my ($controller, $c)    = @_;

    return $self->zapi_man if scalar(keys %{ $self->zapi_man });

    $self->zapi_man({
        methods     => {},
        namespace   => $controller->path_prefix,
    });

    for my $action (values %{ $self->_zapi_actions }) {
        $self->zapi_man->{methods}->{ $action->name } = {
            url => $self->_example_url_from_action($action, @_)
        }
    }

    $self->_load_profile_man(@_);

    return 1;
}

=head2 _example_url_from_action

Generates an example url for poking this API call

=cut

sub _example_url_from_action {
    my $self                = shift;
    my $action              = shift;
    my ($controller, $c)    = @_;
    my (@args, @captures);

    if (
        $action->attributes->{Chained} &&
        $action->attributes->{Chained}[0] ne '/'
    ) {
        push(@captures, '*');
    }

    if ($action->attributes->{Args} && $action->attributes->{Args}[0] > 0) {
        push(@args, '991') for 0..($action->attributes->{Args}[0]);
    }

    return $c->uri_for(
        $action,
        \@captures,
        @args
    );
}

=head2 _load_profile_man($controller)

Will load the profile man page for all actions in zapi_actions

=cut

sub _load_profile_man {
    my $self    = shift;

    $self->_load_profiles(@_);

    for my $action (keys %{ $self->_profile_list }) {
        next unless defined(
            $self->zapi_man->{methods}->{ $action } &&
            $self->_profile_list->{ $action }
        );

        my $profile = $self->_profile_list->{ $action };

        $self->zapi_man->{methods}->{ $action }->{profile} = $profile;

        $self->zapi_man->{methods}->{ $action }->{json} =
            $self->_generate_json_template($profile);
    }
};

=head2 _generate_json_template($profile)

Generates a representation from given profile

=cut

sub _generate_json_template {
    my $self        = shift;
    my $profile     = shift;
    my (@params);

    my $rv          = {};

    push(@params, @{ $profile->{required} }) if (
        defined($profile->{required}) &&
        $profile->{required}
    );

    push(@params, @{ $profile->{required} }) if (
        defined($profile->{optional}) &&
        $profile->{optional}
    );

    for my $param (@params) {
        if (
            defined($profile->{constraint_methods}) &&
            defined($profile->{constraint_methods}->{$param}) &&
            $profile->{constraint_methods}->{$param}
        ) {
            my $regex       = "" . $profile->{constraint_methods}->{$param} .  "";
            $regex          =~ s/^\(\?-xism:(.*?)\)/$1/;
            $rv->{$param}   = $regex;
        } else {
            $rv->{$param}   = 'example';
        }
    }

    return JSON->new()->encode($rv);
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 DEFAULT_API_ATTR

TODO: Fix the POD

=cut

=head2 DEFAULT_API_ATTR_MAN

TODO: Fix the POD

=cut

=head2 DEFAULT_JSON_VIEW

TODO: Fix the POD

=cut

=head2 DEFAULT_TT_TEMPLATE

TODO: Fix the POD

=cut

=head2 DEFAULT_TT_VIEW

TODO: Fix the POD

=cut

=head2 EXCEPTION_MAP

TODO: Fix the POD

=cut

