package Zaaksysteem::DB::Component::BibliotheekNotificaties;

use strict;
use warnings;


use base qw/DBIx::Class/;
use Mail::Track;
use Zaaksysteem::Tools;
use Zaaksysteem::ZTT;

sub insert {
    my ($self) = @_;

    # we need the database to generate the new searchable_id param.
    # if we don't supply it, it will take its chance. so get rid
    # of whatever the app thinks it should pass.
    # is there a way to put this in the Schema? that's be less hacky.
    delete $self->{_column_data}->{searchable_id};

    $self->_set_search_term();
    return $self->next::method(@_);}

sub update {
    my ($self) = shift;

    $self->_set_search_term();
    return $self->next::method(@_);
}

sub TO_JSON {
    my $self = shift;


    my @attachments = $self->bibliotheek_notificatie_kenmerks->all;

    my @files = map {
        my $result = {
            naam  => $_->bibliotheek_kenmerken_id->naam,
            bibliotheek_kenmerk_id => $_->bibliotheek_kenmerken_id->id,
        };
        $result;
    } @attachments;

    return {
        attachments => \@files,
        label       => $self->label,
        message     => $self->message,
        subject     => $self->subject,
        sender      => $self->sender,
        sender_address => $self->sender_address,
        %{ $self->next::method() },
    }
}

sub _set_search_term {
    my ($self) = @_;
    my $search_term = '';
    if($self->label) {
        $search_term .= $self->label;
    }
    if($self->subject) {
        $search_term .= $self->subject;
    }
    if($self->message) {
        $search_term .= $self->message;
    }

    $self->search_term($search_term);
    $self->search_order($search_term);
}

=head2 send_mail

Arguments: \%PARAMS

Return value: $TRUE_ON_SUCCES

    $self->send_mail(
        {
            to  => 'michiel@example.com',
            from => 'info@example.com',
            ztt_context => {
                magicstring1 => 'value_magistring1',
                surname      => 'Jansen',
            }
        }
    )

Sends a mail containing the current template. With C<ztt_context> filled, it will replace
the magic strings with the given values.

B<Params>

=over 4

=item to [required]

The rcpt of this message

=item ztt_context [required]

A key-value HASHREF containing magic strings and their values.

=item from [optional]

The return-address of this message.

=back

=cut

define_profile 'send_mail' => (
    required    => [qw/to ztt_context/],
    optional    => [qw/from/],
);

sub send_mail {
    my $self = shift;
    my $params = assert_profile(shift || {})->valid;

    my $obj = Mail::Track->new(
        identifier_regex     => qr/ZS (\d{4})/,
        identifier           => '1234',
    );

    my $ztt = Zaaksysteem::ZTT->new;

    $ztt->add_context($params->{ztt_context});

    $params->{subject} = $ztt->process_template($self->subject)->string;
    my $body           = $ztt->process_template($self->message)->string;

    if (!$params->{from}) {
        $params->{from} = $self->sender_address
            ? $self->sender_address
            : $self->result_source->schema->resultset('Config')->get_customer_config->{zaak_email};
    }

    my $msg = $obj->prepare($params);   
    $msg->add_body(
        {
            content      => $body,
            content_type => 'text/plain',
        }
    );
    
    return $msg->send;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 TO_JSON

TODO: Fix the POD

=cut

=head2 insert

TODO: Fix the POD

=cut

=head2 update

TODO: Fix the POD

=cut

