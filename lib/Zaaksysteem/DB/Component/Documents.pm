package Zaaksysteem::DB::Component::Documents;

use strict;
use warnings;

use Moose;
use HTTP::Request::Common;
use Image::Magick;
use Data::UUID;
use Params::Profile;

use Data::Dumper;
use Zaaksysteem::Constants;

extends 'DBIx::Class';

sub document_identifier {
    my $self    = shift;

    my $zaak_id = (
        ref($self->zaak_id)
            ? $self->zaak_id->id
            : $self->zaak_id
    );

    return $zaak_id . '.' . $self->id;
}


sub insert {
    my $self    = shift;

    # we need the database to generate the new searchable_id param.
    # if we don't supply it, it will take it's chance. so get rid
    # of whatever the app thinks it should pass.
    # is there a way to put this in the Schema? that's be less hacky.
    delete $self->{_column_data}->{searchable_id};

    my $ug = new Data::UUID;
    my $uuid = $ug->create();
    my $uuid_string = $ug->to_string($uuid);
    $self->{_column_data}->{uuid} = $uuid_string;

    $self->_set_search_string({insert => 1});
    $self->next::method(@_);
}


sub update {
    my $self    = shift;
    my $columns = shift;

    $self->_set_search_string;
    $self->next::method(@_);
}


sub _set_search_string {
    my ($self) = @_;

    my $search_string = $self->filename;

    if($self->id) {
        $search_string .= " " . $self->id;
    }

    $self->search_term($search_string);
    $self->search_order($search_string);
}


#
# converts saves this document as a pdf file on server disk, returns the filepath.
#
sub save_as_pdf {
    my ($self, $opts) = @_;

    my $basedir = $opts->{basedir} or die "need basedir";
    my $destination = $opts->{destination} or die "need destination";

    my $source = $basedir . '/documents/' . $self->id;
#    warn "document filename: " . $source;
#    warn "destination: " . $destination;


    my $mimetypes_allowed = MIMETYPES_ALLOWED;
    my $pdfmimetypes = {
        map {
            MIMETYPES_ALLOWED->{$_}->{mimetype} => MIMETYPES_ALLOWED->{$_}->{conversion}
        }
        grep {
            defined MIMETYPES_ALLOWED->{$_}->{conversion}
        }
        keys %$mimetypes_allowed
    };

    my $mimetype = $self->mimetype;
    my $convertor = $pdfmimetypes->{$mimetype};
    die "no convertor available for mimetype $mimetype" unless($convertor);

    my $pdf_filepath;
    if($convertor eq 'jodconvertor') {

        return $self->_convert_with_jodconvertor({
            source          => $source,
            destination     => $destination,
        });

    } elsif($convertor eq 'imagemagick') {

        return $self->_convert_with_imagemagick({
            source          => $source,
            destination     => $destination,
        });
    } else {
        die "unknown convertor, check configuration";
    }
}



Params::Profile->register_profile(
    method  => '_convert_with_jodconvertor',
    profile => {
        required => [ qw/destination source/ ]
    }
);
sub _convert_with_jodconvertor {
    my ($self, $params) = @_;

    my $dv = Params::Profile->check(params => $params);
    die "invalid options for _convert_with_jodconvertor" unless $dv->success;

    my $source = $dv->valid('source');

     # read file contents
    CORE::open FILE, $source
        or die "couldnot open file [$source]: $!";
    my $content = join "", <FILE>;
    close FILE;


#    my $ua = LWP::UserAgent->new;
#    my $result = $ua->request(POST 'http://127.0.0.1:8080/converter/service',
#        Content         => $content,
#        Content_Type    => $self->mimetype,
#        Accept          => 'application/pdf',
#    );

#    warn "jodconvertor result: " . $result->code;

#    unless($result->code eq '200') {
#        warn "could not download doc";
#        warn "result: " . Dumper $result->content();
#    }

    my $destination = $dv->valid('destination');
#    warn "Writing to destination: " . $destination;
    CORE::open FILE2, '>' . $destination
        or die "could not open file for writing: $!";
    print FILE2 "dummy content"; #$result->content || 'no content';
    close FILE2;
}



Params::Profile->register_profile(
    method  => '_convert_with_imagemagick',
    profile => {
        required => [ qw/source destination/ ]
    }
);
sub _convert_with_imagemagick {
    my ($self, $params) = @_;

    my $dv = Params::Profile->check(params => $params);
    die "invalid options for _convert_with_imagemagick" unless $dv->success;

    my $magick = new Image::Magick();

    my $status = $magick->Read($dv->valid('source'));
    warn "Read failed: $status" if $status;

    $status = $magick->Write($dv->valid('destination'));
    warn "Write failed: $status" if $status;

    warn 'finito';
}

1; #__PACKAGE__->meta->make_immutable;




__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 document_identifier

TODO: Fix the POD

=cut

=head2 insert

TODO: Fix the POD

=cut

=head2 save_as_pdf

TODO: Fix the POD

=cut

=head2 update

TODO: Fix the POD

=cut

