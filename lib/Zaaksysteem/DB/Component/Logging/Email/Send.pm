package Zaaksysteem::DB::Component::Logging::Email::Send;

use Data::Dumper;
use Moose::Role;
use HTML::Strip;

use Zaaksysteem::Constants;
use Encode qw(encode_utf8 decode_utf8);

has case => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        my $self = shift;

        $self->rs('Zaak')->find($self->data->{case_id});
    }
);

=head2 onderwerp

Generate the main description for this event.

=cut

sub onderwerp {
    my $self = shift;

    my $data = $self->data;
    my ($subject) = HTML::Strip->new->parse(encode_utf8($data->{ subject })) =~ m/^\s*(.*?)\s*$/;
    my $onderwerp = sprintf('E-mail "%s" verstuurd naar "%s"', decode_utf8($subject), $data->{ recipient });

    # optional
    if ($data->{cc}) {
        $onderwerp .= ", cc: " . $data->{cc};
    }

    if ($data->{bcc}) {
        $onderwerp .= ", bcc: " . $data->{bcc};
    }

    return $onderwerp;
}


=head2 export_as_pdf

Returns a PDF version the e-mail. This is a binary stream that is intended
to be downloaded directly to the browser. At this point there's no requirement
for storing it.

=cut

sub export_as_pdf {
    my $self = shift;

    my $data = $self->data;

    # Be backwards compatible with event data pre-from in the logging table
    my $from = $data->{from};
    if (!defined $from) {
        my $gemeente = $self->result_source->schema->default_resultset_attributes->{config}->{gemeente};
        require Email::Address;
        $from = Email::Address->new($gemeente->{naam}, $gemeente->{zaak_email})->format;
    }

    my $args = {
        from        => $from,
        to          => encode_utf8($data->{recipient}),
        subject     => encode_utf8($data->{subject}),
        body        => encode_utf8($data->{content}),
        attachments => $data->{attachments},
    };
    foreach (qw(cc bcc)) {
        $args->{$_} = encode_utf8($data->{$_}) if $data->{$_};
    }

    my $outlookMailParser = Zaaksysteem::OutlookMailParser->new();
    my $formatted = $outlookMailParser->format($args);

    my $converted = $self->rs('Filestore')->send_to_converter(
        Content      => $formatted,
        Content_Type =>  MIMETYPES_ALLOWED->{'.txt'}{mimetype},
        Accept       => 'application/pdf'
    );

    return $converted;
}

sub event_category { 'contactmoment' }

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 event_category

TODO: Fix the POD

=cut

=head2 onderwerp

TODO: Fix the POD

=cut

