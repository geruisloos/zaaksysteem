package Zaaksysteem::DB::Component::Logging::Case::CloseChild;
use Moose::Role;

=head2 onderwerp

The human-readable "subject" of this log message.

=cut

sub onderwerp {
    my $self = shift;

    return sprintf(
        'Deelzaak %s afgehandeld: %s',
        $self->data->{ child_case_id },
        $self->data->{ case_result }
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
