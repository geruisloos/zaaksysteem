package Zaaksysteem::DB::Component::Logging::Case::IdRequested;
use Moose::Role;

=head2 onderwerp

Creates a human readable subject for "case/id_requested" type log entries.

=cut

sub onderwerp {
    my $self = shift;

    return sprintf(
        "Zaaknummer %d gereserveerd door %s via %s.",
        $self->component_id,
        $self->data->{remote_system} // '(onbekend)',
        $self->data->{interface} // '(onbekende bron)',
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
