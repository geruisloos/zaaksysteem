package Zaaksysteem::DB::Component::Logging::Case::Document::Properties::Update;

use Moose::Role;
use HTML::Entities qw(encode_entities);

sub onderwerp {
    my $self = shift;

    if($self->data->{ updated_properties}{ accepted }) {
        return sprintf(
            'Document "%s" geaccepteerd',
            encode_entities($self->file->name)
        );
    }

    return sprintf(
        'Document "%s" aangepast',
        encode_entities($self->file->name)
    );
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 onderwerp

TODO: Fix the POD

=cut

