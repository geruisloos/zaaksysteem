package Zaaksysteem::DB::Component::Logging::NatuurlijkPersoon::Enable;
use Moose::Role;

=head1 NAME

Zaaksysteem::DB::Component::Logging::NatuurlijkPersoon::Disable - Logging component for Natuurlijk Persoon enable actions

=head2 onderwerp

Generate the main description for this event.

=cut

sub onderwerp {
    my $self = shift;

    my $data = $self->data;

    return sprintf(
        'Natuurlijk persoon met BSN %s (id: %d) is toegevoegd op datum %s',
        $data->{bsn}, $data->{id}, $data->{date}
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
