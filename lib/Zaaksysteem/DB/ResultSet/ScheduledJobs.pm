package Zaaksysteem::DB::ResultSet::ScheduledJobs;


use strict;
use warnings;

use Moose;
use Data::Dumper;
use Data::Serializer;

use HTML::Strip;

use Zaaksysteem::Profile;
use Zaaksysteem::Exception;

use constant SCHEDULE_TYPE_MANUAL   => 'manual';
use constant SCHEDULE_TYPE_TIME     => 'time';
use constant SCHEDULE_TYPE_DEFAULT  => SCHEDULE_TYPE_TIME;


extends 'DBIx::Class::ResultSet';


=head2 $resultset->pending()

Return value: $RESULTSET_SCHEDULEDJOBS

  my $rs_jobs = $resultset->pending()

Returns the resultset of jobs of schedule_type = 'TIME' and which are
still in the future (won't run anytime soon)

=cut

sub pending {
    my $self        = shift;

    return $self->search({
        schedule_type   => { '!='   => SCHEDULE_TYPE_MANUAL },
        scheduled_for   => { '>'    => DateTime->now()},
        deleted         => undef,
    }, {
        order_by => {'-asc' => 'scheduled_for'}
    });
}

=head2 $resultset->ready([ \%OPTIONS ])

Return value: $RESULTSET_SCHEDULEDJOBS

  my $rs_jobs = $resultset->ready()

Returns the resultset of jobs which are ready to run, e.g. manual jobs
or jobs which are scheduled in the past.

B<Options>

=over 4

=item schedule_type OPTIONAL (DEFAULT: time)

This defines whether the searched schedule item is of type manual or time.

=back

=cut

sub ready {
    my ($self, $options)    = @_;
    $options                ||= {};

    my $dv              = Data::FormValidator->check(
        $options,
        {
            optional    => [qw/
                schedule_type
            /],
            defaults    => {
                schedule_type => SCHEDULE_TYPE_DEFAULT,
            }
        }
    );

    die("ScheduledJobs->ready: Profile ERROR: " . Dumper($dv))
        unless $dv->success;

    if($options->{ schedule_type } eq SCHEDULE_TYPE_MANUAL) {
        return $self->search({
            schedule_type   => SCHEDULE_TYPE_MANUAL,
            deleted         => undef,
        }, {
            order_by => {'-asc' => 'last_modified'}
        });
    }

    return $self->search({
        schedule_type   => { '!='   => SCHEDULE_TYPE_MANUAL },
        scheduled_for   => {'<=' => DateTime->now()},
        deleted         => undef,
    }, {
        order_by => {'-asc' => 'scheduled_for'}
    });
}


=head1 $resultset->cancel_pending(\%OPTIONS)

Return value: $TRUE_OR_FALSE

  $resultset->cancel_pending({ $zaak_id => 1234 });

Cancels all the jobs that do not have ran yet for the given zaak_id

B<Options>

=over 4

=item zaak_id

A case id defining the case for which all jobs are canceled

=back

=cut

sub cancel_pending {
    my ($self, $options) = @_;

    my $zaak_id = $options->{zaak_id} or die "need zaak_id";

    ### Get mailer interfae
    my $mail_object     = $self
                        ->result_source
                        ->schema
                        ->resultset('Interface')
                        ->search_active(
                            {
                                module => 'email',
                            }
                        )->first;

    $mail_object->process_trigger(
        'delete_pending',
        {
            case_id                     => $zaak_id,
        }
    );

    # my $pending = $self->pending({
    #     task => 'case/mail'
    # });

    # while(my $job = $pending->next()) {

    #     # eagerly awaiting json querying, this will become incrementally slower
    #     my $job_zaak_id = $job->parameters->{zaak_id} or next;

    #     if ($job_zaak_id eq $zaak_id) {
    #         $job->delete;
    #     }
    # }

    return 1;
}

=head2 $resultset->reschedule_pending(\%OPTIONS)

TODO DOC THIS

=cut


sub reschedule_pending {
    my ($self, $options) = @_;

    my $zaak_id         = $options->{zaak_id}           or die "need zaak_id";
    my $scheduled_mails = $options->{scheduled_mails}   or die "need scheduled_mails";
    my $notifications   = $options->{notifications}     or die "need notifications";

    my $rescheduled_mails = {};

    my $notification_index = 0;

    while(my $notification = $notifications->next()) {
        $notification_index++;

        if(my $schedule_mail = $scheduled_mails->{$notification_index}) {

            # mails that have been scheduled for the phase transition will be executed by the
            # action mechanism
            next if $schedule_mail->{phase_transition};

            my $send_date_dt = $schedule_mail->{send_date};

            my $new = $self->create_zaak_notificatie({
                bibliotheek_notificaties_id => $notification->bibliotheek_notificaties_id->id,
                zaaktype_notificatie_id => $notification->id,
                scheduled_for   => $send_date_dt,
                recipient_type  => $notification->rcpt,
                zaak_id         => $zaak_id,
                behandelaar     => $notification->behandelaar,
                email           => $notification->email,
            });

            $rescheduled_mails->{$notification_index}++;
        }
    }

    return $rescheduled_mails;
}

=head2 email_interface

Convenience cached method for getting a handle to the email interface.

=cut

has email_interface => (
    is => 'ro',
    lazy => 1,
    default => sub {
        my $self = shift;

        my $resultset = $self->result_source->schema->resultset('Interface');

        return $resultset->search_active({module => 'email'})->first || throw (
            'scheduled_jobs/no_mail_interface',
            'Er is geen actieve e-mail koppeling gevonden. Neem contact op met de beheerder.'
        );
    }
);

=head2 $resultset->create_zaak_notificatie(\%OPTIONS)

TODO DOC THIS

wrapper around create - purpose is to enforce required parameters

=cut

sub create_zaak_notificatie {
    my ($self, $options) = @_;

    my $scheduled_for   = $options->{scheduled_for}     or die "need scheduled_for";
    my $zaak_id         = $options->{zaak_id}           or die "need zaak_id";
    my $recipient_type  = $options->{recipient_type}    or die "need recipient_type";
    my $behandelaar     = $options->{behandelaar};      # optional
    my $email           = $options->{email};            # optional

    my $bibliotheek_notificaties_id = $options->{bibliotheek_notificaties_id}
        or die "need bibliotheek_notificaties_id";

    my $zaaktype_notificatie_id = $options->{zaaktype_notificatie_id}; # optional

    $self->email_interface->process_trigger(
        'schedule_mail',
        {
            case_id                     => $zaak_id,
            scheduled_for               => $scheduled_for,
            zaaktype_notificatie_id     => $zaaktype_notificatie_id,
            notification                => {
                id                          => $bibliotheek_notificaties_id,
                recipient_type              => $recipient_type,
                behandelaar                 => $behandelaar,
                email                       => $email,
                case_id                     => $zaak_id,
            }
        }
    );
}


=head2 $resultset->_create_task(%OPTIONS)

Return value: $ROW_SCHEDULEDJOBS

    $resultset->_create_task(
        {
            parameters          => {
                beermanufacturer    => 'Grolsch',
                like                => 'No',
            },
            schedule_type       => 'auto',
            scheduled_for       => DateTime->now(),
        }
    )

Creates a task in the scheduler for run at a later time. Tasks could be scheduled
for another time, or for review by a case manager.

B<OPTIONS>

=over 4

=item parameters OPTIONAL

An HASHREF of parameters to send to the given task.

=item task REQUIRED

The task to be run. Needs to be a valid task from L<task_list>.

=item schedule_type REQUIRED

The schedule_type can be one of 'auto' or 'manual'. When an 'auto' type is given,
make sure the scheduled_for is set with a valid L<DateTime> object.

=item scheduled_for REQUIRED when SCHEDULE_TYPE = 'auto'

A valid DateTime object when schedule_type = 'auto'

=back

B<Schedulers>

Below a list of scheduled tasks

=head3 case/update_kenmerk %OPTIONS

Return value: $ROW_SCHEDULEDJOBS

    $resultset->schedule_kenmerk(
        {
            bibliotheek_kenmerken_id    => 55,
            value                       => 'Nee',
            requestor                   => $case->aanvrager_object,
            case                        => $case,
        }
    )

Schedules a zaak kenmerk for updating. This function only supports scheduling
for approval by a behandelaar (not scheduled on a certain time)

B<Options>

=over 4

=item bibliotheek_kenmerken_id

The kenmerk ID to change the value for.

=item value

Value for the 'to be changed' kenmerk.

=item reason

The reason for this change. A text explaining why the requestor would like to change
this kenmerk

=item case

The case object for this change

=item requestor OPTIONAL

The requestor object for this change.

=back

=cut

use constant SCHEDULE_TASK_LIST => {
    'case/update_kenmerk'  => {
        'scheduler'     => {
            'profile'       => {
                'missing_optional_valid'  => 1,
                required    => [qw/
                    bibliotheek_kenmerken_id
                    case
                /],
                optional    => [qw/
                    value
                    reason
                    parameters
                    created_by
                /],
                defaults    => {
                    schedule_type   => SCHEDULE_TYPE_MANUAL,
                    parameters      => sub {
                        my ($dfv)   = @_;

                        my $values  = $dfv->get_filtered_data;

                        return unless ($values->{case});

                        my $stripper = HTML::Strip->new;

                        my $reason = $values->{reason} ? $stripper->parse($values->{reason}): '';
                        my $value = defined $values->{value} ? $values->{value} : '';

                        my $parameters = {
                            value => $value,
                            reason => $reason,
                            bibliotheek_kenmerken_id => $values->{bibliotheek_kenmerken_id},
                            case_id => $values->{case}->id,
                        };

                        $parameters->{created_by} = $values->{created_by}
                            if defined $values->{created_by};

                        return $parameters;
                    },
                    case_id         => sub {
                        my ($dfv)   = @_;

                        my $values  = $dfv->get_filtered_data;

                        return unless $values->{case};

                        return $values->{case}->id;
                    },
                }
            }
        },
    }
};

has 'task_list' => (
    is          => 'ro',
    lazy        => 1,
    default     => sub {
        return SCHEDULE_TASK_LIST();
    }
);

use constant CREATE_TASK_PROFILE => {
    required    => [qw/
        task
        schedule_type
    /],
    optional    => [qw/
        parameters
        scheduled_for
        case_id
    /],
    constraint_methods => {
        task    => sub {
            my ($dfv, $val) = @_;

            return (SCHEDULE_TASK_LIST()->{$val});
        },
        parameters => sub {
            my ($dfv, $val) = @_;

            return UNIVERSAL::isa($val, 'HASH');
        }
    },
    dependencies    => {
        schedule_type   => sub {
            my $dfv     = shift;
            my $type    = shift;

            if ($type eq SCHEDULE_TYPE_MANUAL) {
                return [''];
            }

            return ['scheduled_for'];
        }
    }
};


sub create_task {
    my ($self, $opts)   = @_;
    $opts               ||= {};

    my ($taskdef, $create_options);

    {
        ### Check for valid INPUT
        die("ScheduledJobs->create_task: input error: no task given.")
            unless $opts && $opts->{task};

        die("ScheduledJobs->create_task: input error: invalid task given.")
            unless defined($self->task_list->{ $opts->{task} });

        $taskdef     = $self->task_list->{ $opts->{task} };

        ### Check for valid TASK
        die("ScheduledJobs->create_task: Definition error: no scheduler set.")
            unless $taskdef && $taskdef->{scheduler};

        die("ScheduledJobs->create_task: Definition error: no profile set.")
            unless $taskdef->{scheduler}->{profile};
    }

    ###
    ### VALIDATE TASK
    ###
    {
        my $scheduler_dv    = Data::FormValidator->check(
            $opts,
            $taskdef->{scheduler}->{profile}
        );

        die(
            "ScheduledJobs[" . $opts->{task} . ']: Profile Error: '
            . Dumper($scheduler_dv)
        ) unless $scheduler_dv->success;

        my $schedule_params = $scheduler_dv->valid;

        $create_options  = { map { $_ => $schedule_params->{ $_ } }
            $self->result_source->columns };
        delete($create_options->{$_}) for qw/id deleted last_modified created/;

        $create_options->{task} = $opts->{task};
    }

    ### Validate database rows for ScheduledJobs
    my $dv              = Data::FormValidator->check(
        $create_options,
        CREATE_TASK_PROFILE
    );

    die("ScheduledJobs->create_task: Profile ERROR: " . Dumper($dv))
        unless $dv->success;

    ### Strip unknown columns
    my $create_params = {
        map { $_ => $dv->valid($_) }
        grep { defined($dv->valid( $_ )) } $self->result_source->columns
    };

    $self->create($create_params);
}


=head2 search_update_field_tasks

Given a case and a list of bibliotheek_kenmerken_ids - find the
outstanding update_kenmerk requests in the scheduled_job queue.

Used for displaying the number on top of the phase, and embedding
the approve/deny form within the case fields view.

=cut

define_profile search_update_field_tasks => (
    required => [qw[case_id]],
    optional => [qw[kenmerken]]
);

sub search_update_field_tasks {
    my $self      = shift;
    my $arguments = assert_profile(shift)->valid;
    my $case_id   = $arguments->{case_id} or die "need case_id";

    my $rs = $self->search({task => 'case/update_kenmerk', deleted => undef, case_id => $arguments->{case_id} });

    if(my $kenmerken = $arguments->{kenmerken}) {
        my $regexp = join "|", map { quotemeta($_) } @$kenmerken;

        $rs = $rs->search({
            parameters => {
                'similar to' => '%"bibliotheek_kenmerken_id":"(' . $regexp . ')"%'
            }
        });
    }

    return $rs->search({}, {order_by => { -desc => 'id' }});
}



=head2 only_most_recent_field_update

this method expects case/update_kenmerk type records with a
bibliotheek_kenmerken_id field in the json parameters.

filter the tasks on the field_id. if a user issues multiple changes
to a field, we're only interested in the last update anyway, the rest
we just keep for bookkeeping - or maybe undo functionality in the future

since this happens in a json column in the database we do it in perl, assuming
that no more than 20-30 field updates per caseview would be involved.

=cut

sub only_most_recent_field_update {
    my ($self) = @_;

    my $seen = {};

    return grep {
        my $kenmerk_id = $_->parameters->{bibliotheek_kenmerken_id};

        # only interested in the first occurence - sorted descending
        my $isFirstOccurence = !exists $seen->{$kenmerk_id};
        $seen->{$kenmerk_id} = 1;

        # pass this value back to grep
        $isFirstOccurence;
    } $self->all;
}


sub set_deleted {
    my ($self) = @_;

    while (my $row = $self->next()) {
        $row->reject();
    }
}

1;





__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 CREATE_TASK_PROFILE

TODO: Fix the POD

=cut

=head2 SCHEDULE_TASK_LIST

TODO: Fix the POD

=cut

=head2 SCHEDULE_TYPE_DEFAULT

TODO: Fix the POD

=cut

=head2 SCHEDULE_TYPE_MANUAL

TODO: Fix the POD

=cut

=head2 SCHEDULE_TYPE_TIME

TODO: Fix the POD

=cut

=head2 cancel_pending

TODO: Fix the POD

=cut

=head2 create_task

TODO: Fix the POD

=cut

=head2 set_deleted

TODO: Fix the POD

=cut

