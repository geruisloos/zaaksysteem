package Zaaksysteem::DB::Profile;

use Moose;

extends 'DBIx::Class::Storage::Statistics';

use Time::HiRes qw(time);
use Carp qw[longmess];

=head2 start_time

The time the measure was started

=cut

has 'start_time' => (
    is      => 'rw',
    default => 0,
);

=head2 query_count

The amount of queries run

=cut

has 'query_count' => (
    is      => 'rw',
    isa     => 'HashRef',
    lazy    => 1,
    default => sub { return {}; },
);

=head2 _queries

A store with a list of queries, for later reference

=cut

has '_queries'       => (
    is      => 'rw',
    lazy    => 1,
    isa     => 'ArrayRef',
    default => sub { return []; }
);

=head2 test_count

A simple counter, for the testsuite, so it can detect how much queries a code block took

=cut

has 'test_count' => (
    is      => 'ro',
    traits  => ['Counter'],
    isa     => 'Int',
    default => 0,
    handles => {
        inc_test_count   => 'inc',
        dec_test_count   => 'dec',
        reset_test_count => 'reset',
    },
);

=head2 in_test_count

Boolean indicating we should up the test_count attribute per query

=cut

has 'in_test_count' => (
    is      => 'rw',
    isa     => 'Bool',
    default => 1,
);

sub query_start {
    my ($self, $sql, @params) = @_;

    $self->query_count->{ $sql } = 0 unless $self->query_count->{ $sql };
    $self->query_count->{ $sql }++;


    ### This way the TestUtils->allow_num_queries can calculate the num queries
    $self->inc_test_count if $self->in_test_count;

    if ($ENV{DBIC_TRACE}) {
        $self->print("Query : $sql\n");

        if(scalar @params) {
            $self->print("Params: " . join(', ', @params) . "\n");
        }

        $self->print("Seen  : " . ($self->query_count->{ $sql } || 'never') . "\n");

        if ($ENV{DBIC_FULL_TRACE}) {
            $self->print(longmess('Trace :'));
        } else {
            my $iter = 10;
            my $caller_package = "";
            my $caller_line;

            while (   defined $caller_package
                   && (   $caller_package !~ m[^Zaaksysteem]
                       || $caller_package eq 'Zaaksysteem::Tools')
            ) {
                $iter++;

                ($caller_package, $caller_line) = (caller $iter)[0, 2];
            }

            $self->print("Caller: $caller_package at $caller_line\n");
        }
    }

    $self->start_time(time);

    return;
}

sub query_end {
    my $self    = shift();
    my $sql     = shift();
    my @params  = @_;

    my $elapsed = sprintf("%0.4f", time() - $self->start_time);

    if ($ENV{DBIC_TRACE}) {
        $self->print("Time  : $elapsed seconds\n\n");
    }

    push @{ $self->_queries }, {
        took    => $elapsed,
        query   => $sql,
    };

    return;
}

sub reset_query_count {
    my $self    = shift;

    $self->_queries([]);
    $self->start_time(0);

    return;
}

sub queries {
    my $self    = shift;
    return $self->_queries;
}

#sub interval {return sprintf("%0.4f", time() - $start); }

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 queries

TODO: Fix the POD

=cut

=head2 query_end

TODO: Fix the POD

=cut

=head2 query_start

TODO: Fix the POD

=cut

=head2 reset_query_count

TODO: Fix the POD

=cut
