=head1 NAME

Zaaksysteem::Manual::API::V1::Case::Relation - Modification of inter-case relationships

=head1 Description

This API-document describes the usage of our JSON Case/Relation API. This API
allows callers to create and remove links between two cases, and to re-order
those links.

=head2 API

This document is based on the V1 API of Zaaksysteem, more information about the
default format of this API can be found in L<Zaaksysteem::Manual::API::V1>.
Please make sure you read this document before continuing.

=head1 Modify data

=head2 add

    /api/v1/case/d7c73f04-5fcc-4c03-b089-b5fc760a08d5/relation/add

Creates a new relationship between two cases. The other side of the
relationship should be specified in the C<related_id> key in the POSTed JSON
object.

B<Example call>

    curl --anyauth -k --data @file_with_request_json --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/case/32da49c7-d6d9-449e-9de7-18203049f181/relation/add

B<Request JSON>

=begin javascript

{
    "related_id" : "24724299-0bfb-4959-b18b-e5216b35a0a5"
}

=end javascript


B<Response JSON>

See L<Zaaksysteem::Manual::API::V1::Case/get>

=head2 delete

    /api/v1/case/d7c73f04-5fcc-4c03-b089-b5fc760a08d5/relation/delete

Delete the relationship with the case specified in the C<related_id> key in the
POSTed JSON object.

B<Example call>

    curl --anyauth -k --data @file_with_request_json --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/case/32da49c7-d6d9-449e-9de7-18203049f181/relation/delete

B<Request JSON>

=begin javascript

{
    "related_id" : "a2512302-de68-46e4-acb3-79a09ee9207d"
}

=end javascript

B<Response JSON>

See L<Zaaksysteem::Manual::API::V1::Case/get>

=head2 move

    /api/v1/case/d7c73f04-5fcc-4c03-b089-b5fc760a08d5/relation/move

Moves the specified relationship to the specified location in the list of case
relationships.

The relation will be moved to after the relation with the case whose UUID is
specified in the C<after> key in the POSTed JSON message.

If no C<after> case UUID is specified, the relation is moved to the top of the list.

B<Example call>

    curl --anyauth -k --data @file_with_request_json --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/case/32da49c7-d6d9-449e-9de7-18203049f181/relation/move

B<Request JSON>

=begin javascript

{
    "related_id" : "28722d3d-e9f4-4dea-ac43-6f3a6215434e"
    "after" : "a2512302-de68-46e4-acb3-79a09ee9207d"
}

=end javascript

B<Response JSON>

See L<Zaaksysteem::Manual::API::V1::Case/get>

=head1 Support

The data in this document is supported by the following test. Please make sure
you use the API as described in this test. Any use of this API outside the
scope of this test is B<unsupported>

L<TestFor::Catalyst::API::V1::Case::Relation>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
