=head1 NAME

Zaaksysteem::Manual::API::V1::Types::Set - Type definition for set objects

=head1 DESCRIPTION

This page documents the serialization for C<set> objects.

=head1 JSON

=begin javascript

{
    "reference": null,
    "type": "set",
    "instance": {
        "pager": {
            "page": 1,
            "pages": 2,
            "rows": 10,
            "total_rows": 14,
            "next": "https://zs.instance.tld/api/v1/my/endpoint?page=2",
            "prev": null
        },
        "rows": [
            { ... },
            { ... }
        ]
    }
}

=end javascript

=head1 INSTANCE ATTRIBUTES

=head2 pager

This attribute consists of a set of key-value pairs describing the pagination
state of the request.

=over 4

=item page

The current page.

=item pages

The total amount of pages in the set.

=item rows

The amount of objects returned in the request

=item total_rows

The total amount of objects in the set.

=item next

An URL which, when called, returns the next page of results in the set.

=item prev

An URL which, when called, returns the previous page of results in the set.

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
