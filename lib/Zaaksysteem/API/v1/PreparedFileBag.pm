package Zaaksysteem::API::v1::PreparedFileBag;

use Moose;

use Zaaksysteem::Tools;

=head1 NAME

Zaaksysteem::API::v1::PreparedFileBag - Datawrapper object to hold onto
multiple files

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 file_references

This attribute contains a hashref of C<< UUID => filename >> tuples. It has
the Moose Hash trait, and exposes C<set> via C<set_reference>.

=cut

has file_references => (
    is => 'rw',
    isa => 'HashRef',
    traits => [qw[Hash]],
    default => sub { {} },
    handles => {
        set_reference => 'set'
    }
);

=head1 METHODS

=head2 add

Add file references to the bag.

    $bag->add($file1, $file2, ..., $fileN);

=cut

sig add => '@Zaaksysteem::Backend::Filestore::Component';

sub add {
    shift->set_reference(map { $_->uuid => $_->original_name } @_);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
