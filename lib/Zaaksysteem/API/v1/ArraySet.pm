package Zaaksysteem::API::v1::ArraySet;

use Moose;
use namespace::autoclean -except => 'meta';

use Zaaksysteem::Tools;
use Data::Page;

=head1 NAME

Zaaksysteem::API::v1::ArraySet - Marry the concepts of an API view of data with an ArrayRef

=head1 SYNOPSIS

    use Zaaksysteem::API::v1::ArraySet;

    my $set = Zaaksysteem::API::v1::ArraySet->new(
        content => [Object1, Object2]
    );

    $set->init_paging($c->request);

    # Serialize the dataview with a page.
    $c->stash->{ result } = $set;

=head1 DESCRIPTION

This class exists for the purpose of marrying the concepts of a 'dataview', as
experienced by an APIv1 consumer and the underlying arrayref. This
object has all the metadata required for the API's serializer to produce
representations of a paged dataset.

The idea is that instead of stitching together page state data from query
parameters (like most actions do), instances of this object abstract that
need away in a consistent way.

=head1 ATTRIBUTES

=head2 content

Holds an arrayref that is used as the content to page.

=cut

has content => (
    is => 'rw',
    isa => 'ArrayRef',
    required => 1,
);

=head2 page

State field that determines the current page within the dataview is to be
used. Defaults to page C<1>.

=cut

has page => (
    is => 'rw',
    isa => 'Int',
    required => 1,
    default => 1
);

=head2 pager

Returns a L<Data::Page> object containing the pager for this array set

=cut

sub pager {
    my $self        = shift;

    my $pager       = Data::Page->new();
    $pager->total_entries(scalar @{ $self->content });
    $pager->entries_per_page($self->rows_per_page);
    $pager->current_page($self->page);

    return $pager;
}

=head2 paged_content

Only returns the part of the array which is bound to the current page

=cut

sub paged_content {
    my $self        = shift;
    my $pager       = shift || $self->pager;

    return [ $pager->splice($self->content) ];
}

=head2 rows_per_page

Limit on the maximum amount of entries/items/rows on a single page. Defaults
to C<10>.

=cut

has rows_per_page => (
    is => 'rw',
    isa => 'Int',
    required => 1,
    default => 10
);

=head2 allow_rows_per_page

Limit on the maximum amount of entries/items/rows which can be set via the
"rows_per_page" query parameter. Defaults to C<50>.

=cut

has allow_rows_per_page => (
    is => 'rw',
    isa => 'Int',
    required => 1,
    default => 250,
);

=head2 uri

Holds an L<URI> object that is used as a base to provide paged access to the
resource.

=cut

has uri => (
    is => 'rw',
    isa => 'URI',
    predicate => 'has_uri'
);

=head1 METHODS

=head2 init_paging

=over 4

=item Arguments: L<Catalyst::Request>

=item Return value: $self

=item Signature: C<< Catalyst::Request => Zaaksysteem::API::v1::Set >>

=back

This method initializes the set with the current 'view', according to the
request being made.

=cut

sig init_paging => 'Catalyst::Request => Zaaksysteem::API::v1::ArraySet';

sub init_paging {
    my $self = shift;
    my $res = shift;
    my $params = $res->query_params;

    $self->uri($res->uri->clone);

    if (exists $params->{ page }) {
        $self->page($params->{ page });
    }

    if (
        exists $params->{ rows_per_page } &&
        int($params->{ rows_per_page }) <= $self->allow_rows_per_page
    ) {
        $self->rows_per_page($params->{ rows_per_page });
    }

    return $self;
}

=head2 mangle_uri

Produces a string representation of an URI that can be passed to the frontend
to retrieve a view of the object's data set to a specific page.

=over 4

=item Arguments: $INT_PAGENUMBER

=item Return value: $STRING_URL

=item Signature: C<< Maybe[Int] => Maybe[Str] >>

=back

    my $uri = $set->mangle_uri($next_page);

=cut

sub mangle_uri {
    my $self = shift;
    my $page = shift;

    return undef unless defined $page && $self->has_uri;

    my $uri = $self->uri->clone;

    $uri->query_param(page => $page);

    return $uri->as_string;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
