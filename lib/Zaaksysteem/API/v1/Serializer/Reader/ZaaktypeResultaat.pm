package Zaaksysteem::API::v1::Serializer::Reader::ZaaktypeResultaat;

use Moose;

with 'Zaaksysteem::API::v1::Serializer::ReaderRole';

=head1 NAME

=head1 SYNOPSIS

=head1 METHODS

=head2 class

=cut

sub class { 'Zaaksysteem::Model::DB::ZaaktypeResultaten' }

=head2 read

=cut

sub read {
    my ($class, $serializer, $resultaat) = @_;

    return {
        resultaat_id            => $resultaat->id,
        type                    => $resultaat->resultaat,
        archive_procedure       => ($resultaat->ingang // undef),
        period_of_preservation  => ($resultaat->bewaartermijn // undef),
        type_of_dossier         => ($resultaat->dossiertype // undef),
        label                   => ($resultaat->label // undef),
        selection_list          => ($resultaat->selectielijst // undef),
        type_of_archiving       => ($resultaat->archiefnominatie // undef),
        comments                => ($resultaat->comments // undef),
        external_reference      => ($resultaat->external_reference // undef),
        trigger_archival        => ($resultaat->trigger_archival // undef),
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
