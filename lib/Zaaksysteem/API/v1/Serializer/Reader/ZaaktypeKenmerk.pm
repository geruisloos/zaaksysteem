package Zaaksysteem::API::v1::Serializer::Reader::ZaaktypeKenmerk;
use Moose;

use Zaaksysteem::Tools;

with 'Zaaksysteem::API::v1::Serializer::ReaderRole';

=head1 NAME

Zaaksysteem::API::v1::Serializer::Reader::ZaaktypeKenmerk - A Zaaktype kenmerk reader

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 METHODS

=head2 class

=cut

sub class { 'Zaaksysteem::DB::Component::ZaaktypeKenmerken' }

=head2 read

=cut

sig read => 'Zaaksysteem::API::v1::Serializer, Object => HashRef';

sub read {
    my ($class, $serializer, $kenmerk) = @_;

    my $rv      = {
        is_group            => $class->to_bool($kenmerk->is_group),
        help                => $kenmerk->help,
        label               => $kenmerk->label,
        required            => $class->to_bool($kenmerk->value_mandatory),
        type                => undef,
        id                  => int($kenmerk->id),
        label_multiple      => $kenmerk->label_multiple,
        multiple_values     => undef,
        values              => undef,
    };

    my $properties = $kenmerk->properties;
    if ($properties->{date_limit}) {
        foreach my $key (qw(start end)) {
            $properties->{date_limit}{$key}{active} = $properties->{date_limit}{$key}{active} ? JSON::true : JSON::false;
            $properties->{date_limit}{$key}{num}    = int($properties->{date_limit}{$key}{num});
            $properties->{date_limit}{$key}{reference} = $properties->{date_limit}{$key}{reference} eq 'current' ? 'now' : $properties->{date_limit}{$key}{reference};
        }
    }

    if (exists $properties->{ map_case_location }) {
        $properties->{ map_case_location } = $properties->{ map_case_location } ? \1 : \0;
    }

    if (defined $properties->{ map_wms_feature_attribute_id }) {
        $properties->{ map_wms_feature_attribute_id } = int($properties->{ map_wms_feature_attribute_id });
    }

    if (my $attr = $kenmerk->bibliotheek_kenmerken_id) {
        $rv = {
            %$rv,
            type                => $attr->value_type,
            catalogue_id        => $attr->id,
            magic_string        => $attr->magic_string,
            limit_values        => ($attr->type_multiple ? -1 : 1),
            values              => $attr->options,
            permissions         => $kenmerk->format_required_permissions($serializer),
            label               => ($rv->{label} || $attr->naam),
            original_label      => $attr->naam,
            is_system           => $class->to_bool($kenmerk->is_systeemkenmerk),
            referential         => $class->to_bool($kenmerk->referential),
            pip                 => $class->to_bool($kenmerk->pip),
            publish_public      => $class->to_bool($kenmerk->publish_public),
            properties          => $properties,
            default_value       => $attr->value_default
        };
    }
    elsif(my $object_type_id = $kenmerk->object_id) {
        my $prefix = $object_type_id->get_object_attribute('prefix');

        $rv = {
            %$rv,
            type               => 'object',
            object_id          => $kenmerk->get_column('object_id'),
            object_type_prefix => $prefix->value,
            object_metadata    => $kenmerk->object_metadata,
        };
    }

    return $rv;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
