package Zaaksysteem::API::v1::Serializer::Reader::Widget;

use Moose;

use Zaaksysteem::Tools;

=head1 NAME

Zaaksysteem::API::v1::Serializer::Reader::Widget - Read ObjectData rows of type: dashboard/widget

=head1 SYNOPSIS

    my $reader = Zaaksysteem::API::v1::Serializer::Reader::Widget->grok($object);

    my $data = $reader->($serializer, $object);

=head1 DESCRIPTION

This class implements a serializer reader for
L<Zaaksysteem::Backend::Object::Data::Component> objects.

This class should not be used outside of the
L<Zaaksysteem::API::v1::Serializer> infrastructure.

=head1 METHODS

=head2 grok

Implements sub required by L<Zaaksysteem::API::v1::Serializer>.

=cut

sub grok {
    my ($class, $object) = @_;

    return unless blessed $object && $object->isa($class->class);

    my $reader_method = 'read_' . $object->object_class;

    if ($class->can($reader_method)) {
        return sub { $class->$reader_method(@_) };
    }

    return;
}


=head2 class

Implements sub required by L<Zaaksysteem::API::v1::Serializer::ReaderRole>.

=cut

sub class { 'Zaaksysteem::Backend::Object::Data::Component' }


=head2 parse_value

TODO: Fix me

=cut

sub parse_value {
    my $class       = shift;
    my $serializer  = shift;
    my $value       = shift;

    ### Blessed objects
    return $serializer->read($value) if blessed($value) && !JSON::is_bool($value);

    ### Integer values
    return $value;
}

=head2 read_widget

Reader for widget object

=cut

sig read_widget => 'Zaaksysteem::API::v1::Serializer, Object => HashRef';

sub read_widget {
    my ($class, $serializer, $object) = @_;

    return {
        type => 'widget',
        reference => $object->id,
        instance => {
            id                      => $object->id,
            (map { my $val = $class->parse_value($serializer, $object->get_object_attribute($_)->value); $_ => (defined $val ? $val : undef) }
                qw(widget data)),
            (map { my $val = $class->parse_value($serializer, $object->get_object_attribute($_)->value); $val += 0; $_ => (defined $val ? $val : undef) }
                qw(row column size_x size_y))
        }
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

