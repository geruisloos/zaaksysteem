package Zaaksysteem::API::v1::Message::Case::PhaseTransition;

use Moose;

use Zaaksysteem::Types qw[UUID];

extends 'Zaaksysteem::API::v1::Message';

=head1 NAME

Zaaksysteem::API::v1::Message::Ping - Model for case phase transition messages

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 case_id

This attribute is to hold the ID of the case for which the notification
is being sent.

=cut

has case_id => (
    is => 'rw',
    isa => UUID,
    required => 1
);

=head2 base_url

=cut

has base_url => (
    is => 'rw',
    isa => 'URI',
    required => 1
);

=head1 METHODS

=head2 field_hash

Override for L<Zaaksysteem::API::v1::Message/field_hash> that provides
message type and payload information for the notification

    {
        type => 'phase_transition',
        case_url => 'http://host.to/zaaksysteem/instance/case/id'

        case => {
            reference => '<UUID>',
            instance => undef,
            type => 'case'
        }
    }

=cut

override field_hash => sub {
    my $self = shift;

    my $case_url = $self->base_url->clone;

    $case_url->path_segments(qw[api v1 case], $self->case_id);

    return {
        type => 'phase_transition',
        case_url => $case_url->as_string,
        case => {
            reference => $self->case_id,
            instance => undef,
            type => 'case'
        }
    }
};

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
