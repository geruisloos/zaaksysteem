package Zaaksysteem::Controller::Betrokkene::Contactmoment;

use Moose;

use Zaaksysteem::Tools;

BEGIN { extends 'Zaaksysteem::Controller' }

sub base : Chained('/betrokkene/betrokkene') : PathPart('contactmoment') : CaptureArgs(0) {
    my ($self, $c) = @_;
}

=head2 create

Create a new contact moment related to a subject

=head3 URL

C</betrokkene/betrokkene-B<[type]>-B<[gmid]>/contactmoment/create>

=head3 Request method

The request method must be POST for this call.

=head3 Parameters

=over 4

=item contactkanaal (I<required>)

One of C<behandelaar>, C<balie>, C<telefoon>, C<post>, C<email> or C<webformulier>

=item content (I<required>)

Plaintext content to store

=item case_id (I<optional>)

Id of the case mentioned during contact

=back

=head3 Response

A single JSON hydrated event triggered by contact moment creation

=head3 Event

=over 4

=item C<subject/contactmoment/create>

This event reflects the fact that a contact moment has been created for this subject

=back

=cut

define_profile create => (
    required => [ qw[contactkanaal content] ],
    optional => [ qw[case_id] ]
);

sub create : Chained('base') : PathPart('create') : Args(0) {
    my ($self, $c) = @_;

    assert_profile($c->req->params);

    unless($c->req->method eq 'POST') {
        throw('request/invalid', "Invalid request method, should be POST.");
    }

    $c->model('DB::Contactmoment')->contactmoment_create({
        type => 'note',
        subject_id => $c->stash->{ betrokkene }->betrokkene_identifier,
        case_id => $c->req->param('case_id') || undef,
        created_by => $c->model('DB::Zaak')->current_user->betrokkene_identifier,
        medium => $c->req->param('contactkanaal'),
        message => $c->req->param('content')
    });

    my @logging_arguments = (
        'subject/contactmoment/create',
        {
            component   => 'betrokkene',
            created_for => $c->stash->{betrokkene}->betrokkene_identifier,
            data        => {
                case_id => $c->req->param('case_id') || undef,
                subject_id => $c->stash->{betrokkene}->betrokkene_identifier,
                contact_channel => $c->req->param('contactkanaal'),
                content         => $c->req->param('content')
            }
        }
    );

    my $event;
    if ($c->req->params->{case_id}) {
        my $case = $c->model('DB::Zaak')->find($c->req->params->{case_id});
        $event = $case->trigger_logging(@logging_arguments);
    }
    else {
        $event = $c->model('DB::Logging')->trigger(@logging_arguments);
    }

    $c->stash->{json} = $event->discard_changes;

    $c->detach('Zaaksysteem::View::JSON');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 base

TODO: Fix the POD

=cut

