package Zaaksysteem::Controller::Objects;

use Moose;
use namespace::autoclean;

use Zaaksysteem::Tools;

use Zaaksysteem::ZAPI::CRUD::Interface;
use Zaaksysteem::ZAPI::CRUD::Interface::Column;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::Objects - ZAPI Controller

=head1 SYNOPSIS

=head1 DESCRIPTION

Zaaksysteem API Controller for the generic object search.

=head1 INSTRUCTIONS

=head2 FORM LAYOUT

You can use the C<zapi_form=1> query parameter to retrieve more information
from a function regarding the needed form parameters.

=head1 METHODS

=head2 /object/tablename [GET READ]

Returns a resultset of loaded interfaces

=cut

sub index
    : Chained('/')
    : PathPart('objects')
    : Args()
    : ZAPI
{
    my ($self, $c, $class, $class_id) = @_;

    # Very open search, limit to admins.
    $c->assert_any_user_permission('admin');

    if (exists $c->req->params->{zapi_crud}) {
        my @columns = _get_columns($c, $class);

        $c->stash->{zapi} = [
            {
                'records' => Zaaksysteem::ZAPI::CRUD::Interface->new(
                    options     => {
                        select      => 'all',
                    },
                    actions => [],
                    columns => \@columns,
                ),
            }
        ];
        $c->detach();
    }
    elsif (exists $c->req->params->{zapi_detail}) {

        my @columns = _get_columns($c, $class);

        $c->stash->{zapi} = [
            {
                title => $class,
                name  => $class,
                actions => [
                    Zaaksysteem::ZAPI::CRUD::Interface::Action->new(
                        id      => "object-subscription-delete",
                        type    => "update",
                        label   => "Afnemerindicatie verwijderen",
                        when    => "item.object_subscription",
                        data    => {
                            url     => "/objects/delete_subscription/$class/$class_id",
                            confirm => {
                                label   => 'Weet u zeker dat u de afnemerindicatie wil verwijderen?',
                                verb    => 'Verwijderen'
                            }
                        }
                    ),
                ],
                fieldsets => [
                    {
                        fields => \@columns,
                        name => 'generic',
                    }
                ]
            }
        ];
        $c->detach();
    }

    # Filter by local id if given
    if ($class_id) {
        $c->req->params->{id} = $class_id;
    }

    # Default to DBIx::Class search.
    $c->stash->{zapi} = $c->model("DB::$class")->search(
        $c->req->params,
    );
}

sub _get_columns {
    my ($c, $class) = @_;

    my @columns = $c->model("DB::$class")->result_source->columns;

    # Object subscription isn't an actual column
    my @generic_columns;
    push @generic_columns, Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
        id => 'object_subscription',
        label => 'afnemerindicatie',
        template => '<[item.object_subscription.external_id]>',
        when => 'item.object_subscription'
    );

    # Process columns
    for my $col (@columns) {
        push @generic_columns, Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
            id          => $col,
            label       => $col,
            resolve     => $col,
            # Do something proper with type later. Inflating dates and such.
            # Possibly map schema-types to default handling?
            # type        => 'bla',
        );
    }

    return @generic_columns;
}

sub subscription_delete
    : Chained('/')
    : PathPart('objects/delete_subscription')
    : Args(2)
    : ZAPI
{
    my ($self, $c, $class, $class_id) = @_;

    $c->assert_any_user_permission('admin');

    throw('httpmethod/post', 'Invalid HTTP Method, no POST', []) unless
        lc($c->req->method) eq 'post';

    my ($object_subscription) = $c->model('DB::ObjectSubscription')->search({
        local_table  => $class,
        local_id     => $class_id,
        date_deleted => undef,
    });

    # If no object is found, simply return without throwing.
    my ($object) = $c->model("DB::$class")->find($class_id);
    if (!$object) {
        $c->stash->{zapi} = [];
        return;
    }

    $object_subscription->object_subscription_delete;

    # $c->model('DB::Logging')->trigger('object/subscription/delete', {
    #     component => $object->result_source->name,
    #     component_id => $object->id,
    #     data => {
    #         action => "Object subscription (afnemerindicatie) is verwijderd.",
    #     }
    # });

    # $object_subscription->delete;

    $c->stash->{zapi} = [];
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 index

TODO: Fix the POD

=cut

=head2 subscription_delete

TODO: Fix the POD

=cut

