package Zaaksysteem::Controller::Zaak::Email;
use Moose;

use Zaaksysteem::ZTT;
use Zaaksysteem::Tools;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::Zaak::Email - The proper mailer for Zaaksysteem

=head1 DESCRIPTION

A simple controller which is responsible for sending out e-mails and returns ZAPI instead of legacy JSON.

=head1 METHODS

=head2 send_mail

Send an email.

=head3 Arguments

=over

=item recipient [required]

Who the mail gets delivered to.

=item subject [required]

Subject of the mail.

=item from [optional]

Who the mail is sent as. Defaults as the value set in the config. And if that is
missing as well it will use the system default.

=item body [required]

Content of the mail.

=item case_id [required]

=item case_document_ids [optional]

A list of the case document (zaaktype_kenmerken.id) IDs that should get
attached.

=item file_attachments [optional]

A list of the file IDs you wish to send with this message.

=back

=head3 Location

POST: /zaak/send_mail

=head3 Returns

True upon message send success.

=cut

sub send_mail :Chained('/'): PathPart('zaak/send_mail') : Args(0) {
    my ($self, $c) = @_;

    my %params  = %{ $c->req->params };

    my $case = $c->model('DB::Zaak')->find($params{case_id});

    if (!$case) {
        throw("zaak/send_mail/404", "send_mail: case with id '$params{case_id}' not found");
    }

    $params{recipient} = Zaaksysteem::Controller::Zaak::_resolve_recipient($c, $params{recipient}, $params{recipient_type}, $case);

    my $ztt = Zaaksysteem::ZTT->new;
    $ztt->add_context($case);

    $params{body}    = $ztt->process_template($params{body})->string;
    $params{subject} = $ztt->process_template($params{subject})->string;

    for (qw(cc bcc)) {
        next unless $params{$_};
        $params{$_} = $ztt->process_template($params{$_})->string;
    }

    my $ok = $case->mailer->send_from_case(
        {
            sender           => $params{sender},
            sender_address   => $params{sender_address},
            body             => $params{body},
            recipient        => $params{recipient},
            subject          => $params{subject},
            contactmoment    => 'behandelaar',
            cc               => $params{cc},
            bcc              => $params{bcc},
            attachments      => $params{case_document_ids}|| [],
            file_attachments => $params{file_attachments} || [],
            log_error        => $params{log_error} // 1,
        }
    );

    $c->stash->{zapi} = [ { result => $ok } ];

}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
