package Zaaksysteem::Controller::API::v1::Map;

use Moose;

use Zaaksysteem::Tools;


BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

has 'api_capabilities' => (
    is          => 'ro',
    default     => sub { return [qw/public_access/] }
);

=head1 NAME

Zaaksysteem::Controller::API::v1::Map - APIv1 controller for Geo Map functions

=head1 DESCRIPTION

This is the controller API class for C<api/v1/map>. Extensive documentation about this
API can be found in:

L<Zaaksysteem::Manual::API::V1::Map>

Extensive tests about the usage via the JSON API can be found in:

L<TestFor::Catalyst::Controller::API::V1::Map>

=head1 ACTIONS

=head2 base

=cut

sub base : Chained('/api/v1/base') : PathPart('map') : CaptureArgs(0) : Scope('widget') {
    my ($self, $c) = @_;

    $c->stash->{map_interface} = $c->model('DB::Interface')->search_active({module => 'map'})->first;
}


=head2 ol_settings

Gets the Open Layer settings from the Map module

=cut

sub ol_settings : Chained('base') : PathPart('ol_settings') : Args(0) : RW {
    my ($self, $c)      = @_;

    $c->stash->{result} = $c->stash->{map_interface}
        ? $c->stash->{map_interface}->process_trigger('get_ol_settings')
        : Zaaksysteem::API::v1::ArraySet->new(content => []);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
