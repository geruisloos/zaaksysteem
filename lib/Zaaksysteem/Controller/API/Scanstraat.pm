package Zaaksysteem::Controller::API::Scanstraat;

use Moose;
use namespace::autoclean;

use Zaaksysteem::Tools;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::API::Scanstraat - Controller voor de scanstraat

=head1 METHODS

=cut

=head2 upload_document

Upload the document via a POST call

=cut

sub upload_document : Local : ZAPI {
    my ($self, $c) = @_;

    my $module = 'scanstraat';
    my $err_class = "api/$module";

    my $interface = $c->model('DB::Interface')->search_active({module => $module})->first;
    throw($err_class, 'Scanstraat koppeling is niet beschikbaar of inactief') if !$interface;

    $c->stash->{interface} = $interface;

    my $upload = $c->request->upload('filename');

    throw($err_class, 'No file submitted') if !$upload;

    my $params = $c->req->params;

    $upload->fh;

    $c->stash->{zapi} = $c->stash->{interface}->process_trigger('upload_document', {
        filename => $params->{filename},
        filepath => $upload->tempname,
        api_key => $params->{api_key}
    });
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
