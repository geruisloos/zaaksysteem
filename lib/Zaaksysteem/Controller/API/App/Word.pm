package Zaaksysteem::Controller::API::App::Word;
use Moose;
use namespace::autoclean;
use Zaaksysteem::XML::Generator::Wordaddin;
use Zaaksysteem::Tools;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::API::App::Word - Word app calls

=head1 DESCRIPTION

This controller returns a the XML needed to use the word add-in.

=cut

=head1 ACTIONS

=head2 base

api/app/word

=cut

sub base :Chained('/api/base') :PathPart('app/word') :CaptureArgs(0) {
    my ($self, $c) = @_;

    my $active = $c->model('DB::Config')->get_value('app_enabled_wordapp');
    if (!$active) {
        throw('app/word/unconfigured', 'Word app is not configured to be active');
    }

    my $interface = $c->model('DB::Interface')->search_active(
        'module' => 'app_word'
    )->first;

    if (!$interface) {
        throw('app/word/inactive', 'Word app is inactive');
    }

    $c->stash->{interface} = $interface;

}

=head2 get_xml

Get the XML for the MS Word add-in for this environment

=head3 URI

    api/app/word/xml

=cut

sub get_xml : Chained('base') : PathPart('xml') {
    my ($self, $c) = @_;

    my $generator = Zaaksysteem::XML::Generator::Wordaddin->new();
    my $xml = $generator->wordaddin(writer => { c => $c, uuid => $c->stash->{interface}->uuid });

    $c->res->content_type('application/xml');
    $c->res->body($xml);
    $c->detach();
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
