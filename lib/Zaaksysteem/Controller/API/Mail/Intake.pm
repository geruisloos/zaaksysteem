package Zaaksysteem::Controller::API::Mail::Intake;

use Moose;

use Zaaksysteem::Mail::Parser;

BEGIN { extends 'Zaaksysteem::Controller' }

sub handle : Private {
    my ($self, $c) = @_;
    my $message = $c->req->params->{message};

    my $parser = Zaaksysteem::Mail::Parser->new(
        schema => $c->model('DB')->schema,
    );
    $parser->parse_message($message);

    $c->res->body('ok');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 handle

TODO: Fix the POD

=cut
