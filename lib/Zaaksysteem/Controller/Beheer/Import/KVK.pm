package Zaaksysteem::Controller::Beheer::Import::KVK;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::Controller' }

sub base : Chained('/') : PathPart('beheer/import/kvk'): CaptureArgs(1) {
    my ( $self, $c, $import_id ) = @_;

    $c->assert_any_user_permission('beheer');

    $c->stash->{import_data} = $c->model('DB::BeheerImport')->find($import_id);
    $c->stash->{import_type} = 'kvk';

    if (!$c->stash->{import_data}) {
        $c->res->redirect($c->uri_for('/beheer/import/kvk'));
        $c->detach;
    }
}

sub index : Chained('/') : PathPart('beheer/import/kvk'): Args(0) {
    my ( $self, $c ) = @_;

    $c->assert_any_user_permission('beheer');

    $c->stash->{import_type} = 'kvk';
    $c->stash->{import_list} = $c->model('DB::BeheerImport')->search(
        {
            importtype  => 'KVK',
        },
        {
            order_by    => { -desc => ['created'] },
        }
    );

    $c->stash->{template} = 'beheer/import/kvk/list.tt';
}

sub view : Chained('base') : PathPart(''): Args() {
    my ( $self, $c ) = @_;

    $c->stash->{template} = 'beheer/import/kvk/view.tt'
}


sub run : Local {
    my ( $self, $c ) = @_;

    $c->model('Beheer::Import::KVK')->import_kvk(
        'type'      => 'KVKCsv',
        'options'   => {
            'filename'  => (
                $ENV{USER} eq 'michiel'
                    ?'/home/michiel/dev/Zaaksysteem/tmp/kvkexport.csv'
                    :'/home/zaaksysteem/bussum/import/kvk.csv'
                ),
        }
    );

    $c->res->body('OK');


}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 base

TODO: Fix the POD

=cut

=head2 index

TODO: Fix the POD

=cut

=head2 run

TODO: Fix the POD

=cut

=head2 view

TODO: Fix the POD

=cut

