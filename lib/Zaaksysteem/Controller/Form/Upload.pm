package Zaaksysteem::Controller::Form::Upload;

use Moose;

use HTML::TagFilter;
use File::stat;

use Zaaksysteem::Tools;

BEGIN { extends 'Zaaksysteem::Controller' }

=head2 display_uploads

Files are stored in a separate place in the session, in a
structure { $kenmerken_id => [$upload1, $upload2] }.

Prep the structure so it resembles how it will look in case
context.

The prefix 'form_' is used because TT uses global names by
default, and somewhere in the file view code the name
'uploaded_files' is used too. In case context this is not
problematic because documents are retrieved separately.

=cut

sub display_uploads : Private {
    my ($self, $c) = @_;

    my $uploads = $c->session->{_zaak_create}->{uploads};

    $c->stash->{form_uploaded_files} = {};

    while (my ($kenmerken_id, $files) = each %$uploads) {
        $c->stash->{form_uploaded_files}->{$kenmerken_id} = [
            map { $self->display_upload($_) } @$files
        ];
    }
}


=head2 display_upload

Once a file is uploaded, a filestore_id is retrieved,
which is stored in the session until the case is created,
after which the information is added to zaak_kenmerken.
Until that point the information may be edited.

This prepares the file information for display in the mintloader,
allowing download and deletion.

=cut

sub display_upload {
    my ($self, $upload) = @_;

    # the Catalyst upload object is not preserved between sessions
    my $mimetype = $upload->{upload} ? $upload->{upload}->type : '';
    return {
        filestore_id => $upload->{filestore_id},
        filename     => $upload->{name},
        mimetype     => $mimetype
    };
}


=head2 index

handle one or more file uploads by delegating

=cut

sub index : Path {
    my ($self, $c) = @_;

    $c->stash->{uploaded_files} = [];

    foreach my $upload_param (keys %{$c->req->uploads}) {
        $c->forward('handle_single_file_upload', [$upload_param]);
    }

    $c->stash->{template}  = 'uploadresponse.tt';
    $c->stash->{nowrapper} = 1;
}


=head2 download

allow the user to re-download a file he just uploaded

=cut

sub download : Local {
    my ($self, $c) = @_;

    my $t0 = Zaaksysteem::StatsD->statsd->start;


    my $filestore_id = $c->req->params->{filestore_id};

    # make sure this user should have access.
    # anything that you upload you may see.
    my $uploads = $c->session->{_zaak_create}->{uploads};

    die "filestore_id $filestore_id has not been uploaded" unless
        # try to find at least one occurence of this filestore_id
        # yo dawg, i heard you like grep
        grep { grep { $_->{filestore_id} eq $filestore_id } @$_ }
        values %$uploads;

    my $file = $c->model('DB::Filestore')->find($filestore_id);
    my $path = $file->get_path;

    $c->serve_static_file($path);

    my $original_name = $file->original_name;
    $c->res->header('Content-Disposition', qq[attachment; filename="$original_name"]);

    $c->res->headers->content_length(stat($path)->size);
    $c->res->headers->content_type($file->mimetype);

    Zaaksysteem::StatsD->statsd->increment('serve_file.form_download', 1);
    Zaaksysteem::StatsD->statsd->end('serve_file.form_download.time', $t0);

}


=head2 handle_single_file_upload

Given a single file upload parameter, handle upload

=cut

sub handle_single_file_upload : Private {
    my ($self, $c, $upload_param) = @_;

    my $upload   = $c->req->upload($upload_param);
    my $tf       = HTML::TagFilter->new;
    my $filename = $tf->filter($upload->filename);

    unless($self->validate_upload($c, $filename, $upload)) {
        $c->stash->{upload_error} = 'Het bestandsformaat van dit document is niet toegestaan vanwege digitale duurzaamheid';
        $c->log->debug('Form.pm: Document rejected by NEN validation rules');
        return;
    }

   try {
        $c->stash->{filestore_id} = $c->model('DB::Filestore')->filestore_create({
            file_path     => $upload->tempname,
            original_name => $filename,
        })->id;

        my ($kenmerk_id) = $c->req->params->{file_id} =~ m|(\d+)$|;

        # IE stuff
        unless ($kenmerk_id) {
            ($kenmerk_id) = $upload_param =~ m|(\d+)$|;
        }
        $c->stash->{fieldname} = $upload_param;
        $c->stash->{filename} = $filename;
        # end IE

        my $last_fileupload = {
            upload        => $upload,
            filestore_id  => $c->stash->{filestore_id},
            name          => $filename,
        };

        push @{$c->session->{_zaak_create}->{uploads}->{$kenmerk_id}},
            $last_fileupload;

        my $display = {
            filename     => $upload->filename,
            mimetype     => $upload->type,
            filestore_id => $c->stash->{filestore_id}
        };
        my $files = $c->stash->{uploaded_files};
        push @$files, $display;
    }
    catch {
        $c->log->error("Error storing uploaded file: " . $_);
        $c->stash->{upload_error} = $_->message;
    }
}


sub validate_upload {
    my ($self, $c, $filename, $upload) = @_;

    return if $filename =~ m|\.ztb$|;

    return $c->model('DocumentValidator')->validate_upload({
        filename => $filename,
        upload => $upload,
    });
}


=head2 remove_upload

the uploads reside in the session

to consider: remove file from filestore?

=cut

sub remove_upload : Local {
    my ($self, $c) = @_;

    my ($kenmerk_id) = $c->req->params->{kenmerk_id} =~ m|(\d+)$|;

    die "need kenmerk_id" unless $kenmerk_id;

    die 'No uploads could be found for kenmerk '. $kenmerk_id
        unless delete $c->session->{ _zaak_create }{ uploads }{ $kenmerk_id };

    $c->stash->{ json } = {
        success => 1,
        message => 'Uploads for kenmerk ' . $kenmerk_id . ' cleared'
    };

    $c->detach('Zaaksysteem::View::JSON');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 validate_upload

TODO: Fix the POD

=cut

