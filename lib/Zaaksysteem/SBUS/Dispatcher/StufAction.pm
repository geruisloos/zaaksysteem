package Zaaksysteem::SBUS::Dispatcher::StufAction;

use strict;
use base qw(Catalyst::Action::SOAP);
use IO::Seekable qw/SEEK_SET/;
use Zaaksysteem::SBUS::Constants;

sub execute {
    my $self = shift;
    my ( $controller, $c ) = @_;

    $self->prepare_soap_helper(@_);

    my $prefix          = $controller->soap_action_prefix;

    ### Custom mintlab code, to find soapaction according to berichtsoort
    {
        my $body            = $c->req->body;
        my $xml_str         = join('', <$body>);

        if (ref($c->req->body)) {
            ### Reset body to 0 pointer
            $c->req->body->seek(0,SEEK_SET);

            my ($berichtsoort)  = $xml_str =~ /<.*?berichtsoort>(.*)?<\/.*?berichtsoort>/;
            if ($berichtsoort) {
                my $operation   = STUF_BERICHTSOORT_ACTION->{lc($berichtsoort)};
                $c->req->headers->header('SOAPAction', $prefix . $operation);
            }
        }
    }

    my $soapaction      = $c->req->headers->header('SOAPAction');
    unless ($soapaction) {
        $c->log->error('No SOAP Action');
        $c->stash->{soap}->fault({code => 500,reason => 'Invalid SOAP message'});
        $c->detach;
    }

    $soapaction     =~ s/(^\"|\"$)//g;
    unless ($prefix eq substr($soapaction,0,length($prefix))) {
        $c->log->error('Bad SOAP Action');
        $c->stash->{soap}->fault({code => 500,reason => 'Invalid SOAP message'});
        $c->detach;
    }

    my $operation   = substr($soapaction,length($prefix));
    my $action      = $controller->action_for($operation);

    unless ($action) {
        $c->log->error('SOAP Action does not map to any operation');
        $c->stash->{soap}->fault({code => 500,reason => 'Invalid SOAP message'});
        $c->detach;
    }

    $c->forward($operation);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 execute

TODO: Fix the POD

=cut

