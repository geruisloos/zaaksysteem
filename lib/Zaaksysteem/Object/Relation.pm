package Zaaksysteem::Object::Relation;
use Moose;
use namespace::autoclean;

use MooseX::Types::Moose qw(Maybe);
use Zaaksysteem::Tools;
use Zaaksysteem::Types qw(UUID Boolean);

=head1 NAME

Zaaksysteem::Object::Relation - Represents a relation to another object

=head1 SYNOPSIS

    my $r = Zaaksysteem::Object::Relation->new(
        related_object_id   => 'uuid',
        related_object_type => 'some_type',
        relationship_name_a => 'mother',
        relationship_name_b => 'child',
        blocks_deletion     => 1,
    );

=head1 ATTRIBUTES

All attributes are required.

=head2 related_object_id

UUID of the related object.

=head2 related_object_type

Type of the related object. This is de-normalized so don't have to hit the database before

=head2 blocks_deletion

Boolean value. If true, neither member of the relationship can be deleted while
the relationship exists.

=head2 is_inherited

Boolean value. If true for a given instance, that relation has been injected
from a parent-class of the object that contains the relation instance. Implies
immutability.

=head2 relationship_name_a

Name of the relationship, seen from the object's point of view.

=head2 relationship_name_b

Name of the relationship, seen from the related object's point of view.

=head2 related_object

Related object. This is only used if the API user requested full hydration of
related objects.

=cut

has owner_object_id => (
    is => 'rw',
    isa => Maybe[UUID],
    predicate => 'has_owner'
);

has related_object_id => (
    is       => 'rw',
    isa      => UUID,
    required => 1,
);

has related_object_type => (
    is       => 'rw',
    isa      => 'Str',
    required => 1,
);

has relationship_name_a => (
    is       => 'rw',
    isa      => 'Str',
    default  => 'related',
    required => 1,
);

has relationship_name_b => (
    is       => 'rw',
    isa      => 'Str',
    default  => 'related',
    required => 1,
);

has relationship_title_a => (
    is       => 'rw',
    isa      => 'Maybe[Str]',
    required => 0,
);

has relationship_title_b => (
    is       => 'rw',
    isa      => 'Maybe[Str]',
    required => 0,
);

has blocks_deletion => (
    is => 'rw',
    isa => Boolean,
    default => 0,
    required => 0
);

has is_inherited => (
    is => 'rw',
    isa => Boolean,
    default => 0
);

has related_object => (
    is       => 'rw',
    isa      => 'Maybe[Zaaksysteem::Object]',
    required => 0,
);

=head1 METHODS

=head2 can_unrelate

=cut

sub can_unrelate {
    my $self = shift;
    my $context = shift;

    return 1 unless $self->has_owner && defined $self->owner_object_id;
    return 1 if $self->owner_object_id eq $context->id;

    return;
}

=head2 equal_to

=cut

sub equal_to {
    my $self = shift;
    my $other = shift;

    return unless blessed $other;
    return unless $other->isa(__PACKAGE__);

    return unless $self->related_object_id eq $other->related_object_id;
    return unless $self->relationship_name_a eq $other->relationship_name_a;
    return unless $self->relationship_name_b eq $other->relationship_name_b;

    return 1;
}

=head2 TO_JSON

Returns the JSON representation of this object.

Called internally by the encode_json().

=cut

sub TO_JSON {
    my $self = shift;

    return {
        related_object_id   => $self->related_object_id,
        related_object_type => $self->related_object_type,
        relationship_name_a => $self->relationship_name_a,
        relationship_name_b => $self->relationship_name_b,

        related_object  => $self->related_object,
        is_inherited    => $self->is_inherited    ? \1 : \0,
        blocks_deletion => $self->blocks_deletion ? \1 : \0,

        owner_object_id => $self->owner_object_id,
    };
}

__PACKAGE__->meta->make_immutable();

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
