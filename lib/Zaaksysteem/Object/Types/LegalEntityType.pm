package Zaaksysteem::Object::Types::LegalEntityType;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';

use Zaaksysteem::Types qw(NonEmptyStr JSONBoolean);
use Zaaksysteem::Tools;

use Zaaksysteem::Object::ConstantTables qw/LEGAL_ENTITY_TYPE_TABLE/;


=head1 NAME

Zaaksysteem::Object::Types::LegalEntitytype - Built-in object type implementing
a class for legal entity types as defined by the chamber of commerce.

=head1 DESCRIPTION

A class for legal entity types as defined by the Dutch chamber of commerce.

=head1 ATTRIBUTES

=head2 code

=cut

has code => (
    is       => 'rw',
    isa      => 'Int',
    traits   => [qw(OA)],
    label    => 'Legal entity ID',
    required => 1,
    unique   => 1,
);

=head2 label

The label of the rechtsvorm (legal entity type).

=cut

has label => (
    is       => 'rw',
    isa      => NonEmptyStr,
    traits   => [qw(OA)],
    label    => 'Name of the legal entity',
    required => 1,
);

=head2 description

A description about the legal entity

=cut

has description => (
    is       => 'rw',
    isa      => NonEmptyStr,
    traits   => [qw(OA)],
    label    => 'Description of the legal entity type',
    required => 0,
);

=head2 active

Whether this entity is active in zaaksysteem.

=cut

has active => (
    is       => 'rw',
    isa      => JSONBoolean,
    traits   => [qw(OA)],
    label    => 'Active',
    coerce   => 1,
    required => 0,
);

=head1 METHODS

=head2 relatable_types

Returns a list of types that can be related to a legal entity type object.

=cut

sub relatable_types {
    return qw();
}

=head2 new_from_code

    $country = Zaaksysteem::Object::Types::LegalEntityType->new_from_code(1);

Loads object from dutch_code

=cut

sub new_from_code {
    my ($class, $code)  = @_;
    $code = int($code);

    throw('object/types/legalentitytype/invalid_code', 'Invalid code: need integer') unless $code && $code =~ /^\d+$/;

    my ($legalentitytype)       = grep { $_->{code} eq $code } @{ LEGAL_ENTITY_TYPE_TABLE() };

    throw('object/types/legalentitytype/unknown_code', 'Unknown code: no legal entity by code: ' . $code) unless $legalentitytype;

    return $class->new(%$legalentitytype);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
