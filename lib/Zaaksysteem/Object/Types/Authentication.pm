package Zaaksysteem::Object::Types::Authentication;
use Moose;
use namespace::autoclean;

use Moose::Util::TypeConstraints;

extends 'Zaaksysteem::Object';
with qw/Zaaksysteem::BR::Subject::Types::Authentication/;

use Zaaksysteem::Types qw(NonEmptyStr HashedPassword);
use Zaaksysteem::Tools;
use Crypt::SaltedHash;

=head1 NAME

Zaaksysteem::Object::Types::Authentication - Authentication object containing the authentication
                                             type and parameters.

=head1 DESCRIPTION

An object class for authentication. This module implements login/password etc of an entity

=head1 ATTRIBUTES

=head2 username

The username of this entity.

=cut

has username => (
    is       => 'rw',
    isa      => NonEmptyStr,
    traits   => [qw(OA)],
    label    => 'Username',
    unique   => 1,
);

=head2 password

The password of this entity. Most of the time this one is empty, only filled when saved

=cut

has password => (
    is       => 'rw',
    isa      => NonEmptyStr,
    traits   => [qw(OA)],
    label    => 'Password',
);

=head2 hashed_password

The hashed version of the password

=cut

has hashed_password => (
    is       => 'rw',
    isa      => HashedPassword,
    traits   => [qw(OA)],
    label    => 'Hashed password',
);

=head2 entity_type

The authentication type, for now only supports "company'"

=cut

has entity_type => (
    is       => 'rw',
    isa      => enum([qw[company]]),
    traits   => [qw(OA)],
    label    => 'Hashed password',
    required => 1,
);



__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
