package Zaaksysteem::Object::Queue::Model::Subject;

use Moose::Role;

use Zaaksysteem::Tools;

=head1 NAME

Zaaksysteem::Object::Queue::Model::Subject - Subject queue item handler

=head1 DESCRIPTION

=head1 METHODS

=head2 update_cases_of_deceased

    $self->update_cases_of_deceased(
        $schema->resultset('Queue')->first
    );

Will touch all related cases of given queue item. This way, cases of deceased persons will be updated
with the proper "decease" data

=cut

sig update_cases_of_deceased => 'Zaaksysteem::Backend::Object::Queue::Component';

sub update_cases_of_deceased {
    my $self = shift;
    my $item = shift;

    my $natuurlijk_persoon_id   = $item->data->{natuurlijk_persoon_id};

    my $cases = $self->_retrieve_cases_for_aanvrager(
        $item->result_source->schema, 
        {
            natuurlijk_persoon_id   => $natuurlijk_persoon_id,
        }
    );

    $self->log->info('Touching cases for deceased person with natuurlijk_persoon_id: ' . $natuurlijk_persoon_id);
    $self->_touch_cases($cases);

    return 1;
}

=head1 PRIVATE METHODS

=head2 _retrieve_cases_for_aanvrager

    my $cases = $self->_retrieve_cases_for_aanvrager(
        $schema,
        {
            natuurlijk_persoon_id   => 12984,
        }
    );

    my $first_case = $cases->first;

Will retrieve a list of cases from schema C<Zaak>. 

=cut

sig _retrieve_cases_for_aanvrager => 'Any, HashRef';

sub _retrieve_cases_for_aanvrager {
    my ($self, $schema, $params) = @_;

    my $cases = $schema->resultset('Zaak')->search(
        {
            'aanvrager.gegevens_magazijn_id'  => $params->{natuurlijk_persoon_id},
            'aanvrager.betrokkene_type'       => 'natuurlijk_persoon',
        },
        {
            join    => 'aanvrager'
        }
    );

    return $cases;
}

=head2 _touch_cases

    my $cases = $self->_touch_cases(
        $schema->resultset('Zaak')->search
    );

Will touch every case in given iterator

=cut

sub _touch_cases {
    my ($self, $cases) = @_;

    while (my $case = $cases->next) {
        $self->log->info('Touching case "' . $case->id . '" for deceased person');
        $case->_touch;
    }

    return 1;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
