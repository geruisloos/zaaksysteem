package Zaaksysteem::Object::Queue::Model::Case;

use Moose::Role;

use Zaaksysteem::Constants qw(
    LOGGING_COMPONENT_ZAAK
);

use Zaaksysteem::Tools;
use Zaaksysteem::Types qw[UUID];

use Zaaksysteem::Object::Types::Address;
use Zaaksysteem::Object::Types::Location;

=head1 NAME

Zaaksysteem::Object::Queue::Model::Case - Case queue item handler

=head1 DESCRIPTION

=head1 METHODS

=head2 create_case_subcase

=cut

sig create_case_subcase => 'Zaaksysteem::Backend::Object::Queue::Component';

sub create_case_subcase {
    my $self = shift;
    my $item = shift;

    $item->object_data->get_source_object->start_subcase(
        action_data => $item->data,
        object_model => $self->object_model,
        betrokkene_model => $self->subject_model,
        current_user => $self->subject_table->find($item->data->{ _subject_id })
    );

    return;
}

=head2 create_case_document

=cut

sig create_case_document => 'Zaaksysteem::Backend::Object::Queue::Component';

sub create_case_document {
    my $self = shift;
    my $item = shift;

    my %template_action_args = (
        action_data => $item->data
    );

    if ($item->data->{ _subject_id }) {
        $template_action_args{ current_user } = $self->subject_table->find(
            $item->data->{ _subject_id }
        );
    }

    my $file = $item->object_data->get_source_object->template_action(
        %template_action_args
    );

    my $data = $item->data;

    $data->{result} = {
        file_id        => $file->id,
        filestore_uuid => $file->filestore_id->uuid,
    };
    $item->data($data);

    return;
}

=head2 update_case_location

Handler for C<update_case_location> queue item instances.

This handler requires that L<Zaaksysteem::Object::Queue::Model> has a valid
L<Zaaksysteem::Object::Queue::Model|geocoder/geocoder> instance.

After validating input, a reverse geocoding lookup is performed, which is used
to update a L<Zaaksysteem::Zaken::Component|zaak> instance.

=cut

sig update_case_location => 'Zaaksysteem::Backend::Object::Queue::Component';

sub update_case_location {
    my $self = shift;
    my $item = shift;

    unless ($self->has_geocoder) {
        throw('object/queue/update_case_location/geocoder_required', sprintf(
            'Cannot update case location without a geocoder'
        ));
    }

    my $zaak = $item->object_data->get_source_object;

    my $location = Zaaksysteem::Object::Types::Location->new($item->data);

    $location->address($self->geocoder->reverse_geocode($location));

    $zaak->update_location($location);
    $zaak->touch;

    return;
}

=head2 allocate_case

=cut

sig allocate_case => 'Zaaksysteem::Backend::Object::Queue::Component';

sub allocate_case {
    my $self = shift;
    my $item = shift;

    $item->object_data->get_source_object->allocation_action($item->data);

    return;
}

=head2 add_case_subject

Add a new subject to a case, in a specified role.

If the (role, subject) combination already exists for this case, this queue
item will do nothing.

=cut

sub add_case_subject {
    my $self = shift;
    my $item = shift;

    my $case = $item->object_data->get_source_object;
    my $schema = $case->result_source->schema;

    my $subject = $schema->betrokkene_model->get_by_string($item->data->{betrokkene_identifier});

    if (
        !$subject &&
        (
            $subject->can('gm_extern_np') &&
            $subject->gm_extern_np &&
            $subject->gm_extern_np->can('deleted_on') &&
            $subject->gm_extern_np->deleted_on
        )
    ) {
        $self->log->error(sprintf("Subject with identifier %s not found", $item->data->{betrokkene_identifier}));
        return;
    }

    my $rv = $case->betrokkene_relateren({
        betrokkene_identifier  => $item->data->{betrokkene_identifier},
        magic_string_prefix    => $item->data->{magic_string_prefix},
        rol                    => $item->data->{rol},
        pip_authorized         => $item->data->{gemachtigd},
        send_auth_confirmation => $item->data->{notify},
    });

    if ($rv && $item->data->{notify} && $item->data->{gemachtigd}) {
        try {
            my $template_id = $schema->resultset('Config')->get(
                'subject_pip_authorization_confirmation_template_id'
            );

            unless($template_id) {
                my $message = 'Kon geen e-mail notificatie template vinden, geen notificatie verstuurd naar ingestelde betrokkene';
                $self->log->error($message);

                return;
            }

            my $template = $schema->resultset('BibliotheekNotificaties')->find($template_id);

            unless($template) {
                my $message = 'Notificatie template kon niet gevonden worden, geen e-mail notificatie verstuurd';
                $self->log->error($message);
                return;
            }

            unless($subject->email) {
                my $message = sprintf(
                    'Geen e-mail adres gevonden voor "%s", geen notificatie verstuurd',
                    $subject->display_name
                );
                $self->log->error($message);
                return;
            }

            $case->mailer->send_case_notification({
                notification => $template,
                recipient => $subject->email
            });


        } catch {
            $self->log->warn('Sending e-mail to authorized subject for subcase failed: ' . $_);
        };
    }

    if (!$rv) {
        $case->trigger_logging(
            'case/subject/exists',
            {
                component => LOGGING_COMPONENT_ZAAK,
                data => {
                    case_id      => $case->id,
                    subject_name => $item->data->{naam},
                    role         => $item->data->{rol},
                },
            },
        );
    }

    return 1;
}

=head2 touch_case

Handler for asynchronous case touches.

=cut

define_profile touch_case => (
    required => {
        case_object_id => UUID
    }
);

sub touch_case {
    my $self = shift;
    my $item = shift;

    my $args = assert_profile($item->data)->valid;

    my $case = $self->object_model->new_resultset->find($args->{ case_object_id });

    $case->get_source_object->_touch;

    return;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
