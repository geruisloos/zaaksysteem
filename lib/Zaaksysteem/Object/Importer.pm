package Zaaksysteem::Object::Importer;
use Moose;

use Moose::Util qw[apply_all_roles];

has 'state' => (is => 'rw', isa => 'Zaaksysteem::Object::ImportState');
has 'schema'      => (is => 'rw');
has 'object_type' => (is => 'rw');
has 'format'      => (is => 'rw');

=head2 model

This attribute holds a reference to a model capable of retrieving objects.

=cut

has 'model' => ( is => 'rw' );

sub BUILD {
    my $self = shift;
    my $args = shift;

    unless(exists $args->{ format }) {
        die('Require import format');
    }

    apply_all_roles($self, sprintf(
        'Zaaksysteem::Object::Importer::%s',
        ucfirst($args->{ format })
    ));

    unless($self->handles($args->{ object_type })) {
        die($args->{ format } . ' importer cannot handle ' . $args->{ object_type });
    }
}

# Stub handles, implicitly return false
# for all, abstract instance don't do shit.
sub handles { }
sub redirect { }

sub hydrate_import_state {
    die('Abstract importer object cannot hydrate the import state.');
}

sub execute_import_state {
    die('Abstract importer object cannot import anything.');
}

sub execute {
    my $self = shift;
    my $schema = shift;

    $self->execute_import_state($schema);
}

sub hydrate_from_files {
    my ($self, @files) = @_;

    return { map { $_->uuid => $self->hydrate_import_state($_) } @files };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 BUILD

TODO: Fix the POD

=cut

=head2 execute

TODO: Fix the POD

=cut

=head2 execute_import_state

TODO: Fix the POD

=cut

=head2 handles

TODO: Fix the POD

=cut

=head2 hydrate_from_files

TODO: Fix the POD

=cut

=head2 hydrate_import_state

TODO: Fix the POD

=cut

=head2 redirect

TODO: Fix the POD

=cut

