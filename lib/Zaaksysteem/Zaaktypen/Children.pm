package Zaaksysteem::Zaaktypen::Children;

use Moose::Role;
use Zaaksysteem::Tools;

use Zaaksysteem::Backend::Tools::DottedPath qw/get_by_dotted_path set_by_dotted_path/;

with 'MooseX::Log::Log4perl';

=pod

To update child casetypes with the settings of the mother, both are retrieved
as session hashrefs, and the relevant portions of the mother are copied into
the child. after that the child is committed back into the database using the
handy normal system.

e.g.

$mother = {
    node => { titel => 'Mother' },
    definitie => { some_setting => 'i cannot have another cookie' }
};

$child = {
    node => { titel => 'Child' },
    definitie => { some_setting => 'i can have another cookie' }
};

$settings = {
    definitie => 1
};

apply_mother_settings($mother, $child, $settings)

# After:

$child = {
    node => { titel => 'Child' },
    definitie => { some_settings => 'i cannot have another cookie' }
};

=cut


=head2 apply_mother_settings

One by one alter settings from the mother casetype

=cut

sub apply_mother_settings {
    my ($self, $mother, $child, $settings) = @_;

    my @paths = ();

    push @paths, 'authorisaties' if $settings->{authorisaties};

    if ($settings->{betrokkenen}) {
        push @paths, qw/
            betrokkenen
            node.adres_aanvrager
            node.adres_andere_locatie
            definitie.pdc_tarief
            node.properties.pdc_tarief_balie
            node.properties.pdc_tarief_telefoon
            node.properties.pdc_tarief_email
            node.properties.pdc_tarief_behandelaar
            node.properties.pdc_tarief_post
        /;
    }

    if ($settings->{actions}) {
        my $actions = $self->casetype_actions;
        push @paths, grep { defined } map { $_->{field} } @$actions;
    }

    $self->apply_mother_phase_settings($mother, $child, $settings);
    map { apply_mother($mother, $child, $_) } @paths;
}


=head2 get_phase_config

organize the different phase sections

e.g. 1 2 3 4 5

shift 1 -- registration phase/first phase
pop 5 -- afhandelfase/last phase

2,3,4: middle phases

=cut

sub get_phases {
    my ($self, $casetype) = @_;

    my $statussen     = $casetype->{statussen} or die "need statussen";
    my @phases        = sort keys %$statussen;
    my $first_phase   = shift @phases;
    my $last_phase    = pop @phases;

    # what remains in @phases are the middle phases, can be empty

    return {
        first_phase   => $statussen->{$first_phase},
        middle_phases => [map { $statussen->{$_} } @phases],
        last_phase    => $statussen->{$last_phase},
        resultaten    => $statussen->{$last_phase}->{elementen}{resultaten}
    };
}


sub apply_mother_phase_settings {
    my ($self, $mother, $child, $settings) = @_;

    my $phases = {
        mother => $self->get_phases($mother),
        child  => $self->get_phases($child)
    };

    my @new = ();

    foreach my $section (qw/first_phase middle_phases last_phase/) {
        # grab phase either from mother or child, depending on setting
        my $source    = $settings->{$section} ? 'mother' : 'child';
        my $selection = $phases->{$source}->{$section};


        if ($section eq 'first_phase') {
            # allocation settings are applied separately.
            my $allocation_source = $settings->{allocation} ? 'mother' : 'child';
            my $allocation_phase = $phases->{$allocation_source}->{first_phase};

            for my $path (qw/definitie.ou_id definitie.role_id definitie.role_set/) {
                my $value = get_by_dotted_path({target => $allocation_phase, path => $path});
                set_by_dotted_path({target => $selection, path => $path, value => $value});
            }
            push @new, $selection;
        }
        # results are handled separately, but are contained in the
        # structure for the last_phase
        elsif ($section eq 'last_phase') {
            my $resultaten_source = $settings->{resultaten} ? 'mother' : 'child';
            $selection->{elementen}{resultaten} = $phases->{$resultaten_source}->{resultaten};
            push @new, $selection;
        } else {
            # middle phases, come as a arrayref
            push @new, @$selection;
        }
    }

    $self->write_phases_to_child($child, \@new);
}


=pod

the phases exists in the casetype session in a format like
{
    1 => { bla bla bla },
    2 => { blah blah blah }
}

=cut

sub write_phases_to_child {
    my ($self, $child, $phases) = @_;

    # do the legacy indexed hash map thingy
    my $index = 0;
    $child->{statussen} = { map { $index += 1; $index => $_ } @$phases };
}


# using a dotted string ('node.properties'), get value from
# the mother and copy to child
sub apply_mother {
    my ($mother, $child, $path) = @_;

    my $value = get_by_dotted_path({target => $mother, path => $path});

    set_by_dotted_path({target => $child, path => $path, value => $value});
}

1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 apply_mother

TODO: Fix the POD

=cut

=head2 apply_mother_phase_settings

TODO: Fix the POD

=cut

=head2 get_phases

TODO: Fix the POD

=cut

=head2 write_phases_to_child

TODO: Fix the POD

=cut

