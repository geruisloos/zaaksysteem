package Zaaksysteem::Zaaktypen::Dependency;
use Moose;

use Params::Profile;

use namespace::autoclean;
use Clone qw(clone);
use Carp qw(cluck);

with 'MooseX::Log::Log4perl';

has solution => (
    is        => 'rw',
    predicate => 'has_solution',
);

has [qw/keyname ancestry_hash name id bibliotheek_categorie_id local_id/] => (
    'is'    => 'rw',
);




#
# ancestry is the tree location of an item. we need it to manipulate the zaaktype structure
# to inform it of the new id for the dependency.
#
# say we received id = 302 for kenmerk 'aantal kinderen'. in the local zaaksysteem this kenmerk
# is already present, but with id = 511. so every instance where kenmerk 502 was used
# now a reference has to be made to kenmerk 511. ancestry_hash contains the locations where
# these references were made
#
sub add_ancestry {
    my ($self, $ancestry, $key) = @_;

    $self->ancestry_hash({}) unless ref $self->ancestry_hash;

    my $clone_ancestry = clone($ancestry);

    push @$clone_ancestry, $key;
    my $joined = join ",", @$clone_ancestry;

    $self->ancestry_hash->{$joined} ||= $clone_ancestry;

}

sub remove_solution {
    my ($self) = @_;

    $self->solution(undef);
}



#
# return references to the tree items that need modification
#
sub sub_items {
    my ($self, $zaaktype) = @_;

    my $ancestry_hash = clone $self->ancestry_hash;

    unless($ancestry_hash) {
        $self->log->trace("No ancestry list found!");
        return [];
    }

    my $sub_items = [];

    foreach my $ancestry (values %$ancestry_hash) {
        die "ancestry list empty" unless @$ancestry;
        # track back to the proper tree leaf

        my $sub_item = $zaaktype;
        my $key_name = pop @$ancestry;
        foreach my $ancestor (@$ancestry) {
            $sub_item = $sub_item->{$ancestor};
        }
        push @$sub_items, {
            sub_item => $sub_item,
            key_name => $key_name
        };
    }

    return $sub_items;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 add_ancestry

TODO: Fix the POD

=cut

=head2 remove_solution

TODO: Fix the POD

=cut

=head2 sub_items

TODO: Fix the POD

=cut

