package Zaaksysteem::Model::Attributes;

use Moose;

use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

use Zaaksysteem::Attributes qw[ZAAKSYSTEEM_SYSTEM_ATTRIBUTES];

use Zaaksysteem::Constants qw[ZAAKSYSTEEM_CONSTANTS];

with 'Catalyst::Component::InstancePerContext',
    'MooseX::Log::Log4perl';

has schema => ( is => 'ro', required => 1 );

=head1 NAME

Zaaksysteem::Model::Attributes - Generic interface to all object-like attributes

=head1 SYNOPSIS

    my @attrs = $c->model('Attributes')->search('keyword');

=cut

=head1 CONSTRUCTORS

=head2 build_per_context_instance

This function instantiates this model object according to L<Catalyst>'s rules
regarding context instantiation. It roughly means this model gets a fresh object
every request.

This constructor captures a hard reference to the C<< $c->model('DB') >> object.

=cut

sub build_per_context_instance {
    my $class = shift;
    my $c = shift;

    return $class->new(
        schema => $c->model('DB')->schema
    );
}

=head1 METHODS

=head2 search

Main entry to this model, allows full-text free-form querying of all attributes
known to Zaaksysteem.

    my $attrs = $c->model('Attributes');

    for my $attr ($attrs->search('bag')) {
        ...
    }

If no query is provided, only system attributes will be returned.

=head3 Parameters

    $c->model('Attributes')->search($keyword, param1 => '', param2 => 1337);

=over 4

=item exclude_system_attributes

This parameter, when set and true-ish, will exclude the list of
systemattributes from being searched

=back

=cut

define_profile search => (
    optional => {
        exclude_system_attributes => 'Bool',
        value_type => 'Str'
    }
);

sub search {
    my $self = shift;
    my $keyword = shift;
    my %params = @_;

    my $opts = assert_profile(\%params)->valid;

    my @ret;

    if($keyword) {
        push @ret, $self->_search_db($keyword, $opts);
    }

    unless($opts->{ exclude_system_attributes }) {
        push @ret, $self->_search_sys($keyword, $opts);
    }

    return @ret;
}

=head1 INTERNAL METHODS

=head2 _search_sys

Issue: 'E Formulier' almost always shows up because of splitting on underscores, so any keyword
with a E in it will match.

=cut

sub _search_sys {
    my $self = shift;
    my $keyword = shift;
    my $opts = shift;

    my @ret;

    for my $attr (Zaaksysteem::Attributes::predefined_case_attributes) {
        next unless $attr->bwcompat_name;
        next if $opts->{ value_type } && $opts->{ value_type } ne $attr->attribute_type;

        # BUG: this only makes the first bwcompat_name available
        my $name = $attr->bwcompat_name;

        $name = join "_", @$name if ref $name;

        my @label_parts = map { ucfirst } split '_', $name;
        my $label = join(' ', @label_parts);

        # Pass over the current attribute if it doesn't match our query
        # OR add by default if no keyword is provided.
        if (length $keyword) {
            next unless $label =~ m[\Q$keyword]i;
        }

        my $field_config = ZAAKSYSTEEM_CONSTANTS->{ veld_opties }{ $attr->attribute_type };

        push @ret, {
            id          => $attr->name,
            is_multiple => $field_config->{ multiple_values } ? 1 : 0,
            label       => $label,
            object_type => 'attribute',
            object      => {
                source      => 'sys-attr',
                value_type  => $attr->attribute_type,
                column_name => $attr->name,
            },
        };
    }

    return @ret;
}

=head2 _search_db

=cut

sub _search_db {
    my $self = shift;
    my $keyword = sprintf('%%%s%%', shift);
    my $opts = shift;

    my @ret;

    my $attributes = $self->schema->resultset('BibliotheekKenmerken')->search({
        deleted => undef,
        -or => [
            naam => { 'ilike' => $keyword },
            naam_public => { 'ilike' => $keyword },
            label => { 'ilike' => $keyword }
        ],
        system => [ 0, undef ],
    });

    if ($opts->{ value_type }) {
        $attributes = $attributes->search({ value_type => $opts->{ value_type } });
    }

    for my $attr ($attributes->all) {
        my $field_config = ZAAKSYSTEEM_CONSTANTS->{ veld_opties }{ $attr->value_type };

        my $is_multiple = $attr->type_multiple || $field_config->{ multiple_values };

        my %result = (
            id          => $attr->id,
            is_multiple => $is_multiple ? 1 : 0,
            label           => $attr->naam,
            public_label    => $attr->naam_public,
            object_type => 'attribute',
            object      => {
                source      => 'db-attr',
                value_type  => $attr->value_type,
                column_name => "attribute." . ($attr->magic_string // ''),
            },
        );

        if ($attr->bibliotheek_kenmerken_values->count) {
            $result{ object }{ values } = [ map
                { { value => $_->value, active => $_->active, sort_order => $_->sort_order } }
                $attr->bibliotheek_kenmerken_values->all
            ];
        }

        push @ret, \%result;
    }

    return @ret;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 ZAAKSYSTEEM_CONSTANTS

TODO: Fix the POD

=cut

