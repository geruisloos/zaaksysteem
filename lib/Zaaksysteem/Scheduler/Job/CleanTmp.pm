package Zaaksysteem::Scheduler::Job::CleanTmp;

use Moose::Role;
use namespace::autoclean;

use Zaaksysteem::Tools;

=head1 NAME

Zaaksysteem::Scheduler::Job::CleanTmp - Clean self-cleaning tempfiles.

=head1 SYNOPSIS

=head1 METHODS

=head2 run

Run the scheduled job: clean up something from the tmp model

=cut

sub run {
    my ($self, $c) = @_;

    my $file = $c->model('DB::Filestore')->search({uuid => $self->data})->first;

    unless (defined $file) {
        $c->log->warn(sprintf(
            "File %s scheduled for deletion, but it could not be found.",
            $self->data
        ));

        return;
    }

    # ZS-11176
    # Return before actually deleting the entries
    # We need to be 100% sure we can delete the filestore object in this
    # case.
    $c->log->warn(
        sprintf(
            "Refusing to delete filestore object %d with UUID %s",
            $file->id, $file->uuid
        )
    );
    return 1;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
