package Zaaksysteem::Tools::MergeObject;
use warnings;
use strict;

use autodie;
use v5.14;

use Exporter qw(import);

our @EXPORT_OK = qw(get_diff merge_dbix_row merge_moose_obj);
our %EXPORT_TAGS = (
    all => \@EXPORT_OK,
);

=head1 NAME

Zaaksysteem::Tools::MergeObjects - Merge moose objects with data and/or DB rows

=head1 DESCRIPTION

=head1 SYNOPSIS

=cut

sub get_diff {
    my ($object, $hash) = @_;

    # DBIX::Resulset
    my @columns;
    if ($object->can('get_columns')) {
        @columns = @{$object->get_columns};
    }
    elsif($object->can('meta') && $object->meta->can('get_all_attributes')) {
        @columns = map { $_->name } $object->meta->get_all_attributes;
    }

    my ($orig, $new);
    foreach my $attr (@columns) {
        $orig = $object->$attr;
        $new  = $hash->{$attr};

        if (($orig// '') eq ($new // '')) {
            delete $hash->{$attr};
        }
        else {
            $hash->{$attr} = $new;
        }
    }
    return $hash;
}

sub merge_dbix_row {
    my ($object, $hash) = @_;

    my $diff = get_diff($object, $hash);
    $object->update($diff);
    return $object;
}

sub merge_moose_obj {
    my ($object, $hash) = @_;

    my $diff = get_diff($object, $hash);
    foreach (keys %$diff) {
        if (!defined $diff->{$_} ) {
            my $attr = $object->meta->find_attribute_by_name($_);
            if ($attr->has_clearer) {
                $attr->clearer;
                next;
            }
        }
        if (defined $diff->{$_}) {
            $object->$_($diff->{$_});
        }
    }
    return $object;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
