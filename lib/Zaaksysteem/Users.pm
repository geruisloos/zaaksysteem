package Zaaksysteem::Users;

use strict;
use warnings;

use Params::Profile;
use Zaaksysteem::Constants;
use Zaaksysteem::Tools;

use Digest::SHA;
use Encode qw(decode);
use MIME::Base64;
use Try::Tiny;

use Moose;
use namespace::autoclean;

with 'MooseX::Log::Log4perl';

use constant    DEFAULT_IMPORT_OU   => 'Import';
use constant    INTERNAL_USERS      => [qw/admin/];
use constant    OBJECT_CLASSES      => [qw/
    inetOrgPerson
    person
    posixAccount
    shadowAccount
    top
/];

use constant    OBJECT_CLASSES_POSIXGROUP   => [qw/
    posixGroup
    top
/];

use constant    OBJECT_CLASSES_OU   => [qw/
    organizationalUnit
    top
/];

use constant    LDAP_COLUMNS    => [qw/
    cn
    sn
    displayName
    givenName
    mail
    telephoneNumber

    homeDirectory
    userPassword
    uidNumber
    gidNumber
    loginShell
    initials
/];

has 'customer' => (
    is       => 'rw',
    isa      => 'HashRef',
    required => 1,
);

has 'config' => (
    is       => 'rw',
    isa      => 'HashRef',
    required => 1,
);

has 'prod' => (
    is => 'rw',
);

has 'uidnumber' => (
    is => 'rw',
);

has 'schema' => (
    is       => 'rw',
    weak_ref => 1,
);

has 'interface_id' => (
    is      => 'rw',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return $self->schema->resultset('Interface')
          ->find_by_module_name('authldap');
    });

has 'components' => (
    'is'      => 'rw',
    'lazy'    => 1,
    'default' => sub {
        return [qw/sync_users sync_password/];
    },
);

has 'uidnumber_start' => (
    'is'      => 'rw',
    'lazy'    => 1,
    'default' => sub {
        return 55100;
    },
);

has 'uidnumber' => (
    'is'      => 'rw',
    'lazy'    => 1,
    'default' => sub {
        return shift->uidnumber_start;
    },
);


has 'import_ou' => (
    'is'      => 'rw',
    'lazy'    => 1,
    'default' => sub {
        return (shift->customer->{start_config}{LDAP}{import_ou}
              || DEFAULT_IMPORT_OU);
    },
);

has 'ldap_system_roles'     => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $roles = ZAAKSYSTEEM_AUTHORIZATION_ROLES;

        my $ldaproles = {};
        for my $role (keys %{ $roles }) {
            my $data    = $roles->{ $role };

            $ldaproles->{ $data->{ldapname} } = $data;
            $ldaproles->{ $data->{ldapname} }->{
                'internalname'
            } = $role;
        }

        return $ldaproles;
    }
);

sub sync_users {
    my $self    = shift;
    my $users   = shift;
    $self->schema(shift);

    $self->log->info('Start synchronizing AD users');

    my $local_users     = $self->_retrieve_local_users();

    my $compared_users = $self->_compare_users(
        local_users  => $local_users,
        remote_users => $users->{Bussum},
    );

    $self->_load_users(
        compared_users => $compared_users,
        local_users    => $local_users,
    );

    return 1;
}

=head2 _load_users

my $ok = _load_users(
    $compared_users,
    $local_users,
);

Modify LDAP to reflect the actual state

=head3 ARGUMENTS

=over

=item compared_users [hashref]

=item local_users [hashref]

=back

=head3 RETURNS

True

=cut

define_profile _load_users => (
    required => {
        compared_users => 'HashRef',
        local_users    => 'HashRef',
    }
);

sub _load_users {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    my ($compared_users, $local_users) = ($opts->{compared_users}, $opts->{local_users});

    my $total_users = scalar (keys %{$compared_users->{modify}});
    $self->log->info("Modifying $total_users user(s)");
    if ($total_users) {
        $self->_modify_all_users(
            users       => $compared_users->{modify},
            users_in_zs => $local_users,
        );
    }

    $total_users = scalar (keys %{$compared_users->{delete}});
    $self->log->info("Removing $total_users user(s)");
    if ($total_users) {
        $self->_delete_users(
            users => $compared_users->{delete},
        );
    }

    $total_users = scalar (keys %{$compared_users->{create}});
    $self->log->info("Adding $total_users user(s)");
    if ($total_users) {
        $self->_create_all_users(users => $compared_users->{create});
    }

    return 1;
}

=head2 _create_user

my $ok = $self->_create_user(
    user    => $username,
    details => $details,
);

Modify Subjects to reflect their actual state

=head3 ARGUMENTS

=over

=item user

=item details

=back

=head3 RETURNS

True

=cut

define_profile _create_user => (
    required => [qw(username details)],
    typed    => {
        username => 'Str',
        details  => 'HashRef',
    },
);

sub _create_user {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    my @must_exist = qw(sn displayName);
    if (grep { !defined $opts->{details}{$_} || !length($opts->{details}{$_}) } @must_exist) {
        throw('/Zaaksysteem/User/create_ldap_user', "Minimal LDAP data not found!");
    }

    my $departments         = {};

    my %add;
    for my $column (keys %{$self->_return_clean_user($opts->{details})}) {
        if ($column eq 'userPassword') {
            $add{password}    = $opts->{details}{$column};
        } else {
            $add{lc($column)} = $opts->{details}{$column};
        }
    }

    $self->log->info("Adding user: $opts->{details}{cn}");

    my $user_entity = $self->schema->resultset('Subject')->create_user(
        {
            username    => $opts->{details}{cn},
            interface   => $self->interface_id,
            %add,
        }
    );

    return $user_entity->subject_id;
}

=head2 _create_all_users

my $ok = $self->_create_all_users(
    add_users => $users_to_add
);

Modify LDAP to reflect the actual state

=head3 ARGUMENTS

=over

=item ldaphandle not used

=item compared_users [hashref]

=item local_users [hashref]

=back

=head3 RETURNS

True

=cut

define_profile _create_all_users => (
    required => [qw(users)],
    typed => {
        users => 'HashRef',
    },
);

sub _create_all_users {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;
    my $compared_users = $opts->{users};

    for my $username (keys %{$compared_users}) {
        # The function dies in case of an LDAP error (handy for testing).
        # But we don't want to fail the complete sync because of it
        try {
            $self->_create_user(
                username => $username,
                details  => $compared_users->{$username},
            );
        } catch {
            $self->log->error("Error creating user $username: $_");
        };
    }
}

=head2 _delete_users

my $ok = $self->_delete_users(
    users => $users_to_delete
    force => 0, # defaults to true
);

Modify LDAP to reflect the actual state

=head3 ARGUMENTS

=over

=item users

A HashRef, required.

=item force

A boolean, optional. Defaults to true

=back

=head3 RETURNS

True

=cut

define_profile _delete_users => (
    required => { users => 'HashRef' },
    optional => { force => 'Bool' },
    defaults => { force => 1, },
);

sub _delete_users {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    if (!$opts->{force}) {
        $self->log->info("Refusing to delete users no force parameter given");
        return 0;
    }

    for my $username (keys %{$opts->{users}}) {
        # The function dies in case of an LDAP error (handy for testing).
        # But we don't want to fail the complete sync because of it
        try {
            $self->_delete_user(
                username => $username,
            );
        } catch {
            $self->log->error("Error deleting user $username: $_");
        };
    }
}

define_profile _delete_user => (
    required => { username => 'Str' },
);

sub _delete_user {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    my $username = $opts->{username};

    my $ue = $self->_get_user_entity_from_username($username);

    if ($ue->subject_id->is_system_user) {
        throw('users/cannot_delete_system_user', "Refusing to delete system user with username $username");
    }

    $self->log->info("Deleting user: $username");
    $ue->update({date_deleted => DateTime->now()});

    return 1;
}

sub _return_clean_user {
    my ($self, $userdata) = @_;

    my %rv;
    my @columns = @{LDAP_COLUMNS()};
    for my $column (@columns) {
        next unless $userdata->{$column};

        if ( $column eq 'telephoneNumber'
            && length($userdata->{$column})
            && (   $userdata->{$column} =~ /^\s+$/
                || $userdata->{$column} !~ /^[\+0-9\s-]+$/)
          ) {
            next;
        }
        $rv{$column} = $userdata->{$column};
    }
    return \%rv if (keys %rv);
    throw('/Zaaksysteem/User/clean_user', "No userdata found!");
}

=head2 _get_user_entity_from_username

my $ue = $self->_get_user_entity_from_username($username);

Get the UserEntity for the user

=head3 ARGUMENTS

=over

=item username

=back

=head3 RETURNS

=cut

sub _get_user_entity_from_username {
    my ($self, $username) = @_;

    my $users = $self->schema->resultset('UserEntity')->search({
        source_interface_id => $self->interface_id->id,
        source_identifier   => $username,
    });

    return $users->first;
}

define_profile _modify_all_users => (
    required => [qw(users users_in_zs)],
    typed    => {
        users       => 'HashRef',
        users_is_zs => 'HashRef',
    },
);

sub _modify_all_users {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;
    for my $username (keys %{$opts->{users}}) {
        # The function dies in case of an LDAP error (handy for testing).
        # But we don't want to fail the complete sync because of it
        try {
            $self->_modify_user(
                username        => $username,
                details         => $opts->{users}{$username},
                current_details => $opts->{users_in_zs}{$username},
            );
        } catch {
            $self->log->error("Error while modifying user $username: $_");
        };
    }
}

define_profile _modify_user => (
    required => [qw(username details current_details)],
    typed    => {
        username        => 'Str',
        details         => 'HashRef',
        current_details => 'HashRef',
    },
);

sub _modify_user {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    my $username = $opts->{username};

    my %modify;

    for my $column (keys %{$self->_return_clean_user($opts->{details})}) {
        next if ($column =~ m/^(?:(?:gid|uid)Number|userPassword|uid)$/);

        $modify{$column} = $opts->{details}{$column};
    }

    my $ue = $self->_get_user_entity_from_username($username);
    my $subject = $ue->subject_id;

    # Make sure the UserEntity is active (again)
    if ($ue->date_deleted) {
        $ue->update({date_deleted => undef});
    }

    if (   exists $opts->{details}{userPassword}
        && $opts->{details}{userPassword} ne $ue->password
    ) {
        $self->log->info("Changing password for $username");
        $ue->update({ password => $opts->{details}{userPassword} });
    }

    # Fall through in case there aren't any changes.
    my %changes = $subject->update_properties_from_adsync(
        \%modify,
        {
            interface_id => $self->interface_id->id,
        }
    );

    return unless keys %changes;

    $self->log->info(sprintf("Modify user: '%s', applied changes %s", $username, dump_terse(\%changes)));
    return $subject;
}

=head2 _compare_users(@args);

my $res = $self->_compare_users(
    ldap_handle,
    local_users,
    users,
);

Compares users with something ... from Bussum.

=head3 ARGUMENTS

=over

=item $ldap_handle [undef, not used!]

=item $local_users

=item $users

=back

=head3 RETURNS

{
    'modify'    => {},
    'create'    => {},
    'delete'    => {},
};

=cut

define_profile _compare_users => (
    required => [qw(local_users remote_users)],
    #typed => {
    #    local_users  => 'HashRef',
    #    remote_users => 'HashRef',
    #},
);

sub _compare_users {
    my $self        = shift;
    my $opts        = assert_profile({@_})->valid;
    my $local_users = $opts->{local_users};
    my $users       = $opts->{remote_users};

    my $rv = {
        'modify'    => {},
        'create'    => {},
        'delete'    => {},
    };

    for my $username (keys %{$users}) {
        $username       = lc($username);
        my $userdata    = $users->{$username};

        ### Required
        next unless $userdata->{sn};

        $userdata->{initials}      = $userdata->{givenName} || '';
        $userdata->{initials}      =~ s/( ?[a-zA-Z])\w+/$1./g;

        if (
            defined($local_users->{$username}) &&
            $local_users->{$username}->{userPassword} &&
            !$userdata->{userPassword}
        ) {
            $userdata->{userPassword} = $local_users->{$username}->{userPassword};
        }

        if (!$local_users->{$username}) {
            $rv->{create}->{$username}  = $userdata;
            next;
        }

        ### Edit
        $rv->{modify}->{$username}      = $userdata;
    }

    for my $username (keys %{$local_users}) {
        if (!$users->{$username}) {
            $rv->{delete}{$username}  = $local_users->{$username};
        }
    }

    return $rv;
}

=head2 _retreive_local_users($ldap_handle)

Retreives local users from LDAP.

=head3 ARGUMENTS

=over

=item LDAP handle

=back

=head3 RETURNS

An hashref with user information:

{
    'admin' => {
        'cn'          => 'admin',
        'displayName' => 'A. Admin',
        'dn' => 'cn=admin,ou=Management,o=zaaksysteem,dc=zaaksysteem,dc=nl',
        'gidNumber'       => '50001',
        'givenName'       => 'Anton',
        'homeDirectory'   => '/home/admin',
        'initials'        => 'A',
        'loginShell'      => '/nologin',
        'mail'            => 'demo-gebruiker@zaaksysteem.nl',
        'sn'              => 'Admin',
        'telephoneNumber' => '0612345678',
        'uid'             => 'admin',
        'uidNumber'       => '500001',
        'userPassword'    => '{SSHA}iwbtUYAf4wwRPgUW221L6BGrUrB4iWP+CgPBRA=='
    },
    'gebruiker' => {
        'cn'          => 'gebruiker',
        'displayName' => 'G. Bruiker',
        'dn' => 'cn=gebruiker,ou=Management,o=zaaksysteem,dc=zaaksysteem,dc=nl',
        'gidNumber'       => '55101',
        'givenName'       => 'Bruiker',
        'homeDirectory'   => '/home/gebruiker',
        'initials'        => 'G.',
        'loginShell'      => '/nologin',
        'mail'            => 'demo-gebruiker@zaaksysteem.nl',
        'sn'              => 'Bruiker',
        'telephoneNumber' => '0612345678',
        'uid'             => 'Ge',
        'uidNumber'       => '55101',
        'userPassword'    => '{SSHA}Red5g+BaYYoqutIs/LdfY4GCyC4XTQWluhmFkA=='
    },
};

=cut

sub _retrieve_local_users {
    my $self    = shift;

    $self->log->info('- Retrieve all users for comparison');

    my $users = $self->schema->resultset('UserEntity')->search(
        {
            source_interface_id => $self->interface_id->id,
            date_deleted        => undef,
            subject_id          => { '!=' => undef },
        }
    );
    if (!$users->count) {
        return {};
    }

    my $local_users = {};

    my @columns = @{LDAP_COLUMNS()};
    my $uidNumber   = 0;
    while (my $userentity = $users->next) {
        my $user    = $userentity->subject_id;
        my $rv = {};

        foreach (@columns) {
            my $col = lc($_);
            if (defined($user->$col)) {
                $rv->{$_} = $user->$col;
            }
        }
        $rv->{dn} = $user->id;

        $local_users->{ lc($user->username) } = $rv;
    }

    return $local_users;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 DEFAULT_IMPORT_OU

TODO: Fix the POD

=cut

=head2 INTERNAL_USERS

TODO: Fix the POD

=cut

=head2 LDAP_COLUMNS

TODO: Fix the POD

=cut

=head2 LDAP_TRANSLATE_DEPARTMENTS

TODO: Fix the POD

=cut

=head2 OBJECT_CLASSES

TODO: Fix the POD

=cut

=head2 OBJECT_CLASSES_OU

TODO: Fix the POD

=cut

=head2 OBJECT_CLASSES_POSIXGROUP

TODO: Fix the POD

=cut

=head2 ZAAKSYSTEEM_AUTHORIZATION_ROLES

TODO: Fix the POD

=cut

=head2 add_ou

TODO: Fix the POD

=cut

=head2 add_role

TODO: Fix the POD

=cut

=head2 add_role_to

TODO: Fix the POD

=cut

=head2 delete_entry

TODO: Fix the POD

=cut

=head2 delete_role_from

TODO: Fix the POD

=cut

=head2 deploy_user_in_roles

TODO: Fix the POD

=cut

=head2 find

TODO: Fix the POD

=cut

=head2 get_all_roles

TODO: Fix the POD

=cut

=head2 get_array_of_roles

TODO: Fix the POD

=cut

=head2 get_list_of_entries

TODO: Fix the POD

=cut


=head2 get_ou_by_id

TODO: Fix the POD

=cut

=head2 get_parent_ou_ids

TODO: Fix the POD

=cut

=head2 get_posix_group

TODO: Fix the POD

=cut

=head2 get_role_by_id

TODO: Fix the POD

=cut

=head2 get_tree_view

TODO: Fix the POD

=cut

=head2 has_children

TODO: Fix the POD

=cut

=head2 ldaph

TODO: Fix the POD

=cut

=head2 move_to_ou

TODO: Fix the POD

=cut

=head2 primary_roles

TODO: Fix the POD

=cut

=head2 sync_user

TODO: Fix the POD

=cut

=head2 sync_users

TODO: Fix the POD

=cut

