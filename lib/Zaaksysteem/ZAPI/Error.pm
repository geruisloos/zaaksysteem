package Zaaksysteem::ZAPI::Error;

use Moose;

use Zaaksysteem::ZAPI::Response;
use Zaaksysteem::Exception;

with qw/
    Zaaksysteem::ZAPI::Error::Profile
/;

use constant    ZAPI_ERROR_KEYS => [
    qw/
        messages
        type
        stacktrace
        data
    /
];

use constant    ZAPI_ERROR_REQUIRED  => [
    qw/
        type
        messages
    /
];

has 'debug'         => (is => 'ro', default => sub { $ENV{ CATALYST_DEBUG } || $ENV{ ZS_TEST } });
has 'status_code'   => (is => 'ro', default => '500');

has [@{ ZAPI_ERROR_KEYS() }] => (
    is      => 'rw'
);

sub _validate_response {
    my $self        = shift;

    if (
        (my @filled_attrs = grep { $self->$_ } @{ ZAPI_ERROR_REQUIRED() })
            != scalar( @{ ZAPI_ERROR_REQUIRED() })
    ) {
        Zaaksysteem::Exception->throw(
            'zapi/error',
            'Missing required attributes: ' . join(',',
                grep { !$self->$_ } @{ ZAPI_ERROR_REQUIRED() }
            )
        );
    }

    if ($self->data && !UNIVERSAL::isa($self->data, 'ARRAY')) {
        die('Not a valid ARRAY for data attribute: ' . Data::Dumper::Dumper($self->data));
    }

    if ($self->messages && !UNIVERSAL::isa($self->messages, 'ARRAY')) {
        $self->messages([$self->messages]);
    }
}

sub response {
    my $self        = shift;

    $self->_validate_response;

    my $rep_object  = {
        map { $_ => $self->$_ }
            @{ ZAPI_ERROR_KEYS() }
    };

    if(blessed $self->stacktrace) {
        $rep_object->{ stacktrace } = [ map { $_->as_string } $self->stacktrace->frames ];
    }

    ### Do we want to expose a stack trace? Guess not
    $rep_object->{stacktrace} = [] unless $self->debug;

    return Zaaksysteem::ZAPI::Response->new(
        status_code => $self->status_code,
        unknown     => [ $rep_object ],
    )->response;
}

1;

=head1 NAME

Zaaksysteem::ZAPI::Error - Generates a correct ZAPI Error

=head1 SYNOPSIS

    my $error    = Zaaksysteem::ZAPI::Response->new(
        resultset       => $c->model('DB::Zaak')->search_extended(),
        uri_prefix      => $c->uri_for('/zaak'),
        page_size    => $c->req->params->{next} || 1,
    )

    $c->res->body($response->response);


=head1 DESCRIPTION

To make sure every response is valid, this object validates the input
parameters and implements a correct return HASHREF.




__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 ZAPI_ERROR_KEYS

TODO: Fix the POD

=cut

=head2 ZAPI_ERROR_REQUIRED

TODO: Fix the POD

=cut

=head2 response

TODO: Fix the POD

=cut

