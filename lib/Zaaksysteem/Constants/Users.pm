package Zaaksysteem::Constants::Users;
use warnings;
use strict;

=head1 NAME

Zaaksysteem::Constants::Users - Constants for user rights

=head1 DESCRIPTION

This module defined constants for user rights. Currently only does:

C<REGULAR> meaning logged in user
C<API>  meaning API user
C<PIP>  meaning PIP user

=head1 SYNOPSIS

    use Zaaksysteem::Constants::Users qw(PIP API FULL);

    # Check if the user is logged in or not
    $c->assert_user(PIP);

=cut

require Exporter;
our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(REGULAR PIP API);
our %EXPORT_TAGS  = ( all => \@EXPORT_OK );

use constant REGULAR => 4;
use constant API     => 2;
use constant PIP     => 1;

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
