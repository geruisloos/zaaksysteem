package Zaaksysteem::Test::Backend::Sysin::Auth::Alternative;

=head1 NAME

Zaaksysteem::Test::Backend::Sysin::Auth::Alternative - Test OverheidIO model

=head2 SYNOPSIS

    prove -l :: Zaaksysteem::Test::Backend::Sysin::Auth::Alternative

=cut

use Zaaksysteem::Backend::Sysin::Auth::Alternative;
use Zaaksysteem::Test;

sub test_setup {
    my $test = shift;

    my $test_method = $test->test_report->current_method->name;

    if ('test_alt_auth_live' eq $test_method
        && !$ENV{ZS_ALT_AUTH_API_KEY})
    {
        $test->test_skip("Skipping $test_method: set ZS_ALT_AUTH_API_KEY");
    }
}

sub _create_alt_auth_ok {
    my $options = {@_};
    my $model   = Zaaksysteem::Backend::Sysin::Auth::Alternative->new(
        schema       => mock_dbix_schema,
        sms_endpoint => $options->{sms_endpoint} // 'https://sendsms.zaaksysteem.nl/api/sms',
        sms_product_token => $options->{sms_product_token} // 'zs-testsuite',
        sms_sender => $options->{sms_sender} // 'zs-testsuite',
    );
    isa_ok($model, 'Zaaksysteem::Backend::Sysin::Auth::Alternative');
    return $model;
}

sub test_assert_mobile_phone_number {

    my $oio = _create_alt_auth_ok();

    my $number = '10094296';
    my $number_ok = "00316$number";

    my @good = (
        "06$number",
        "+316$number",
        $number_ok,
        "06-$number",
    );

    foreach my $number (@good) {
        is($oio->assert_mobile_phone_number($number), $number_ok, "Sets $number to $number_ok");
    }

    my @failure = (
        # Dutch number failures
        '+31(0)6 71988729',
        '025 8971592',
        '(071) 8926248',
        '+316-71556838',
        '(025) 6272958',
        '059 6654197',
        '+3155 1577125',
        '+3152 2776927',
        '+31900 496046',

        # Foreign numbers
        '+2975812345678',
        '+498944 97737'


    );

    foreach my $number (@failure) {
        throws_ok(
            sub {
                $oio->assert_mobile_phone_number($number)
            },
            qr#^auth/alternative/phone_number/#,
            "Number $number is failure"
        );
    }

}

sub test_create_sms_xml {

    my $oio = _create_alt_auth_ok();
    my $xml = $oio->create_sms_xml(
        phone_number => '0610094296',
        message      => 'This is the testsuite',
        reference    => 'Reference',
    );

    ok($xml, "create_sms_xml");

}

sub test_send_sms {
    my $oio = _create_alt_auth_ok();

    my $record = mock_transaction_record;
    my $xml    = "<foo>bar</foo>";

    my %args = (
        xml    => $xml,
        record => $record,
    );

    my $record_input = sprintf("POST %s\nContent-Type: application/xml\n\n%s\n", $oio->sms_endpoint, $xml);

    {
        no warnings qw(redefine once);
        use HTTP::Response;
        my $answer = "Send sms";
        local *LWP::UserAgent::request = sub {
            my $self = shift;
            return HTTP::Response->new(200, undef, undef, $answer);
        };
        use warnings;

        ok($oio->send_sms(%args), "send_sms with 200");

        is($record->output, "200 OK\n\n$answer\n", "We have output in our transaction record");
        is($record->input, $record_input, "We have input in our transaction record");

    }

    {
        no warnings qw(redefine once);
        use HTTP::Response;
        my $answer = "This is failure";
        local *LWP::UserAgent::request = sub {
            my $self = shift;
            return HTTP::Response->new(403, undef, undef, $answer);
        };
        use warnings;

        throws_ok(
            sub {
                $oio->send_sms(%args);
            },
            qr#^twofactor/sms_sending_failed: \d+#,
            "Failure with sending SMS",
        );

        is($record->output, "403 Forbidden\n\n$answer\n", "We have output in our transaction record when failing");
        is($record->input, $record_input, "We have input in our transaction record when failing");
    }

}

1;

__END__

=head1 NAME

Zaaksysteem::Test::Backend::Sysin::Auth::Alternative - A OverheidIO test package

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
