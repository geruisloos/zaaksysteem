package Zaaksysteem::Test::API::v1::Object::Session;

use Zaaksysteem::Test;

use Zaaksysteem::API::v1::Object::Session;
use Zaaksysteem::Backend::Subject::Component;

use URI;

=head1 NAME

Zaaksysteem::Test::API::v1::Object::Session - Test session object construction

=head1 DESCRIPTION

Test queue model

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::API::v1::Object::Session

=head2 test_run

=cut

sub new_from_catalyst : Tests {
    my $self    = shift;

    my $user = qclass(
        -with_new => 1,
        -subclass => ['Zaaksysteem::Backend::Subject::Component'],

        has_legacy_permission => qmeth {
            return 0
        },
    )->package->new;

    my $uexists = 0;

    my $c = qobj(
        user => qmeth {
            return $user
        },
        user_exists => qmeth { return $uexists },
        req => qobj(
            uri => qmeth { return URI->new('http://localhost/api/v1') }
        ),
        config => qmeth {
            return {
                gemeente_id => 'template'
            }
        },
        get_customer_info => qmeth {
            return {
                naam        => 'Mintlab B.V.',
                naam_lang   => 'Mintlab B.V.',
                naam_kort   => 'mintlab',
                email       => 'info@mintlab.nl',
            }
        }
    );

    ### No logged in user, e.g. /form
    {
        $uexists = 0;
        my $session = Zaaksysteem::API::v1::Object::Session->new_from_catalyst($c);

        ok(!$session->logged_in_user, 'Found no logged in user');
        is($session->hostname, 'localhost', 'Found correct hostname');

        isa_ok($session->account, 'Zaaksysteem::API::v1::Object::Session::Account');

        ok ($session->account->$_, "Got value for $_") for qw/company email/;
    }

    ### Logged in user
    {
        $uexists = 1;
        my $session = Zaaksysteem::API::v1::Object::Session->new_from_catalyst($c);

        ok($session->logged_in_user, 'Found a logged in user');
        is($session->hostname, 'localhost', 'Found correct hostname');

        isa_ok($session->account, 'Zaaksysteem::API::v1::Object::Session::Account');

        ok ($session->account->$_, "Got value for $_") for qw/company email/;
    }
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
