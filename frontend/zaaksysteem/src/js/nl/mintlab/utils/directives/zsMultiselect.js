/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsMultiselect', [ function ( ) {
			
			var element = angular.element,
				indexOf = window.zsFetch('nl.mintlab.utils.shims.indexOf');
			
			function findMultiselectTarget ( trg ) {
				var target = element(trg);
				while(target.length) {
					// TODO: combine ng-multiselect and zs-multiselects
					if(target.attr('data-ng-multiselect') !== undefined) {
						return null;
					}
					if(target.attr('data-zs-multiselectable') !== undefined) {
						return target;
					}
					if(target.attr('data-zs-multiselect') !== undefined) {
						return null;
					}
					target = target.parent();
				}
			}
			
			return {
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
					return function link ( scope, element, attrs ) {
						
						var lastClicked = null,
							list = null,
							enabled,
							unwatch;
							
						function enable ( ) {
							enabled = true;
							
							unwatch = scope.$watch(function ( ) {
								list = null;
							});
							
							element.bind('click', onElementClick);
						}
						
						function disable ( ) {
							enabled = false;
							
							if(unwatch) {
								unwatch();
							}
							
							element.unbind('click', onElementClick);
						}
													
						function onElementClick ( event ) {
							var target = findMultiselectTarget(event.target);
							if(!target) {
								return;
							}
							if(lastClicked && event.shiftKey) {
								selectUpto(target);
							} else {
								lastClicked = target;
							}
						}
						
						function selectUpto ( target ) {
							var i,
								l,
								nativeTarget = target[0],
								nativeLast = lastClicked[0],
								input,
								selectableList = getSelectableList(),
								start = indexOf(list, nativeLast),
								end = indexOf(list, nativeTarget);
							
							if(end < start) {
								i = end,
								l = i + (start-end + 1);
							} else {
								i = start;
								l = i + (end-start + 1);
							}
							
							for(; i < l; ++i) {
								// TODO(dario): don't exactly know what this does,
								// but we should just use zsSelectableList instead of zsMultiselect
								input = selectableList[i] ? selectableList[i].querySelector('input[type="checkbox"]') : null;
								if(input && !input.checked) {
									angular.element(input).triggerHandler('click');
								}
							}
						}
						
						function getSelectableList ( ) {
							if(list) {
								return list;
							}
							
							list = element[0].querySelectorAll('[data-zs-multiselectable]');
							
							return list;
						}
						
						attrs.$observe('zsMultiselect', function ( ) {
							var shouldBeEnabled = attrs.zsMultiselect === "false" ? false : !!attrs.zsMultiselect;
							if(shouldBeEnabled !== enabled) {
								if(shouldBeEnabled) {
									enable();
								} else {
									disable();
								}
							}
						});
						
						
						
					};
					
				}
			};
			
		} ]);
	
})();
