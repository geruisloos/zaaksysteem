/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.object.view')
		.directive('zsObjectRelationList', [ '$parse', '$interpolate', 'zqlService', 'objectService', 'objectRelationService', 'translationService', 'systemMessageService', 'urlencodeFilter', 'zqlEscapeFilter', function ( $parse, $interpolate, zqlService, objectService, objectRelationService, translationService, systemMessageService, urlencodeFilter, zqlEscapeFilter ) {
			
			return {
				require: [ 'zsObjectRelationList', '^zsObjectView' ],
				controller: [ '$scope', function ( $scope ) {
					
					var ctrl = this,
						zsObjectView,
						object,
						objectTypes,
						relatableTypes = [],
						types,
						loading = true;
						
					objectTypes = {
						'case': {
							label: translationService.get('Zaak'),
							values: {
								prefix: 'case'
							}
						},
						casetype: {
							label: translationService.get('Zaaktype'),
							values: {
								prefix: 'casetype'
							}
						}
					};
						
					function onObjectUpdate ( ) {
						
						refreshObject();
					}
					
					function onObjectLoad ( ) {
						
						object = zsObjectView.getObject();
						
						types = [ object.type ].concat(object.relatable_types);
						
						zqlService.query('SELECT {} FROM type WHERE prefix in (' + _.map(types, zqlEscapeFilter).join(',') + ')', { deep_relations: true })
							.success(function ( response ) {
								_.each(response.result, function ( objectType ) {
									
									var prefix = objectType.values.prefix;
									
									objectTypes[prefix] = objectType;
									
									refreshObject();
									
									loading = false;
									
								});
							});
						
						refreshObject();
						
						zsObjectView.onUpdateListeners.push(onObjectUpdate);
						
						$scope.$on('$destroy', function ( ) {
							_.pull(zsObjectView.onUpdateListeners, onObjectUpdate);
						});
					}
						
					function refreshObject ( ) {
						
						var caseObj;
						
						object = zsObjectView.getObject();
						
						if(object) {
							relatableTypes = _.map(object.relatable_types.concat(['casetype']), function ( type ) {
								var label = type,
									objectLabel = 'label',
									restrict = 'objects',
									params = {
										object_type: type
									},
									objType = objectTypes[type];
									
								if(objType) {
									label = objType.label;
								}
									
								switch(type) {
									case 'case':
									case 'casetype':
									restrict = '';
									params = {
										object_type: type.replace('case', 'zaak')
									};
									if(type === 'casetype') {
										objectLabel = 'values["casetype.name"]';
									}
									break;
									
									case 'scheduled_job':
									label = 'Geplande zaken';
									break;
								}
								
								
								return {
									id: type,
									label: label,
									objectLabel: objectLabel,
									restrict: restrict,
									params: params
								};
							});
							
							// move case to end of array
							caseObj = _.remove(relatableTypes, { id: 'case'} )[0];
							if(caseObj) {
								relatableTypes.push(caseObj);
							}
							
							
						}
					}
					
					function hasRelation ( obj, type ) {
						var exists = !!_.find(objectService.getRelations(object), { 'related_object_type': type, 'related_object_id': objectService.getObjectId(obj)});
						return exists;
					}
					
					function isObject ( obj, type ) {
						return object.type === type && object.id === obj.id;
					}
					
					function convertObjectToNewStyle ( obj, type ) {
						var object;
						
						switch(type) {
							case 'case':
							object = obj.zs_object;
							break;
						}
						
						return object;
					}
					
					ctrl.link = function ( controllers ) {
						zsObjectView = controllers[0];
						
						zsObjectView.loadObject()
							.then(onObjectLoad);
					};
					
					ctrl.getRelatedObjects = function ( type ) {
						var objects;

						objects = _.sortBy(_.filter(objectService.getRelations(object), { 'related_object_type': type.id }), function ( o ) {
							return o.related_object.date_created;
						}).reverse();

						return objects;
					};
					
					ctrl.getRelatedObjectLabel = function ( relatedObject, type  ) {
						return $parse(type.objectLabel)(relatedObject);
					};
					
					ctrl.getRelatedObjectLink = function ( relatedObject, type ) {
						var url;

						switch(type.id) {
							case 'casetype':
							url = $interpolate('/zaak/create/?prefill_zaaktype_id=<[values["casetype.name"]]>&prefill_zaaktype_name=<[values["casetype.id"]|urlencode]>&related_object=' + object.id)(relatedObject);
							break;
							
							case 'case':
							url = $interpolate('/zaak/<[values["case.number"]]>')(relatedObject);
							break;

							default:
							url = '/object/' + relatedObject.id;
							break;
						}
						
						return url;
					};
					
					ctrl.getRelatableTypes = function ( ) {
						return _.reject(relatableTypes, { id: 'casetype' });
					};
					
					ctrl.getSpotEnlighterRestrict = function ( type )  {
						return type.restrict;
					};
					
					ctrl.getSpotEnlighterParams = function ( type ) {
						return type.params;
					};
					
					ctrl.addRelation = function ( obj, type ) {	
					
						var relation;				
						
						if(type === 'case') {
							obj = convertObjectToNewStyle(obj, type);
						}
							
						if(hasRelation(obj, type)) {
							$scope.$emit('systemMessage', {
								'content': translationService.get('U kunt een object slechts één keer toevoegen'),
								'type': 'error'
							});
						} else if(isObject(obj, type)) {
							$scope.$emit('systemMessage', {
								'content': translationService.get('U kunt het object niet relateren aan zichzelf'),
								'type': 'error'
							});
						} else {
							
							relation = objectRelationService.createRelationTo(obj);
							
							objectRelationService.addRelation(object, relation)
								['catch'](function ( ) {
									systemMessageService.emitSaveError('de relatie');
								});
						}
					};
					
					ctrl.removeRelation = function ( obj ) {
						
						var relation = _.find(object.related_objects, { related_object_type: obj.type, related_object_id: obj.id });
						
						if(relation) {
							objectRelationService.removeRelation(object, relation)
								['catch'](function ( ) {
									systemMessageService.emitSaveError();
								});	
						}
						
					};

					ctrl.canRemove = function ( obj ) {
						var allowed = true;
						
						if(obj.owner_object_id && object) {
							allowed = obj.owner_object_id === object.id;
						}
						
						return allowed;
					};
					
					ctrl.handleSpotEnlighterSelect = function ( $object, type ) {
						ctrl.addRelation($object.object, type.id);
						ctrl.newObject = null;
					};

					ctrl.isRelatableType = function ( type ) {
						var relatable;
						
						if(object) {
							relatable = _.indexOf(object.relatable_types, type) !== -1;
						}
						
						return relatable;
					};
					
					ctrl.canAdd = function ( type ) {
						// TODO: base on rights
						return true;
					};
					
					ctrl.isLoading = function ( ) {
						return loading;
					};
					
					return ctrl;
					
				}],
				controllerAs: 'objectRelationList',
				link: function ( scope, element, attrs, controllers ) {
					controllers[0].link(controllers.slice(1));
				}
				
			};
			
		}]);
	
})();
