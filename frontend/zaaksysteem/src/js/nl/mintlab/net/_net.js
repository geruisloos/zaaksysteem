/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.net', [ 'Zaaksysteem.events', 'Zaaksysteem.locale' ] );
	
})();
