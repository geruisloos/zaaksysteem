/*global angular,fetch,$*/
(function ( ) {
	
	angular.module('Zaaksysteem.docs')
		.controller('nl.mintlab.docs.DocumentIntakeController', [ '$scope', '$injector', 'smartHttp', function ( $scope, $injector, smartHttp ) {
			
			var File = window.zsFetch('nl.mintlab.docs.File'),
				Folder = window.zsFetch('nl.mintlab.docs.Folder'),
				contextualActionService = $injector.get('contextualActionService');
				
			$scope.visibilities = [];
			$scope.visibility = 'all';
			
			$scope.readOnly = false;
			
			$scope.loading = false;
			
			$scope.initialize = function ( ) {
				
				$scope.docGlobal = new Folder( { name: 'global' } );
				$scope.loading = true;
					
				smartHttp.connect({
					url: '/zaak/intake/get_visibility_for_user'
				})
					.success(function ( response ) {
						$scope.visibilities = response.result;
						$scope.visibility = $scope.visibilities[0] ? $scope.visibilities[0].value : null;
					})
					.error(function ( ) {
						$scope.loading = false;
					});
			};
			
			$scope.reloadData = function ( ) {
				
				$scope.loading = true;
				
				if(!$scope.visibility) {
					// $scope.docGlobal = new Folder( { name: 'global' } );
					return;
				}
				
				smartHttp.connect({
					url: 'file/search_queue',
					method: 'GET',
					params: {
						visibility: $scope.visibility
					}
				})
					.success(function ( data ) {
						var docs,
							file,
							i,
							l;
							
						docs = data.result || [];
						
						$scope.docGlobal.empty();
						
						
						for(i = 0, l = docs.length; i < l; ++i) {
							file = new File();
							file.updateWith(docs[i]);
							$scope.docGlobal.add(file);
						}
						
					})
					.error(function ( ) {
						
					})
					.then(function ( ) {
						$scope.loading = false;
					}, function ( ) {
						$scope.loading = false;
					});
			};
			
			$scope.registerCase = function ( file ) {

				var action = contextualActionService.findActionByName('zaak');

				contextualActionService.openAction(
					action,
					{
						file: _.pick(file, 'id', 'filestore_id')
					}
				);
			};
			
			$scope.$watch('visibility', function ( ) {
				$scope.reloadData();
			});
			
			
		}]);
})();
