/*global angular, Blob, _, gui, ops, odf*/
(function ( ) {
	'use strict';
	angular.module('Zaaksysteem.docs')
		.factory('WebODFCanvas', [ '$q', 'fileUploader', 'smartHttp', 'systemMessageService', '$document', function ( $q, fileUploader, smartHttp, systemMessageService, $document ) {

			function createCanvas ( element ) {
				var canvas = {},
					memberId = 'localuser',
					session,
					odfCanvas,
					sessionController,
					annotationsEnabled,
					sessionView,
					formattingController,
					cursor,
					selectionViewManager,
					caretManager,
					webOdfCanvas = angular.element('<div></div>')[0];

				element.append(webOdfCanvas);
				canvas.onTextChange = [];
				canvas.onParagraphChange = [];

				function setListeners () {
					formattingController.subscribe(gui.DirectFormattingController.textStylingChanged, function(params) {
						_.invoke(canvas.onTextChange, 'call', canvas, params);
					});

					formattingController.subscribe(gui.DirectFormattingController.paragraphStylingChanged, function(params) {
						_.invoke(canvas.onParagraphChange, 'call', canvas, params);
					});
				}

				function initialize ( filePath ) {
					var deferred = $q.defer();

					var lib = $document[0].createElement('script');
					lib.async = false;
					lib.src = '/webodf/webodf.js';
					lib.type = 'text/javascript';
					lib.onload = function () {
						odfCanvas = new odf.OdfCanvas(webOdfCanvas);
						odfCanvas.addListener('statereadychange', function ( ) {
							startSession();
							deferred.resolve();
							setListeners();
							canvas.toggleAnnotations(); // by default annotations are off, this turns them on.
						});
						odfCanvas.load(filePath);
					};
					$document[0].head.appendChild(lib);

					return deferred.promise;
				}

				function startSession ( ) {

					session = new ops.Session(odfCanvas);
					var odfDocument = session.getOdtDocument();
					cursor = new gui.ShadowCursor(odfDocument);
					sessionController = new gui.SessionController(session, memberId, cursor, {
						annotationsEnabled: true,
						directTextStylingEnabled: true,
						directParagraphStylingEnabled: true
					});
					formattingController = sessionController.getDirectFormattingController();

					var viewOptions = {
						editInfoMarkersInitiallyVisible: false,
						caretAvatarsInitiallyVisible: false,
						caretBlinksOnRangeSelect: true
					};
					caretManager = new gui.CaretManager(sessionController, odfCanvas.getViewport());
					selectionViewManager = new gui.SelectionViewManager(gui.SvgSelectionView);
					var sessionConstraints = sessionController.getSessionConstraints();
					sessionView = new gui.SessionView(viewOptions, memberId, session, sessionConstraints, caretManager, selectionViewManager);
					selectionViewManager.registerCursor(cursor, true);
					sessionController.setUndoManager(new gui.TrivialUndoManager());

					var op = new ops.OpAddMember();
					op.init({
						memberid: memberId,
						setProperties: {
							fullName: '',
							color: 'black',
							imageUrl: ''
						}
					});
					session.enqueue([op]);

					sessionController.insertLocalCursor();
					sessionController.startEditing();
				}

				canvas.getFormattingController = function ( ) {
					return formattingController;
				};

				canvas.load = function (filePath) {
					return initialize(filePath);
				};
				canvas.getBlob = function ( ) {
					var deferred = $q.defer();
					var odfContainer = odfCanvas.odfContainer();
					odfContainer.createByteArray(function(a) {
						var blob = new Blob([a], {type: 'application/vnd.oasis.opendocument.text'});
						deferred.resolve(blob);
					}, function(a) {
						systemMessageService.emit('Er gaat iets fout bij het opslaan van uw document. '+ a);
					});
					return deferred.promise;
				};

				canvas.destroy = function ( ) {
					sessionController.endEditing();
					session.close (function () {
						sessionController.removeLocalCursor();
					});

					[
						sessionView,
						selectionViewManager,
						caretManager,
						odfCanvas,
						sessionController
					].forEach(function ( component ) {
						component.destroy(angular.noop);
					});

					sessionController = null;
					session = null;
				};

				canvas.undo = function ( ) {
					sessionController.undo();
				};
				canvas.redo = function ( ) {
					sessionController.redo();
				};
				canvas.bold = function ( ) {
					formattingController.toggleBold();
				};
				canvas.italic = function ( ) {
					formattingController.toggleItalic();
				};
				canvas.underline = function ( ) {
					formattingController.toggleUnderline();
				};
				canvas.alignLeft = function ( ) {
					formattingController.alignParagraphLeft();
				};
				canvas.alignCenter = function ( ) {
					formattingController.alignParagraphCenter();
				};
				canvas.alignRight = function ( ) {
					formattingController.alignParagraphRight();
				};
				canvas.justify = function ( ) {
					formattingController.alignParagraphJustified();
				};
				canvas.outdent = function ( ) {
					formattingController.outdent();
				};
				canvas.indent = function ( ) {
					formattingController.indent();
				};
				canvas.bulletlist = function ( ) {
					var listController = sessionController.getListController();
					listController.addList('bullet');
				};
				canvas.numberedlist = function ( ) {
					var listController = sessionController.getListController();
					listController.addList('number');
				};
				canvas.setFontSize = function (value) {
					formattingController.setFontSize(value);
				};
				canvas.setHyperlink = function (url) {
					var hyperlinkController = sessionController.getHyperlinkController();
					hyperlinkController.addHyperlink(url);
				};
				canvas.insertImage = function (fileType, image, imageWidth, imageHeight) {
					sessionController.getTextController().removeCurrentSelection();
					sessionController.getImageController().insertImage(fileType, image, imageWidth, imageHeight);
					sessionController.getEventManager().focus();
				};

				canvas.toggleAnnotations = function ( ) {
					annotationsEnabled = !annotationsEnabled;
					odfCanvas.enableAnnotations(annotationsEnabled, true);
				};

				return canvas;
			}

			return {
				createCanvas: createCanvas
			};

		}]);
}());
