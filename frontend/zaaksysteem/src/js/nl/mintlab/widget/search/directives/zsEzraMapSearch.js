/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.widget.search')
		.directive('zsEzraMapSearch', [ function ( ) {
			
			return {
				require: 'zsEzraMap',
				link: function ( scope, element, attrs, zsEzraMap ) {
					
					function setLocations ( locations ) {
						var markers = _.map(locations, function ( location ) {
							var marker = location.marker,
								gps;
								
							gps = zsEzraMap.fromLatLngToGps(location.marker.latitude, location.marker.longitude);
							marker.latitude = gps.y;
							marker.longitude = gps.x;
							return marker;
						});
						zsEzraMap.setMarkers(markers);
					}
					
					scope.$watch('getLocations()', function ( nwVal/*, oldVal, scope*/ ) {
						setLocations(nwVal);
					}, true);
					
				}
			};
		}]);
	
})();