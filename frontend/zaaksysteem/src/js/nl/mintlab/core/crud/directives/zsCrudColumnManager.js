/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsCrudColumnManager', [ '$parse', function ( $parse ) {
			
			return {
				require: 'zsCrudTemplateParser',
				link: function ( scope, element, attrs, zsCrudTemplateParser ) {
					
					zsCrudTemplateParser.enableColumnManagement();
					
					scope.getColumns = function ( query ) {
						return $parse(attrs.zsCrudColumnManager)(scope, {
							$query: query
						});
					};
					
				}
			};
			
		}]);
	
})();