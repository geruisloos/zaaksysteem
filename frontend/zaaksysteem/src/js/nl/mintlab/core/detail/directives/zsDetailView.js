/*global angular,fetch,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.core.detail')
		.directive('zsDetailView', [ '$interpolate', '$window', '$parse', 'smartHttp', 'translationService', function ( $interpolate, $window, $parse, smartHttp, translationService ) {
			
			var parseUrlParams = window.zsFetch('nl.mintlab.utils.parseUrlParams');
			
			return {
				scope: true,
				templateUrl: '/html/core/detail/detail-view.html',
				compile: function ( ) {
					
					return function link ( scope, element, attrs ) {
						
						function setConfig ( config ) {
							clearScope();
							for(var key in config) {
								scope[key] = config[key];
							}
							
							reloadData();
						}
						
						function reloadConfig ( ) {
							if(attrs.zsDetailView) {
								
								var config,
									url = attrs.zsDetailView,
									params = parseUrlParams(url);
									
								params.zapi_detail = 1;
								
								try {
									config = JSON.parse(attrs.zsDetailView);
									setConfig(config);
								} catch ( error ) {
									smartHttp.connect({
										method: 'GET',
										url: attrs.zsDetailView,
										params: params
									})
										.success(function ( data ) {
											var config = data.result[0];
											setConfig(config);
										})
										.error(function ( /*data*/ ) {
											emitError();
											setItem(null);
										});
								}
								
								
							} else {
								clearScope();
								setItem(null);
							}
						}
						
						function reloadData ( ) {
							var url = scope.url ? $interpolate(scope.url)(scope) : attrs.zsDetailView;
							
							if(url) {
								
								smartHttp.connect({
									method: 'GET',
									url: url
								})
									.success(function ( data) {
										setItem(data.result[0]);
									})
									.error(function ( /*data*/ ) {
										emitError();
										setItem(null);
									});
							} else {
								setItem(null);
							}
						}
						
						function clearScope ( ) {
							scope.actions = [];
							scope.fieldsets = [];
							scope.name = '';
							scope.title = '';
							scope.url = '';
						}
						
						function setItem ( item ) {
							scope.item = item;
							scope.$emit('detail.item.change', item);
						}
						
						function emitError ( ) {
							scope.$emit('systemMessage', {
								type: 'error',
								content: translationService.get('Er ging iets fout bij het ophalen van het item')
							});
						}
						
						function getHttpConfig ( action, method ) {
							var url = $interpolate(action.data.url)(scope),
								params = _.clone(action.data.params),
								config;
							
							if(method === undefined) {
								method = 'POST';
							}
							
							for(var key in params) {
								if(params[key] && typeof params[key] !== 'number' && typeof params[key] !== 'boolean') {
									params[key] = $interpolate(params[key])(scope);
								}
							}
							
							config = {
								url: url,
								method: method,
								data: params
							};
							
							return config;
						}
						
						scope.del = function ( action ) {
							var config = getHttpConfig(action);
							
							smartHttp.connect(config)
								.success(function ( /*response*/ ) {
									var to = action.data.redirect;
									
									scope.$emit('systemMessage', {
										type: 'info',
										content: 'Object succesvol verwijderd'
									});
									
									if(to) {
										$window.location.href = to;
									}
									
								})
								.error(function ( /*response*/ ) {
									
									scope.$emit('systemMessage', {
										type: 'error',
										content: 'Object kan niet worden verwijderd'
									});
									
								});
							
						};
						
						scope.update = function ( action ) {
							var config = getHttpConfig(action);
							
							smartHttp.connect(config)
								.success(function ( response ) {
									
									var itemData = response.result[0];
									for(var key in itemData) {
										scope.item[key] = itemData[key];
									}
									
								})
								.error(function ( /*response*/ ) {
									scope.$emit('systemMessage', {
										type: 'error',
										content: 'Actie kon niet worden uitgevoerd'
									});
								});
						};
						
						scope.getTitle = function ( ) {
							return scope.title ? $interpolate(scope.title)(scope) : '';
						};
						
						scope.isVisible = function ( item ) {
							var isVisible = (item.when === null || item.when === undefined || item.when === true) || !!($parse(item.when)(scope));
							return isVisible;
						};
						
						scope.isActionDisabled = function ( action ) {
							return false;
						};
						
						scope.hasVisibleActions = function ( ) {
							var i,
								l,
								action,
								actions = scope.actions || [];
								
							for(i = 0, l = actions.length; i < l; ++i) {
								action = actions[i];
								if(scope.isVisible(action)) {
									return true;
								}
							}
							
							return false;
						};
						
						scope.getActionTemplate = function ( action ) {
							var template;
							switch(action.type) {
								default:
								template = 'default';
								break;
								// case 'delete':
								// template = action.type;
								// break;
							}
							
							return template;
						};
						
						attrs.$observe('zsDetailView', function ( ) {
							reloadConfig();
						});
						
					};
					
				}
			};
			
		}]);
	
})();
