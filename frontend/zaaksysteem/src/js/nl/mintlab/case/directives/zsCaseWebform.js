/*global angular,_,fetch,loadWebform,$*/
(function ( ) {
	
	angular.module('Zaaksysteem.case')
		.directive('zsCaseWebform', [ '$http', '$q', '$sce', '$timeout', 'objectService', 'systemMessageService', function ( $http, $q, $sce, $timeout, objectService, systemMessageService ) {
			
			var safeApply = window.zsFetch('nl.mintlab.utils.safeApply');
			
			return {
				require: [ 'zsCaseWebform', 'form', '^zsCaseWebformRuleManager', '?^zsCaseView', '?^zsCasePhaseView' ],
				controller: [ '$scope', '$element', '$attrs', function ( $scope, $element, $attrs ) {
					
					var ctrl = this,
						form,
						rules = [],
						loadedPromise = $q.defer(),
						zsCaseWebformRuleManager,
						zsCaseView,
						zsCasePhaseView,
						controls = [],
						controlsByName = {},
						loaded = false,
						invalidateForm,
						zsCaseWebformInner;

						
					function getActiveControls ( ) {
						return _.filter(controls, function ( child ) {
							return ctrl.isFieldVisible(child.getAttributeName());
						});
					}

					function getFormValues ( name ) {
						var values = [];
						
						_.each(getActiveControls(), function ( child ) {
							if(name === undefined || child.getAttributeName() === name) {
								values.push({
									name: child.getAttributeName(),
									value: child.getValue(),
									formValue: child.getFormValue()
								});
							}
						});
						
						values = _.filter(values, function ( child ) {
							return child.formValue !== undefined;
						});

						return values;
					}
					
					function loadRules ( values ) {
									
						var params = _.extend({ zapi_no_pager: 1 }, values);
						
						return $http({
							method: 'GET',
							url: '/api/rules',
							params: params
						})
							.success(function ( response ) {
								rules = response.result;
							})
							.error(function ( ) {
								systemMessageService.emitLoadError('de regels');
							});
					}
					
					function parseRules ( ) {
						
						var params;
						
						rules = $scope.$eval($attrs.zsCaseWebformRules) || [];
						
						params = $scope.$eval($attrs.zsCaseWebformParams) || {};
						
						_.each(params, function ( value, key ) {
							zsCaseWebformRuleManager.setValue(key, value);
						});
					}
					
					function setFormValues ( ) {
						_.each(controls, function ( child ) {
							child.setValue(angular.copy(zsCaseWebformRuleManager.getValue(child.getAttributeName())));
						});
					}
					
					function getApplicationPauseAction ( ) {
						var rule = zsCaseWebformRuleManager ? zsCaseWebformRuleManager.getApplicationPauseRule(ctrl.getPhaseId()) : null,	
							action;
							
						if(rule) {
							action = rule.valid ? rule.then[0] : rule['else'][0];
						}
						
						return action;
					}
					
					function getIndexOfPauseAttribute ( ) {
						var rule = zsCaseWebformRuleManager ? zsCaseWebformRuleManager.getApplicationPauseRule(ctrl.getPhaseId()) : null,
							index,
							values = zsCaseWebformRuleManager ? zsCaseWebformRuleManager.getValues() : null;
							
						if(!rule) {
							index = -1;
						} else {
							// find the index of the last element (in order of visibility) which has triggered the pause action
							index = Math.max.apply(null, (_.map(rule.conditions.conditions, function ( condition ) {
								var matches = zsCaseWebformRuleManager.matchCondition(condition, values),
									i = -1;
									
								if(matches) {
									i = _.findIndex(controls, function ( child ) {
										return child.getAttributeName() === condition.attribute_name;
									});
								}
								return i;
							})));
							
						}
						
						return index; 
					}
					
					function onCaseUpdate ( ) {
						setFormValues();
					}
					
					ctrl.link = function ( controllers ) {
						
						var promises,
							isImmediate = $scope.$eval($attrs.zsCaseWebformImmediate);
						
						form = controllers[0];
						
						zsCaseWebformRuleManager = controllers[1];
						zsCaseView = controllers[2];
						zsCasePhaseView = controllers[3];
						
						parseRules();
						
						zsCaseWebformRuleManager.addControl(ctrl);
						$scope.$on('$destroy', function ( ) {
							zsCaseWebformRuleManager.removeControl(ctrl);
						});
						
						promises = [ ];
						
						if(zsCaseView) {
							promises.unshift(zsCaseView.loadData());
							zsCaseView.updateListeners.push(onCaseUpdate);
							$scope.$on('$destroy', function ( ) {
								_.pull(zsCaseView.updateListeners, onCaseUpdate);
							});
						}
						
						promises = promises.concat();
						promises.push(loadedPromise.promise);
						
						$q.all(promises).then(function ( ) {
							loaded = true;
							
							if(isImmediate) {
								ctrl.invalidate();
							} else {
								zsCaseWebformRuleManager.invalidateRules();
								setFormValues();
							}
							
						});
						
						if(isImmediate) {
							loadedPromise.resolve();
						}

					};
					
					ctrl.invalidate = function ( name ) {

						safeApply($scope, function ( ) {
							
							var values = getFormValues(name);
							
							_.each(values, function ( obj ) {
								var val = angular.copy(obj.value);

								if(!objectService.isEqualValue(val, zsCaseWebformRuleManager.getValue(obj.name))) {
									zsCaseWebformRuleManager.setValue(obj.name, val, angular.copy(obj.formValue));
								}
							});
							
							zsCaseWebformRuleManager.invalidateRules();
							
							ctrl.onInvalidate.forEach(function ( handler ) {
								handler(name);
							});
							
						});
					};
					
					ctrl.isLoaded = function ( ) {
						return loaded;
					};
					
					ctrl.isFieldVisible = function ( attributeName ) {
						var visible = loaded && zsCaseWebformRuleManager.isFieldVisible(attributeName),
							index;
							
						if(visible && ctrl.isApplicationPaused()) {
							index = getIndexOfPauseAttribute();
							visible = index >= _.findIndex(controls, function ( child ) {
								return child.getAttributeName() === attributeName;
							});
						}
						return visible;
					};
					
					ctrl.isFixedValue = function ( attrName ) {
						return zsCaseWebformRuleManager.isFixedValue(attrName);
					};
					
					ctrl.getFixedValue = function ( attrName ) {
						return zsCaseWebformRuleManager.getFixedValue(attrName);
					};
					
					ctrl.setLoaded = function ( ) {
						$scope.$apply(function ( ) {
							loadedPromise.resolve();
						});
					};
					
					ctrl.getRules = function ( ) {
						return rules;
					};
					
					ctrl.invalidateForm = function ( ) {
						invalidateForm();
					};
					
					ctrl.getPhaseId = function ( ) {
						return zsCasePhaseView ? zsCasePhaseView.getPhaseId() : null;
					};
					
					ctrl.addControl = function ( child ) {
						controls.push(child);
						controlsByName[child.getAttributeName()] = child;
						if(loaded) {
							child.setValue(zsCaseWebformRuleManager.getValue(child.getAttributeName()));
						}
					};
					
					ctrl.removeControl = function ( child ) {
						_.pull(controls, child);
						delete controlsByName[child.getAttributeName()];
					};
					
					ctrl.getControls = function ( ) {
						return controls;
					};
					
					ctrl.setWebformInner = function ( webformInner ) {
						zsCaseWebformInner = webformInner;
					};
					
					ctrl.getAttributeNames = function ( ) {
						return _.map(getActiveControls(), function ( child ) {
							return child.getAttributeName();
						});
					};
					
					ctrl.hasVisibleFields = function ( ) {
						return getActiveControls().length;
					};
					
					ctrl.isApplicationPaused = function ( ) {
						return !!getApplicationPauseAction();
					};
					
					ctrl.isApplicationPausedWithoutResumal = function ( ) {
						var action = getApplicationPauseAction(),
							without = false;
							
						if(action && !action.data.start_case) {
							without = true;
						}
						
						return without;
					};
					
					ctrl.getApplicationPausedMessage = function ( ) {
						var action = getApplicationPauseAction(),
							msg = '';
						
						if(action) {
							msg = $sce.trustAsHtml(action.data.message);
						}
						
						return msg;
					};
					
					ctrl.getApplicationPausedData = function ( ) {
						var action = getApplicationPauseAction(),
							data;
							
						if(action) {
							data = action.data;
						}
						
						return data;
					};
					
					ctrl.reloadRules = function ( attributeName, value ) {
						var params = _.extend({}, $scope.$eval($attrs.zsCaseWebformParams));
						
						params[attributeName] = value;
						
						loadRules(params)
							.then(function ( ) {
								zsCaseWebformRuleManager.invalidateRules();
								setFormValues();
							});
					};
					
					ctrl.reloadHtml = function ( params ) {
						var form;
						
						if(!params) {
							params = {};
						}
						
						form = $element[0].tagName.toLowerCase() !== 'form' ? $($element[0]).closest('form') : $($element[0]);
						params.form = form;
						
						zsCaseWebformInner.destroy();
						
						loadWebform(params);
						
					};
					
					ctrl.onInvalidate = [];
					
					invalidateForm = _.throttle(function ( ) {
						if($scope.$$phase || $scope.$root.$$phase) {
							setFormValues();
						} else {
							$scope.$apply(setFormValues);
						}
					}, { leading: false, trailing: true });


					// Since value might change in map component that still uses jQuery, we must hook in to change in value to retrigger rules.
					var mapEl = angular.element(document.getElementsByClassName('ezra_map-address'));

					$scope.$watch(function() {

						return mapEl.attr('value'); 

					}, function(newValue){

						ctrl.invalidate();

					});
					
					return ctrl;
				}],
				controllerAs: 'caseWebform',
				link: function ( scope, element, attrs, controllers ) {
					
					var webformCtrl = controllers[0];
					
					webformCtrl.link(controllers.slice(1));
					
				}
			};
			
		}]);
	
})();
