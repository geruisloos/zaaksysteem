(function ( ) {
	
	angular.module('Zaaksysteem.admin.objecttype')
		.directive('zsObjectTypeCasetypeFormField', [ function ( ) {
			
			return {
				controller: [ function ( ) {
					
					var ctrl = this;

					ctrl.transform = function ( $object ) {

						return {
							// leave .values here for backward compatibility
							'related_object_title': $object.values['casetype.name'] || $object.label,
							'related_object': null,
							'related_object_id': $object.id,
							'related_object_type': 'casetype'
						};
					};

					return ctrl;
					
				}],
				controllerAs: 'objectTypeCasetypeFormField'
				
			};
			
		}]);
	
})();
