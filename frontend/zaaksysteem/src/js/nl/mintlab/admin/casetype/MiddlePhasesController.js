/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.admin.casetype')
		.controller('nl.mintlab.admin.casetype.MiddlePhasesController', [ '$scope', function ( $scope ) {
			
			var safeApply = window.zsFetch('nl.mintlab.utils.safeApply');
			
			$scope.$on('confirm.cancel', function ( ) {
				safeApply($scope.$parent, function ( ) {
					$scope.relation.middle_phases = false;
				});
			});
			
		}]);
	
})();
