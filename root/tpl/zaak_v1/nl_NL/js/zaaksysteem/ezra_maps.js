/*global _*/
function load_ezra_map( options ) {
    if ($('.ezra_map').length) {
        $('.ezra_map').ezra_map(_.assign({
            firstTime_addLayers   : true,
            layer_type  : 'pdoc',
            theme       : '/tpl/zaak_v1/nl_NL/js/OpenLayers-2.12/theme/dark/style.css',
            img_path    : '/tpl/zaak_v1/nl_NL/js/OpenLayers-2.12/theme/dark/img/',
            marker_icon : '/images/marker.png',
            geocode_url : '/plugins/maps',
            viewport    : {
                smallfixed:  {
                    css: {
                        width: '520px',
                        height: '350px'
                    }
                },
                smallauto:  {
                    css: {
                        width: 'auto',
                        height: '350px'
                    }
                },
                medium:  {
                    css: {
                        width: '1024px',
                        height: '768px'
                    }
                },
                large:  {
                    css: {
                        width: '900px',
                        height: '400px'
                    }
                }
            },
            proxy       : '/maps-tiles',
            update      : function (element) {
                var input_element = element.find('.ezra_map-kenmerkfield');
                updateField(input_element);
            }
        }, options));
    }
}

(function( $ ){
    var map;
    var isIntern = false;
    var isLoadingMapLayers = false;
    var mapCallsStore = [];
    var methods = {
        required: function(options) {
            var required = [
                'layer_type',
                'theme',
                'viewport'
            ];

            if (!options && required && required.length) { 
                $(this).ezra_map(
                    'log',
                    'ezra_map error: No options given'
                );
                return false;
            }

            for(var i = 0; i < required.length; i++) {
                var param = required[i];

                if(!options[param]) {
                    $(this).ezra_map(
                        'log',
                        'ezra_map error: Missing required parameter ' + param
                    );
                    return false;
                }
            }

            return true;
        },
        init : function( options ) {

            return this.each(function(){
                var $this   = $(this),
                    data    = $this.data('ezra_map');

                // Check required parameters
                if (!$this.ezra_map('required', options)) {
                    return false;
                }

                if ($this.hasClass('ezra_map-initialized')) {
                    return true;
                }

                // If the plugin hasn't been initialized yet
                if ( ! data ) {
                    /*
                    Do more setup stuff here
                    */
                    var layer   = $this.ezra_map('_load_base_layer', options);
                    var map     = $this.ezra_map('_load_map', options, layer);

                    $(this).data('ezra_map', {
                        target  : $this,
                        map     : map,
                        options : options
                    });

                    // Check for option center or class center
                    if ($this.find('.ezra_map-center').length) {
                        options.center = $this.find('.ezra_map-center').val();
                    }

                    $this.ezra_map('_load_autocomplete');
                    $this.ezra_map('_load_markers');

                    if(!$this.hasClass('ezra_map-readonly')) {
                        var vectorLayer = map.getLayer('Markers');

                        vectorLayer.events.includeXY = false;
                        vectorLayer.events.fallThrough = true;

                        vectorLayer.events.register('touchstart', this, function() {
                            var touch = event.changedTouches[0];
                            $this._start_clientX = event.clientX; 
                            $this._start_clientY = event.clientY; 
                            $this.touchStartTime = new Date().getTime();                      
                            return false;
                        });
    
                        vectorLayer.events.register('touchend', this, function(event) {
                            var touch = event.changedTouches[0];
                            var clientX = touch.clientX;
                            var clientY = touch.clientY;
    
                            //if finger moves more than 10px flag to cancel
                            // alternative: use time diff
                            //http://trac.osgeo.org/openlayers/attachment/ticket/3424/click_handler_patch.txt
                            //var touchLength = new Date().getTime() - $this.touchStartTime;
    
                            if (Math.abs(clientX - $this._start_clientX) > 10 || Math.abs(clientY - $this._start_clientY) > 10) {
                            } else {
                                setTimeout( function() { 
                                    $this.ezra_map('_set_click_handler', event)
                                }, 0);
                            }
                            return false; 
                        });
                    }

                    $this.addClass('ezra_map-initialized');
                    $this.ezra_map('_createFullscreenButton');
                    $this.ezra_map('_createMeasureStuff');

                    $this.trigger('ezra_map_initialized');
                }
            });
        },
        _createFullscreenButton: function () {
            var fullScreenButton = $('<div class="openlayers-icon-fullscreen" title="Volledig scherm"></div>'),
                $this = $(this),
                data    = $this.data('ezra_map'),
                map     = data.map;

            $('body').keydown(function(e) {
                if($this.hasClass('map-fullscreen') && e.keyCode === 27) {
                    fullScreenButton.click();
                }
            });
            
            fullScreenButton.click(function() {
                $this.toggleClass('map-fullscreen');
                $(this).toggleClass('openlayers-icon-fullscreen-active');
                $('body').toggleClass('modal-open');
                map.updateSize();
                return false;
            });
            $this.find('.olControlLayerSwitcher').append(fullScreenButton);
        },
        _createMeasureStuff: function () {
            var $this = $(this),
                data    = $this.data('ezra_map'),
                map     = data.map,
                measureControls,
                sketchSymbolizers,
                measureDistanceButton,
                measureAreaButton,
                searchContainer,
                $compile,
                renderer,
                style,
                scope,
                hasKeyListener = false,
                styleMap,
                control;

            // style the sketch fancy
            sketchSymbolizers = {
                'Point': {
                    pointRadius: 4,
                    graphicName: 'square',
                    fillColor: 'white',
                    fillOpacity: 1,
                    strokeWidth: 1,
                    strokeOpacity: 1,
                    strokeColor: '#333333'
                },
                'Line': {
                    strokeWidth: 3,
                    strokeOpacity: 1,
                    strokeColor: '#666666',
                    strokeDashstyle: 'dash'
                },
                'Polygon': {
                    strokeWidth: 2,
                    strokeOpacity: 1,
                    strokeColor: '#666666',
                    fillColor: 'white',
                    fillOpacity: 0.3
                }
            };

            style = new OpenLayers.Style();
            style.addRules([
                new OpenLayers.Rule({symbolizer: sketchSymbolizers})
            ]);
            styleMap = new OpenLayers.StyleMap({'default': style});
            
            renderer = OpenLayers.Util.getParameters(window.location.href).renderer;
            renderer = (renderer) ? [renderer] : OpenLayers.Layer.Vector.prototype.renderers;

            measureControls = {
                line: new OpenLayers.Control.Measure(
                    OpenLayers.Handler.Path, {
                        persist: true,
                        handlerOptions: {
                            layerOptions: {
                                renderers: renderer,
                                styleMap: styleMap
                            }
                        }
                    }
                ),
                polygon: new OpenLayers.Control.Measure(
                    OpenLayers.Handler.Polygon, {
                        persist: true,
                        handlerOptions: {
                            layerOptions: {
                                renderers: renderer,
                                styleMap: styleMap
                            }
                        }
                    }
                )
            };
            $this.find('.olControlLayerSwitcher #OpenLayers_Control_MaximizeDiv').attr('title', 'Toon lagen');

            measureOutputWrapper = $('<div class="map-measure-output-wrapper"></div>'),
            $this.find('.olMapViewport').append(measureOutputWrapper);
            measureOutputCloseButton = $('<button class="map-measure-output-close-button"><i class="mdi mdi-close"></i></button>'),
            measureOutputWrapper.append(measureOutputCloseButton);
            measureOutput = $('<div class="map-measure-output"></div>'),
            measureOutputWrapper.append(measureOutput);
            measureOutputWrapper.hide();

            function setDefaultOutputValue ( type ) {
                if(type === 'line') {
                    measureOutput[0].innerHTML = '<span>Klik op de kaart om de afstand te meten</span>';
                } else {
                    measureOutput[0].innerHTML = '<span>Klik op de kaart om de oppervlakte te meten</span>';
                }
            }

            function startMeasure ( type ) {
                stopMeasure();
                setDefaultOutputValue(type);
                
                if(type === 'line') {
                    measureDistanceButton.addClass('active');
                } else if(type === 'polygon') {
                    measureAreaButton.addClass('active');
                }
                measureControls[type].activate();
                startKeyListener();
            }

            function stopMeasure ( ) {
                stopKeyListener();
                measureOutputWrapper.hide();
                measureDistanceButton.removeClass('active');
                measureAreaButton.removeClass('active');
                for(var key in measureControls) {
                    control = measureControls[key];
                    control.deactivate();
                }
            }

            function onKeyUp ( event ) {
                var keyCode = event.keyCode;
                if(keyCode === 27) {
                    stopMeasure();
                }
            }

            function stopKeyListener( ) {
                $(document).unbind('keyup', onKeyUp);
                hasKeyListener = false;
            }

            function startKeyListener( ) {
                if(hasKeyListener) {
                    return;
                }
                measureOutputWrapper.show();
                hasKeyListener = true;
                $(document).keyup(onKeyUp);
            }

            measureOutputCloseButton.click(function() {
                stopMeasure();
                return false;
            });

            // Distance:
            measureDistanceButton = $('<div class="openlayers-icon-measuredistance" title="Meet afstand"></div>'),
            
            measureDistanceButton.click(function() {
                if($(this).hasClass('active')) {
                    stopMeasure();
                } else {
                    startMeasure('line');
                }
                return false;
            });
            $this.find('.olControlLayerSwitcher').append(measureDistanceButton);
            // Area:
            measureAreaButton = $('<div class="openlayers-icon-measurearea" title="Meet oppervlakte"></div>'),
            
            measureAreaButton.click(function() {
                if($(this).hasClass('active')) {
                    stopMeasure();
                } else {
                    startMeasure('polygon');
                }
                return false;
            });
            $this.find('.olControlLayerSwitcher').append(measureAreaButton);

            function handleMeasurements(event) {
                var geometry = event.geometry;
                var units = event.units;
                var order = event.order;
                var measure = event.measure;
                var out = '';
                if(order == 1) {
                    out += 'Totale afstand: ' + measure.toFixed(3) + ' ' + units;
                } else {
                    out += 'Oppervlakte: ' + measure.toFixed(3) + ' ' + units + '<sup>2</sup>';
                }
                measureOutput[0].innerHTML = out;
            }

            for(var key in measureControls) {
                control = measureControls[key];
                control.events.on({
                    'measure': handleMeasurements,
                    'measurepartial': handleMeasurements
                });
                control.setImmediate(true);
                map.addControl(control);
            }
        },
        _load_autocomplete: function() {
            var $this   = $(this),
                data    = $this.data('ezra_map'),
                map     = data.map,
                options = data.options,
                isWMSMap = $this.attr('map-wms-layer-id') && $this.attr('map-wms-feature-attribute-id');

            if (! jQuery().autocomplete ) {
                // No autocomplete function available, stop
                return false;
            }

            $this.find('input[type="text"].ezra_map-autocomplete').each(function() {

                var foundAddresses;

                $(this).autocomplete(
                    {
                        source: function(tag, response) {
                            $.getJSON(options.geocode_url,
                                {
                                    'term': tag['term']
                                },
                                function(data) {
                                    if (!(data.json.success == '1'))
                                    {
                                        response();
                                        return false;
                                    }

                                    var addresses = data.json.addresses,
                                        list_of_addresses = [];

                                    foundAddresses = data.json.addresses;

                                    for(var i = 0; i < addresses.length; i++) {
                                        var address = addresses[i];

                                        if (!address.identification) {
                                            continue;
                                        }

                                        list_of_addresses.push(address.identification);
                                    }

                                    response(list_of_addresses);
                                }
                            );
                        },
                        select: function(event, ui) {
                            var only_center = 0;
                            if ($this.hasClass('ezra_map-readonly')) {
                                only_center = 1;
                            }

                            $this.ezra_map('setAddress', { 
                                value       : ui.item.value, 
                                only_center : only_center
                            });

                            if (isWMSMap) {

                                var selectedAddress =
                                    _.first(
                                        foundAddresses.filter( function(address) {
                                            return address.identification === ui.item.value
                                        })
                                    ),
                                    epsg = $this.ezra_map('_get_epsg_points', selectedAddress.coordinates.lat, selectedAddress.coordinates.lng);

                                $this.ezra_map('setMarker', {
                                    longitude       : epsg.x,
                                    latitude        : epsg.y,
                                    identification  : selectedAddress.identification,
                                    popup_data      : selectedAddress,
                                    center          : 1,
                                    no_update       : true
                                });

                            }

                        },
                        open: function() {
                            $(this).autocomplete('widget').css('z-index', 2000);
                        }
                    }
                );

                $(this).autocomplete('widget').css('z-index', 1000);
            });
        },
        _set_click_handler: function(event) {
            var $this   = $(this),
                data    = $this.data('ezra_map'),
                map     = data.map,
                options = data.options;

//            var pixel = new OpenLayers.Pixel(event.layerX, event.layerY);

            var p;
            if(typeof event.hasOwnProperty != 'undefined' && event.hasOwnProperty('changedTouches')) {
                p = window.zsFetch('nl.mintlab.utils.dom.getMousePosition')(event.changedTouches[0]); 
            } else {
                p = window.zsFetch('nl.mintlab.utils.dom.getMousePosition')(event); 
            }
            
            var local = window.zsFetch('nl.mintlab.utils.dom.fromGlobalToLocal')($this.find('.map')[0], p);
            var pixel = new OpenLayers.Pixel(local.x, local.y);
            var lonlat  = map.getLonLatFromViewPortPx(pixel);
            var wgs     = $this.ezra_map('_get_wgs_latlon', lonlat.lon, lonlat.lat);

            $(this).trigger('ezra_map_click', wgs);

            if($this.hasClass('ezra_map-latlononly')) {

                var featureWmsLayerId = $this.attr('map-wms-layer-id');

                $this.ezra_map('setMarker', {
                    longitude: lonlat.lon,
                    latitude: lonlat.lat,
                    identification: wgs.lat + ',' + wgs.lon,
                    popup_data: {
                        coordinates: {
                            lat: wgs.lat,
                            lng: wgs.lon
                        },
                        address: wgs.lat + ',' + wgs.lon
                    },
                    no_popup: featureWmsLayerId ? true : false
                });

            } else {
                $.getJSON(options.geocode_url,
                    {
                        'term'              : wgs.lat +' ' + wgs.lon
                    },
                    function(data) {
                        if (data.json.success == '1')
                        {
                            var geo     = data.json.addresses[0],
                            lat         = geo.coordinates.lat,
                            lon         = geo.coordinates.lng,
                            address     = geo.address;

                            var epsg    = $this.ezra_map('_get_epsg_points', lat, lon);

                            $this.ezra_map('setMarker', {
                                longitude       : epsg['x'], 
                                latitude        : epsg['y'], 
                                identification  : geo.identification, 
                                popup_data      : geo
                            });
                        }
                    }
                );
            }

            
        },
        _load_markers: function() {
            var $this   = $(this),
                data    = $this.data('ezra_map'),
                map     = data.map,
                options = data.options;
            
            markers     = new OpenLayers.Layer.Vector(
                'Markers',
                {
                    eventListeners: {
                        'featureselected': function(evt) {

                            var feature         = evt.feature;

                            var popup           = new OpenLayers.Popup.FramedCloud(
                                "popup",
                                OpenLayers.LonLat.fromString(feature.geometry.toShortString()),
                                new OpenLayers.Size(240,150),
                                feature.attributes.htmlcontent,
                                null,
                                null
                            );

                            popup.panMapIfOutOfView = true;

                            popup.autoSize      = true;
                            popup.minSize       = new OpenLayers.Size(240,150);

                            feature.popup       = popup;

                            if(!$this.attr('map-wms-layer-id')){

                                map.addPopup(popup);
                                popup.show();

                            }
                        },
                        'featureunselected': function(evt) {

                            var feature = evt.feature;
                            map.removePopup(feature.popup);
                            feature.popup.destroy();
                            feature.popup = null;
                        }
                    }
                }
            );

            markers.id  = 'Markers';

            var selector = new OpenLayers.Control.SelectFeature(
                markers,
                {
                    autoActivate:true
                }
            );

            map.addControl(selector);
            map.addLayer(markers);

            data.selector = selector;

            // Make map clickable
            if (!$this.hasClass('ezra_map-readonly')) {
                map.events.register('click', this, methods['_set_click_handler']);
            }

            // Search for input fields
            $this.find('input.ezra_map-address').each(function() {                
                $this.ezra_map('setAddress', {
                    value : $(this).val(),
                    no_update: 1
                });
            });

            // add markers from a array that contains address:
            if(data.options.markerAddresses) {
                var no_popup = false;
                
                // if there are multiple markers hide the popup:
                if(data.options.markerAddresses.length > 1) {
                    no_popup = true;
                    $this.addClass('ezra_map-multiple');
                }

                // add the markers:
                data.options.markerAddresses.forEach(function(address) {
                    $this.ezra_map('setAddress', {
                        value: address,
                        center: 1,
                        no_update: 1,
                        no_popup: no_popup
                    });
                });
            }

            // add markers from a array that contains coordinates:
            if(data.options.markerCoordinates) {
                var no_popup = false,
                    hasActiveMarker = false;
                
                // if there are multiple markers hide the popup:
                if(data.options.markerCoordinates.length > 1) {
                    no_popup = true;
                    $this.addClass('ezra_map-multiple');
                }


                var newBound = new OpenLayers.Bounds();
                // add the markers:
                data.options.markerCoordinates.forEach(function(coordinateData) {

                    hasActiveMarker = coordinateData.activeMarker ? true : hasActiveMarker;

                    var epsg    = $this.ezra_map('_get_epsg_points', coordinateData.coordinate[0], coordinateData.coordinate[1]);
                    $this.ezra_map('setMarker', {
                        longitude: epsg.x,
                        latitude: epsg.y,
                        popup_data: coordinateData.popupData,
                        center: data.options.markerCoordinates.length > 1 && !coordinateData.activeMarker ? 0 : 1,
                        no_update: 1,
                        no_popup: coordinateData.activeMarker ? false : no_popup
                    });
                    newBound.extend(new OpenLayers.LonLat(epsg.x, epsg.y));
                });

                // when markers are really close to each other. Do nothing.
                if(!hasActiveMarker && newBound.getWidth() + newBound.getHeight() > 500) {
                    var currentZoom = map.getZoomForExtent(newBound);
                    // zoom a bit out:
                    newBound = newBound.scale(map.getZoomForExtent(newBound) * .5);
                    map.zoomToExtent(newBound, true);
                }
            }

            // Search for input fields with json content
            $this.find('input.ezra_map-json').each(function() {
                $this.ezra_map('setFromJSON', jQuery.parseJSON($(this).val()));
            });

            if (options.center) {
                var lonlat = options.center.split(' ');

                if (lonlat) {
                    var epsg    = $this.ezra_map('_get_epsg_points', lonlat[0], lonlat[1]);
                    $this.ezra_map('setCenter', epsg['x'], epsg['y']);
                }
            }
        },
        clearMarkers: function() {
            var $this   = $(this),
                data    = $this.data('ezra_map'),
                map     = data.map,
                markers = map.getLayer('Markers');

            // data.selector.unselectAll();

            markers.removeAllFeatures();
        },
        setCenter: function(lon, lat) {
            var $this   = $(this),
                data    = $this.data('ezra_map'),
                map     = data.map,
                markers = map.getLayer('Markers');

            var lonlat  = new OpenLayers.LonLat(lon, lat);
            map.setCenter(lonlat, 9);
        },
        setMarker: function(options) {
            if(
                !options.hasOwnProperty('longitude') && 
                !options.hasOwnProperty('latitude')  &&      
                !options.hasOwnProperty('popup_data')       // data fields to fill out template
            ) {
                // required fields
                return;
            }
            var $this   = $(this),
                data    = $this.data('ezra_map'),
                map     = data.map,
                markers = map.getLayer('Markers');

            var lonlat  = new OpenLayers.LonLat(options.longitude, options.latitude);

            if (options.center) {
                map.setCenter(lonlat, 9);
            }

            var point   = new OpenLayers.Geometry.Point(options.longitude, options.latitude);
            var popup_data;
            if(typeof options.popup_data === 'string') {
                popup_data = options.popup_data;
            } else {
                popup_data = $this.ezra_map('get_template', options.popup_data);
            }
            var marker  = new OpenLayers.Feature.Vector(
                    point,
                    {
                        htmlcontent: popup_data
                    },
                    {
                        externalGraphic: data.options.marker_icon,
                        graphicOpacity: 1.0,
                        graphicWith: 21,
                        graphicHeight: 34,
                        graphicYOffset: -28
                    }
            );

//            marker.style = {
//                externalGraphic: data.options.marker_icon,
//                graphicOpacity: 1.0,
//                graphicWith: 21,
//                graphicHeight: 34,
//                graphicYOffset: -5
//            };

            // Clear all markers
            if (!$this.hasClass('ezra_map-multiple')) {
                $this.ezra_map('clearMarkers');
            }

            markers.addFeatures(marker);
            if (!options.no_popup) {
                data.selector.select(marker);
            }

            // Set address to input type = hidden
            if (options.identification) {

                $this.find('input.ezra_map-address').val(options.identification);

                if (!options.no_update) {
                    data.options.update(this);
                }
            }
        },
        get_template: function(data_mapping) {
            var $this   = $(this),
                data    = $this.data('ezra_map');

            var tpl     = $this.find('.ezra_map-template').clone().show().html();

            tpl         = tpl.replace(/(%%.*?%%)/g, function(match, contents) {
                var map_key = contents.replace(/^%%/, '');
                map_key     = map_key.replace(/%%$/, '');

                if (data_mapping[map_key]) {
                    return data_mapping[map_key];
                }

                return '';
            });

            return tpl;
        },
        setAddress: function(options) {
            var address = options.value;
            var only_center = options.only_center;

            var $this   = $(this),
                data    = $this.data('ezra_map');

            var map     = data.map;

            function applyCoords ( data ) {

                if(only_center) {
                    $this.ezra_map('setCenter', data.x, data.y);
                } else {
                    $this.ezra_map('setMarker', {
                        longitude       : data.x,
                        latitude        : data.y,
                        identification  : data.identification,
                        popup_data      : data,
                        center          : 1,
                        no_update       : options.no_update
                    });
                }

            }

            if($this.hasClass('ezra_map-latlononly') && !$this.attr('map-wms-layer-id') ) {

                var coordinates,
                    espg,
                    lat,
                    lng;

                if(_.isArray(address)) {
                    address = address[0];
                }

                coordinates = address ? address.split(',') : [];

                if(coordinates.length !== 2) {
                    return;
                }

                lat = coordinates[0].trim();
                lng = coordinates[1].trim();

                espg = $this.ezra_map('_get_epsg_points', lat, lng);

                applyCoords(_.merge(espg, {
                    identification: lat + ',' + lng,
                    address: lat + ', ' + lng
                }));

            } else {

                $.getJSON('/plugins/maps', {
                        'term': address
                    },
                    function(data) {

                        if (!data || !data.json || !data.json.success ) { return; }

                        if (data.json.success == '1') {

                            var geo = data.json.addresses[0],
                                lat = geo.coordinates.lat,
                                lon = geo.coordinates.lng;

                            var espg = $this.ezra_map('_get_epsg_points', lat, lon);

                            applyCoords(_.merge({}, geo, espg));

                            if ($this.attr('map-wms-layer-id') && $this.attr('map-wms-feature-attribute-id')) {

                                $this.find('input.ezra_map-address').val(lat+','+lon);
                                $this.ezra_map('getFeature');
                            }

                        }
                    }
                );
            }
        },
        getFeature: function( ){

            // Retrieves feature based on the position of the center of the map that is currently in view.
            // Use after setting a marker that also centers the map.

            var $this = $(this),
                data = $this.data('ezra_map'),
                map = data.map,
                featureWmsLayerId = $this.attr('map-wms-layer-id'),
                featureWmsAttributeId = $this.attr('map-wms-feature-attribute-id'),
                featureLayer =
                    map.layers.filter(
                        function ( layer ) {
                            return layer.$layerId === featureWmsLayerId;
                        }
                    )[0],
                WMSControl =
                    _.find(map.controls,
                        function ( control ) {
                            return control.displayClass === 'olControlWMSGetFeatureInfo';
                        }
                    );

            if (featureLayer && WMSControl){

                WMSControl.request(new OpenLayers.Pixel(this[0].clientWidth/2,this[0].clientHeight/2));

            }

        },
        setFromJSON: function(json, only_center) {
            var $this   = $(this),
                data    = $this.data('ezra_map');

            var map     = data.map;

            lat         = json.coordinates.lat;
            lon         = json.coordinates.lng;

            var epsg    = $this.ezra_map('_get_epsg_points', lat, lon);
            $this.ezra_map('setMarker', { 
                longitude       : epsg['x'], 
                latitude        : epsg['y'], 
                identification  : null, 
                popup_data      : json, 
                center          : null, 
                no_popup        : 1,
                no_update       : 1
            });
        },
        _get_wgs_latlon: function(point_x,point_y) {
            // This calculation was based from the sourcecode of Ejo Schrama's software <schrama@geo.tudelft.nl>.
            // You can find his software on: http://www.xs4all.nl/~digirini/contents/gps.html

            // Fixed constants / coefficients
            var x0      = 155000;
            var y0      = 463000;
            var k       = 0.9999079;
            var bigr    = 6382644.571;
            var m       = 0.003773954;
            var n       = 1.000475857;
            var lambda0 = 0.094032038;
            var phi0    = 0.910296727;
            var l0      = 0.094032038;
            var b0      = 0.909684757;
            var e       = 0.081696831;
            var a       = 6377397.155;

            // Convert RD to Bessel

            // Get radius from origin.
            d_1 = point_x - x0;
            d_2 = point_y - y0;
            r   = Math.sqrt( Math.pow(d_1, 2) + Math.pow(d_2, 2) );  // Pythagoras

            // Get Math.sin/Math.cos of the angle
            sa  = (r != 0 ? d_1 / r : 0);  // the if prevents devision by zero.
            ca  = (r != 0 ? d_2 / r : 0);

            psi  = Math.atan2(r, k * 2 * bigr) * 2;   // php does (y,x), excel does (x,y)
            cpsi = Math.cos(psi);
            spsi = Math.sin(psi);

            sb = (ca * Math.cos(b0) * spsi) + (Math.sin(b0) * cpsi);
            d_1 = sb;
            
            cb = Math.sqrt(1 - Math.pow(d_1, 2));  // = Math.cos(b)
            b  = Math.acos(cb);
            
            sdl = sa * spsi / cb;  // = Math.sin(dl)
            dl  = Math.asin(sdl);         // delta-lambda

            lambda_1 = dl / n + lambda0;
            w        = Math.log(Math.tan((b / 2) + (Math.PI / 4)));
            q        = (w - m) / n;

            // Create first phi and delta-q
            phiprime = (Math.atan(Math.exp(q)) * 2) - (Math.PI / 2);
            dq_1     = (e / 2) * Math.log((e * Math.sin(phiprime) + 1) / (1 - e * Math.sin(phiprime)));
            phi_1    = (Math.atan(Math.exp(q + dq_1)) * 2) - (Math.PI / 2);

            // Create new phi with delta-q
            dq_2     = (e / 2) * Math.log((e * Math.sin(phi_1) + 1) / (1 - e * Math.sin(phi_1)));
            phi_2    = (Math.atan(Math.exp(q + dq_2)) * 2) - (Math.PI / 2);

            // and again..
            dq_3     = (e / 2) * Math.log((e * Math.sin(phi_2) + 1) / (1 - e * Math.sin(phi_2)));
            phi_3    = (Math.atan(Math.exp(q + dq_3)) * 2) - (Math.PI / 2);
            
            // and again...
            dq_4     = (e / 2) * Math.log((e * Math.sin(phi_3) + 1) / (1 - e * Math.sin(phi_3)));
            phi_4    = (Math.atan(Math.exp(q + dq_4)) * 2) - (Math.PI / 2);

            // radians to degrees
            lambda_2 = lambda_1 / Math.PI * 180;  // 
            phi_5    = phi_4    / Math.PI * 180;


            // Bessel to wgs84 (lat/lon)
            dphi   = phi_5    - 52;   // delta-phi
            dlam   = lambda_2 -  5;   // delta-lambda
            
            phicor = (-96.862 - (dphi * 11.714) - (dlam * 0.125)) * 0.00001; // correction factor?
            lamcor = ((dphi * 0.329) - 37.902 - (dlam * 14.667))  * 0.00001;
            
            phiwgs = phi_5    + phicor;
            lamwgs = lambda_2 + lamcor;


            // Return as anonymous object
            return { lat: phiwgs, lon: lamwgs };
        },
        _get_epsg_points: function(lat, lon) {
            // Fixed constants / coefficients
            x0      = 155000;
            y0      = 463000;
            k       = 0.9999079;
            bigr    = 6382644.571;
            m       = 0.003773954;
            n       = 1.000475857;
            lambda0 = 0.094032038;
            phi0    = 0.910296727;
            l0      = 0.094032038;
            b0      = 0.909684757;
            e       = 0.081696831;
            a       = 6377397.155;
            
            // wgs84 to bessel
            dphi = lat - 52;
            dlam = lon - 5;
                
            phicor = ( -96.862 - dphi * 11.714 - dlam * 0.125 ) * 0.00001;
            lamcor = ( dphi * 0.329 - 37.902 - dlam * 14.667 ) * 0.00001;

            phibes = lat - phicor;
            lambes = lon - lamcor;

            // bessel to rd
            phi         = phibes / 180 * Math.PI;
            lambda      = lambes / 180 * Math.PI;
            qprime      = Math.log( Math.tan( phi / 2 + Math.PI / 4 ));
            dq          = e / 2 * Math.log(( e * Math.sin(phi) + 1 ) / ( 1 - e * Math.sin( phi ) ) );
            q           = qprime - dq;
                
            w           = n * q + m;
            b           = Math.atan( Math.exp( w ) ) * 2 - Math.PI / 2;
            dl          = n * ( lambda - lambda0 );

            d_1         = Math.sin( ( b - b0 ) / 2 );
            d_2         = Math.sin( dl / 2 );
            
            s2psihalf   = d_1 * d_1 + d_2 * d_2 * Math.cos( b ) * Math.cos ( b0 );
            cpsihalf    = Math.sqrt( 1 - s2psihalf );
            spsihalf    = Math.sqrt( s2psihalf );
            tpsihalf    = spsihalf / cpsihalf;
            
            spsi        = spsihalf * 2 * cpsihalf;
            cpsi        = 1 - s2psihalf * 2;
            
            sa          = Math.sin( dl ) * Math.cos( b ) / spsi;
            ca          = ( Math.sin( b ) - Math.sin( b0 ) * cpsi ) / ( Math.cos( b0 ) * spsi );
            
            r           = k * 2 * bigr * tpsihalf;
            x           = r * sa + x0;
            y           = r * ca + y0;

            // Return as anonymous object.
            return { x: x, y: y };
        },
        _load_custom_marker_icon: function() {
            var $this   = $(this),
                data    = $this.data('ezra_map'),
                options = data.options;

            if (!options.marker_icon) {
                return false;
            }

            var size    = new OpenLayers.Size(21,34);
            var offset  = new OpenLayers.Pixel(-(size.w/2), -size.h);
            var icon    = new OpenLayers.Icon(options.marker_icon, size, offset);

            return icon.clone();
        },
        _load_layer: function(options, name) {
            return options.layers[name].clone();
        },
        _load_layers: function(options) {
            var layers = [];
            jQuery.each(options.layers, function (key, value) {
                layers.push(value.clone());
            });
            return layers;
        },
        _load_base_layer: function(options) {
            var $this   = $(this);

            var base_layer_name = options.base_layer;

            return $this.ezra_map('_load_layer', options, base_layer_name);
        },
        refreshViewport: function(options) {
            var $this   = $(this),
                data    = $this.data('ezra_map');

            if (!data && !options) {
                return false;
            }

            if (!options) { options = data.options; }

            var mapelem = $this.find('.ezra_map-viewport');

            $.each(options.viewport, function(key,value) {
                var viewport_conf = options.viewport[key];
                if ($this.hasClass('ezra_map-' + key)) {
                    $.each(viewport_conf.css, function(csskey, cssval) {
                        mapelem.css(csskey,cssval);
                    });
                }
            });

            mapelem.attr('id', 'ezra_map-id-' + Math.floor((Math.random() * 10000)+1));

            return mapelem;
        },
        clearMouseCache: function() {
            var $this   = $(this),
                data    = $this.data('ezra_map'),
                map     = data.map;

            markers = map.getLayer('Markers');
            markers.events.clearMouseCache();
        },
        _load_map: function(options, layer) {
            var $this   = $(this),
                data    = $this.data('ezra_map');

            var mapelem = $this.ezra_map('refreshViewport', options);

            if (options.img_path) {
                OpenLayers.ImgPath = options.img_path;
            }

            map = new OpenLayers.Map(
                mapelem.attr('id'),
                {
                    theme: options.theme,
                    xy_precision: 3,
                    zoom: 3,
                    center: '155000,463000',
                    maxExtent       : new OpenLayers.Bounds(-285401.92,22598.08,595401.92,903401.92),
                    controls: [
                        new OpenLayers.Control.Navigation(),
                        new OpenLayers.Control.ArgParser(),
                        new OpenLayers.Control.PanZoomBar(),
                        new OpenLayers.Control.LayerSwitcher({ 'ascending':false }),
                        new OpenLayers.Control.ScaleLine(),
                        new OpenLayers.Control.MousePosition(),
                        new OpenLayers.Control.KeyboardDefaults(
                            {
                                observeElement: mapelem.attr('id')
                            }
                        )
                    ],
                    eventListeners: {
                        "changelayer": function(e) {
                            if(!isIntern) { // do not save setting on pip
                                return;
                            }

                            var settings = {};

                            _.each(e.object.layers, function ( layer ) {
                                
                                var visible = layer.visibility === false ? false : true,
                                    layerId = layer.name;

                                settings[layer.name] = visible;

                                if(layer.name === 'Markers') {
                                    _.each(map.popups, function ( popup ) {
                                        if(visible) {
                                            popup.show();
                                        } else {
                                            popup.hide();
                                        }
                                    });
                                }

                            });

                            $.ajax({
                                type: "POST",
                                url: '/api/user/settings',
                                data: JSON.stringify({pdokSettings: settings }),
                                contentType : 'application/json;charset=UTF-8',
                                success: function(data) {
                                    // your settings are saved.
                                },
                                dataType: 'json'
                            });
                        }
                    }
                }
            );
            
            map.addLayers($this.ezra_map('_load_layers', options));
            map.setBaseLayer(layer);
            map.zoomToMaxExtent();

            var lonlat  = new OpenLayers.LonLat(155000,463000);
            map.setCenter(lonlat, 3);

            if ($this.attr('map-wms-layer-id')) {

                // set name of clicked feature as value of feature attribute id

                var featureWmsLayerId = $this.attr('map-wms-layer-id'),
                    featureWmsAttributeId = $this.attr('map-wms-feature-attribute-id'),
                    featureLayer,
                    featureControl;

                featureLayer =
                    map.layers.filter(
                        function ( layer ) {
                            return layer.$layerId === featureWmsLayerId;
                        }
                    )[0];

                if (featureLayer) {

                    featureControl = new OpenLayers.Control.WMSGetFeatureInfo({
                        url: featureLayer.url,
                        title: '',
                        layers: [ featureLayer ],
                        infoFormat: 'text/xml',
                        queryVisible: true
                    });

                    featureControl.events.register('getfeatureinfo', this, function ( event ) {

                        var parser = new DOMParser(), 
                            xml = parser.parseFromString(event.text, 'text/xml'),
                            path = featureLayer.$featureInfoXPath,
                            featureName,
                            field = $('[name="kenmerk_id_' + featureWmsAttributeId + '"]'),
                            fieldType = field.prop('nodeName'),
                            oldValue;

                        // Install Wicked Good XPath Parser to make sure IE11 has a functioning XPath parser
                        if (!xml.evaluate){
                            wgxpath.install();
                        }

                        try {
                            featureName = xml.evaluate(path, xml, null, XPathResult.ANY_TYPE, null).iterateNext().value;

                            oldValue = $this.find('input.ezra_map-address').val();

                            // if text field or select
                            if ( (fieldType === 'INPUT' && field.attr('type') === 'text') || fieldType === 'SELECT' ){
                                field.val(featureName);
                            }

                            // if radio field
                            if (fieldType === 'INPUT' && field.attr('type') !== 'text'){
                                field.filter('[value="'+ featureName +'"]').attr('checked', true);
                            }

                            $this.find('input.ezra_map-address').val(featureName);

                            // Reset to original value because we need that original value when completing form
                            setTimeout( function(){
                                $this.find('input.ezra_map-address').val(oldValue);
                            }, 1000);

                        } catch ( error ) {
                            console.warn('xpath' + path + ' did not return a value, resolving value as empty');

                            $('[name="kenmerk_id_' + featureWmsAttributeId + '"]').val('');
                        }

                    });

                    map.addControl(featureControl);

                    featureControl.activate();

                }

            }


            map.mapLayerIsLoading = function(isLoadingStart) {
                if(isLoadingStart) {
                    $this.find('.spinner-groot').css('visibility', 'visible');
                    mapLayerIsLoadingCounter++;
                } else {
                    mapLayerIsLoadingCounter--;
                    if(mapLayerIsLoadingCounter <= 0) {
                        mapLayerIsLoadingCounter = 0;
                        $this.find('.spinner-groot').css('visibility', 'hidden');
                    }
                }
            }
            return map;
        },
        destroy : function( ) {
            return this.each(function(){
                var $this = $(this),
                    data = $this.data('ezra_map');

                // Namespacing FTW
                $(window).unbind('.ezra_map');
                data.ezra_map.remove();
                $this.removeData('ezra_map');
            });
        },
        //reposition : function( ) { // ... },
        //show : function( ) { // ... },
        //hide : function( ) { // ... },
        //update : function( content ) { // ...}
        log: function(message) {
            if (typeof console != 'undefined' && typeof console.log == 'function') {
                console.log(message);
            }
        }
    };

    var firstTime_loadSettings = true,
        additionalLayerOptions = {
            isBaseLayer: false, 
            singleTile: true, 
            buffer: 0, 
            visibility: false, 
            featureInfoFormat: "application/vnd.ogc.gml",
            tileOptions: {
                eventListeners: {
                    'loaderror': function(evt) {
                        console.error('Error loading map layer:', evt);
                        var bodyScope = angular.element(document.body).scope();
                        if(bodyScope) {
                            bodyScope.$broadcast('systemMessage', {
                                content: mapLayerLoadErrorMessage,
                                type: 'error'
                            });
                        }
                    },
                    'loadstart': function(evt) {
                        map.mapLayerIsLoading(true);
                    },
                    'loadend': function(evt) {
                        map.mapLayerIsLoading(false);
                    }
                }
            }
        },
        mapLayerIsLoadingCounter = 0,
        mapLayerLoadErrorMessage = 'Kaartlaag kan niet worden geladen';

    function getLayersObject ( featureWmsLayerId ) {
        return {
            layers: {
                nationaalregister: new OpenLayers.Layer.TMS(
                    'Nationaal Register',
                    'https://geodata.nationaalgeoregister.nl/tms/',
                    {
                        displayInLayerSwitcher: false,
                        layername       : 'brtachtergrondkaart',
                        type            : "png8",
                        transparent     : true,
                        visibility      : true,
                        singleTile      : false,
                        alpha           : true,
                        numZoomLevels   : 14,
                        zoom            : 6,
                        projection      : 'EPSG:28992',
                        units           : 'm',
                        resolutions     : [3440.640,1720.320, 860.160, 430.080, 215.040, 107.520, 53.760, 26.880, 13.440, 6.720, 3.360, 1.680, 0.840, 0.420, 0.210, 0.105, 0.0525],
                        tileOptions: {
                            eventListeners: {
                                'loadend': function(evt) {
                                    if(firstTime_loadSettings) {
                                        firstTime_loadSettings = false;
                                        if(!isIntern) { // do not load setting on pip

                                            var mapSettings = _.defaults({}, { Markers: true, 'Nationaal Register': true });

                                            _.each(map.layers, function ( layer ) {

                                                var layerId = layer.name;

                                                layer.setVisibility(mapSettings[layerId] || layer.$layerId === featureWmsLayerId);
                                            });

                                            return;
                                        }

                                        $.ajax({
                                            type: "GET",
                                            url: '/api/user/settings',
                                            contentType : 'application/json;charset=UTF-8',
                                            success: function(data) {

                                                if (!('pdokSettings' in data.result[0])) {
                                                    data.result[0].pdokSettings = {};
                                                }

                                                var pdokSettings = _.defaults(data.result[0].pdokSettings, { Markers: true, 'Nationaal Register': true });

                                                _.each(map.layers, function ( layer ) {

                                                    var layerId = layer.name;

                                                    layer.setVisibility(pdokSettings[layerId] || layer.$layerId === featureWmsLayerId);

                                                });

                                            },
                                            dataType: 'json'
                                        });
                                    }
                                }
                            }
                        }
                    }
                )
            },
            base_layer: 'nationaalregister'
        };
    }

    $.fn.ezra_map = function( method ) {

        function proxyCall ( method, target, args ) {
            if ( methods[method] ) {
                return methods[method].apply(target, Array.prototype.slice.call( args, 1 ));
            } else if ( typeof method === 'object' || !method ) {
                return methods.init.apply(target, args );
            } else {
                $.error( 'Method ' + method + ' does not exist on jQuery.ezra_map' );
            }
        }

        if(isLoadingMapLayers) {
            mapCallsStore.push({
                method: method,
                target: this,
                arguments: arguments
            });
            return;
        }

        if(method.firstTime_addLayers) {
            if($('body').hasClass('intern')) {
                isIntern = true;
            }
            var ezra_maps_layers,
                that = this;

            delete method.firstTime_addLayers;
            firstTime_loadSettings = true;
            isLoadingMapLayers = true;

            function setLayers( data ) {

                var featureWmsLayerId = $(that).attr('map-wms-layer-id');
                
                ezra_maps_layers = getLayersObject(featureWmsLayerId);

                method.layers = ezra_maps_layers.layers;
                method.base_layer = ezra_maps_layers.base_layer;

                // add layer data to ezra_maps_layers
                data.forEach(function(layerData) {

                    if(layerData.instance.active) {
                        var layer = method.layers[layerData.instance.id] = new OpenLayers.Layer.WMS(layerData.instance.label,
                            layerData.instance.url,
                            {'layers': layerData.instance.id, 'format': 'image/png', transparent: true},
                            additionalLayerOptions
                        );

                        // so we can identify it in getLayersObject()
                        layer.$layerId = layerData.instance.id;
                        // so we can use it when user clicks on map
                        layer.$featureInfoXPath = layerData.instance.feature_info_xpath;
                    }
                });

                isLoadingMapLayers = false;
                // start it again:
                that.ezra_map(method);
                while(mapCallsStore.length > 0) {
                    var obj = mapCallsStore.pop();
                    proxyCall(obj.method, obj.target, obj.arguments);
                }
            }

            var DEFAULT_LAYERS = [
                {
                    instance: {
                        active: true,
                        id: 'gemeenten',
                        label: 'Gemeentegrenzen',
                        url: 'https://geodata.nationaalgeoregister.nl/bestuurlijkegrenzen/wms',
                    }
                },
                {
                    instance: {
                        active: true,
                        id: 'provincies',
                        label: 'Provinciegrenzen',
                        url: 'https://geodata.nationaalgeoregister.nl/bestuurlijkegrenzen/wms',
                    }
                },
                {
                    instance: {
                        active: true,
                        id: 'pand',
                        label: 'BAG-objecten',
                        url: 'https://geodata.nationaalgeoregister.nl/bag/wms',
                    }
                }
            ];

            // load layerdata:
            $.ajax({
                type: "GET",
                url: '/api/v1/map/ol_settings',
                contentType : 'application/json;charset=UTF-8',
                // on error supply default layers:
                error: function(err) {
                    console.log('Could not receive map settings. Setting default map layers.');
                    setLayers(DEFAULT_LAYERS);
                },
                success: function(data) {
                    if ('wms_layers' in data.result.instance) {
                        setLayers(data.result.instance.wms_layers);
                    } else {
                        console.log('Could receive map settings, but no wms_layers found. Setting default map layers.');
                        setLayers(DEFAULT_LAYERS);
                    }
                },
                dataType: 'json'
            });
            return;
        }

        return proxyCall(method, this, arguments);
    };
})( jQuery );


OpenLayers.Lang["nl"] = OpenLayers.Util.applyDefaults({
    'Overlays': "Lagen"
});
