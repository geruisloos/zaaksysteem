

[% USE Dumper %]
[% config = dependency_config.$dependency_type %]

[% BLOCK debug %]
    <pre>
    [% Dumper.dump(dependency_config.$dependency_type) %]
    [% Dumper.dump(dependency_item) %]
    </pre>
[% END %]
[% #PROCESS debug %]

[% BLOCK kenmerken_options %]

    [% IF dependency_item.solution.action == 'use_existing' &&
          dependency_type == 'BibliotheekKenmerken' &&
          dependency_item.solution.exists('use_remote_field_options')
    %]
        <div class="vergelijking-keuzemogelijkheden clearfix">
            <div class="vergelijking-keuzemogelijkheden-lokaal">
                <label>
                    <input type="radio" name="use_remote_field_options" value="0" [% IF !dependency_item.solution.use_remote_field_options %]checked="checked"[% END %] /> Gebruik lokaal aanwezige keuzemogelijkheden
                </label>
                <ul>
                    [% WHILE (option = local_field_options.next) %]
                        <li [% UNLESS option.active %] class="inactive_field_option" style="color: #999"[% END %]>[% option.value %]</li>
                    [% END %]
                </ul>
            </div>
            <div class="vergelijking-keuzemogelijkheden-import">
                <label>
                    <input type="radio" name="use_remote_field_options" value="1" [% IF dependency_item.solution.use_remote_field_options %]checked="checked"[% END %] /> Voeg geimporteerde keuzemogelijkheden in.
                </label>
                <ul>
                [% FOREACH option = remote_field_options %]
                    [% remote_inactive = option.remote && !option.active %]
                    <li class="[% IF remote_inactive %]remote_inactive[% END %]
                        [% IF remote_inactive && only_import_active_fieldvalues %]field_value_not_imported[% END %]"
                        [% UNLESS option.active %]style="color: #999"[% END %]>[% option.value %]</li>
                [% END %]
                </ul>
            </div>
        </div>
    [% END %]

[% END %]


<input type="hidden" name="action" value="[% dependency_item.solution.action %]" />
<input type="hidden" name="dependency_type" value="[% dependency_type %]" />
<input type="hidden" name="id" value="[% id %]" />

[% is_not_addable = (dependency_type == 'LdapRole' || dependency_type == 'LdapOu' || dependency_type == 'ObjectData') -%]
[% is_object = (dependency_type == 'ObjectData') -%]

[% IF is_not_addable || (options && options.size > 0) -%]
    <div class="[% IF dependency_item.solution.action == 'use_existing' %]import_item_active [% ELSE %] import_item_inactive[% END %] import_dependency_option import_item_inner top clearfix"
        id="use_existing">

        <div class="spinner-klein"></div>

        <div class="title">
            <div class="radio-button"></div>
            <strong class="ie7"><span class="ie7">Gebruik bestaand[% config.letter_e%] [% config.label %]</span></strong>
        </div>

        <div class="rfa">
            <div class="action">
                [% IF dependency_type == 'LdapRole' %]
                    Let op! Selecteer de rol uit de juiste groep.<br>
                [% END %]
                <label>Maak een keuze:</label>

                <select name="new_id" class="ezra_import_change_field_selection">
                [% selected_id = dependency_item.solution.id %]
                [% IF !selected_id %]
                    <option value="0">Kies ...</option>
                [% END %]
                [% FOREACH option = options %]
                    <option value="[% option.id %]"
                        [% IF option.id == selected_id %]selected="selected"[% END -%]
                        >[% IF option.name %][% option.name %][% ELSIF is_object %][% c.model('Object').retrieve('uuid', option.id).name %][% ELSE %](geen label)[% END %]</option>
                [% END %]
                </select>

                <span class="import_error_tooltip import_dependency_error">
                    <span class="tip"></span>
                    <span class="import_error_tooltip_text"></span>
                </span>

                [% PROCESS kenmerken_options %]
            </div>
            <button name="opslaan" class="button button-secondary button-small button-on-hover">
                <i class="mdi mdi-content-save"></i>
            </button>
        </div>

    </div>
[% END %]


[% cannot_add_new_item = is_not_addable || (dependency_type == 'Zaaktype' && !dependency_item.main_zaaktype) || dependency_type == 'ZaaktypeStandaardBetrokkenen' %]
[% UNLESS cannot_add_new_item %]
    <div class="import_dependency_option[% IF dependency_item.solution.action == 'add' %] import_item_active[% ELSE %] import_item_inactive[% END %] import_item_inner bottom clearfix [% IF config.has_category %] with-category[% ELSE %] no-category[% END %]" id="add">
        <div class="title">
            <div class="radio-button"></div>
            <strong class="ie7"><span class="ie7">Voeg meegeleverd[% config.letter_e %] [% config.label %] toe</span></strong>
        </div>

        <div class="rfa">
            <div class="voeg-meegeleverd-toe action">
                <label>Wijzig naam: </label>
                <input type="text" class="dependency_item_name medium" name="new_name" value="[% IF dependency_item.solution.action == 'add' %][% dependency_item.solution.name %][% ELSE %][% dependency_item.name %][% END %]" />
                <span class="import_error_tooltip import_dependency_error">
                    <span class="tip"></span>
                    <span class="import_error_tooltip_text"></span>
                </span>
            </div>

            [% IF config.has_category %]
                <div class="geef-categorie action">
                    <label>Plaats in Categorie:</label>
                    [% PROCESS widgets/general/categorie.tt
                        fieldname = 'bibliotheek_categorie_id'
                        categorie_id = dependency_item.solution.bibliotheek_categorie_id
                    %]
                    <span class="import_error_tooltip import_dependency_categorie_error">
                        <span class="tip"></span>
                        <span class="import_error_tooltip_text"></span>
                    </span>
                </div>
                <div class="action multi-categorie" style="color: #444444;font-size: 11px;font-weight: bold;padding-top: 10px;">
                    <input name="multi_cat" type="checkbox" style="margin-left: 145px;margin-right: 6px;" id="multi_cat" /><label for="multi_cat">Geselecteerde categorie voor alle nieuwe [% config.label %] gebruiken</label>
                </div>
            [% END %]

            <button name="action" class="import_dependency_name_change button button-secondary button-small button-on-hover">
                <i class="mdi mdi-content-save"></i>
            </button>
        </div>

    </div>

[% END %]

[% IF dependency_type == 'ZaaktypeStandaardBetrokkenen' %]
<div class="import_dependency_option[% IF dependency_item.solution.action == 'add' %] import_item_active[% ELSE %] import_item_inactive[% END %] import_item_inner bottom clearfix [% IF config.has_category %] with-category[% ELSE %] no-category[% END %]" id="add">
  <div class="title">
    <div class="radio-button"></div>
    <strong class="ie7"><span class="ie7">Voeg meegeleverde betrokkene rol toe</span></strong>
  </div>
  <div class="rfa">
    <div class="voeg-meegeleverd-toe action">
      [% PROCESS widgets/betrokkene/select_ztb.tt
        betrokkene_type = dependency_item.solution.betrokkene_type
        instance_id     = dependency_item.solution.id
        input_name      = 'betrokkene_id'
        betrokkene_naam = dependency_item.solution.naam
        betrokkene_id   = dependency_item.solution.betrokkene_id
        label           = "Betrokkene"
        row_classes     = 0
      %]
    </div>
    <div>
      <button name="action" class="import_dependency_name_change button button-secondary button-small button-on-hover">
          <i class="mdi mdi-content-save"></i>
      </button>
    </div>
  </div>
[% END %]

