BEGIN;

    ALTER TABLE bibliotheek_sjablonen ADD COLUMN interface_id INTEGER;
    ALTER TABLE bibliotheek_sjablonen ADD COLUMN template_uuid UUID;

    ALTER TABLE ONLY bibliotheek_sjablonen
        ADD CONSTRAINT bibliotheek_sjablonen_interface_id_fkey FOREIGN KEY (interface_id)
        REFERENCES interface(id);

    ALTER TABLE transaction ADD COLUMN uuid UUID DEFAULT uuid_generate_v4() NOT NULL;
    ALTER TABLE transaction_record ADD COLUMN uuid UUID DEFAULT uuid_generate_v4() NOT NULL;
    ALTER TABLE interface ADD COLUMN uuid UUID DEFAULT uuid_generate_v4() NOT NULL;

COMMIT;
