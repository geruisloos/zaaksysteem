BEGIN;

    CREATE INDEX transaction_error_count_idx ON transaction ( error_count );

COMMIT;
