BEGIN;
ALTER TABLE bag_verblijfsobject_gebruiksdoel ALTER COLUMN begindatum DROP NOT NULL;
ALTER TABLE bag_verblijfsobject_pand ALTER COLUMN begindatum DROP NOT NULL;
COMMIT;
