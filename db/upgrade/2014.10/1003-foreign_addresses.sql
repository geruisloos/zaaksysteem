BEGIN;

-- personen

ALTER TABLE natuurlijk_persoon DROP COLUMN adres_buitenland1;
ALTER TABLE natuurlijk_persoon DROP COLUMN adres_buitenland2;
ALTER TABLE natuurlijk_persoon DROP COLUMN adres_buitenland3;
ALTER TABLE gm_natuurlijk_persoon DROP COLUMN adres_buitenland1;
ALTER TABLE gm_natuurlijk_persoon DROP COLUMN adres_buitenland2;
ALTER TABLE gm_natuurlijk_persoon DROP COLUMN adres_buitenland3;

ALTER TABLE adres ADD COLUMN adres_buitenland1 TEXT;
ALTER TABLE adres ADD COLUMN adres_buitenland2 TEXT;
ALTER TABLE adres ADD COLUMN adres_buitenland3 TEXT;
ALTER TABLE adres ADD COLUMN landcode INTEGER default 6030;

ALTER TABLE gm_adres ADD COLUMN adres_buitenland1 TEXT;
ALTER TABLE gm_adres ADD COLUMN adres_buitenland2 TEXT;
ALTER TABLE gm_adres ADD COLUMN adres_buitenland3 TEXT;
ALTER TABLE gm_adres ADD COLUMN landcode INTEGER default 6030;

-- Bedrijven

ALTER TABLE bedrijf ADD COLUMN vestiging_adres_buitenland1 TEXT;
ALTER TABLE bedrijf ADD COLUMN vestiging_adres_buitenland2 TEXT;
ALTER TABLE bedrijf ADD COLUMN vestiging_adres_buitenland3 TEXT;
ALTER TABLE bedrijf ADD COLUMN vestiging_landcode INTEGER default 6030;

ALTER TABLE bedrijf ADD COLUMN correspondentie_adres_buitenland1 TEXT;
ALTER TABLE bedrijf ADD COLUMN correspondentie_adres_buitenland2 TEXT;
ALTER TABLE bedrijf ADD COLUMN correspondentie_adres_buitenland3 TEXT;
ALTER TABLE bedrijf ADD COLUMN correspondentie_landcode INTEGER default 6030;


ALTER TABLE gm_bedrijf ADD COLUMN vestiging_adres_buitenland1 TEXT;
ALTER TABLE gm_bedrijf ADD COLUMN vestiging_adres_buitenland2 TEXT;
ALTER TABLE gm_bedrijf ADD COLUMN vestiging_adres_buitenland3 TEXT;
ALTER TABLE gm_bedrijf ADD COLUMN vestiging_landcode INTEGER default 6030;

ALTER TABLE gm_bedrijf ADD COLUMN correspondentie_adres_buitenland1 TEXT;
ALTER TABLE gm_bedrijf ADD COLUMN correspondentie_adres_buitenland2 TEXT;
ALTER TABLE gm_bedrijf ADD COLUMN correspondentie_adres_buitenland3 TEXT;
ALTER TABLE gm_bedrijf ADD COLUMN correspondentie_landcode INTEGER default 6030;

COMMIT;