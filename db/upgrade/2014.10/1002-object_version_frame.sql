BEGIN;

DROP TABLE IF EXISTS object_mutation;

CREATE TABLE object_mutation (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    object_uuid UUID UNIQUE REFERENCES object_data(uuid),
    object_type TEXT DEFAULT 'object',
    lock_object_uuid UUID REFERENCES object_data(uuid),
    type TEXT NOT NULL CHECK ( type in ('create', 'update', 'delete', 'relate') ),
    values TEXT NOT NULL DEFAULT '{}',
    date_created timestamp without time zone DEFAULT NOW(),
    subject_id INTEGER NOT NULL REFERENCES subject(id)
);

COMMIT;
