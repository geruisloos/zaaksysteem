BEGIN;

ALTER TABLE bag_woonplaats DROP CONSTRAINT pk_woonplaats;

ALTER TABLE bag_woonplaats ALTER COLUMN officieel DROP NOT NULL;
ALTER TABLE bag_woonplaats ALTER COLUMN correctie DROP NOT NULL;

ALTER TABLE bag_woonplaats ADD COLUMN id SERIAL PRIMARY KEY;

ALTER TABLE bag_openbareruimte DROP CONSTRAINT pk_openbareruimte;

ALTER TABLE bag_openbareruimte ALTER COLUMN officieel DROP NOT NULL;
ALTER TABLE bag_openbareruimte ALTER COLUMN correctie DROP NOT NULL;

ALTER TABLE bag_openbareruimte ADD COLUMN id SERIAL PRIMARY KEY;


ALTER TABLE bag_nummeraanduiding DROP CONSTRAINT pk_nummeraanduiding;

ALTER TABLE bag_nummeraanduiding ALTER COLUMN officieel DROP NOT NULL;
ALTER TABLE bag_nummeraanduiding ALTER COLUMN correctie DROP NOT NULL;

ALTER TABLE bag_nummeraanduiding ADD COLUMN id SERIAL PRIMARY KEY;

ALTER TABLE bag_verblijfsobject DROP CONSTRAINT pk_verblijfsobject;


ALTER TABLE bag_verblijfsobject ALTER COLUMN officieel DROP NOT NULL;
ALTER TABLE bag_verblijfsobject ALTER COLUMN correctie DROP NOT NULL;
ALTER TABLE bag_verblijfsobject ALTER COLUMN oppervlakte DROP NOT NULL;

ALTER TABLE bag_verblijfsobject ADD COLUMN id SERIAL PRIMARY KEY;


ALTER TABLE bag_pand DROP CONSTRAINT pk_pand;

ALTER TABLE bag_pand ALTER COLUMN officieel DROP NOT NULL;
ALTER TABLE bag_pand ALTER COLUMN bouwjaar DROP NOT NULL;
ALTER TABLE bag_pand ALTER COLUMN correctie DROP NOT NULL;

ALTER TABLE bag_pand ADD COLUMN id SERIAL PRIMARY KEY;


ALTER TABLE bag_verblijfsobject_gebruiksdoel DROP CONSTRAINT pk_verblijfsobject_gebrdoel;


ALTER TABLE bag_verblijfsobject_gebruiksdoel ALTER COLUMN correctie DROP NOT NULL;

ALTER TABLE bag_verblijfsobject_gebruiksdoel ADD COLUMN id SERIAL PRIMARY KEY;

ALTER TABLE bag_verblijfsobject_pand DROP CONSTRAINT pk_verblijfsobject_pand;

ALTER TABLE bag_verblijfsobject_pand ALTER COLUMN correctie DROP NOT NULL;

ALTER TABLE bag_verblijfsobject_pand ADD COLUMN id SERIAL PRIMARY KEY;

ALTER TABLE bag_ligplaats DROP CONSTRAINT pk_ligplaats;


ALTER TABLE bag_ligplaats ALTER COLUMN officieel DROP NOT NULL;
ALTER TABLE bag_ligplaats ALTER COLUMN correctie DROP NOT NULL;

ALTER TABLE bag_ligplaats ADD COLUMN id SERIAL PRIMARY KEY;


ALTER TABLE bag_standplaats DROP CONSTRAINT pk_standplaats;

ALTER TABLE bag_standplaats ALTER COLUMN officieel DROP NOT NULL;
ALTER TABLE bag_standplaats ALTER COLUMN correctie DROP NOT NULL;

ALTER TABLE bag_standplaats ADD COLUMN id SERIAL PRIMARY KEY;

COMMIT;